idem_test_network_pools_is_present:
  vcf.network_pools.present:
  - id_: string
  - networks:
    - free_ips:
      - value
      gateway: string
      id_: string
      ip_pools:
      - end: string
        start: string
      mask: string
      mtu: int
      subnet: string
      type_: string
      used_ips:
      - value
      vlan_id: int

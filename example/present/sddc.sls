idem_test_sddc_is_present:
  vcf.sddc.present:
  - ceip_enabled: bool
  - cluster_spec:
      cluster_evc_mode: string
      cluster_image_enabled: bool
      cluster_name: string
      host_failures_to_tolerate: int
      personality_name: string
      resource_pool_specs:
      - cpu_limit: int
        cpu_reservation_expandable: bool
        cpu_reservation_mhz: int
        cpu_reservation_percentage: int
        cpu_shares_level: string
        cpu_shares_value: int
        memory_limit: int
        memory_reservation_expandable: bool
        memory_reservation_mb: int
        memory_reservation_percentage: int
        memory_shares_level: string
        memory_shares_value: int
        name: string
        type_: string
      vm_folders: Dict
  - dns_spec:
      domain: string
      nameserver: string
      secondary_nameserver: string
      subdomain: string
  - dv_switch_version: string
  - dvs_specs:
    - dvs_name: string
      is_used_by_nsxt: bool
      mtu: int
      networks:
      - value
      nioc_specs:
      - traffic_type: string
        value: string
      nsxt_switch_config:
        host_switch_operational_mode: string
        transport_zones:
        - name: string
          transport_type: string
      vmnics:
      - value
      vmnics_to_uplinks:
      - nsx_uplink_name: string
        vds_uplink_name: string
  - esx_license: string
  - excluded_components:
    - value
  - fips_enabled: bool
  - host_specs:
    - association: string
      credentials:
        password: string
        username: string
      hostname: string
      ip_address_private:
        cidr: string
        gateway: string
        ip_address: string
        subnet: string
      ssh_thumbprint: string
      ssl_thumbprint: string
      v_switch: string
      vswitch: string
  - management_pool_name: string
  - network_specs:
    - active_uplinks:
      - value
      exclude_ip_address_ranges:
      - value
      exclude_ipaddresses:
      - value
      gateway: string
      include_ip_address:
      - value
      include_ip_address_ranges:
      - end_ip_address: string
        start_ip_address: string
      mtu: string
      network_type: string
      port_group_key: string
      standby_uplinks:
      - value
      subnet: string
      subnet_mask: string
      teaming_policy: string
      vlan_id: string
  - nsxt_spec:
      ip_address_pool_spec:
        description: string
        ignore_unavailable_nsxt_cluster: bool
        name: string
        subnets:
        - cidr: string
          gateway: string
          ip_address_pool_ranges:
          - end: string
            start: string
      nsxt_admin_password: string
      nsxt_audit_password: string
      nsxt_license: string
      nsxt_manager_size: string
      nsxt_managers:
      - hostname: string
        ip: string
      over_lay_transport_zone:
        network_name: string
        zone_name: string
      root_nsxt_manager_password: string
      transport_vlan_id: int
      vip: string
      vip_fqdn: string
  - ntp_servers:
    - value
  - proxy_spec:
      host: string
      port: int
  - psc_specs:
    - admin_user_sso_password: string
      psc_sso_spec:
        sso_domain: string
  - sddc_id: string
  - sddc_manager_spec:
      hostname: string
      ip_address: string
      local_user_password: string
      root_user_credentials:
        password: string
        username: string
      second_user_credentials:
        password: string
        username: string
  - security_spec:
      esxi_certs_mode: string
      root_ca_certs:
      - alias: string
        cert_chain:
        - value
  - skip_esx_thumbprint_validation: bool
  - skip_gateway_ping_validation: bool
  - task_name: string
  - vcenter_spec:
      license_file: string
      root_vcenter_password: string
      ssh_thumbprint: string
      ssl_thumbprint: string
      storage_size: string
      vcenter_hostname: string
      vcenter_ip: string
      vm_size: string
  - vsan_spec:
      datastore_name: string
      esa_config:
        enabled: bool
      hcl_file: string
      license_file: string
      vsan_dedup: bool
  - vx_manager_spec:
      default_admin_user_credentials:
        password: string
        username: string
      default_root_user_credentials:
        password: string
        username: string
      ssh_thumbprint: string
      ssl_thumbprint: string
      vx_manager_host_name: string

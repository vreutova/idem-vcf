idem_test_license_keys_is_present:
  vcf.license_keys.present:
  - description: string
  - id_: string
  - is_unlimited: bool
  - key: string
  - license_key_usage:
      license_unit: string
      remaining: int
      total: int
      used: int
  - license_key_validity:
      expiry_date: string
      license_key_status: string
  - product_type: string
  - product_version: string

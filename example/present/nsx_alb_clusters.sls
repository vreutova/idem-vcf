idem_test_nsx_alb_clusters_is_present:
  vcf.nsx_alb_clusters.present:
  - domain_id: string
  - cluster_fqdn: string
  - cluster_ip_address: string
  - form_factor: string
  - admin_password: string
  - nodes:
    - ip_address: string
  - alb_bundle_id: string

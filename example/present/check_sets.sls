idem_test_check_sets_is_present:
  vcf.check_sets.present:
  - query_id: string
  - resources:
    - check_sets:
      - check_set_id: string
      domain:
        domain_id: string
        domain_name: string
        domain_type: string
      resource_id: string
      resource_name: string
      resource_type: string

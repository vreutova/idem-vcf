idem_test_manifests_is_present:
  vcf.manifests.present:
  - async_patches: Dict
  - creation_time: string
  - published_date: string
  - recalled_bundles:
    - recalled_bundle_ids:
      - value
      replacement_bundle_ids:
      - value
      replacement_bundle_status: string
  - releases:
    - bom:
      - additional_metadata: string
        name: string
        public_name: string
        release_url: string
        version: string
      description: string
      eol: string
      is_applicable: bool
      min_compatible_vcf_version: string
      not_applicable_reason: string
      patch_bundles:
      - bundle_elements:
        - value
        bundle_id: string
        bundle_type: string
        cumulative_from_vcf_version: string
      product: string
      release_date: string
      sku:
      - bom:
        - additional_metadata: string
          name: string
          public_name: string
          release_url: string
          version: string
        description: string
        name: string
        sku_specific_patch_bundles:
        - bundle_type: string
          bundle_version: string
          version: string
      updates:
      - base_product_version: string
        description: string
        id_: string
        product_name: string
        release_date: string
        release_update_url:
          authority: string
          content: Dict
          default_port: int
          deserialized_fields: '{}'
          file: string
          host: string
          path: string
          port: int
          protocol: string
          query: string
          ref: string
          serialized_hash_code: int
          user_info: string
      version: string
  - sequence_number: int
  - version: int
  - vvs_mappings: Dict

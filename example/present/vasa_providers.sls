idem_test_vasa_providers_is_present:
  vcf.vasa_providers.present:
  - id_: string
  - storage_containers:
    - cluster_id: string
      id_: string
      name: string
      protocol_type: string
  - url: string
  - users:
    - id_: string
      password: string
      username: string

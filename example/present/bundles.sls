idem_test_bundles_is_present:
  vcf.bundles.present:
  - bundle_file_path: string
  - compatibility_sets_file_path: string
  - manifest_file_path: string
  - partner_extension_spec:
      partner_bundle_metadata_file_path: string
      partner_bundle_version: string
  - signature_file_path: string

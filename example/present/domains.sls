idem_test_domains_is_present:
  vcf.domains.present:
  - compute_spec:
      cluster_specs:
      - advanced_options:
          evc_mode: string
          high_availability:
            enabled: bool
        cluster_image_id: string
        datastore_spec:
          nfs_datastore_specs:
          - datastore_name: string
            nas_volume:
              path: string
              read_only: bool
              server_name:
              - value
              user_tag: string
          vmfs_datastore_spec:
            fc_spec:
            - datastore_name: string
          vsan_datastore_spec:
            datastore_name: string
            dedup_and_compression_enabled: bool
            esa_config:
              enabled: bool
            failures_to_tolerate: int
            license_key: string
          vsan_remote_datastore_cluster_spec:
            vsan_remote_datastore_spec:
            - datastore_uuid: string
          vvol_datastore_specs:
          - name: string
            vasa_provider_spec:
              storage_container_id: string
              storage_protocol_type: string
              user_id: string
              vasa_provider_id: string
        host_specs:
        - az_name: string
          host_name: string
          host_network_spec:
            network_profile_name: string
            vm_nics:
            - id_: string
              move_to_nvds: bool
              uplink: string
              vds_name: string
          id_: string
          ip_address: string
          license_key: string
          password: string
          serial_number: string
          ssh_thumbprint: string
          username: string
        name: string
        network_spec:
          network_profiles:
          - description: string
            is_default: bool
            name: string
            nsxt_host_switch_configs:
            - ip_address_pool_name: string
              uplink_profile_name: string
              vds_name: string
              vds_uplink_to_nsx_uplink:
              - nsx_uplink_name: string
                vds_uplink_name: string
          nsx_cluster_spec:
            nsx_t_cluster_spec:
              geneve_vlan_id: int
              ip_address_pool_spec:
                description: string
                ignore_unavailable_nsxt_cluster: bool
                name: string
                subnets:
                - cidr: string
                  gateway: string
                  ip_address_pool_ranges:
                  - end: string
                    start: string
              ip_address_pools_spec:
              - description: string
                ignore_unavailable_nsxt_cluster: bool
                name: string
                subnets:
                - cidr: string
                  gateway: string
                  ip_address_pool_ranges:
                  - end: string
                    start: string
              uplink_profiles:
              - name: string
                supported_teaming_policies: Dict
                teamings:
                - active_uplinks:
                  - value
                  policy: string
                  stand_by_uplinks:
                  - value
                transport_vlan: int
          vds_specs:
          - is_used_by_nsxt: bool
            mtu: int
            name: string
            nioc_bandwidth_allocation_specs:
            - nioc_traffic_resource_allocation:
                limit: int
                reservation: int
                shares_info:
                  level: string
                  shares: int
              type_: string
            nsxt_switch_config:
              host_switch_operational_mode: string
              transport_zones:
              - name: string
                transport_type: string
            port_group_specs:
            - active_uplinks:
              - value
              name: string
              stand_by_uplinks:
              - value
              teaming_policy: string
              transport_type: string
        skip_thumbprint_validation: bool
        vx_rail_details:
          admin_credentials:
            credential_type: string
            password: string
            username: string
          array_context_with_key_value_pair: Dict
          context_with_key_value_pair: Dict
          dns_name: string
          ip_address: string
          networks:
          - free_ips:
            - value
            gateway: string
            id_: string
            ip_pools:
            - end: string
              start: string
            mask: string
            mtu: int
            subnet: string
            type_: string
            used_ips:
            - value
            vlan_id: int
          nic_profile: string
          root_credentials:
            credential_type: string
            password: string
            username: string
          ssh_thumbprint: string
          ssl_thumbprint: string
      skip_failed_hosts: bool
  - domain_name: string
  - network_separation_spec:
      enable_security: bool
      segment_spec:
        name: string
        vlan_id: string
  - nsx_t_spec:
      form_factor: string
      ip_address_pool_spec:
        description: string
        ignore_unavailable_nsxt_cluster: bool
        name: string
        subnets:
        - cidr: string
          gateway: string
          ip_address_pool_ranges:
          - end: string
            start: string
      license_key: string
      nsx_manager_admin_password: string
      nsx_manager_audit_password: string
      nsx_manager_specs:
      - name: string
        network_details_spec:
          dns_name: string
          gateway: string
          ip_address: string
          subnet_mask: string
      vip: string
      vip_fqdn: string
  - org_name: string
  - sso_domain_spec:
      sso_domain_name: string
      sso_domain_password: string
  - vcenter_spec:
      datacenter_name: string
      name: string
      network_details_spec:
        dns_name: string
        gateway: string
        ip_address: string
        subnet_mask: string
      root_password: string
      storage_size: string
      vm_size: string

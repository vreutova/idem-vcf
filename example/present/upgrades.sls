idem_test_upgrades_is_present:
  vcf.upgrades.present:
  - bundle_id: string
  - draft_mode: bool
  - nsxt_upgrade_user_input_specs:
    - nsxt_edge_cluster_upgrade_specs:
      - edge_cluster_id: string
        edge_parallel_upgrade: bool
      nsxt_host_cluster_upgrade_specs:
      - host_cluster_id: string
        host_parallel_upgrade: bool
        live_upgrade: bool
      nsxt_id: string
      nsxt_upgrade_options:
        is_edge_clusters_upgrade_parallel: bool
        is_edge_only_upgrade: bool
        is_host_clusters_upgrade_parallel: bool
  - parallel_upgrade: bool
  - resource_type: string
  - resource_upgrade_specs:
    - custom_iso_spec:
        host_ids:
        - value
        id_: string
      enable_quickboot: bool
      esx_upgrade_options_spec:
        disable_dpm: bool
        disable_hac: bool
        enable_quickboot: bool
        enforce_hcl_validation: bool
        esx_upgrade_failure_action:
          action: string
          retry_count: int
          retry_delay: int
        evacuate_offline_vms: bool
        pre_remediation_power_action: string
      evacuate_offline_vms: bool
      hosts_to_upgrade:
      - value
      personality_spec:
        hardware_support_specs:
        - name: string
          package_spec:
            name: string
            version: string
        personality_id: string
      resource_id: string
      scheduled_timestamp: string
      shutdown_vms: bool
      to_version: string
      upgrade_now: bool
  - vcenter_upgrade_user_input_specs:
    - temporary_network:
        gateway: string
        ip_address: string
        subnet_mask: string

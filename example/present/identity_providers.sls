idem_test_identity_providers_is_present:
  vcf.identity_providers.present:
  - cert_chain:
    - value
  - fed_idp_spec:
      directory:
        default_domain: string
        directory_id: string
        domains:
        - value
        name: string
      name: string
      oidc_spec:
        client_id: string
        client_secret: string
        discovery_endpoint: string
      sync_client_token_ttl: int
  - ldap:
      domain_alias: string
      domain_name: string
      password: string
      source_details:
        cert_chain:
        - value
        groups_base_dn: string
        server_endpoints:
        - value
        users_base_dn: string
      type_: string
      username: string
  - oidc:
      client_id: string
      client_secret: string
      discovery_endpoint: string
  - type_: string

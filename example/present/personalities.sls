idem_test_personalities_is_present:
  vcf.personalities.present:
  - upload_mode: string
  - upload_spec_raw_mode:
      personality_info_json_file_path: string
      personality_iso_file_path: string
      personality_json_file_path: string
      personality_zip_file_path: string
  - upload_spec_raw_with_file_upload_id_mode:
      file_upload_id: string
  - upload_spec_referred_mode:
      cluster_id: string
      v_center_id: string
      vcenter_id: string

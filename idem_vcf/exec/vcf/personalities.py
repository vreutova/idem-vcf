"""Exec module for managing Personalitieses. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List

__contracts__ = ["soft_fail"]

__func_alias__ = {"list_": "list"}


async def get(
    hub, ctx, resource_id: str, personality_id: str, name: str = None
) -> Dict[str, Any]:
    """
    Get a Personality by its id
        Get the Personality for id

    Args:
        resource_id(str):
            Personalities unique ID.

        personality_id(str):
            Personality ID.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            unmanaged_resource:
              exec.run:
                - path: vcf.personalities.get
                - kwargs:
                  resource_id: value
                  personality_id: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.personalities.get resource_id=value, personality_id=value
    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change function methods params if needed
    get = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/personalities/{personalityId}".format(
            **{"personalityId": personality_id}
        ),
        query_params={},
        data={},
        headers={},
    )

    if not get["result"]:
        # Send empty result for not found
        if get["status"] == 404:
            result["comment"].append(f"Get '{name}' result is empty")
            return result

        result["comment"].append(get["comment"])
        result["result"] = False
        return result

    # Case: Empty results
    if not get["ret"]:
        result["comment"].append(f"Get '{name}' result is empty")
        return result

    # Convert raw response into present format
    raw_resource = get["ret"]

    # TODO: Make sure resource_id is mapped in get response
    resource_in_present_format = {"name": name, "resource_id": resource_id}
    resource_parameters = OrderedDict(
        {
            "createdBy": "created_by",
            "description": "description",
            "displayName": "display_name",
            "imageChecksum": "image_checksum",
            "imageSize": "image_size",
            "kbArticles": "kb_articles",
            "personalityId": "personality_id",
            "personalityName": "personality_name",
            "releaseDate": "release_date",
            "softwareInfo": "software_info",
            "tags": "tags",
            "version": "version",
            "vsphereExportedIsoPath": "vsphere_exported_iso_path",
            "vsphereExportedJsonPath": "vsphere_exported_json_path",
            "vsphereExportedZipPath": "vsphere_exported_zip_path",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def list_(
    hub,
    ctx,
    name: str = None,
    base_os_version: str = None,
    add_on_name: str = None,
    add_on_vendor_name: str = None,
    component_name: str = None,
    component_vendor_name: str = None,
    personality_name: str = None,
) -> Dict[str, Any]:
    """
    Get the Personalities
        Get the Personalities which are available via depot access.

    Args:
        name(str, Optional):
            Idem name of the resource. Defaults to None.

        base_os_version(str, Optional):
            The base OS version. Defaults to None.

        add_on_name(str, Optional):
            The add on name. Defaults to None.

        add_on_vendor_name(str, Optional):
            The add on vendor name. Defaults to None.

        component_name(str, Optional):
            The component name. Defaults to None.

        component_vendor_name(str, Optional):
            The component vendor name. Defaults to None.

        personality_name(str, Optional):
            personalityName. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:

        Resource State:

        .. code-block:: sls

            unmanaged_resources:
              exec.run:
                - path: vcf.personalities.list
                - kwargs:


        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.personalities.list

        Describe call from the CLI:

        .. code-block:: bash

            $ idem describe vcf.personalities

    """

    result = dict(comment=[], ret=[], result=True)

    # TODO: Change function methods params if needed
    list = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/personalities",
        query_params={
            "baseOSVersion": base_os_version,
            "addOnName": add_on_name,
            "addOnVendorName": add_on_vendor_name,
            "componentName": component_name,
            "componentVendorName": component_vendor_name,
            "personalityName": personality_name,
        },
        data={},
        headers={},
    )

    if not list["result"]:
        result["comment"].append(list["comment"])
        result["result"] = False
        return result

    for resource in list["ret"]:

        # Convert raw response into present format
        resource_in_present_format = {
            # TODO: Make sure name, resource_id is mapped accordingly
            "name": "name",
            "resource_id": "resource_id",
        }
        resource_parameters = OrderedDict(
            {
                "createdBy": "created_by",
                "description": "description",
                "displayName": "display_name",
                "imageChecksum": "image_checksum",
                "imageSize": "image_size",
                "kbArticles": "kb_articles",
                "personalityId": "personality_id",
                "personalityName": "personality_name",
                "releaseDate": "release_date",
                "softwareInfo": "software_info",
                "tags": "tags",
                "version": "version",
                "vsphereExportedIsoPath": "vsphere_exported_iso_path",
                "vsphereExportedJsonPath": "vsphere_exported_json_path",
                "vsphereExportedZipPath": "vsphere_exported_zip_path",
            }
        )

        for parameter_raw, parameter_present in resource_parameters.items():
            if parameter_raw in resource and resource.get(parameter_raw):
                resource_in_present_format[parameter_present] = resource.get(
                    parameter_raw
                )

        result["ret"].append(resource_in_present_format)

    return result


async def create(
    hub,
    ctx,
    upload_mode: str,
    resource_id: str = None,
    name: str = None,
    upload_spec_raw_mode: make_dataclass(
        "upload_spec_raw_mode",
        [
            ("personality_info_json_file_path", str),
            ("personality_json_file_path", str),
            ("personality_zip_file_path", str),
            ("personality_iso_file_path", str, field(default=None)),
        ],
    ) = None,
    upload_spec_raw_with_file_upload_id_mode: make_dataclass(
        "upload_spec_raw_with_file_upload_id_mode",
        [("file_upload_id", str, field(default=None))],
    ) = None,
    upload_spec_referred_mode: make_dataclass(
        "upload_spec_referred_mode",
        [
            ("cluster_id", str),
            ("v_center_id", str),
            ("vcenter_id", str, field(default=None)),
        ],
    ) = None,
) -> Dict[str, Any]:
    """
    Upload a Personality
        Upload Personality to SDDC Manager.

    Args:
        upload_mode(str):
            Personality upload mode.

        resource_id(str, Optional):
            Personalities unique ID. Defaults to None.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

        upload_spec_raw_mode(dict[str, Any], Optional):
            uploadSpecRawMode. Defaults to None.

            * personality_iso_file_path (str, Optional):
                Personality ISO File Path. Defaults to None.

            * personality_info_json_file_path (str):
                Personality Info JSON File Path.

            * personality_json_file_path (str):
                Personality JSON File Path.

            * personality_zip_file_path (str):
                Personality Zip File Path.

        upload_spec_raw_with_file_upload_id_mode(dict[str, Any], Optional):
            uploadSpecRawWithFileUploadIdMode. Defaults to None.

            * file_upload_id (str, Optional):
                Personality file upload id. This id is obtained by calling v1/personalities/files and uploading files into SDDC Manager. Defaults to None.

        upload_spec_referred_mode(dict[str, Any], Optional):
            uploadSpecReferredMode. Defaults to None.

            * cluster_id (str):
                Source cluster UUID from VCF inventory.

            * v_center_id (str):
                vCenterId.

            * vcenter_id (str, Optional):
                vcenterId. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.personalities.present:
                - upload_mode: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.personalities.create upload_mode=value
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {
        "name": "name",
        "upload_mode": "uploadMode",
        "upload_spec_raw_mode": "uploadSpecRawMode",
        "upload_spec_raw_with_file_upload_id_mode": "uploadSpecRawWithFileUploadIdMode",
        "upload_spec_referred_mode": "uploadSpecReferredMode",
    }

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    create = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/personalities",
        query_params={},
        data=payload,
        headers={},
    )

    if not create["result"]:
        result["comment"].append(create["comment"])
        result["result"] = False
        return result

    result["comment"].append(
        f"Created vcf.personalities '{name}'",
    )

    result["ret"] = create["ret"]
    # TODO: add "resource_id" to returned response by mapping to correct resource identifier
    return result


async def update(
    hub,
    ctx,
    resource_id: str,
    personality_id: str,
    description: str,
    display_name: str,
    image_checksum: str,
    image_size: str,
    personality_name: str,
    software_info: make_dataclass(
        "software_info",
        [
            (
                "base_image",
                make_dataclass(
                    "base_image",
                    [
                        ("version", str),
                        (
                            "details",
                            make_dataclass(
                                "details",
                                [
                                    ("display_name", str),
                                    ("display_version", str),
                                    (
                                        "release_date",
                                        make_dataclass(
                                            "release_date",
                                            [
                                                (
                                                    "calendar_type",
                                                    str,
                                                    field(default=None),
                                                ),
                                                (
                                                    "first_day_of_week",
                                                    int,
                                                    field(default=None),
                                                ),
                                                ("lenient", bool, field(default=None)),
                                                (
                                                    "minimal_days_in_first_week",
                                                    int,
                                                    field(default=None),
                                                ),
                                                ("time", str, field(default=None)),
                                                (
                                                    "time_in_millis",
                                                    int,
                                                    field(default=None),
                                                ),
                                                (
                                                    "time_zone",
                                                    make_dataclass(
                                                        "time_zone",
                                                        [
                                                            (
                                                                "display_name",
                                                                str,
                                                                field(default=None),
                                                            ),
                                                            (
                                                                "dstsavings",
                                                                int,
                                                                field(default=None),
                                                            ),
                                                            (
                                                                "id",
                                                                str,
                                                                field(default=None),
                                                            ),
                                                            (
                                                                "raw_offset",
                                                                int,
                                                                field(default=None),
                                                            ),
                                                        ],
                                                    ),
                                                    field(default=None),
                                                ),
                                                (
                                                    "week_date_supported",
                                                    bool,
                                                    field(default=None),
                                                ),
                                                ("week_year", int, field(default=None)),
                                                (
                                                    "weeks_in_week_year",
                                                    int,
                                                    field(default=None),
                                                ),
                                            ],
                                        ),
                                        field(default=None),
                                    ),
                                ],
                            ),
                            field(default=None),
                        ),
                    ],
                ),
            ),
            (
                "add_on",
                make_dataclass(
                    "add_on",
                    [
                        ("name", str),
                        ("vendor", str),
                        ("version", str),
                        ("display_name", str, field(default=None)),
                        ("display_version", str, field(default=None)),
                    ],
                ),
                field(default=None),
            ),
            ("components", Dict, field(default=None)),
            (
                "hardware_support",
                make_dataclass("hardware_support", [("packages", Dict)]),
                field(default=None),
            ),
        ],
    ),
    version: str,
    vsphere_exported_iso_path: str,
    vsphere_exported_json_path: str,
    vsphere_exported_zip_path: str,
    name: str = None,
    created_by: str = None,
    kb_articles: make_dataclass(
        "kb_articles",
        [
            ("authority", str, field(default=None)),
            ("content", Dict, field(default=None)),
            ("default_port", int, field(default=None)),
            ("deserialized_fields", {}, field(default=None)),
            ("file", str, field(default=None)),
            ("host", str, field(default=None)),
            ("path", str, field(default=None)),
            ("port", int, field(default=None)),
            ("protocol", str, field(default=None)),
            ("query", str, field(default=None)),
            ("ref", str, field(default=None)),
            ("serialized_hash_code", int, field(default=None)),
            ("user_info", str, field(default=None)),
        ],
    ) = None,
    release_date: int = None,
    tags: List[str] = None,
) -> Dict[str, Any]:
    """
    Rename personality based on ID
        Rename personality with the ID passed in the URL

    Args:
        resource_id(str):
            Personalities unique ID.

        personality_id(str):
            The personality id.

        description(str):
            Personality description.

        display_name(str):
            Personality displayName.

        image_checksum(str):
            Personality image checksum.

        image_size(str):
            Personality image size.

        personality_id(str):
            Personality id.

        personality_name(str):
            Personality name.

        software_info(dict[str, Any]):
            softwareInfo.

            * add_on (dict[str, Any], Optional):
                addOn. Defaults to None.

                * display_name (str, Optional):
                    Add On display name. Defaults to None.

                * display_version (str, Optional):
                    Add On display version. Defaults to None.

                * name (str):
                    Component name.

                * vendor (str):
                    vendor.

                * version (str):
                    Add on software version.

            * base_image (dict[str, Any]):
                baseImage.

                * details (dict[str, Any], Optional):
                    details. Defaults to None.

                    * display_name (str):
                        BaseImage display name.

                    * display_version (str):
                        BaseImage display version.

                    * release_date (dict[str, Any], Optional):
                        releaseDate. Defaults to None.

                        * calendar_type (str, Optional):
                            calendarType. Defaults to None.

                        * first_day_of_week (int, Optional):
                            firstDayOfWeek. Defaults to None.

                        * lenient (bool, Optional):
                            lenient. Defaults to None.

                        * minimal_days_in_first_week (int, Optional):
                            minimalDaysInFirstWeek. Defaults to None.

                        * time (str, Optional):
                            time. Defaults to None.

                        * time_in_millis (int, Optional):
                            timeInMillis. Defaults to None.

                        * time_zone (dict[str, Any], Optional):
                            timeZone. Defaults to None.

                            * display_name (str, Optional):
                                displayName. Defaults to None.

                            * dstsavings (int, Optional):
                                dstsavings. Defaults to None.

                            * id (str, Optional):
                                id. Defaults to None.

                            * raw_offset (int, Optional):
                                rawOffset. Defaults to None.

                        * week_date_supported (bool, Optional):
                            weekDateSupported. Defaults to None.

                        * week_year (int, Optional):
                            weekYear. Defaults to None.

                        * weeks_in_week_year (int, Optional):
                            weeksInWeekYear. Defaults to None.

                * version (str):
                    BaseImage Version.

            * components (Dict, Optional):
                Personality components. Defaults to None.

            * hardware_support (dict[str, Any], Optional):
                hardwareSupport. Defaults to None.

                * packages (Dict):
                    Hardware support packages.

        version(str):
            Personality Version.

        vsphere_exported_iso_path(str):
            Personality depot path.

        vsphere_exported_json_path(str):
            Personality depot path.

        vsphere_exported_zip_path(str):
            Personality depot path.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

        created_by(str, Optional):
            Personality created by. Defaults to None.

        kb_articles(dict[str, Any], Optional):
            kbArticles. Defaults to None.

            * authority (str, Optional):
                authority. Defaults to None.

            * content (Dict, Optional):
                content. Defaults to None.

            * default_port (int, Optional):
                defaultPort. Defaults to None.

            * deserialized_fields ({}, Optional):
                deserializedFields. Defaults to None.

            * file (str, Optional):
                file. Defaults to None.

            * host (str, Optional):
                host. Defaults to None.

            * path (str, Optional):
                path. Defaults to None.

            * port (int, Optional):
                port. Defaults to None.

            * protocol (str, Optional):
                protocol. Defaults to None.

            * query (str, Optional):
                query. Defaults to None.

            * ref (str, Optional):
                ref. Defaults to None.

            * serialized_hash_code (int, Optional):
                serializedHashCode. Defaults to None.

            * user_info (str, Optional):
                userInfo. Defaults to None.

        release_date(int, Optional):
            Personality Release date. Defaults to None.

        tags(List[str], Optional):
            Personality tags. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.personalities.present:
                - resource_id: value
                - personality_id: value
                - description: value
                - display_name: value
                - image_checksum: value
                - image_size: value
                - personality_id: value
                - personality_name: value
                - software_info: value
                - version: value
                - vsphere_exported_iso_path: value
                - vsphere_exported_json_path: value
                - vsphere_exported_zip_path: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.personalities.update resource_id=value, personality_id=value, description=value, display_name=value, image_checksum=value, image_size=value, personality_id=value, personality_name=value, software_info=value, version=value, vsphere_exported_iso_path=value, vsphere_exported_json_path=value, vsphere_exported_zip_path=value
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {
        "created_by": "createdBy",
        "description": "description",
        "display_name": "displayName",
        "image_checksum": "imageChecksum",
        "image_size": "imageSize",
        "kb_articles": "kbArticles",
        "personality_id": "personalityId",
        "personality_name": "personalityName",
        "release_date": "releaseDate",
        "software_info": "softwareInfo",
        "tags": "tags",
        "version": "version",
        "vsphere_exported_iso_path": "vsphereExportedIsoPath",
        "vsphere_exported_json_path": "vsphereExportedJsonPath",
        "vsphere_exported_zip_path": "vsphereExportedZipPath",
    }

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    if payload:
        update = await hub.tool.vcf.session.request(
            ctx,
            method="patch",
            path="/v1/personalities/{personalityId}".format(
                **{"personalityId": personality_id}
            ),
            query_params={},
            data=payload,
            headers={},
        )

        if not update["result"]:
            result["comment"].append(update["comment"])
            result["result"] = False
            return result

        result["ret"] = update["ret"]
        result["comment"].append(
            f"Updated vcf.personalities '{name}'",
        )

    return result


async def delete(hub, ctx) -> Dict[str, Any]:
    """

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            resource_is_absent:
              vcf.personalities.absent:
                -

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.personalities.delete
    """

    result = dict(comment=[], ret=[], result=True)

    delete = await hub.tool.vcf.session.request(
        ctx,
        method="TODO",
        path="TODO".format(**{}),
        query_params={},
        data={},
        headers={},
    )

    if not delete["result"]:
        result["comment"].append(delete["comment"])
        result["result"] = False
        return result

    result["comment"].append(f"Deleted '{name}'")
    return result

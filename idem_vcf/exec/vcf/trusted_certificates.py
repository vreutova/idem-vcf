"""Exec module for managing Trusted Certificateses. """
from collections import OrderedDict
from typing import Any
from typing import Dict

__contracts__ = ["soft_fail"]

__func_alias__ = {"list_": "list"}


async def get(hub, ctx) -> Dict[str, Any]:
    """

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            unmanaged_resource:
              exec.run:
                - path: vcf.trusted_certificates.get
                - kwargs:


        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.trusted_certificates.get
    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change function methods params if needed
    get = await hub.tool.vcf.session.request(
        ctx,
        method="TODO",
        path="TODO".format(**{}),
        query_params={},
        data={},
        headers={},
    )

    if not get["result"]:
        # Send empty result for not found
        if get["status"] == 404:
            result["comment"].append(f"Get '{name}' result is empty")
            return result

        result["comment"].append(get["comment"])
        result["result"] = False
        return result

    # Case: Empty results
    if not get["ret"]:
        result["comment"].append(f"Get '{name}' result is empty")
        return result

    # TODO: Make sure resource_id is mapped in get response
    get["ret"]["resource_id"] = resource_id
    result["ret"] = get["ret"]

    return result


async def list_(hub, ctx) -> Dict[str, Any]:
    """
    Retrieve all trusted certificates from SDDC Manager
        Retrieve all trusted certificates from SDDC Manager


    Returns:
        Dict[str, Any]

    Examples:

        Resource State:

        .. code-block:: sls

            unmanaged_resources:
              exec.run:
                - path: vcf.trusted_certificates.list
                - kwargs:


        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.trusted_certificates.list

        Describe call from the CLI:

        .. code-block:: bash

            $ idem describe vcf.trusted_certificates

    """

    result = dict(comment=[], ret=[], result=True)

    # TODO: Change function methods params if needed
    list = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/sddc-manager/trusted-certificates",
        query_params={},
        data={},
        headers={},
    )

    if not list["result"]:
        result["comment"].append(list["comment"])
        result["result"] = False
        return result

    for resource in list["ret"]:

        # Convert raw response into present format
        resource_in_present_format = {
            # TODO: Make sure name, resource_id is mapped accordingly
            "name": "name",
            "resource_id": "resource_id",
        }
        resource_parameters = OrderedDict(
            {"elements": "elements", "pageMetadata": "page_metadata"}
        )

        for parameter_raw, parameter_present in resource_parameters.items():
            if parameter_raw in resource and resource.get(parameter_raw):
                resource_in_present_format[parameter_present] = resource.get(
                    parameter_raw
                )

        result["ret"].append(resource_in_present_format)

    return result


async def create(
    hub,
    ctx,
    certificate: str,
    certificate_usage_type: str,
    resource_id: str = None,
    name: str = None,
) -> Dict[str, Any]:
    """
    Add a trusted certificate to the SDDC Manager
        Add a trusted certificate to the SDDC Manager

    Args:
        certificate(str):
            Certificate in PEM format.

        certificate_usage_type(str):
            Certificate usage.

        resource_id(str, Optional):
            Trusted_certificates unique ID. Defaults to None.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.trusted_certificates.present:
                - certificate: value
                - certificate_usage_type: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.trusted_certificates.create certificate=value, certificate_usage_type=value
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {
        "certificate": "certificate",
        "certificate_usage_type": "certificateUsageType",
    }

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    create = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/sddc-manager/trusted-certificates",
        query_params={},
        data=payload,
        headers={},
    )

    if not create["result"]:
        result["comment"].append(create["comment"])
        result["result"] = False
        return result

    result["comment"].append(
        f"Created vcf.trusted_certificates '{name}'",
    )

    result["ret"] = create["ret"]
    # TODO: add "resource_id" to returned response by mapping to correct resource identifier
    return result


async def update(hub, ctx) -> Dict[str, Any]:
    """

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.trusted_certificates.present:
                -

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.trusted_certificates.update
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {}

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    if payload:
        update = await hub.tool.vcf.session.request(
            ctx,
            method="TODO",
            path="TODO".format(**{}),
            query_params={},
            data=payload,
            headers={},
        )

        if not update["result"]:
            result["comment"].append(update["comment"])
            result["result"] = False
            return result

        result["ret"] = update["ret"]
        result["comment"].append(
            f"Updated vcf.trusted_certificates '{name}'",
        )

    return result


async def delete(
    hub, ctx, resource_id: str, alias: str, name: str = None
) -> Dict[str, Any]:
    """
    Delete a trusted certificate from the SDDC Manager
        Delete a trusted certificate from the SDDC Manager. Restart the services to reflect the changes.

    Args:
        resource_id(str):
            Trusted_certificates unique ID.

        alias(str):
            Certificate Alias.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            resource_is_absent:
              vcf.trusted_certificates.absent:
                - resource_id: value
                - alias: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.trusted_certificates.delete resource_id=value, alias=value
    """

    result = dict(comment=[], ret=[], result=True)

    delete = await hub.tool.vcf.session.request(
        ctx,
        method="delete",
        path="/v1/sddc-manager/trusted-certificates/{alias}".format(**{"alias": alias}),
        query_params={},
        data={},
        headers={},
    )

    if not delete["result"]:
        result["comment"].append(delete["comment"])
        result["result"] = False
        return result

    result["comment"].append(f"Deleted '{name}'")
    return result

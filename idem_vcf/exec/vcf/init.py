def __init__(hub):
    hub.exec.vcf.ENDPOINT_URLS = ["https://sfo-vcf01.rainpole.io/"]
    # The default is the first in the list
    hub.exec.vcf.DEFAULT_ENDPOINT_URL = "https://sfo-vcf01.rainpole.io/"

    # This enables acct profiles that begin with "vcf" for vcf modules
    hub.exec.vcf.ACCT = ["vcf"]

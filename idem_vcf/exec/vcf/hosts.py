"""Exec module for managing Hostss. """
from collections import OrderedDict
from typing import Any
from typing import Dict

__contracts__ = ["soft_fail"]

__func_alias__ = {"list_": "list"}


async def get(hub, ctx, resource_id: str, id_: str, name: str = None) -> Dict[str, Any]:
    """
    Get a host by its ID
        None

    Args:
        resource_id(str):
            Hosts unique ID.

        id_(str):
            id.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            unmanaged_resource:
              exec.run:
                - path: vcf.hosts.get
                - kwargs:
                  resource_id: value
                  id_: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.hosts.get resource_id=value, id_=value
    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change function methods params if needed
    get = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/hosts/{id}".format(**{"id": id_}),
        query_params={},
        data={},
        headers={},
    )

    if not get["result"]:
        # Send empty result for not found
        if get["status"] == 404:
            result["comment"].append(f"Get '{name}' result is empty")
            return result

        result["comment"].append(get["comment"])
        result["result"] = False
        return result

    # Case: Empty results
    if not get["ret"]:
        result["comment"].append(f"Get '{name}' result is empty")
        return result

    # Convert raw response into present format
    raw_resource = get["ret"]

    # TODO: Make sure resource_id is mapped in get response
    resource_in_present_format = {"name": name, "resource_id": resource_id}
    resource_parameters = OrderedDict(
        {
            "bundleRepoDatastore": "bundle_repo_datastore",
            "cluster": "cluster",
            "compatibleStorageType": "compatible_storage_type",
            "cpu": "cpu",
            "domain": "domain",
            "esxiVersion": "esxi_version",
            "fqdn": "fqdn",
            "hardwareModel": "hardware_model",
            "hardwareVendor": "hardware_vendor",
            "hybrid": "hybrid",
            "id": "id",
            "ipAddresses": "ip_addresses",
            "isPrimary": "is_primary",
            "memory": "memory",
            "networkpool": "networkpool",
            "networks": "networks",
            "physicalNics": "physical_nics",
            "serialNumber": "serial_number",
            "softwareInfo": "software_info",
            "sshThumbprint": "ssh_thumbprint",
            "sslThumbprint": "ssl_thumbprint",
            "status": "status",
            "storage": "storage",
            "tags": "tags",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def list_(
    hub,
    ctx,
    name: str = None,
    status: str = None,
    domain_id: str = None,
    cluster_id: str = None,
    networkpool_id: str = None,
    storage_type: str = None,
    datastore_name: str = None,
) -> Dict[str, Any]:
    """
    Get all hosts
        None

    Args:
        name(str, Optional):
            Idem name of the resource. Defaults to None.

        status(str, Optional):
            Status of the Host.One among: ASSIGNED, UNASSIGNED_USEABLE, UNASSIGNED_UNUSEABLE. Defaults to None.

        domain_id(str, Optional):
            ID of the Domain. Defaults to None.

        cluster_id(str, Optional):
            ID of the Cluster. Defaults to None.

        networkpool_id(str, Optional):
            ID of the Network pool. Defaults to None.

        storage_type(str, Optional):
            Type of the Storage.VMFS_FC. Defaults to None.

        datastore_name(str, Optional):
            Name of the datastore. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:

        Resource State:

        .. code-block:: sls

            unmanaged_resources:
              exec.run:
                - path: vcf.hosts.list
                - kwargs:


        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.hosts.list

        Describe call from the CLI:

        .. code-block:: bash

            $ idem describe vcf.hosts

    """

    result = dict(comment=[], ret=[], result=True)

    # TODO: Change function methods params if needed
    list = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/hosts",
        query_params={
            "status": status,
            "domainId": domain_id,
            "clusterId": cluster_id,
            "networkpoolId": networkpool_id,
            "storageType": storage_type,
            "datastoreName": datastore_name,
        },
        data={},
        headers={},
    )

    if not list["result"]:
        result["comment"].append(list["comment"])
        result["result"] = False
        return result

    for resource in list["ret"]:

        # Convert raw response into present format
        resource_in_present_format = {
            # TODO: Make sure name, resource_id is mapped accordingly
            "name": "name",
            "resource_id": "resource_id",
        }
        resource_parameters = OrderedDict(
            {
                "bundleRepoDatastore": "bundle_repo_datastore",
                "cluster": "cluster",
                "compatibleStorageType": "compatible_storage_type",
                "cpu": "cpu",
                "domain": "domain",
                "esxiVersion": "esxi_version",
                "fqdn": "fqdn",
                "hardwareModel": "hardware_model",
                "hardwareVendor": "hardware_vendor",
                "hybrid": "hybrid",
                "id": "id",
                "ipAddresses": "ip_addresses",
                "isPrimary": "is_primary",
                "memory": "memory",
                "networkpool": "networkpool",
                "networks": "networks",
                "physicalNics": "physical_nics",
                "serialNumber": "serial_number",
                "softwareInfo": "software_info",
                "sshThumbprint": "ssh_thumbprint",
                "sslThumbprint": "ssl_thumbprint",
                "status": "status",
                "storage": "storage",
                "tags": "tags",
            }
        )

        for parameter_raw, parameter_present in resource_parameters.items():
            if parameter_raw in resource and resource.get(parameter_raw):
                resource_in_present_format[parameter_present] = resource.get(
                    parameter_raw
                )

        result["ret"].append(resource_in_present_format)

    return result


async def create(hub, ctx) -> Dict[str, Any]:
    """
    Commission the Hosts
        None


    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.hosts.present:
                -

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.hosts.create
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {}

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    create = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/hosts",
        query_params={},
        data=payload,
        headers={},
    )

    if not create["result"]:
        result["comment"].append(create["comment"])
        result["result"] = False
        return result

    result["comment"].append(
        f"Created vcf.hosts '{name}'",
    )

    result["ret"] = create["ret"]
    # TODO: add "resource_id" to returned response by mapping to correct resource identifier
    return result


async def update(hub, ctx) -> Dict[str, Any]:
    """

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.hosts.present:
                -

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.hosts.update
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {}

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    if payload:
        update = await hub.tool.vcf.session.request(
            ctx,
            method="TODO",
            path="TODO".format(**{}),
            query_params={},
            data=payload,
            headers={},
        )

        if not update["result"]:
            result["comment"].append(update["comment"])
            result["result"] = False
            return result

        result["ret"] = update["ret"]
        result["comment"].append(
            f"Updated vcf.hosts '{name}'",
        )

    return result


async def delete(hub, ctx) -> Dict[str, Any]:
    """

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            resource_is_absent:
              vcf.hosts.absent:
                -

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.hosts.delete
    """

    result = dict(comment=[], ret=[], result=True)

    delete = await hub.tool.vcf.session.request(
        ctx,
        method="TODO",
        path="TODO".format(**{}),
        query_params={},
        data={},
        headers={},
    )

    if not delete["result"]:
        result["comment"].append(delete["comment"])
        result["result"] = False
        return result

    result["comment"].append(f"Deleted '{name}'")
    return result

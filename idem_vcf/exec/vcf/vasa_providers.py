"""Exec module for managing Vasa Providerss. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List

__contracts__ = ["soft_fail"]

__func_alias__ = {"list_": "list"}


async def get(hub, ctx, resource_id: str, id_: str, name: str = None) -> Dict[str, Any]:
    """
    Get a VASA Provider
        None

    Args:
        resource_id(str):
            Vasa_providers unique ID.

        id_(str):
            VASA Provider ID.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            unmanaged_resource:
              exec.run:
                - path: vcf.vasa_providers.get
                - kwargs:
                  resource_id: value
                  id_: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.vasa_providers.get resource_id=value, id_=value
    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change function methods params if needed
    get = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/vasa-providers/{id}".format(**{"id": id_}),
        query_params={},
        data={},
        headers={},
    )

    if not get["result"]:
        # Send empty result for not found
        if get["status"] == 404:
            result["comment"].append(f"Get '{name}' result is empty")
            return result

        result["comment"].append(get["comment"])
        result["result"] = False
        return result

    # Case: Empty results
    if not get["ret"]:
        result["comment"].append(f"Get '{name}' result is empty")
        return result

    # Convert raw response into present format
    raw_resource = get["ret"]

    # TODO: Make sure resource_id is mapped in get response
    resource_in_present_format = {"name": name, "resource_id": resource_id}
    resource_parameters = OrderedDict(
        {
            "id": "id",
            "name": "name",
            "storageContainers": "storage_containers",
            "url": "url",
            "users": "users",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def list_(hub, ctx) -> Dict[str, Any]:
    """
    Get the VASA Providers
        None


    Returns:
        Dict[str, Any]

    Examples:

        Resource State:

        .. code-block:: sls

            unmanaged_resources:
              exec.run:
                - path: vcf.vasa_providers.list
                - kwargs:


        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.vasa_providers.list

        Describe call from the CLI:

        .. code-block:: bash

            $ idem describe vcf.vasa_providers

    """

    result = dict(comment=[], ret=[], result=True)

    # TODO: Change function methods params if needed
    list = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/vasa-providers",
        query_params={},
        data={},
        headers={},
    )

    if not list["result"]:
        result["comment"].append(list["comment"])
        result["result"] = False
        return result

    for resource in list["ret"]:

        # Convert raw response into present format
        resource_in_present_format = {
            # TODO: Make sure name, resource_id is mapped accordingly
            "name": "name",
            "resource_id": "resource_id",
        }
        resource_parameters = OrderedDict(
            {
                "id": "id",
                "name": "name",
                "storageContainers": "storage_containers",
                "url": "url",
                "users": "users",
            }
        )

        for parameter_raw, parameter_present in resource_parameters.items():
            if parameter_raw in resource and resource.get(parameter_raw):
                resource_in_present_format[parameter_present] = resource.get(
                    parameter_raw
                )

        result["ret"].append(resource_in_present_format)

    return result


async def create(
    hub,
    ctx,
    storage_containers: List[
        make_dataclass(
            "storage_containers",
            [
                ("name", str),
                ("protocol_type", str),
                ("cluster_id", str, field(default=None)),
                ("id", str, field(default=None)),
            ],
        )
    ],
    url: str,
    users: List[
        make_dataclass(
            "users",
            [("password", str), ("username", str), ("id", str, field(default=None))],
        )
    ],
    resource_id: str = None,
    name: str = None,
    id_: str = None,
) -> Dict[str, Any]:
    """
    Add a VASA Provider
        None

    Args:
        storage_containers(List[dict[str, Any]]):
            List of storage containers associated with the VASA Provider.

            * cluster_id (str, Optional):
                ID of the cluster which is using the storage container. Defaults to None.

            * id (str, Optional):
                ID of the storage container. Defaults to None.

            * name (str):
                Name of the storage container.

            * protocol_type (str):
                Storage protocol type.

        url(str):
            URL of the VASA Provider.

        users(List[dict[str, Any]]):
            List of users associated with the VASA Provider.

            * id (str, Optional):
                ID of the VASA User. Defaults to None.

            * password (str):
                Password.

            * username (str):
                VASA User name.

        resource_id(str, Optional):
            Vasa_providers unique ID. Defaults to None.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

        id_(str, Optional):
            ID of the VASA Provider. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.vasa_providers.present:
                - storage_containers: value
                - url: value
                - users: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.vasa_providers.create storage_containers=value, url=value, users=value
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {
        "id": "id",
        "name": "name",
        "storage_containers": "storageContainers",
        "url": "url",
        "users": "users",
    }

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    create = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/vasa-providers",
        query_params={},
        data=payload,
        headers={},
    )

    if not create["result"]:
        result["comment"].append(create["comment"])
        result["result"] = False
        return result

    result["comment"].append(
        f"Created vcf.vasa_providers '{name}'",
    )

    result["ret"] = create["ret"]
    # TODO: add "resource_id" to returned response by mapping to correct resource identifier
    return result


async def update(
    hub, ctx, resource_id: str, id_: str, name: str = None, url: str = None
) -> Dict[str, Any]:
    """
    Update a VASA Provider
        None

    Args:
        resource_id(str):
            Vasa_providers unique ID.

        id_(str):
            VASA Provider ID.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

        url(str, Optional):
            URL of the VASA Provider. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.vasa_providers.present:
                - resource_id: value
                - id_: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.vasa_providers.update resource_id=value, id_=value
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {"name": "name", "url": "url"}

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    if payload:
        update = await hub.tool.vcf.session.request(
            ctx,
            method="patch",
            path="/v1/vasa-providers/{id}".format(**{"id": id_}),
            query_params={},
            data=payload,
            headers={},
        )

        if not update["result"]:
            result["comment"].append(update["comment"])
            result["result"] = False
            return result

        result["ret"] = update["ret"]
        result["comment"].append(
            f"Updated vcf.vasa_providers '{name}'",
        )

    return result


async def delete(
    hub, ctx, resource_id: str, id_: str, name: str = None
) -> Dict[str, Any]:
    """
    Delete a VASA Provider
        None

    Args:
        resource_id(str):
            Vasa_providers unique ID.

        id_(str):
            VASA Provider ID.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            resource_is_absent:
              vcf.vasa_providers.absent:
                - resource_id: value
                - id_: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.vasa_providers.delete resource_id=value, id_=value
    """

    result = dict(comment=[], ret=[], result=True)

    delete = await hub.tool.vcf.session.request(
        ctx,
        method="delete",
        path="/v1/vasa-providers/{id}".format(**{"id": id_}),
        query_params={},
        data={},
        headers={},
    )

    if not delete["result"]:
        result["comment"].append(delete["comment"])
        result["result"] = False
        return result

    result["comment"].append(f"Deleted '{name}'")
    return result

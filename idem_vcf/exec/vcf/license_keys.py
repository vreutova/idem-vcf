"""Exec module for managing License Keyss. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List

__contracts__ = ["soft_fail"]

__func_alias__ = {"list_": "list"}


async def get(hub, ctx, resource_id: str, key: str, name: str = None) -> Dict[str, Any]:
    """
    Retrieve a license key
        None

    Args:
        resource_id(str):
            License_keys unique ID.

        key(str):
            The 29 alpha numeric character license key with hyphens.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            unmanaged_resource:
              exec.run:
                - path: vcf.license_keys.get
                - kwargs:
                  resource_id: value
                  key: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.license_keys.get resource_id=value, key=value
    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change function methods params if needed
    get = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/license-keys/{key}".format(**{"key": key}),
        query_params={},
        data={},
        headers={},
    )

    if not get["result"]:
        # Send empty result for not found
        if get["status"] == 404:
            result["comment"].append(f"Get '{name}' result is empty")
            return result

        result["comment"].append(get["comment"])
        result["result"] = False
        return result

    # Case: Empty results
    if not get["ret"]:
        result["comment"].append(f"Get '{name}' result is empty")
        return result

    # Convert raw response into present format
    raw_resource = get["ret"]

    # TODO: Make sure resource_id is mapped in get response
    resource_in_present_format = {"name": name, "resource_id": resource_id}
    resource_parameters = OrderedDict(
        {
            "description": "description",
            "id": "id",
            "isUnlimited": "is_unlimited",
            "key": "key",
            "licenseKeyUsage": "license_key_usage",
            "licenseKeyValidity": "license_key_validity",
            "productType": "product_type",
            "productVersion": "product_version",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def list_(
    hub,
    ctx,
    name: str = None,
    product_type: List[str] = None,
    license_key_status: List[str] = None,
    product_version: str = None,
) -> Dict[str, Any]:
    """
    Retrieve a list of license keys
        None

    Args:
        name(str, Optional):
            Idem name of the resource. Defaults to None.

        product_type(List[str], Optional):
            Type of a Product. Defaults to None.

        license_key_status(List[str], Optional):
            Status of a License Key. Defaults to None.

        product_version(str, Optional):
            Product Version, gets the license keys matching the major version of the product version. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:

        Resource State:

        .. code-block:: sls

            unmanaged_resources:
              exec.run:
                - path: vcf.license_keys.list
                - kwargs:


        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.license_keys.list

        Describe call from the CLI:

        .. code-block:: bash

            $ idem describe vcf.license_keys

    """

    result = dict(comment=[], ret=[], result=True)

    # TODO: Change function methods params if needed
    list = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/license-keys",
        query_params={
            "productType": product_type,
            "licenseKeyStatus": license_key_status,
            "productVersion": product_version,
        },
        data={},
        headers={},
    )

    if not list["result"]:
        result["comment"].append(list["comment"])
        result["result"] = False
        return result

    for resource in list["ret"]:

        # Convert raw response into present format
        resource_in_present_format = {
            # TODO: Make sure name, resource_id is mapped accordingly
            "name": "name",
            "resource_id": "resource_id",
        }
        resource_parameters = OrderedDict(
            {
                "description": "description",
                "id": "id",
                "isUnlimited": "is_unlimited",
                "key": "key",
                "licenseKeyUsage": "license_key_usage",
                "licenseKeyValidity": "license_key_validity",
                "productType": "product_type",
                "productVersion": "product_version",
            }
        )

        for parameter_raw, parameter_present in resource_parameters.items():
            if parameter_raw in resource and resource.get(parameter_raw):
                resource_in_present_format[parameter_present] = resource.get(
                    parameter_raw
                )

        result["ret"].append(resource_in_present_format)

    return result


async def create(
    hub,
    ctx,
    description: str,
    key: str,
    product_type: str,
    resource_id: str = None,
    name: str = None,
    id_: str = None,
    is_unlimited: bool = None,
    license_key_usage: make_dataclass(
        "license_key_usage",
        [
            ("license_unit", str, field(default=None)),
            ("remaining", int, field(default=None)),
            ("total", int, field(default=None)),
            ("used", int, field(default=None)),
        ],
    ) = None,
    license_key_validity: make_dataclass(
        "license_key_validity",
        [
            ("expiry_date", str, field(default=None)),
            ("license_key_status", str, field(default=None)),
        ],
    ) = None,
    product_version: str = None,
) -> Dict[str, Any]:
    """
    Add a a new license key
        None

    Args:
        description(str):
            Description of the license key given by user.

        key(str):
            The 29 alpha numeric character license key with hyphens.

        product_type(str):
            The type of the product to which the license key is applicable.

        resource_id(str, Optional):
            License_keys unique ID. Defaults to None.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

        id_(str, Optional):
            The ID of the license key. Defaults to None.

        is_unlimited(bool, Optional):
            Indicates if the license key has unlimited usage. Defaults to None.

        license_key_usage(dict[str, Any], Optional):
            licenseKeyUsage. Defaults to None.

            * license_unit (str, Optional):
                Units of the license key. Defaults to None.

            * remaining (int, Optional):
                The remaining/free units of the license key. Defaults to None.

            * total (int, Optional):
                The total units of the license key. Defaults to None.

            * used (int, Optional):
                The consumed/used units of the license key. Defaults to None.

        license_key_validity(dict[str, Any], Optional):
            licenseKeyValidity. Defaults to None.

            * expiry_date (str, Optional):
                The license key expiry date. Defaults to None.

            * license_key_status (str, Optional):
                The validity status of the license key. Defaults to None.

        product_version(str, Optional):
            Product version. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.license_keys.present:
                - description: value
                - key: value
                - product_type: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.license_keys.create description=value, key=value, product_type=value
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {
        "description": "description",
        "id": "id",
        "is_unlimited": "isUnlimited",
        "key": "key",
        "license_key_usage": "licenseKeyUsage",
        "license_key_validity": "licenseKeyValidity",
        "product_type": "productType",
        "product_version": "productVersion",
    }

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    create = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/license-keys",
        query_params={},
        data=payload,
        headers={},
    )

    if not create["result"]:
        result["comment"].append(create["comment"])
        result["result"] = False
        return result

    result["comment"].append(
        f"Created vcf.license_keys '{name}'",
    )

    result["ret"] = create["ret"]
    # TODO: add "resource_id" to returned response by mapping to correct resource identifier
    return result


async def update(hub, ctx) -> Dict[str, Any]:
    """

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.license_keys.present:
                -

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.license_keys.update
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {}

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    if payload:
        update = await hub.tool.vcf.session.request(
            ctx,
            method="TODO",
            path="TODO".format(**{}),
            query_params={},
            data=payload,
            headers={},
        )

        if not update["result"]:
            result["comment"].append(update["comment"])
            result["result"] = False
            return result

        result["ret"] = update["ret"]
        result["comment"].append(
            f"Updated vcf.license_keys '{name}'",
        )

    return result


async def delete(
    hub, ctx, resource_id: str, key: str, name: str = None
) -> Dict[str, Any]:
    """
    Remove a license key
        None

    Args:
        resource_id(str):
            License_keys unique ID.

        key(str):
            The 29 alpha numeric character license key with hyphens.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            resource_is_absent:
              vcf.license_keys.absent:
                - resource_id: value
                - key: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.license_keys.delete resource_id=value, key=value
    """

    result = dict(comment=[], ret=[], result=True)

    delete = await hub.tool.vcf.session.request(
        ctx,
        method="delete",
        path="/v1/license-keys/{key}".format(**{"key": key}),
        query_params={},
        data={},
        headers={},
    )

    if not delete["result"]:
        result["comment"].append(delete["comment"])
        result["result"] = False
        return result

    result["comment"].append(f"Deleted '{name}'")
    return result

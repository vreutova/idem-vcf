"""Exec module for managing Network Poolss. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List

__contracts__ = ["soft_fail"]

__func_alias__ = {"list_": "list"}


async def get(hub, ctx, resource_id: str, id_: str, name: str = None) -> Dict[str, Any]:
    """
    Get a Network Pool
        Get a Network Pool by ID, if it exists

    Args:
        resource_id(str):
            Network_pools unique ID.

        id_(str):
            ID of the network pool to fetch.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            unmanaged_resource:
              exec.run:
                - path: vcf.network_pools.get
                - kwargs:
                  resource_id: value
                  id_: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.network_pools.get resource_id=value, id_=value
    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change function methods params if needed
    get = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/network-pools/{id}".format(**{"id": id_}),
        query_params={},
        data={},
        headers={},
    )

    if not get["result"]:
        # Send empty result for not found
        if get["status"] == 404:
            result["comment"].append(f"Get '{name}' result is empty")
            return result

        result["comment"].append(get["comment"])
        result["result"] = False
        return result

    # Case: Empty results
    if not get["ret"]:
        result["comment"].append(f"Get '{name}' result is empty")
        return result

    # Convert raw response into present format
    raw_resource = get["ret"]

    # TODO: Make sure resource_id is mapped in get response
    resource_in_present_format = {"name": name, "resource_id": resource_id}
    resource_parameters = OrderedDict(
        {"id": "id", "name": "name", "networks": "networks"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def list_(hub, ctx) -> Dict[str, Any]:
    """
    Get the list of all Network Pools
        Get the Network Pools


    Returns:
        Dict[str, Any]

    Examples:

        Resource State:

        .. code-block:: sls

            unmanaged_resources:
              exec.run:
                - path: vcf.network_pools.list
                - kwargs:


        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.network_pools.list

        Describe call from the CLI:

        .. code-block:: bash

            $ idem describe vcf.network_pools

    """

    result = dict(comment=[], ret=[], result=True)

    # TODO: Change function methods params if needed
    list = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/network-pools",
        query_params={},
        data={},
        headers={},
    )

    if not list["result"]:
        result["comment"].append(list["comment"])
        result["result"] = False
        return result

    for resource in list["ret"]:

        # Convert raw response into present format
        resource_in_present_format = {
            # TODO: Make sure name, resource_id is mapped accordingly
            "name": "name",
            "resource_id": "resource_id",
        }
        resource_parameters = OrderedDict(
            {"id": "id", "name": "name", "networks": "networks"}
        )

        for parameter_raw, parameter_present in resource_parameters.items():
            if parameter_raw in resource and resource.get(parameter_raw):
                resource_in_present_format[parameter_present] = resource.get(
                    parameter_raw
                )

        result["ret"].append(resource_in_present_format)

    return result


async def create(
    hub,
    ctx,
    resource_id: str = None,
    name: str = None,
    id_: str = None,
    networks: List[
        make_dataclass(
            "networks",
            [
                ("free_ips", List[str], field(default=None)),
                ("gateway", str, field(default=None)),
                ("id", str, field(default=None)),
                (
                    "ip_pools",
                    List[
                        make_dataclass(
                            "ip_pools",
                            [
                                ("end", str, field(default=None)),
                                ("start", str, field(default=None)),
                            ],
                        )
                    ],
                    field(default=None),
                ),
                ("mask", str, field(default=None)),
                ("mtu", int, field(default=None)),
                ("subnet", str, field(default=None)),
                ("type", str, field(default=None)),
                ("used_ips", List[str], field(default=None)),
                ("vlan_id", int, field(default=None)),
            ],
        )
    ] = None,
) -> Dict[str, Any]:
    """
    Create a Network Pool
        Create a Network Pool

    Args:
        resource_id(str, Optional):
            Network_pools unique ID. Defaults to None.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

        id_(str, Optional):
            The ID of the network pool. Defaults to None.

        networks(List[dict[str, Any]], Optional):
            Representing the networks associated with the network pool. Defaults to None.

            * free_ips (List[str], Optional):
                List of free IPs to use. Defaults to None.

            * gateway (str, Optional):
                Gateway for the network. Defaults to None.

            * id (str, Optional):
                The ID of the network. Defaults to None.

            * ip_pools (List[dict[str, Any]], Optional):
                List of IP pool ranges to use. Defaults to None.

                * end (str, Optional):
                    End IP address of the IP pool. Defaults to None.

                * start (str, Optional):
                    Start IP address of the IP pool. Defaults to None.

            * mask (str, Optional):
                Subnet mask for the subnet of the network. Defaults to None.

            * mtu (int, Optional):
                MTU of the network. Defaults to None.

            * subnet (str, Optional):
                Subnet associated with the network. Defaults to None.

            * type (str, Optional):
                Network Type of the network. Defaults to None.

            * used_ips (List[str], Optional):
                List of used IPs. Defaults to None.

            * vlan_id (int, Optional):
                VLAN ID associated with the network. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.network_pools.present:
                -

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.network_pools.create
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {"id": "id", "name": "name", "networks": "networks"}

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    create = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/network-pools",
        query_params={},
        data=payload,
        headers={},
    )

    if not create["result"]:
        result["comment"].append(create["comment"])
        result["result"] = False
        return result

    result["comment"].append(
        f"Created vcf.network_pools '{name}'",
    )

    result["ret"] = create["ret"]
    # TODO: add "resource_id" to returned response by mapping to correct resource identifier
    return result


async def update(
    hub, ctx, resource_id: str, id_: str, name: str = None
) -> Dict[str, Any]:
    """
    Update Network Pool
        Update a Network Pool by ID, if it exists

    Args:
        resource_id(str):
            Network_pools unique ID.

        id_(str):
            Network Pool ID.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.network_pools.present:
                - resource_id: value
                - id_: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.network_pools.update resource_id=value, id_=value
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {"name": "name"}

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    if payload:
        update = await hub.tool.vcf.session.request(
            ctx,
            method="patch",
            path="/v1/network-pools/{id}".format(**{"id": id_}),
            query_params={},
            data=payload,
            headers={},
        )

        if not update["result"]:
            result["comment"].append(update["comment"])
            result["result"] = False
            return result

        result["ret"] = update["ret"]
        result["comment"].append(
            f"Updated vcf.network_pools '{name}'",
        )

    return result


async def delete(
    hub, ctx, resource_id: str, id_: str, name: str = None
) -> Dict[str, Any]:
    """
    Delete a Network Pool
        Delete the Network Pool by the ID, if it exists and is unused

    Args:
        resource_id(str):
            Network_pools unique ID.

        id_(str):
            ID of the network pool.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            resource_is_absent:
              vcf.network_pools.absent:
                - resource_id: value
                - id_: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.network_pools.delete resource_id=value, id_=value
    """

    result = dict(comment=[], ret=[], result=True)

    delete = await hub.tool.vcf.session.request(
        ctx,
        method="delete",
        path="/v1/network-pools/{id}".format(**{"id": id_}),
        query_params={},
        data={},
        headers={},
    )

    if not delete["result"]:
        result["comment"].append(delete["comment"])
        result["result"] = False
        return result

    result["comment"].append(f"Deleted '{name}'")
    return result

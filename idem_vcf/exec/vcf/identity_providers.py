"""Exec module for managing Identity Providerss. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List

__contracts__ = ["soft_fail"]

__func_alias__ = {"list_": "list"}


async def get(hub, ctx, resource_id: str, id_: str, name: str = None) -> Dict[str, Any]:
    """
    Get an identity provider by its id
        Get a specific identity irovider using its id

    Args:
        resource_id(str):
            Identity_providers unique ID.

        id_(str):
            ID of the Identity Provider.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            unmanaged_resource:
              exec.run:
                - path: vcf.identity_providers.get
                - kwargs:
                  resource_id: value
                  id_: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.identity_providers.get resource_id=value, id_=value
    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change function methods params if needed
    get = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/identity-providers/{id}".format(**{"id": id_}),
        query_params={},
        data={},
        headers={},
    )

    if not get["result"]:
        # Send empty result for not found
        if get["status"] == 404:
            result["comment"].append(f"Get '{name}' result is empty")
            return result

        result["comment"].append(get["comment"])
        result["result"] = False
        return result

    # Case: Empty results
    if not get["ret"]:
        result["comment"].append(f"Get '{name}' result is empty")
        return result

    # Convert raw response into present format
    raw_resource = get["ret"]

    # TODO: Make sure resource_id is mapped in get response
    resource_in_present_format = {"name": name, "resource_id": resource_id}
    resource_parameters = OrderedDict(
        {
            "domainNames": "domain_names",
            "fedIdp": "fed_idp",
            "id": "id",
            "identitySources": "identity_sources",
            "idpMessage": "idp_message",
            "ldap": "ldap",
            "name": "name",
            "oidc": "oidc",
            "status": "status",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def list_(hub, ctx) -> Dict[str, Any]:
    """
    Get all identity providers
        Get a list of all identity providers


    Returns:
        Dict[str, Any]

    Examples:

        Resource State:

        .. code-block:: sls

            unmanaged_resources:
              exec.run:
                - path: vcf.identity_providers.list
                - kwargs:


        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.identity_providers.list

        Describe call from the CLI:

        .. code-block:: bash

            $ idem describe vcf.identity_providers

    """

    result = dict(comment=[], ret=[], result=True)

    # TODO: Change function methods params if needed
    list = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/identity-providers",
        query_params={},
        data={},
        headers={},
    )

    if not list["result"]:
        result["comment"].append(list["comment"])
        result["result"] = False
        return result

    for resource in list["ret"]:

        # Convert raw response into present format
        resource_in_present_format = {
            # TODO: Make sure name, resource_id is mapped accordingly
            "name": "name",
            "resource_id": "resource_id",
        }
        resource_parameters = OrderedDict(
            {
                "domainNames": "domain_names",
                "fedIdp": "fed_idp",
                "id": "id",
                "identitySources": "identity_sources",
                "idpMessage": "idp_message",
                "ldap": "ldap",
                "name": "name",
                "oidc": "oidc",
                "status": "status",
                "type": "type",
            }
        )

        for parameter_raw, parameter_present in resource_parameters.items():
            if parameter_raw in resource and resource.get(parameter_raw):
                resource_in_present_format[parameter_present] = resource.get(
                    parameter_raw
                )

        result["ret"].append(resource_in_present_format)

    return result


async def create(
    hub,
    ctx,
    type_: str,
    resource_id: str = None,
    name: str = None,
    cert_chain: List[str] = None,
    fed_idp_spec: make_dataclass(
        "fed_idp_spec",
        [
            (
                "directory",
                make_dataclass(
                    "directory",
                    [
                        ("default_domain", str),
                        ("domains", List[str]),
                        ("name", str),
                        ("directory_id", str, field(default=None)),
                    ],
                ),
            ),
            ("name", str),
            (
                "oidc_spec",
                make_dataclass(
                    "oidc_spec",
                    [
                        ("client_id", str),
                        ("client_secret", str),
                        ("discovery_endpoint", str),
                    ],
                ),
            ),
            ("sync_client_token_ttl", int, field(default=None)),
        ],
    ) = None,
    ldap: make_dataclass(
        "ldap",
        [
            ("domain_name", str),
            ("password", str),
            (
                "source_details",
                make_dataclass(
                    "source_details",
                    [
                        ("groups_base_dn", str),
                        ("server_endpoints", List[str]),
                        ("users_base_dn", str),
                        ("cert_chain", List[str], field(default=None)),
                    ],
                ),
            ),
            ("type", str),
            ("username", str),
            ("domain_alias", str, field(default=None)),
        ],
    ) = None,
    oidc: make_dataclass(
        "oidc",
        [("client_id", str), ("client_secret", str), ("discovery_endpoint", str)],
    ) = None,
) -> Dict[str, Any]:
    """
    Add a new external identity provider
        Add a new external identity provider

    Args:
        type_(str):
            The type of Identity Identity Provider.

        resource_id(str, Optional):
            Identity_providers unique ID. Defaults to None.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

        cert_chain(List[str], Optional):
            The root certificate chain required to connect to the external server. Defaults to None.

        fed_idp_spec(dict[str, Any], Optional):
            fedIdpSpec. Defaults to None.

            * directory (dict[str, Any]):
                directory.

                * default_domain (str):
                    The trusted default domain of the directory.

                * directory_id (str, Optional):
                    The id of the directory. Defaults to None.

                * domains (List[str]):
                    The set of trusted domains of the directory.

                * name (str):
                    The user-friendly name for the directory.

            * name (str):
                The user-friendly name for the Identity Provider.

            * oidc_spec (dict[str, Any]):
                oidcSpec.

                * client_id (str):
                    Client identifier to connect to the provider.

                * client_secret (str):
                    The secret shared between the client and the provider.

                * discovery_endpoint (str):
                    Endpoint to retrieve the provider metadata.

            * sync_client_token_ttl (int, Optional):
                The lifetime in seconds of the sync client bear token, default to 3 days if not specified. Defaults to None.

        ldap(dict[str, Any], Optional):
            ldap. Defaults to None.

            * domain_alias (str, Optional):
                The optional alias to associate the domain name. Defaults to None.

            * domain_name (str):
                The name to associate with the created domain.

            * password (str):
                Password to connect to the ldap(s) server.

            * source_details (dict[str, Any]):
                sourceDetails.

                * cert_chain (List[str], Optional):
                    SSL certificate chain in base64 encoding. This field can be unset only, if all the active directory server endpoints use the LDAP (not LDAPS) protocol. Defaults to None.

                * groups_base_dn (str):
                    Base distinguished name for groups.

                * server_endpoints (List[str]):
                    Active directory server endpoints. At least one active directory server endpoint must be set.

                * users_base_dn (str):
                    Base distinguished name for users.

            * type (str):
                The type of the LDAP Server.

            * username (str):
                User name to connect to ldap(s) server.

        oidc(dict[str, Any], Optional):
            oidc. Defaults to None.

            * client_id (str):
                Client identifier to connect to the provider.

            * client_secret (str):
                The secret shared between the client and the provider.

            * discovery_endpoint (str):
                Endpoint to retrieve the provider metadata.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.identity_providers.present:
                - type_: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.identity_providers.create type_=value
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {
        "cert_chain": "certChain",
        "fed_idp_spec": "fedIdpSpec",
        "ldap": "ldap",
        "name": "name",
        "oidc": "oidc",
        "type": "type",
    }

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    create = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/identity-providers",
        query_params={},
        data=payload,
        headers={},
    )

    if not create["result"]:
        result["comment"].append(create["comment"])
        result["result"] = False
        return result

    result["comment"].append(
        f"Created vcf.identity_providers '{name}'",
    )

    result["ret"] = create["ret"]
    # TODO: add "resource_id" to returned response by mapping to correct resource identifier
    return result


async def update(
    hub,
    ctx,
    resource_id: str,
    id_: str,
    type_: str,
    name: str = None,
    cert_chain: List[str] = None,
    fed_idp_spec: make_dataclass(
        "fed_idp_spec",
        [
            (
                "directory",
                make_dataclass(
                    "directory",
                    [
                        ("default_domain", str),
                        ("domains", List[str]),
                        ("name", str),
                        ("directory_id", str, field(default=None)),
                    ],
                ),
            ),
            ("name", str),
            (
                "oidc_spec",
                make_dataclass(
                    "oidc_spec",
                    [
                        ("client_id", str),
                        ("client_secret", str),
                        ("discovery_endpoint", str),
                    ],
                ),
            ),
            ("sync_client_token_ttl", int, field(default=None)),
        ],
    ) = None,
    ldap: make_dataclass(
        "ldap",
        [
            ("domain_name", str),
            ("password", str),
            (
                "source_details",
                make_dataclass(
                    "source_details",
                    [
                        ("groups_base_dn", str),
                        ("server_endpoints", List[str]),
                        ("users_base_dn", str),
                        ("cert_chain", List[str], field(default=None)),
                    ],
                ),
            ),
            ("type", str),
            ("username", str),
            ("domain_alias", str, field(default=None)),
        ],
    ) = None,
    oidc: make_dataclass(
        "oidc",
        [("client_id", str), ("client_secret", str), ("discovery_endpoint", str)],
    ) = None,
) -> Dict[str, Any]:
    """
    Update an identity provider
        Update the identity provider by its identifier, if it exists

    Args:
        resource_id(str):
            Identity_providers unique ID.

        id_(str):
            ID of Identity Provider.

        type_(str):
            The type of Identity Identity Provider.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

        cert_chain(List[str], Optional):
            The root certificate chain required to connect to the external server. Defaults to None.

        fed_idp_spec(dict[str, Any], Optional):
            fedIdpSpec. Defaults to None.

            * directory (dict[str, Any]):
                directory.

                * default_domain (str):
                    The trusted default domain of the directory.

                * directory_id (str, Optional):
                    The id of the directory. Defaults to None.

                * domains (List[str]):
                    The set of trusted domains of the directory.

                * name (str):
                    The user-friendly name for the directory.

            * name (str):
                The user-friendly name for the Identity Provider.

            * oidc_spec (dict[str, Any]):
                oidcSpec.

                * client_id (str):
                    Client identifier to connect to the provider.

                * client_secret (str):
                    The secret shared between the client and the provider.

                * discovery_endpoint (str):
                    Endpoint to retrieve the provider metadata.

            * sync_client_token_ttl (int, Optional):
                The lifetime in seconds of the sync client bear token, default to 3 days if not specified. Defaults to None.

        ldap(dict[str, Any], Optional):
            ldap. Defaults to None.

            * domain_alias (str, Optional):
                The optional alias to associate the domain name. Defaults to None.

            * domain_name (str):
                The name to associate with the created domain.

            * password (str):
                Password to connect to the ldap(s) server.

            * source_details (dict[str, Any]):
                sourceDetails.

                * cert_chain (List[str], Optional):
                    SSL certificate chain in base64 encoding. This field can be unset only, if all the active directory server endpoints use the LDAP (not LDAPS) protocol. Defaults to None.

                * groups_base_dn (str):
                    Base distinguished name for groups.

                * server_endpoints (List[str]):
                    Active directory server endpoints. At least one active directory server endpoint must be set.

                * users_base_dn (str):
                    Base distinguished name for users.

            * type (str):
                The type of the LDAP Server.

            * username (str):
                User name to connect to ldap(s) server.

        oidc(dict[str, Any], Optional):
            oidc. Defaults to None.

            * client_id (str):
                Client identifier to connect to the provider.

            * client_secret (str):
                The secret shared between the client and the provider.

            * discovery_endpoint (str):
                Endpoint to retrieve the provider metadata.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.identity_providers.present:
                - resource_id: value
                - id_: value
                - type_: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.identity_providers.update resource_id=value, id_=value, type_=value
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {
        "cert_chain": "certChain",
        "fed_idp_spec": "fedIdpSpec",
        "ldap": "ldap",
        "name": "name",
        "oidc": "oidc",
        "type": "type",
    }

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    if payload:
        update = await hub.tool.vcf.session.request(
            ctx,
            method="patch",
            path="/v1/identity-providers/{id}".format(**{"id": id_}),
            query_params={},
            data=payload,
            headers={},
        )

        if not update["result"]:
            result["comment"].append(update["comment"])
            result["result"] = False
            return result

        result["ret"] = update["ret"]
        result["comment"].append(
            f"Updated vcf.identity_providers '{name}'",
        )

    return result


async def delete(
    hub, ctx, resource_id: str, id_: str, name: str = None
) -> Dict[str, Any]:
    """
    Remove an identity provider
        Delete an Identity Provider by its identifier, if it exists

    Args:
        resource_id(str):
            Identity_providers unique ID.

        id_(str):
            ID of Identity Provider.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            resource_is_absent:
              vcf.identity_providers.absent:
                - resource_id: value
                - id_: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.identity_providers.delete resource_id=value, id_=value
    """

    result = dict(comment=[], ret=[], result=True)

    delete = await hub.tool.vcf.session.request(
        ctx,
        method="delete",
        path="/v1/identity-providers/{id}".format(**{"id": id_}),
        query_params={},
        data={},
        headers={},
    )

    if not delete["result"]:
        result["comment"].append(delete["comment"])
        result["result"] = False
        return result

    result["comment"].append(f"Deleted '{name}'")
    return result

"""Exec module for managing Check Setss. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List

__contracts__ = ["soft_fail"]

__func_alias__ = {"list_": "list"}


async def get(
    hub,
    ctx,
    resource_id: str,
    run_id: str,
    name: str = None,
    obfuscate: bool = None,
    return_all_artefacts: bool = None,
) -> Dict[str, Any]:
    """
    Get the result for a given check run
        Get the result for a given check run

    Args:
        resource_id(str):
            Check_sets unique ID.

        run_id(str):
            UUID of the task.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

        obfuscate(bool, Optional):
            Obfuscate. Only for internal use. Defaults to None.

        return_all_artefacts(bool, Optional):
            Returns all artefacts. Only for internal use. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            unmanaged_resource:
              exec.run:
                - path: vcf.check_sets.get
                - kwargs:
                  resource_id: value
                  run_id: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.check_sets.get resource_id=value, run_id=value
    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change function methods params if needed
    get = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/system/check-sets/{runId}".format(**{"runId": run_id}),
        query_params={
            "obfuscate": obfuscate,
            "returnAllArtefacts": return_all_artefacts,
        },
        data={},
        headers={},
    )

    if not get["result"]:
        # Send empty result for not found
        if get["status"] == 404:
            result["comment"].append(f"Get '{name}' result is empty")
            return result

        result["comment"].append(get["comment"])
        result["result"] = False
        return result

    # Case: Empty results
    if not get["ret"]:
        result["comment"].append(f"Get '{name}' result is empty")
        return result

    # Convert raw response into present format
    raw_resource = get["ret"]

    # TODO: Make sure resource_id is mapped in get response
    resource_in_present_format = {"name": name, "resource_id": resource_id}
    resource_parameters = OrderedDict(
        {
            "completionTimestamp": "completion_timestamp",
            "discoveryProgress": "discovery_progress",
            "inputValidationErrors": "input_validation_errors",
            "physicalPresentedData": "physical_presented_data",
            "presentedArtifactsMap": "presented_artifacts_map",
            "relatedAssessmentId": "related_assessment_id",
            "status": "status",
            "timestamp": "timestamp",
            "validationResult": "validation_result",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def list_(hub, ctx, name: str = None, domain_id: str = None) -> Dict[str, Any]:
    """
    Get information about the last assessment run
        Get information about the last assessment run

    Args:
        name(str, Optional):
            Idem name of the resource. Defaults to None.

        domain_id(str, Optional):
            Id of the domain to filter tasks for. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:

        Resource State:

        .. code-block:: sls

            unmanaged_resources:
              exec.run:
                - path: vcf.check_sets.list
                - kwargs:


        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.check_sets.list

        Describe call from the CLI:

        .. code-block:: bash

            $ idem describe vcf.check_sets

    """

    result = dict(comment=[], ret=[], result=True)

    # TODO: Change function methods params if needed
    list = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/system/check-sets",
        query_params={"domainId": domain_id},
        data={},
        headers={},
    )

    if not list["result"]:
        result["comment"].append(list["comment"])
        result["result"] = False
        return result

    for resource in list["ret"]:

        # Convert raw response into present format
        resource_in_present_format = {
            # TODO: Make sure name, resource_id is mapped accordingly
            "name": "name",
            "resource_id": "resource_id",
        }
        resource_parameters = OrderedDict(
            {
                "completionTimestamp": "completion_timestamp",
                "discoveryProgress": "discovery_progress",
                "inputValidationErrors": "input_validation_errors",
                "physicalPresentedData": "physical_presented_data",
                "presentedArtifactsMap": "presented_artifacts_map",
                "relatedAssessmentId": "related_assessment_id",
                "status": "status",
                "timestamp": "timestamp",
                "validationResult": "validation_result",
            }
        )

        for parameter_raw, parameter_present in resource_parameters.items():
            if parameter_raw in resource and resource.get(parameter_raw):
                resource_in_present_format[parameter_present] = resource.get(
                    parameter_raw
                )

        result["ret"].append(resource_in_present_format)

    return result


async def create(
    hub,
    ctx,
    query_id: str,
    resources: List[
        make_dataclass(
            "resources",
            [
                ("resource_id", str),
                ("resource_name", str),
                ("resource_type", str),
                (
                    "check_sets",
                    List[make_dataclass("check_sets", [("check_set_id", str)])],
                    field(default=None),
                ),
                (
                    "domain",
                    make_dataclass(
                        "domain",
                        [
                            ("domain_id", str, field(default=None)),
                            ("domain_name", str, field(default=None)),
                            ("domain_type", str, field(default=None)),
                        ],
                    ),
                    field(default=None),
                ),
            ],
        )
    ],
    resource_id: str = None,
    name: str = None,
) -> Dict[str, Any]:
    """
    Trigger a run of the selected checks
        Trigger a run of the selected checks

    Args:
        query_id(str):
            Id of the query the selection was based on.

        resources(List[dict[str, Any]]):
            Information about the resource and its selection.

            * check_sets (List[dict[str, Any]], Optional):
                Information about the selected check-set candidates. Defaults to None.

                * check_set_id (str):
                    Id of the selected check-set.

            * domain (dict[str, Any], Optional):
                domain. Defaults to None.

                * domain_id (str, Optional):
                    Id of the domain. Defaults to None.

                * domain_name (str, Optional):
                    Name of the domain. Defaults to None.

                * domain_type (str, Optional):
                    Type of the domain. Defaults to None.

            * resource_id (str):
                Id of the resource.

            * resource_name (str):
                Name of the resource.

            * resource_type (str):
                Type of the resource.

        resource_id(str, Optional):
            Check_sets unique ID. Defaults to None.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.check_sets.present:
                - query_id: value
                - resources: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.check_sets.create query_id=value, resources=value
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {"query_id": "queryId", "resources": "resources"}

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    create = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/system/check-sets",
        query_params={},
        data=payload,
        headers={},
    )

    if not create["result"]:
        result["comment"].append(create["comment"])
        result["result"] = False
        return result

    result["comment"].append(
        f"Created vcf.check_sets '{name}'",
    )

    result["ret"] = create["ret"]
    # TODO: add "resource_id" to returned response by mapping to correct resource identifier
    return result


async def update(
    hub,
    ctx,
    resource_id: str,
    run_id: str,
    name: str = None,
    error_ids: List[str] = None,
    options: Dict = None,
) -> Dict[str, Any]:
    """
    Trigger partial retry of a completed check run
        Trigger partial retry of a completed check run

    Args:
        resource_id(str):
            Check_sets unique ID.

        run_id(str):
            UUID of the assessment to retry.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

        error_ids(List[str], Optional):
            List of error ids to retry. Defaults to None.

        options(Dict, Optional):
            Options map (toggles) to control assessment flow. Available toggles 'discoveryCaching' - if set to true the assessment engine will use the Domain data (if present) from previous discovery execution. If any of the passed in the request domains is not present in the cache the Discovery is triggered for ALL domains (even the ones existing in the cache). Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.check_sets.present:
                - resource_id: value
                - run_id: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.check_sets.update resource_id=value, run_id=value
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {"error_ids": "errorIds", "options": "options"}

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    if payload:
        update = await hub.tool.vcf.session.request(
            ctx,
            method="patch",
            path="/v1/system/check-sets/{runId}".format(**{"runId": run_id}),
            query_params={},
            data=payload,
            headers={},
        )

        if not update["result"]:
            result["comment"].append(update["comment"])
            result["result"] = False
            return result

        result["ret"] = update["ret"]
        result["comment"].append(
            f"Updated vcf.check_sets '{name}'",
        )

    return result


async def delete(hub, ctx) -> Dict[str, Any]:
    """

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            resource_is_absent:
              vcf.check_sets.absent:
                -

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.check_sets.delete
    """

    result = dict(comment=[], ret=[], result=True)

    delete = await hub.tool.vcf.session.request(
        ctx,
        method="TODO",
        path="TODO".format(**{}),
        query_params={},
        data={},
        headers={},
    )

    if not delete["result"]:
        result["comment"].append(delete["comment"])
        result["result"] = False
        return result

    result["comment"].append(f"Deleted '{name}'")
    return result

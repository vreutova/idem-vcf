"""Exec module for managing Manifestss. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List

__contracts__ = ["soft_fail"]

__func_alias__ = {"list_": "list"}


async def get(hub, ctx) -> Dict[str, Any]:
    """

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            unmanaged_resource:
              exec.run:
                - path: vcf.manifests.get
                - kwargs:


        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.manifests.get
    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change function methods params if needed
    get = await hub.tool.vcf.session.request(
        ctx,
        method="TODO",
        path="TODO".format(**{}),
        query_params={},
        data={},
        headers={},
    )

    if not get["result"]:
        # Send empty result for not found
        if get["status"] == 404:
            result["comment"].append(f"Get '{name}' result is empty")
            return result

        result["comment"].append(get["comment"])
        result["result"] = False
        return result

    # Case: Empty results
    if not get["ret"]:
        result["comment"].append(f"Get '{name}' result is empty")
        return result

    # TODO: Make sure resource_id is mapped in get response
    get["ret"]["resource_id"] = resource_id
    result["ret"] = get["ret"]

    return result


async def list_(hub, ctx) -> Dict[str, Any]:
    """
    Get manifest
        Get manifest. There should be only one valid manifest in the System.


    Returns:
        Dict[str, Any]

    Examples:

        Resource State:

        .. code-block:: sls

            unmanaged_resources:
              exec.run:
                - path: vcf.manifests.list
                - kwargs:


        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.manifests.list

        Describe call from the CLI:

        .. code-block:: bash

            $ idem describe vcf.manifests

    """

    result = dict(comment=[], ret=[], result=True)

    # TODO: Change function methods params if needed
    list = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/manifests",
        query_params={},
        data={},
        headers={},
    )

    if not list["result"]:
        result["comment"].append(list["comment"])
        result["result"] = False
        return result

    for resource in list["ret"]:

        # Convert raw response into present format
        resource_in_present_format = {
            # TODO: Make sure name, resource_id is mapped accordingly
            "name": "name",
            "resource_id": "resource_id",
        }
        resource_parameters = OrderedDict(
            {
                "asyncPatches": "async_patches",
                "creationTime": "creation_time",
                "publishedDate": "published_date",
                "recalledBundles": "recalled_bundles",
                "releases": "releases",
                "sequenceNumber": "sequence_number",
                "version": "version",
                "vvsMappings": "vvs_mappings",
            }
        )

        for parameter_raw, parameter_present in resource_parameters.items():
            if parameter_raw in resource and resource.get(parameter_raw):
                resource_in_present_format[parameter_present] = resource.get(
                    parameter_raw
                )

        result["ret"].append(resource_in_present_format)

    return result


async def create(
    hub,
    ctx,
    creation_time: str,
    published_date: str,
    releases: List[
        make_dataclass(
            "releases",
            [
                (
                    "bom",
                    List[
                        make_dataclass(
                            "bom",
                            [
                                ("name", str),
                                ("public_name", str),
                                ("version", str),
                                ("additional_metadata", str, field(default=None)),
                                ("release_url", str, field(default=None)),
                            ],
                        )
                    ],
                ),
                ("description", str),
                ("min_compatible_vcf_version", str),
                ("product", str),
                ("release_date", str),
                ("version", str),
                ("eol", str, field(default=None)),
                ("is_applicable", bool, field(default=None)),
                ("not_applicable_reason", str, field(default=None)),
                (
                    "patch_bundles",
                    List[
                        make_dataclass(
                            "patch_bundles",
                            [
                                ("bundle_elements", List[str]),
                                ("bundle_id", str),
                                ("bundle_type", str),
                                ("cumulative_from_vcf_version", str),
                            ],
                        )
                    ],
                    field(default=None),
                ),
                (
                    "sku",
                    List[
                        make_dataclass(
                            "sku",
                            [
                                (
                                    "bom",
                                    List[
                                        make_dataclass(
                                            "bom",
                                            [
                                                ("name", str),
                                                ("public_name", str),
                                                ("version", str),
                                                (
                                                    "additional_metadata",
                                                    str,
                                                    field(default=None),
                                                ),
                                                (
                                                    "release_url",
                                                    str,
                                                    field(default=None),
                                                ),
                                            ],
                                        )
                                    ],
                                ),
                                ("description", str, field(default=None)),
                                ("name", str, field(default=None)),
                                (
                                    "sku_specific_patch_bundles",
                                    List[
                                        make_dataclass(
                                            "sku_specific_patch_bundles",
                                            [
                                                ("bundle_type", str),
                                                ("bundle_version", str),
                                                ("version", str),
                                            ],
                                        )
                                    ],
                                    field(default=None),
                                ),
                            ],
                        )
                    ],
                    field(default=None),
                ),
                (
                    "updates",
                    List[
                        make_dataclass(
                            "updates",
                            [
                                ("base_product_version", str),
                                ("description", str),
                                ("id", str),
                                ("product_name", str),
                                ("release_date", str),
                                (
                                    "release_update_url",
                                    make_dataclass(
                                        "release_update_url",
                                        [
                                            ("authority", str, field(default=None)),
                                            ("content", Dict, field(default=None)),
                                            ("default_port", int, field(default=None)),
                                            (
                                                "deserialized_fields",
                                                {},
                                                field(default=None),
                                            ),
                                            ("file", str, field(default=None)),
                                            ("host", str, field(default=None)),
                                            ("path", str, field(default=None)),
                                            ("port", int, field(default=None)),
                                            ("protocol", str, field(default=None)),
                                            ("query", str, field(default=None)),
                                            ("ref", str, field(default=None)),
                                            (
                                                "serialized_hash_code",
                                                int,
                                                field(default=None),
                                            ),
                                            ("user_info", str, field(default=None)),
                                        ],
                                    ),
                                    field(default=None),
                                ),
                            ],
                        )
                    ],
                    field(default=None),
                ),
            ],
        )
    ],
    sequence_number: int,
    version: int,
    resource_id: str = None,
    name: str = None,
    async_patches: Dict = None,
    recalled_bundles: List[
        make_dataclass(
            "recalled_bundles",
            [
                ("recalled_bundle_ids", List[str]),
                ("replacement_bundle_status", str),
                ("replacement_bundle_ids", List[str], field(default=None)),
            ],
        )
    ] = None,
    vvs_mappings: Dict = None,
) -> Dict[str, Any]:
    """
    Save/Load manifest
        Save/Load manifest. Make sure manifest is a valid one. If manifest already exists, it gets overridden.

    Args:
        creation_time(str):
            Creation time for the manifest e.g. 2020-06-08T02:20:15.844Z, in yyyy-MM-dd'T'HH:mm:ss[.SSS]XXX ISO 8601 format.

        published_date(str):
            Date of publish of the manifest e.g. 2020-06-08T02:20:15.844Z, in yyyy-MM-dd'T'HH:mm:ss[.SSS]XXX ISO 8601 format.

        releases(List[dict[str, Any]]):
            Releases of VCF in the ascending order of product version.

            * bom (List[dict[str, Any]]):
                Release bill of materials.

                * additional_metadata (str, Optional):
                    any additional metadata. Defaults to None.

                * name (str):
                    Name of the product. e.g ESX.

                * public_name (str):
                    Public name of the product, e.g VMware ESXi.

                * release_url (str, Optional):
                    URL for the release. Defaults to None.

                * version (str):
                    Version for the product, e.g 6.7.0-11675023.

            * description (str):
                Release description with all major features.

            * eol (str, Optional):
                Release eol information e.g. 2020-06-08T02:20:15.844Z in yyyy-MM-dd'T'HH:mm:ss[.SSS]XXX ISO 8601 format. Defaults to None.

            * is_applicable (bool, Optional):
                [Deprecated] Whether bundle is applicable or not. Defaults to None.

            * min_compatible_vcf_version (str):
                Minimum compatible VCF version, used to represent compatibility of SDDC Manager and VMware BOM components.

            * not_applicable_reason (str, Optional):
                [Deprecated] Incompatibility reason for not applicable. Defaults to None.

            * patch_bundles (List[dict[str, Any]], Optional):
                List of patch bundles in this release. Defaults to None.

                * bundle_elements (List[str]):
                    Bundle elements of the patch bundle.

                * bundle_id (str):
                    Bundle ID of the patch bundle.

                * bundle_type (str):
                    Bundle type of the patch bundle.

                * cumulative_from_vcf_version (str):
                    Minimum VCF version that this patch bundle can be directly applied on.

            * product (str):
                Name of the product e.g. "VCF".

            * release_date (str):
                Release date e.g. 2020-06-08T02:20:15.844Z in yyyy-MM-dd'T'HH:mm:ss[.SSS]XXX ISO 8601 format.

            * sku (List[dict[str, Any]], Optional):
                Release sku specific patch and bill of materials. Defaults to None.

                * bom (List[dict[str, Any]]):
                    Sku specific bill of materials.

                    * additional_metadata (str, Optional):
                        any additional metadata. Defaults to None.

                    * name (str):
                        Name of the product. e.g ESX.

                    * public_name (str):
                        Public name of the product, e.g VMware ESXi.

                    * release_url (str, Optional):
                        URL for the release. Defaults to None.

                    * version (str):
                        Version for the product, e.g 6.7.0-11675023.

                * description (str, Optional):
                    Description to be shown in release page. Defaults to None.

                * name (str, Optional):
                    SKU name. Defaults to None.

                * sku_specific_patch_bundles (List[dict[str, Any]], Optional):
                    List of patch bundles in this release. Defaults to None.

                    * bundle_type (str):
                        Bundle type of the patch bundle.

                    * bundle_version (str):
                        Bundle Version of the product.

                    * version (str):
                        Product version.

            * updates (List[dict[str, Any]], Optional):
                Collection of release updates. Defaults to None.

                * base_product_version (str):
                    Base product version for the release.

                * description (str):
                    Description of the release update.

                * id (str):
                    Release version.

                * product_name (str):
                    Product name for which the release update is provided.

                * release_date (str):
                    Release date e.g. 2020-06-08T02:20:15.844Z in yyyy-MM-dd'T'HH:mm:ss[.SSS]XXX ISO 8601 format.

                * release_update_url (dict[str, Any], Optional):
                    releaseUpdateURL. Defaults to None.

                    * authority (str, Optional):
                        authority. Defaults to None.

                    * content (Dict, Optional):
                        content. Defaults to None.

                    * default_port (int, Optional):
                        defaultPort. Defaults to None.

                    * deserialized_fields ({}, Optional):
                        deserializedFields. Defaults to None.

                    * file (str, Optional):
                        file. Defaults to None.

                    * host (str, Optional):
                        host. Defaults to None.

                    * path (str, Optional):
                        path. Defaults to None.

                    * port (int, Optional):
                        port. Defaults to None.

                    * protocol (str, Optional):
                        protocol. Defaults to None.

                    * query (str, Optional):
                        query. Defaults to None.

                    * ref (str, Optional):
                        ref. Defaults to None.

                    * serialized_hash_code (int, Optional):
                        serializedHashCode. Defaults to None.

                    * user_info (str, Optional):
                        userInfo. Defaults to None.

            * version (str):
                Version of the release.

        sequence_number(int):
            Manifest sequence number which signifies an update in manifest.

        version(int):
            Manifest version supported by VCF.

        resource_id(str, Optional):
            Manifests unique ID. Defaults to None.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

        async_patches(Dict, Optional):
            Async patches used by async patch tool. Defaults to None.

        recalled_bundles(List[dict[str, Any]], Optional):
            Collection of bundles that are recalled and their replacements, if applicable. Defaults to None.

            * recalled_bundle_ids (List[str]):
                List of Bundle IDs that are recalled.

            * replacement_bundle_ids (List[str], Optional):
                List of Bundle IDs that act as replacement. Defaults to None.

            * replacement_bundle_status (str):
                Status of bundle replacement.

        vvs_mappings(Dict, Optional):
            VVS product release Id to VCF version mapping. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.manifests.present:
                - creation_time: value
                - published_date: value
                - releases: value
                - sequence_number: value
                - version: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.manifests.create creation_time=value, published_date=value, releases=value, sequence_number=value, version=value
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {
        "async_patches": "asyncPatches",
        "creation_time": "creationTime",
        "published_date": "publishedDate",
        "recalled_bundles": "recalledBundles",
        "releases": "releases",
        "sequence_number": "sequenceNumber",
        "version": "version",
        "vvs_mappings": "vvsMappings",
    }

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    create = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/manifests",
        query_params={},
        data=payload,
        headers={},
    )

    if not create["result"]:
        result["comment"].append(create["comment"])
        result["result"] = False
        return result

    result["comment"].append(
        f"Created vcf.manifests '{name}'",
    )

    result["ret"] = create["ret"]
    # TODO: add "resource_id" to returned response by mapping to correct resource identifier
    return result


async def update(hub, ctx) -> Dict[str, Any]:
    """

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.manifests.present:
                -

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.manifests.update
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {}

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    if payload:
        update = await hub.tool.vcf.session.request(
            ctx,
            method="TODO",
            path="TODO".format(**{}),
            query_params={},
            data=payload,
            headers={},
        )

        if not update["result"]:
            result["comment"].append(update["comment"])
            result["result"] = False
            return result

        result["ret"] = update["ret"]
        result["comment"].append(
            f"Updated vcf.manifests '{name}'",
        )

    return result


async def delete(hub, ctx) -> Dict[str, Any]:
    """

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            resource_is_absent:
              vcf.manifests.absent:
                -

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.manifests.delete
    """

    result = dict(comment=[], ret=[], result=True)

    delete = await hub.tool.vcf.session.request(
        ctx,
        method="TODO",
        path="TODO".format(**{}),
        query_params={},
        data={},
        headers={},
    )

    if not delete["result"]:
        result["comment"].append(delete["comment"])
        result["result"] = False
        return result

    result["comment"].append(f"Deleted '{name}'")
    return result

"""Exec module for managing Releaseses. """
from collections import OrderedDict
from typing import Any
from typing import Dict

__contracts__ = ["soft_fail"]

__func_alias__ = {"list_": "list"}


async def get(hub, ctx) -> Dict[str, Any]:
    """
    Retrieve the lowest deployed release of VMware Cloud Foundation
        Returns release for the lowest deployed VCF version for a domain on the environment.If Management domain is ahead of WLD domain, VCF BOM version for the WLDdomain will be returned.


    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            unmanaged_resource:
              exec.run:
                - path: vcf.releases.get
                - kwargs:


        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.releases.get
    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change function methods params if needed
    get = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/releases/system".format(**{}),
        query_params={},
        data={},
        headers={},
    )

    if not get["result"]:
        # Send empty result for not found
        if get["status"] == 404:
            result["comment"].append(f"Get '{name}' result is empty")
            return result

        result["comment"].append(get["comment"])
        result["result"] = False
        return result

    # Case: Empty results
    if not get["ret"]:
        result["comment"].append(f"Get '{name}' result is empty")
        return result

    # Convert raw response into present format
    raw_resource = get["ret"]

    # TODO: Make sure resource_id is mapped in get response
    resource_in_present_format = {"name": name, "resource_id": resource_id}
    resource_parameters = OrderedDict(
        {
            "bom": "bom",
            "description": "description",
            "eol": "eol",
            "isApplicable": "is_applicable",
            "minCompatibleVcfVersion": "min_compatible_vcf_version",
            "notApplicableReason": "not_applicable_reason",
            "patchBundles": "patch_bundles",
            "product": "product",
            "releaseDate": "release_date",
            "sku": "sku",
            "updates": "updates",
            "version": "version",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def list_(hub, ctx, domain_id: str, name: str = None) -> Dict[str, Any]:
    """
    Retrive a list of future release by domain ID
        Returns all known to the system future target versions for a domain. If some of them are not allowed (e.g. stepping stone) includes message why.

    Args:
        domain_id(str):
            Domain ID to get all feature target versions.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:

        Resource State:

        .. code-block:: sls

            unmanaged_resources:
              exec.run:
                - path: vcf.releases.list
                - kwargs:
                  domain_id: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.releases.list domain_id=value

        Describe call from the CLI:

        .. code-block:: bash

            $ idem describe vcf.releases

    """

    result = dict(comment=[], ret=[], result=True)

    # TODO: Change function methods params if needed
    list = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/releases/domains/{domainId}/future-releases",
        query_params={},
        data={},
        headers={},
    )

    if not list["result"]:
        result["comment"].append(list["comment"])
        result["result"] = False
        return result

    for resource in list["ret"]:

        # Convert raw response into present format
        resource_in_present_format = {
            # TODO: Make sure name, resource_id is mapped accordingly
            "name": "name",
            "resource_id": "resource_id",
        }
        resource_parameters = OrderedDict(
            {
                "bom": "bom",
                "description": "description",
                "eol": "eol",
                "isApplicable": "is_applicable",
                "minCompatibleVcfVersion": "min_compatible_vcf_version",
                "notApplicableReason": "not_applicable_reason",
                "patchBundles": "patch_bundles",
                "product": "product",
                "releaseDate": "release_date",
                "sku": "sku",
                "updates": "updates",
                "version": "version",
            }
        )

        for parameter_raw, parameter_present in resource_parameters.items():
            if parameter_raw in resource and resource.get(parameter_raw):
                resource_in_present_format[parameter_present] = resource.get(
                    parameter_raw
                )

        result["ret"].append(resource_in_present_format)

    return result


async def create(hub, ctx) -> Dict[str, Any]:
    """

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.releases.present:
                -

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.releases.create
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {}

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    create = await hub.tool.vcf.session.request(
        ctx,
        method="TODO",
        path="TODO",
        query_params={},
        data=payload,
        headers={},
    )

    if not create["result"]:
        result["comment"].append(create["comment"])
        result["result"] = False
        return result

    result["comment"].append(
        f"Created vcf.releases '{name}'",
    )

    result["ret"] = create["ret"]
    # TODO: add "resource_id" to returned response by mapping to correct resource identifier
    return result


async def update(hub, ctx) -> Dict[str, Any]:
    """

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.releases.present:
                -

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.releases.update
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {}

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    if payload:
        update = await hub.tool.vcf.session.request(
            ctx,
            method="TODO",
            path="TODO".format(**{}),
            query_params={},
            data=payload,
            headers={},
        )

        if not update["result"]:
            result["comment"].append(update["comment"])
            result["result"] = False
            return result

        result["ret"] = update["ret"]
        result["comment"].append(
            f"Updated vcf.releases '{name}'",
        )

    return result


async def delete(hub, ctx) -> Dict[str, Any]:
    """

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            resource_is_absent:
              vcf.releases.absent:
                -

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.releases.delete
    """

    result = dict(comment=[], ret=[], result=True)

    delete = await hub.tool.vcf.session.request(
        ctx,
        method="TODO",
        path="TODO".format(**{}),
        query_params={},
        data={},
        headers={},
    )

    if not delete["result"]:
        result["comment"].append(delete["comment"])
        result["result"] = False
        return result

    result["comment"].append(f"Deleted '{name}'")
    return result

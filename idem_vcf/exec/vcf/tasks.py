"""Exec module for managing Taskss. """
from collections import OrderedDict
from typing import Any
from typing import Dict

__contracts__ = ["soft_fail"]

__func_alias__ = {"list_": "list"}


async def get(hub, ctx, resource_id: str, id_: str, name: str = None) -> Dict[str, Any]:
    """
    Retrieve a task by its ID
        Get a Task by ID, if it exists

    Args:
        resource_id(str):
            Tasks unique ID.

        id_(str):
            Task id to retrieve.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            unmanaged_resource:
              exec.run:
                - path: vcf.tasks.get
                - kwargs:
                  resource_id: value
                  id_: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.tasks.get resource_id=value, id_=value
    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change function methods params if needed
    get = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/tasks/{id}".format(**{"id": id_}),
        query_params={},
        data={},
        headers={},
    )

    if not get["result"]:
        # Send empty result for not found
        if get["status"] == 404:
            result["comment"].append(f"Get '{name}' result is empty")
            return result

        result["comment"].append(get["comment"])
        result["result"] = False
        return result

    # Case: Empty results
    if not get["ret"]:
        result["comment"].append(f"Get '{name}' result is empty")
        return result

    # Convert raw response into present format
    raw_resource = get["ret"]

    # TODO: Make sure resource_id is mapped in get response
    resource_in_present_format = {"name": name, "resource_id": resource_id}
    resource_parameters = OrderedDict(
        {
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "errors": "errors",
            "id": "id",
            "isCancellable": "is_cancellable",
            "isRetryable": "is_retryable",
            "localizableDescriptionPack": "localizable_description_pack",
            "name": "name",
            "resolutionStatus": "resolution_status",
            "resources": "resources",
            "status": "status",
            "subTasks": "sub_tasks",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def list_(
    hub,
    ctx,
    name: str = None,
    limit: int = None,
    task_status: str = None,
    task_type: str = None,
    resource_id: str = None,
    resource_type: str = None,
    completed_after: int = None,
    page_number: int = None,
    page_size: int = None,
    order_direction: str = None,
    order_by: str = None,
    task_name: str = None,
) -> Dict[str, Any]:
    """
    Retrieve a list of all tasks
        Get the tasks

    Args:
        name(str, Optional):
            Idem name of the resource. Defaults to None.

        limit(int, Optional):
            The number of elements to be returned in the result. Defaults to None.

        task_status(str, Optional):
            taskStatus. Defaults to None.

        task_type(str, Optional):
            taskType. Defaults to None.

        resource_id(str, Optional):
            resourceId. Defaults to None.

        resource_type(str, Optional):
            resourceType. Defaults to None.

        completed_after(int, Optional):
            A time based filter to get tasks which are completed after the given timestamp. A task is completed if its status is 'Successsful' or 'Failed'. Time is in milliseconds. Defaults to None.

        page_number(int, Optional):
            Page number. Defaults to None.

        page_size(int, Optional):
            Size of the page you want to retrieve. Max page size allowed is 100. Defaults to None.

        order_direction(str, Optional):
            orderDirection. Defaults to None.

        order_by(str, Optional):
            orderBy. Defaults to None.

        task_name(str, Optional):
            Search filter when task name contains text. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:

        Resource State:

        .. code-block:: sls

            unmanaged_resources:
              exec.run:
                - path: vcf.tasks.list
                - kwargs:


        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.tasks.list

        Describe call from the CLI:

        .. code-block:: bash

            $ idem describe vcf.tasks

    """

    result = dict(comment=[], ret=[], result=True)

    # TODO: Change function methods params if needed
    list = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/tasks",
        query_params={
            "limit": limit,
            "taskStatus": task_status,
            "taskType": task_type,
            "resourceId": resource_id,
            "resourceType": resource_type,
            "completedAfter": completed_after,
            "pageNumber": page_number,
            "pageSize": page_size,
            "orderDirection": order_direction,
            "orderBy": order_by,
            "taskName": task_name,
        },
        data={},
        headers={},
    )

    if not list["result"]:
        result["comment"].append(list["comment"])
        result["result"] = False
        return result

    for resource in list["ret"]:

        # Convert raw response into present format
        resource_in_present_format = {
            # TODO: Make sure name, resource_id is mapped accordingly
            "name": "name",
            "resource_id": "resource_id",
        }
        resource_parameters = OrderedDict(
            {
                "completionTimestamp": "completion_timestamp",
                "creationTimestamp": "creation_timestamp",
                "errors": "errors",
                "id": "id",
                "isCancellable": "is_cancellable",
                "isRetryable": "is_retryable",
                "localizableDescriptionPack": "localizable_description_pack",
                "name": "name",
                "resolutionStatus": "resolution_status",
                "resources": "resources",
                "status": "status",
                "subTasks": "sub_tasks",
                "type": "type",
            }
        )

        for parameter_raw, parameter_present in resource_parameters.items():
            if parameter_raw in resource and resource.get(parameter_raw):
                resource_in_present_format[parameter_present] = resource.get(
                    parameter_raw
                )

        result["ret"].append(resource_in_present_format)

    return result


async def create(
    hub, ctx, id_: str, resource_id: str = None, name: str = None
) -> Dict[str, Any]:
    """
    Retry a Task
        Retry a failed Task by ID, if it exists

    Args:
        id_(str):
            Task id retry.

        resource_id(str, Optional):
            Tasks unique ID. Defaults to None.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.tasks.present:
                - id_: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.tasks.create id_=value
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {}

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    create = await hub.tool.vcf.session.request(
        ctx,
        method="patch",
        path="/v1/tasks/{id}",
        query_params={},
        data=payload,
        headers={},
    )

    if not create["result"]:
        result["comment"].append(create["comment"])
        result["result"] = False
        return result

    result["comment"].append(
        f"Created vcf.tasks '{name}'",
    )

    result["ret"] = create["ret"]
    # TODO: add "resource_id" to returned response by mapping to correct resource identifier
    return result


async def update(
    hub, ctx, resource_id: str, id_: str, name: str = None
) -> Dict[str, Any]:
    """
    Retry a Task
        Retry a failed Task by ID, if it exists

    Args:
        resource_id(str):
            Tasks unique ID.

        id_(str):
            Task id retry.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.tasks.present:
                - resource_id: value
                - id_: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.tasks.update resource_id=value, id_=value
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {}

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    if payload:
        update = await hub.tool.vcf.session.request(
            ctx,
            method="patch",
            path="/v1/tasks/{id}".format(**{"id": id_}),
            query_params={},
            data=payload,
            headers={},
        )

        if not update["result"]:
            result["comment"].append(update["comment"])
            result["result"] = False
            return result

        result["ret"] = update["ret"]
        result["comment"].append(
            f"Updated vcf.tasks '{name}'",
        )

    return result


async def delete(
    hub, ctx, resource_id: str, id_: str, name: str = None
) -> Dict[str, Any]:
    """
    Cancel a Task
        Cancel a Task by ID, if it exists

    Args:
        resource_id(str):
            Tasks unique ID.

        id_(str):
            Task id for cancelling.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            resource_is_absent:
              vcf.tasks.absent:
                - resource_id: value
                - id_: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.tasks.delete resource_id=value, id_=value
    """

    result = dict(comment=[], ret=[], result=True)

    delete = await hub.tool.vcf.session.request(
        ctx,
        method="delete",
        path="/v1/tasks/{id}".format(**{"id": id_}),
        query_params={},
        data={},
        headers={},
    )

    if not delete["result"]:
        result["comment"].append(delete["comment"])
        result["result"] = False
        return result

    result["comment"].append(f"Deleted '{name}'")
    return result

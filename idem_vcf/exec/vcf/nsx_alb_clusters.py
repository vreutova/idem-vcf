"""Exec module for managing Nsx Alb Clusterss. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List

__contracts__ = ["soft_fail"]

__func_alias__ = {"list_": "list"}


async def get(hub, ctx) -> Dict[str, Any]:
    """
    Retrieve an NSX ALB Cluster form factors
        None


    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            unmanaged_resource:
              exec.run:
                - path: vcf.nsx_alb_clusters.get
                - kwargs:


        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.nsx_alb_clusters.get
    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change function methods params if needed
    get = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/nsx-alb-clusters/form-factors".format(**{}),
        query_params={},
        data={},
        headers={},
    )

    if not get["result"]:
        # Send empty result for not found
        if get["status"] == 404:
            result["comment"].append(f"Get '{name}' result is empty")
            return result

        result["comment"].append(get["comment"])
        result["result"] = False
        return result

    # Case: Empty results
    if not get["ret"]:
        result["comment"].append(f"Get '{name}' result is empty")
        return result

    # Convert raw response into present format
    raw_resource = get["ret"]

    # TODO: Make sure resource_id is mapped in get response
    resource_in_present_format = {"name": name, "resource_id": resource_id}
    resource_parameters = OrderedDict({"formFactors": "form_factors"})

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def list_(hub, ctx, name: str = None, domain_id: str = None) -> Dict[str, Any]:
    """
    Retrieve a list of NSX ALB Clusters
        None

    Args:
        name(str, Optional):
            Idem name of the resource. Defaults to None.

        domain_id(str, Optional):
            Pass an optional domain ID to fetch ALB clusters associated with the workload domain. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:

        Resource State:

        .. code-block:: sls

            unmanaged_resources:
              exec.run:
                - path: vcf.nsx_alb_clusters.list
                - kwargs:


        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.nsx_alb_clusters.list

        Describe call from the CLI:

        .. code-block:: bash

            $ idem describe vcf.nsx_alb_clusters

    """

    result = dict(comment=[], ret=[], result=True)

    # TODO: Change function methods params if needed
    list = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/nsx-alb-clusters",
        query_params={"domainId": domain_id},
        data={},
        headers={},
    )

    if not list["result"]:
        result["comment"].append(list["comment"])
        result["result"] = False
        return result

    for resource in list["ret"]:

        # Convert raw response into present format
        resource_in_present_format = {
            # TODO: Make sure name, resource_id is mapped accordingly
            "name": "name",
            "resource_id": "resource_id",
        }
        resource_parameters = OrderedDict({"formFactors": "form_factors"})

        for parameter_raw, parameter_present in resource_parameters.items():
            if parameter_raw in resource and resource.get(parameter_raw):
                resource_in_present_format[parameter_present] = resource.get(
                    parameter_raw
                )

        result["ret"].append(resource_in_present_format)

    return result


async def create(
    hub,
    ctx,
    resource_id: str = None,
    name: str = None,
    domain_id: str = None,
    cluster_fqdn: str = None,
    cluster_ip_address: str = None,
    form_factor: str = None,
    admin_password: str = None,
    nodes: List[
        make_dataclass("nodes", [("ip_address", str, field(default=None))])
    ] = None,
    alb_bundle_id: str = None,
) -> Dict[str, Any]:
    """
    Create an NSX ALB Cluster
        None

    Args:
        resource_id(str, Optional):
            Nsx_alb_clusters unique ID. Defaults to None.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

        domain_id(str, Optional):
            Associated VCF Domain ID. Defaults to None.

        cluster_fqdn(str, Optional):
            NSX ALB Cluster's FQDN. Defaults to None.

        cluster_ip_address(str, Optional):
            NSX ALB Cluster's VIP. Defaults to None.

        form_factor(str, Optional):
            Size of NSX ALB cluster. Defaults to None.

        admin_password(str, Optional):
            Admin user password of NSX ALB cluster. Defaults to None.

        nodes(List[dict[str, Any]], Optional):
            NSX ALB nodes. Defaults to None.

            * ip_address (str, Optional):
                IPv4 address of NSX ALB Node. Defaults to None.

        alb_bundle_id(str, Optional):
            NSX ALB bundle id. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.nsx_alb_clusters.present:
                -

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.nsx_alb_clusters.create
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {
        "domain_id": "domainId",
        "name": "name",
        "cluster_fqdn": "clusterFqdn",
        "cluster_ip_address": "clusterIpAddress",
        "form_factor": "formFactor",
        "admin_password": "adminPassword",
        "nodes": "nodes",
        "alb_bundle_id": "albBundleId",
    }

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    create = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/nsx-alb-clusters",
        query_params={},
        data=payload,
        headers={},
    )

    if not create["result"]:
        result["comment"].append(create["comment"])
        result["result"] = False
        return result

    result["comment"].append(
        f"Created vcf.nsx_alb_clusters '{name}'",
    )

    result["ret"] = create["ret"]
    # TODO: add "resource_id" to returned response by mapping to correct resource identifier
    return result


async def update(hub, ctx) -> Dict[str, Any]:
    """

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.nsx_alb_clusters.present:
                -

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.nsx_alb_clusters.update
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {}

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    if payload:
        update = await hub.tool.vcf.session.request(
            ctx,
            method="TODO",
            path="TODO".format(**{}),
            query_params={},
            data=payload,
            headers={},
        )

        if not update["result"]:
            result["comment"].append(update["comment"])
            result["result"] = False
            return result

        result["ret"] = update["ret"]
        result["comment"].append(
            f"Updated vcf.nsx_alb_clusters '{name}'",
        )

    return result


async def delete(
    hub, ctx, resource_id: str, id_: str, name: str = None
) -> Dict[str, Any]:
    """
    Delete an NSX ALB Cluster by its ID
        Deletes NSX ALB Cluster

    Args:
        resource_id(str):
            Nsx_alb_clusters unique ID.

        id_(str):
            NSX ALB Cluster ID.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            resource_is_absent:
              vcf.nsx_alb_clusters.absent:
                - resource_id: value
                - id_: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.nsx_alb_clusters.delete resource_id=value, id_=value
    """

    result = dict(comment=[], ret=[], result=True)

    delete = await hub.tool.vcf.session.request(
        ctx,
        method="delete",
        path="/v1/nsx-alb-clusters/{id}".format(**{"id": id_}),
        query_params={},
        data={},
        headers={},
    )

    if not delete["result"]:
        result["comment"].append(delete["comment"])
        result["result"] = False
        return result

    result["comment"].append(f"Deleted '{name}'")
    return result

"""Exec module for managing Bundleses. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict

__contracts__ = ["soft_fail"]

__func_alias__ = {"list_": "list"}


async def get(hub, ctx, resource_id: str, id_: str, name: str = None) -> Dict[str, Any]:
    """
    Retrieve a bundle by its ID
        Get a Bundle

    Args:
        resource_id(str):
            Bundles unique ID.

        id_(str):
            Bundle ID.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            unmanaged_resource:
              exec.run:
                - path: vcf.bundles.get
                - kwargs:
                  resource_id: value
                  id_: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.bundles.get resource_id=value, id_=value
    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change function methods params if needed
    get = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/bundles/{id}".format(**{"id": id_}),
        query_params={},
        data={},
        headers={},
    )

    if not get["result"]:
        # Send empty result for not found
        if get["status"] == 404:
            result["comment"].append(f"Get '{name}' result is empty")
            return result

        result["comment"].append(get["comment"])
        result["result"] = False
        return result

    # Case: Empty results
    if not get["ret"]:
        result["comment"].append(f"Get '{name}' result is empty")
        return result

    # Convert raw response into present format
    raw_resource = get["ret"]

    # TODO: Make sure resource_id is mapped in get response
    resource_in_present_format = {"name": name, "resource_id": resource_id}
    resource_parameters = OrderedDict(
        {
            "applicabilityOrder": "applicability_order",
            "applicabilityStatus": "applicability_status",
            "components": "components",
            "description": "description",
            "downloadStatus": "download_status",
            "id": "id",
            "isCompliant": "is_compliant",
            "isCumulative": "is_cumulative",
            "isPartiallyUpgraded": "is_partially_upgraded",
            "releasedDate": "released_date",
            "severity": "severity",
            "sizeMB": "size_mb",
            "type": "type",
            "vendor": "vendor",
            "version": "version",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def list_(
    hub,
    ctx,
    name: str = None,
    product_type: str = None,
    is_compliant: bool = None,
    bundle_type: str = None,
) -> Dict[str, Any]:
    """
    Retrieve a list of bundles
        Get all Bundles i.e uploaded bundles and also bundles available via depot access.

    Args:
        name(str, Optional):
            Idem name of the resource. Defaults to None.

        product_type(str, Optional):
            The type of the product. Defaults to None.

        is_compliant(bool, Optional):
            Is compliant with the current VCF version. Defaults to None.

        bundle_type(str, Optional):
            The type of the bundle. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:

        Resource State:

        .. code-block:: sls

            unmanaged_resources:
              exec.run:
                - path: vcf.bundles.list
                - kwargs:


        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.bundles.list

        Describe call from the CLI:

        .. code-block:: bash

            $ idem describe vcf.bundles

    """

    result = dict(comment=[], ret=[], result=True)

    # TODO: Change function methods params if needed
    list = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/bundles",
        query_params={
            "productType": product_type,
            "isCompliant": is_compliant,
            "bundleType": bundle_type,
        },
        data={},
        headers={},
    )

    if not list["result"]:
        result["comment"].append(list["comment"])
        result["result"] = False
        return result

    for resource in list["ret"]:

        # Convert raw response into present format
        resource_in_present_format = {
            # TODO: Make sure name, resource_id is mapped accordingly
            "name": "name",
            "resource_id": "resource_id",
        }
        resource_parameters = OrderedDict(
            {
                "applicabilityOrder": "applicability_order",
                "applicabilityStatus": "applicability_status",
                "components": "components",
                "description": "description",
                "downloadStatus": "download_status",
                "id": "id",
                "isCompliant": "is_compliant",
                "isCumulative": "is_cumulative",
                "isPartiallyUpgraded": "is_partially_upgraded",
                "releasedDate": "released_date",
                "severity": "severity",
                "sizeMB": "size_mb",
                "type": "type",
                "vendor": "vendor",
                "version": "version",
            }
        )

        for parameter_raw, parameter_present in resource_parameters.items():
            if parameter_raw in resource and resource.get(parameter_raw):
                resource_in_present_format[parameter_present] = resource.get(
                    parameter_raw
                )

        result["ret"].append(resource_in_present_format)

    return result


async def create(
    hub,
    ctx,
    bundle_file_path: str,
    manifest_file_path: str,
    resource_id: str = None,
    name: str = None,
    compatibility_sets_file_path: str = None,
    partner_extension_spec: make_dataclass(
        "partner_extension_spec",
        [
            ("partner_bundle_metadata_file_path", str, field(default=None)),
            ("partner_bundle_version", str, field(default=None)),
        ],
    ) = None,
    signature_file_path: str = None,
) -> Dict[str, Any]:
    """
    Upload a bundle to SDDC Manager
        Upload Bundle to SDDC Manager. Used when you do not have internet connectivity for downloading bundles from VMWare/VxRail to SDDC Manager. The Bundles are manually downloaded from Depot using Bundle Transfer utility

    Args:
        bundle_file_path(str):
            Bundle Upload File Path.

        manifest_file_path(str):
            Bundle Upload Manifest File Path.

        resource_id(str, Optional):
            Bundles unique ID. Defaults to None.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

        compatibility_sets_file_path(str, Optional):
            [Deprecated] Path to the software compatibility sets file. Defaults to None.

        partner_extension_spec(dict[str, Any], Optional):
            partnerExtensionSpec. Defaults to None.

            * partner_bundle_metadata_file_path (str, Optional):
                Path to the bundle metadata file. The metadata file can have details of multiple bundles. Defaults to None.

            * partner_bundle_version (str, Optional):
                Version of partner bundle to be uploaded. Should match one of the bundle versions available in the partner bundle metadata file. Defaults to None.

        signature_file_path(str, Optional):
            Bundle Upload Signature File Path. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.bundles.present:
                - bundle_file_path: value
                - manifest_file_path: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.bundles.create bundle_file_path=value, manifest_file_path=value
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {
        "bundle_file_path": "bundleFilePath",
        "compatibility_sets_file_path": "compatibilitySetsFilePath",
        "manifest_file_path": "manifestFilePath",
        "partner_extension_spec": "partnerExtensionSpec",
        "signature_file_path": "signatureFilePath",
    }

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    create = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/bundles",
        query_params={},
        data=payload,
        headers={},
    )

    if not create["result"]:
        result["comment"].append(create["comment"])
        result["result"] = False
        return result

    result["comment"].append(
        f"Created vcf.bundles '{name}'",
    )

    result["ret"] = create["ret"]
    # TODO: add "resource_id" to returned response by mapping to correct resource identifier
    return result


async def update(
    hub,
    ctx,
    resource_id: str,
    id_: str,
    name: str = None,
    bundle_download_spec: make_dataclass(
        "bundle_download_spec",
        [
            ("download_now", bool, field(default=None)),
            ("scheduled_timestamp", str, field(default=None)),
        ],
    ) = None,
    compatibility_sets_file_path: str = None,
) -> Dict[str, Any]:
    """
    Start immediate download or schedule download of a bundle by ID
        Update a Bundle for scheduling/triggering download. Only one download can triggered for a Bundle.

    Args:
        resource_id(str):
            Bundles unique ID.

        id_(str):
            Bundle ID.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

        bundle_download_spec(dict[str, Any], Optional):
            bundleDownloadSpec. Defaults to None.

            * download_now (bool, Optional):
                Flag for enabling Download Now. If true, scheduledTimestamp is ignored. Defaults to None.

            * scheduled_timestamp (str, Optional):
                Bundle Download Scheduled Time. Defaults to None.

        compatibility_sets_file_path(str, Optional):
            [Deprecated] Path to the software compatibility sets file. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.bundles.present:
                - resource_id: value
                - id_: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.bundles.update resource_id=value, id_=value
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {
        "bundle_download_spec": "bundleDownloadSpec",
        "compatibility_sets_file_path": "compatibilitySetsFilePath",
    }

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    if payload:
        update = await hub.tool.vcf.session.request(
            ctx,
            method="patch",
            path="/v1/bundles/{id}".format(**{"id": id_}),
            query_params={},
            data=payload,
            headers={},
        )

        if not update["result"]:
            result["comment"].append(update["comment"])
            result["result"] = False
            return result

        result["ret"] = update["ret"]
        result["comment"].append(
            f"Updated vcf.bundles '{name}'",
        )

    return result


async def delete(hub, ctx) -> Dict[str, Any]:
    """

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            resource_is_absent:
              vcf.bundles.absent:
                -

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.bundles.delete
    """

    result = dict(comment=[], ret=[], result=True)

    delete = await hub.tool.vcf.session.request(
        ctx,
        method="TODO",
        path="TODO".format(**{}),
        query_params={},
        data={},
        headers={},
    )

    if not delete["result"]:
        result["comment"].append(delete["comment"])
        result["result"] = False
        return result

    result["comment"].append(f"Deleted '{name}'")
    return result

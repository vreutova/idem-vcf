"""Exec module for managing Upgradeses. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List

__contracts__ = ["soft_fail"]

__func_alias__ = {"list_": "list"}


async def get(
    hub, ctx, resource_id: str, upgrade_id: str, name: str = None
) -> Dict[str, Any]:
    """
    Retrieve an upgrade by ID
        Retrieve an upgrade by ID

    Args:
        resource_id(str):
            Upgrades unique ID.

        upgrade_id(str):
            upgradeId.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            unmanaged_resource:
              exec.run:
                - path: vcf.upgrades.get
                - kwargs:
                  resource_id: value
                  upgrade_id: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.upgrades.get resource_id=value, upgrade_id=value
    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change function methods params if needed
    get = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/upgrades/{upgradeId}".format(**{"upgradeId": upgrade_id}),
        query_params={},
        data={},
        headers={},
    )

    if not get["result"]:
        # Send empty result for not found
        if get["status"] == 404:
            result["comment"].append(f"Get '{name}' result is empty")
            return result

        result["comment"].append(get["comment"])
        result["result"] = False
        return result

    # Case: Empty results
    if not get["ret"]:
        result["comment"].append(f"Get '{name}' result is empty")
        return result

    # Convert raw response into present format
    raw_resource = get["ret"]

    # TODO: Make sure resource_id is mapped in get response
    resource_in_present_format = {"name": name, "resource_id": resource_id}
    resource_parameters = OrderedDict(
        {
            "bundleId": "bundle_id",
            "id": "id",
            "nsxtUpgradeUserInputSpec": "nsxt_upgrade_user_input_spec",
            "parallelUpgrade": "parallel_upgrade",
            "resourceType": "resource_type",
            "resourceUpgradeSpecs": "resource_upgrade_specs",
            "status": "status",
            "taskId": "task_id",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def list_(
    hub, ctx, name: str = None, status: str = None, bundle_id: str = None
) -> Dict[str, Any]:
    """
    Retrieve a list of upgrades
        Retrieve a list of upgrades

    Args:
        name(str, Optional):
            Idem name of the resource. Defaults to None.

        status(str, Optional):
            Status of the upgrades you want to retrieve. Defaults to None.

        bundle_id(str, Optional):
            Bundle Id for the upgrade. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:

        Resource State:

        .. code-block:: sls

            unmanaged_resources:
              exec.run:
                - path: vcf.upgrades.list
                - kwargs:


        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.upgrades.list

        Describe call from the CLI:

        .. code-block:: bash

            $ idem describe vcf.upgrades

    """

    result = dict(comment=[], ret=[], result=True)

    # TODO: Change function methods params if needed
    list = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/upgrades",
        query_params={"status": status, "bundleId": bundle_id},
        data={},
        headers={},
    )

    if not list["result"]:
        result["comment"].append(list["comment"])
        result["result"] = False
        return result

    for resource in list["ret"]:

        # Convert raw response into present format
        resource_in_present_format = {
            # TODO: Make sure name, resource_id is mapped accordingly
            "name": "name",
            "resource_id": "resource_id",
        }
        resource_parameters = OrderedDict(
            {
                "bundleId": "bundle_id",
                "id": "id",
                "nsxtUpgradeUserInputSpec": "nsxt_upgrade_user_input_spec",
                "parallelUpgrade": "parallel_upgrade",
                "resourceType": "resource_type",
                "resourceUpgradeSpecs": "resource_upgrade_specs",
                "status": "status",
                "taskId": "task_id",
            }
        )

        for parameter_raw, parameter_present in resource_parameters.items():
            if parameter_raw in resource and resource.get(parameter_raw):
                resource_in_present_format[parameter_present] = resource.get(
                    parameter_raw
                )

        result["ret"].append(resource_in_present_format)

    return result


async def create(
    hub,
    ctx,
    bundle_id: str,
    resource_type: str,
    resource_upgrade_specs: List[
        make_dataclass(
            "resource_upgrade_specs",
            [
                ("resource_id", str),
                (
                    "custom_iso_spec",
                    make_dataclass(
                        "custom_iso_spec",
                        [("id", str), ("host_ids", List[str], field(default=None))],
                    ),
                    field(default=None),
                ),
                ("enable_quickboot", bool, field(default=None)),
                ("evacuate_offline_vms", bool, field(default=None)),
                (
                    "esx_upgrade_options_spec",
                    make_dataclass(
                        "esx_upgrade_options_spec",
                        [
                            ("enable_quickboot", bool, field(default=None)),
                            ("evacuate_offline_vms", bool, field(default=None)),
                            ("disable_hac", bool, field(default=None)),
                            ("disable_dpm", bool, field(default=None)),
                            ("enforce_hcl_validation", bool, field(default=None)),
                            ("pre_remediation_power_action", str, field(default=None)),
                            (
                                "esx_upgrade_failure_action",
                                make_dataclass(
                                    "esx_upgrade_failure_action",
                                    [
                                        ("retry_delay", int, field(default=None)),
                                        ("retry_count", int, field(default=None)),
                                        ("action", str, field(default=None)),
                                    ],
                                ),
                                field(default=None),
                            ),
                        ],
                    ),
                    field(default=None),
                ),
                ("hosts_to_upgrade", List[str], field(default=None)),
                (
                    "personality_spec",
                    make_dataclass(
                        "personality_spec",
                        [
                            ("personality_id", str),
                            (
                                "hardware_support_specs",
                                List[
                                    make_dataclass(
                                        "hardware_support_specs",
                                        [
                                            ("name", str),
                                            (
                                                "package_spec",
                                                make_dataclass(
                                                    "package_spec",
                                                    [("name", str), ("version", str)],
                                                ),
                                            ),
                                        ],
                                    )
                                ],
                                field(default=None),
                            ),
                        ],
                    ),
                    field(default=None),
                ),
                ("scheduled_timestamp", str, field(default=None)),
                ("shutdown_vms", bool, field(default=None)),
                ("to_version", str, field(default=None)),
                ("upgrade_now", bool, field(default=None)),
            ],
        )
    ],
    resource_id: str = None,
    name: str = None,
    draft_mode: bool = None,
    nsxt_upgrade_user_input_specs: List[
        make_dataclass(
            "nsxt_upgrade_user_input_specs",
            [
                (
                    "nsxt_edge_cluster_upgrade_specs",
                    List[
                        make_dataclass(
                            "nsxt_edge_cluster_upgrade_specs",
                            [
                                ("edge_cluster_id", str),
                                ("edge_parallel_upgrade", bool, field(default=None)),
                            ],
                        )
                    ],
                    field(default=None),
                ),
                (
                    "nsxt_host_cluster_upgrade_specs",
                    List[
                        make_dataclass(
                            "nsxt_host_cluster_upgrade_specs",
                            [
                                ("host_cluster_id", str),
                                ("host_parallel_upgrade", bool, field(default=None)),
                                ("live_upgrade", bool, field(default=None)),
                            ],
                        )
                    ],
                    field(default=None),
                ),
                ("nsxt_id", str, field(default=None)),
                (
                    "nsxt_upgrade_options",
                    make_dataclass(
                        "nsxt_upgrade_options",
                        [
                            (
                                "is_edge_clusters_upgrade_parallel",
                                bool,
                                field(default=None),
                            ),
                            ("is_edge_only_upgrade", bool, field(default=None)),
                            (
                                "is_host_clusters_upgrade_parallel",
                                bool,
                                field(default=None),
                            ),
                        ],
                    ),
                    field(default=None),
                ),
            ],
        )
    ] = None,
    parallel_upgrade: bool = None,
    vcenter_upgrade_user_input_specs: List[
        make_dataclass(
            "vcenter_upgrade_user_input_specs",
            [
                (
                    "temporary_network",
                    make_dataclass(
                        "temporary_network",
                        [("gateway", str), ("ip_address", str), ("subnet_mask", str)],
                    ),
                )
            ],
        )
    ] = None,
) -> Dict[str, Any]:
    """
    Start an upgrade operation
        Schedule/Trigger Upgrade of a Resource. Ex: Resource can be DOMAIN, CLUSTER, UNMANAGED_HOST etc. Performing upgrades are supported on VMware Cloud Foundation 3.5 BOM resources and above.

    Args:
        bundle_id(str):
            Bundle ID for Upgrade.

        resource_type(str):
            Resource Type for Upgrade.

        resource_upgrade_specs(List[dict[str, Any]]):
            Resource Upgrade Specifications.

            * custom_iso_spec (dict[str, Any], Optional):
                customISOSpec. Defaults to None.

                * host_ids (List[str], Optional):
                    List of host IDs. Defaults to None.

                * id (str):
                    Custom ISO Id for VUM Clusters Upgrade.

            * enable_quickboot (bool, Optional):
                [Deprecated] (Please use esxUpgradeOptionsSpec instead) Flag for requesting Quick Boot for ESXi upgrade. Defaults to None.

            * evacuate_offline_vms (bool, Optional):
                [Deprecated] (Please use esxUpgradeOptionsSpec instead) Flag for requesting Evacuation of Offline VMs for ESXi upgrade. Defaults to None.

            * esx_upgrade_options_spec (dict[str, Any], Optional):
                esxUpgradeOptionsSpec. Defaults to None.

                * enable_quickboot (bool, Optional):
                    Flag for requesting Quick Boot for ESXi upgrade. Defaults to None.

                * evacuate_offline_vms (bool, Optional):
                    Flag for requesting Evacuation of Offline VMs for ESXi upgrade. Defaults to None.

                * disable_hac (bool, Optional):
                    Flag for disabling HA admission control on the cluster for ESXi upgrade. This option is only applicable to vLCM based ESXi upgrade. Defaults to None.

                * disable_dpm (bool, Optional):
                    Flag for disabling DPM on the cluster for ESXi upgrade. This option is only applicable to vLCM based ESXi upgrade. Defaults to None.

                * enforce_hcl_validation (bool, Optional):
                    Flag for enforcing HCL validation for ESXi upgrade, when applicable, to prevent remediation if hardware compatibility issues are found. This option is only applicable to vLCM based ESXi upgrade. Defaults to None.

                * pre_remediation_power_action (str, Optional):
                    Flag for setting VM power state before entering maintenance mode during ESXi upgrade. This option is only applicable to vLCM based ESXi upgrade. Defaults to None.

                * esx_upgrade_failure_action (dict[str, Any], Optional):
                    esxUpgradeFailureAction. Defaults to None.

                    * retry_delay (int, Optional):
                        Time to wait before we retry the failed operation in seconds. If action is RETRY, the value should be between 300 and 3600 seconds. If action is FAIL, the value should not be set. Defaults to None.

                    * retry_count (int, Optional):
                        Number of times to retry the failed operation. If the action is RETRY, the value should be between 3 and 100. If the action is FAIL, the value should be 0. Defaults to None.

                    * action (str, Optional):
                        Action to be taken if entering maintenance mode fails on a host within the cluster ESXi upgrade. Defaults to None.

            * hosts_to_upgrade (List[str], Optional):
                Set of hostIDs to be upgraded for ESXi upgrade. Defaults to None.

            * personality_spec (dict[str, Any], Optional):
                personalitySpec. Defaults to None.

                * hardware_support_specs (List[dict[str, Any]], Optional):
                    Hardware Support Specifications for Firmware upgrade. Defaults to None.

                    * name (str):
                        Hardware Support Manager name.

                    * package_spec (dict[str, Any]):
                        packageSpec.

                        * name (str):
                            Package name.

                        * version (str):
                            Package version.

                * personality_id (str):
                    Personality ID for vLCM based Upgrade.

            * resource_id (str):
                Resource ID for Upgrade.

            * scheduled_timestamp (str, Optional):
                Upgrade Scheduled Time. Defaults to None.

            * shutdown_vms (bool, Optional):
                If Resource Type is UNASSIGNED_HOST, set flag for enabling shutting down VM's before Upgrade. Defaults to None.

            * to_version (str, Optional):
                If Resource Type is UNASSIGNED_HOST, set the target version for Upgrade. Defaults to None.

            * upgrade_now (bool, Optional):
                Flag for enabling Upgrade Now. If true, scheduledTimestamp is ignored. Defaults to None.

        resource_id(str, Optional):
            Upgrades unique ID. Defaults to None.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

        draft_mode(bool, Optional):
            Boolean to represent upgrade will be created in DRAFT mode. This allows to run prechecks before user confirm/commit the upgrade. Defaults to None.

        nsxt_upgrade_user_input_specs(List[dict[str, Any]], Optional):
            Resource Upgrade Specifications for NSX upgrade. Defaults to None.

            * nsxt_edge_cluster_upgrade_specs (List[dict[str, Any]], Optional):
                List of edge clusters information if explicit selection is to be made. Defaults to None.

                * edge_cluster_id (str):
                    Resource ID of the edge transport node cluster.

                * edge_parallel_upgrade (bool, Optional):
                    disable/enable parallel upgrade of edges within the cluster. Defaults to None.

            * nsxt_host_cluster_upgrade_specs (List[dict[str, Any]], Optional):
                List of host clusters information if explicit selection is to be made. Defaults to None.

                * host_cluster_id (str):
                    Resource ID of the host transport node cluster.

                * host_parallel_upgrade (bool, Optional):
                    Flag for disabling/enabling parallel upgrade within the cluster. Defaults to None.

                * live_upgrade (bool, Optional):
                    Flag for disabling/enabling live upgrade of hosts in the transportnode clusters. Defaults to None.

            * nsxt_id (str, Optional):
                Identifier of the NSX instance. Defaults to None.

            * nsxt_upgrade_options (dict[str, Any], Optional):
                nsxtUpgradeOptions. Defaults to None.

                * is_edge_clusters_upgrade_parallel (bool, Optional):
                    Flag for disabling/enabling parallel upgrade of edge transportnode clusters. Defaults to None.

                * is_edge_only_upgrade (bool, Optional):
                    Flag for performing edge-only upgrade. Defaults to None.

                * is_host_clusters_upgrade_parallel (bool, Optional):
                    Flag for disabling/enabling parallel upgrade of host transportnode clusters. Defaults to None.

        parallel_upgrade(bool, Optional):
            Boolean to represent components will be upgraded in parallel on not. Defaults to None.

        vcenter_upgrade_user_input_specs(List[dict[str, Any]], Optional):
            User Input for vCenter upgrade. Defaults to None.

            * temporary_network (dict[str, Any]):
                temporaryNetwork.

                * gateway (str):
                    Gateway for vCenter Upgrade temporary network.

                * ip_address (str):
                    IP Address for vCenter Upgrade temporary network.

                * subnet_mask (str):
                    Subnet Mask for vCenter Upgrade temporary network.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.upgrades.present:
                - bundle_id: value
                - resource_type: value
                - resource_upgrade_specs: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.upgrades.create bundle_id=value, resource_type=value, resource_upgrade_specs=value
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {
        "bundle_id": "bundleId",
        "draft_mode": "draftMode",
        "nsxt_upgrade_user_input_specs": "nsxtUpgradeUserInputSpecs",
        "parallel_upgrade": "parallelUpgrade",
        "resource_type": "resourceType",
        "resource_upgrade_specs": "resourceUpgradeSpecs",
        "vcenter_upgrade_user_input_specs": "vcenterUpgradeUserInputSpecs",
    }

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    create = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/upgrades",
        query_params={},
        data=payload,
        headers={},
    )

    if not create["result"]:
        result["comment"].append(create["comment"])
        result["result"] = False
        return result

    result["comment"].append(
        f"Created vcf.upgrades '{name}'",
    )

    result["ret"] = create["ret"]
    # TODO: add "resource_id" to returned response by mapping to correct resource identifier
    return result


async def update(
    hub,
    ctx,
    resource_id: str,
    upgrade_id: str,
    name: str = None,
    scheduled_timestamp: str = None,
    upgrade_now: bool = None,
) -> Dict[str, Any]:
    """
    Change a DRAFT upgrade to a SCHEDULED state
        Commit/Reschedule an existing upgrade. It moves the upgrade from DRAFT state to SCHEDULED state and/or changes the upgrade scheduled date/time.

    Args:
        resource_id(str):
            Upgrades unique ID.

        upgrade_id(str):
            upgradeId.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

        scheduled_timestamp(str, Optional):
            Upgrade Scheduled Time. Defaults to None.

        upgrade_now(bool, Optional):
            Flag for enabling Upgrade Now. If true, scheduledTimestamp is ignored. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.upgrades.present:
                - resource_id: value
                - upgrade_id: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.upgrades.update resource_id=value, upgrade_id=value
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {
        "scheduled_timestamp": "scheduledTimestamp",
        "upgrade_now": "upgradeNow",
    }

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    if payload:
        update = await hub.tool.vcf.session.request(
            ctx,
            method="patch",
            path="/v1/upgrades/{upgradeId}".format(**{"upgradeId": upgrade_id}),
            query_params={},
            data=payload,
            headers={},
        )

        if not update["result"]:
            result["comment"].append(update["comment"])
            result["result"] = False
            return result

        result["ret"] = update["ret"]
        result["comment"].append(
            f"Updated vcf.upgrades '{name}'",
        )

    return result


async def delete(hub, ctx) -> Dict[str, Any]:
    """

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            resource_is_absent:
              vcf.upgrades.absent:
                -

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.upgrades.delete
    """

    result = dict(comment=[], ret=[], result=True)

    delete = await hub.tool.vcf.session.request(
        ctx,
        method="TODO",
        path="TODO".format(**{}),
        query_params={},
        data={},
        headers={},
    )

    if not delete["result"]:
        result["comment"].append(delete["comment"])
        result["result"] = False
        return result

    result["comment"].append(f"Deleted '{name}'")
    return result

"""Exec module for managing Clusterss. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List

__contracts__ = ["soft_fail"]

__func_alias__ = {"list_": "list"}


async def get(hub, ctx, resource_id: str, id_: str, name: str = None) -> Dict[str, Any]:
    """
    Get a cluster by its ID
        None

    Args:
        resource_id(str):
            Clusters unique ID.

        id_(str):
            Cluster ID.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            unmanaged_resource:
              exec.run:
                - path: vcf.clusters.get
                - kwargs:
                  resource_id: value
                  id_: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.clusters.get resource_id=value, id_=value
    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change function methods params if needed
    get = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/clusters/{id}".format(**{"id": id_}),
        query_params={},
        data={},
        headers={},
    )

    if not get["result"]:
        # Send empty result for not found
        if get["status"] == 404:
            result["comment"].append(f"Get '{name}' result is empty")
            return result

        result["comment"].append(get["comment"])
        result["result"] = False
        return result

    # Case: Empty results
    if not get["ret"]:
        result["comment"].append(f"Get '{name}' result is empty")
        return result

    # Convert raw response into present format
    raw_resource = get["ret"]

    # TODO: Make sure resource_id is mapped in get response
    resource_in_present_format = {"name": name, "resource_id": resource_id}
    resource_parameters = OrderedDict(
        {
            "capacity": "capacity",
            "hosts": "hosts",
            "id": "id",
            "isDefault": "is_default",
            "isImageBased": "is_image_based",
            "isStretched": "is_stretched",
            "name": "name",
            "primaryDatastoreName": "primary_datastore_name",
            "primaryDatastoreType": "primary_datastore_type",
            "tags": "tags",
            "vdsSpecs": "vds_specs",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def list_(
    hub, ctx, name: str = None, is_stretched: bool = None
) -> Dict[str, Any]:
    """
    Retrieve a list of clusters
        None

    Args:
        name(str, Optional):
            Idem name of the resource. Defaults to None.

        is_stretched(bool, Optional):
            isStretched. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:

        Resource State:

        .. code-block:: sls

            unmanaged_resources:
              exec.run:
                - path: vcf.clusters.list
                - kwargs:


        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.clusters.list

        Describe call from the CLI:

        .. code-block:: bash

            $ idem describe vcf.clusters

    """

    result = dict(comment=[], ret=[], result=True)

    # TODO: Change function methods params if needed
    list = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/clusters",
        query_params={"isStretched": is_stretched},
        data={},
        headers={},
    )

    if not list["result"]:
        result["comment"].append(list["comment"])
        result["result"] = False
        return result

    for resource in list["ret"]:

        # Convert raw response into present format
        resource_in_present_format = {
            # TODO: Make sure name, resource_id is mapped accordingly
            "name": "name",
            "resource_id": "resource_id",
        }
        resource_parameters = OrderedDict(
            {
                "capacity": "capacity",
                "hosts": "hosts",
                "id": "id",
                "isDefault": "is_default",
                "isImageBased": "is_image_based",
                "isStretched": "is_stretched",
                "name": "name",
                "primaryDatastoreName": "primary_datastore_name",
                "primaryDatastoreType": "primary_datastore_type",
                "tags": "tags",
                "vdsSpecs": "vds_specs",
            }
        )

        for parameter_raw, parameter_present in resource_parameters.items():
            if parameter_raw in resource and resource.get(parameter_raw):
                resource_in_present_format[parameter_present] = resource.get(
                    parameter_raw
                )

        result["ret"].append(resource_in_present_format)

    return result


async def create(
    hub,
    ctx,
    compute_spec: make_dataclass(
        "compute_spec",
        [
            (
                "cluster_specs",
                List[
                    make_dataclass(
                        "cluster_specs",
                        [
                            (
                                "datastore_spec",
                                make_dataclass(
                                    "datastore_spec",
                                    [
                                        (
                                            "nfs_datastore_specs",
                                            List[
                                                make_dataclass(
                                                    "nfs_datastore_specs",
                                                    [
                                                        ("datastore_name", str),
                                                        (
                                                            "nas_volume",
                                                            make_dataclass(
                                                                "nas_volume",
                                                                [
                                                                    ("path", str),
                                                                    ("read_only", bool),
                                                                    (
                                                                        "server_name",
                                                                        List[str],
                                                                    ),
                                                                    (
                                                                        "user_tag",
                                                                        str,
                                                                        field(
                                                                            default=None
                                                                        ),
                                                                    ),
                                                                ],
                                                            ),
                                                            field(default=None),
                                                        ),
                                                    ],
                                                )
                                            ],
                                            field(default=None),
                                        ),
                                        (
                                            "vmfs_datastore_spec",
                                            make_dataclass(
                                                "vmfs_datastore_spec",
                                                [
                                                    (
                                                        "fc_spec",
                                                        List[
                                                            make_dataclass(
                                                                "fc_spec",
                                                                [
                                                                    (
                                                                        "datastore_name",
                                                                        str,
                                                                    )
                                                                ],
                                                            )
                                                        ],
                                                        field(default=None),
                                                    )
                                                ],
                                            ),
                                            field(default=None),
                                        ),
                                        (
                                            "vsan_datastore_spec",
                                            make_dataclass(
                                                "vsan_datastore_spec",
                                                [
                                                    ("datastore_name", str),
                                                    (
                                                        "dedup_and_compression_enabled",
                                                        bool,
                                                        field(default=None),
                                                    ),
                                                    (
                                                        "esa_config",
                                                        make_dataclass(
                                                            "esa_config",
                                                            [("enabled", bool)],
                                                        ),
                                                        field(default=None),
                                                    ),
                                                    (
                                                        "failures_to_tolerate",
                                                        int,
                                                        field(default=None),
                                                    ),
                                                    (
                                                        "license_key",
                                                        str,
                                                        field(default=None),
                                                    ),
                                                ],
                                            ),
                                            field(default=None),
                                        ),
                                        (
                                            "vsan_remote_datastore_cluster_spec",
                                            make_dataclass(
                                                "vsan_remote_datastore_cluster_spec",
                                                [
                                                    (
                                                        "vsan_remote_datastore_spec",
                                                        List[
                                                            make_dataclass(
                                                                "vsan_remote_datastore_spec",
                                                                [
                                                                    (
                                                                        "datastore_uuid",
                                                                        str,
                                                                    )
                                                                ],
                                                            )
                                                        ],
                                                        field(default=None),
                                                    )
                                                ],
                                            ),
                                            field(default=None),
                                        ),
                                        (
                                            "vvol_datastore_specs",
                                            List[
                                                make_dataclass(
                                                    "vvol_datastore_specs",
                                                    [
                                                        ("name", str),
                                                        (
                                                            "vasa_provider_spec",
                                                            make_dataclass(
                                                                "vasa_provider_spec",
                                                                [
                                                                    (
                                                                        "storage_container_id",
                                                                        str,
                                                                    ),
                                                                    (
                                                                        "storage_protocol_type",
                                                                        str,
                                                                    ),
                                                                    ("user_id", str),
                                                                    (
                                                                        "vasa_provider_id",
                                                                        str,
                                                                    ),
                                                                ],
                                                            ),
                                                        ),
                                                    ],
                                                )
                                            ],
                                            field(default=None),
                                        ),
                                    ],
                                ),
                            ),
                            (
                                "host_specs",
                                List[
                                    make_dataclass(
                                        "host_specs",
                                        [
                                            ("id", str),
                                            ("az_name", str, field(default=None)),
                                            ("host_name", str, field(default=None)),
                                            (
                                                "host_network_spec",
                                                make_dataclass(
                                                    "host_network_spec",
                                                    [
                                                        (
                                                            "network_profile_name",
                                                            str,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "vm_nics",
                                                            List[
                                                                make_dataclass(
                                                                    "vm_nics",
                                                                    [
                                                                        (
                                                                            "id",
                                                                            str,
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                        (
                                                                            "move_to_nvds",
                                                                            bool,
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                        (
                                                                            "uplink",
                                                                            str,
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                        (
                                                                            "vds_name",
                                                                            str,
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                    ],
                                                                )
                                                            ],
                                                            field(default=None),
                                                        ),
                                                    ],
                                                ),
                                                field(default=None),
                                            ),
                                            ("ip_address", str, field(default=None)),
                                            ("license_key", str, field(default=None)),
                                            ("password", str, field(default=None)),
                                            ("serial_number", str, field(default=None)),
                                            (
                                                "ssh_thumbprint",
                                                str,
                                                field(default=None),
                                            ),
                                            ("username", str, field(default=None)),
                                        ],
                                    )
                                ],
                            ),
                            ("name", str),
                            (
                                "network_spec",
                                make_dataclass(
                                    "network_spec",
                                    [
                                        (
                                            "nsx_cluster_spec",
                                            make_dataclass(
                                                "nsx_cluster_spec",
                                                [
                                                    (
                                                        "nsx_t_cluster_spec",
                                                        make_dataclass(
                                                            "nsx_t_cluster_spec",
                                                            [
                                                                (
                                                                    "geneve_vlan_id",
                                                                    int,
                                                                    field(default=None),
                                                                ),
                                                                (
                                                                    "ip_address_pool_spec",
                                                                    make_dataclass(
                                                                        "ip_address_pool_spec",
                                                                        [
                                                                            (
                                                                                "name",
                                                                                str,
                                                                            ),
                                                                            (
                                                                                "description",
                                                                                str,
                                                                                field(
                                                                                    default=None
                                                                                ),
                                                                            ),
                                                                            (
                                                                                "ignore_unavailable_nsxt_cluster",
                                                                                bool,
                                                                                field(
                                                                                    default=None
                                                                                ),
                                                                            ),
                                                                            (
                                                                                "subnets",
                                                                                List[
                                                                                    make_dataclass(
                                                                                        "subnets",
                                                                                        [
                                                                                            (
                                                                                                "cidr",
                                                                                                str,
                                                                                            ),
                                                                                            (
                                                                                                "gateway",
                                                                                                str,
                                                                                            ),
                                                                                            (
                                                                                                "ip_address_pool_ranges",
                                                                                                List[
                                                                                                    make_dataclass(
                                                                                                        "ip_address_pool_ranges",
                                                                                                        [
                                                                                                            (
                                                                                                                "end",
                                                                                                                str,
                                                                                                            ),
                                                                                                            (
                                                                                                                "start",
                                                                                                                str,
                                                                                                            ),
                                                                                                        ],
                                                                                                    )
                                                                                                ],
                                                                                            ),
                                                                                        ],
                                                                                    )
                                                                                ],
                                                                                field(
                                                                                    default=None
                                                                                ),
                                                                            ),
                                                                        ],
                                                                    ),
                                                                    field(default=None),
                                                                ),
                                                                (
                                                                    "ip_address_pools_spec",
                                                                    List[
                                                                        make_dataclass(
                                                                            "ip_address_pools_spec",
                                                                            [
                                                                                (
                                                                                    "name",
                                                                                    str,
                                                                                ),
                                                                                (
                                                                                    "description",
                                                                                    str,
                                                                                    field(
                                                                                        default=None
                                                                                    ),
                                                                                ),
                                                                                (
                                                                                    "ignore_unavailable_nsxt_cluster",
                                                                                    bool,
                                                                                    field(
                                                                                        default=None
                                                                                    ),
                                                                                ),
                                                                                (
                                                                                    "subnets",
                                                                                    List[
                                                                                        make_dataclass(
                                                                                            "subnets",
                                                                                            [
                                                                                                (
                                                                                                    "cidr",
                                                                                                    str,
                                                                                                ),
                                                                                                (
                                                                                                    "gateway",
                                                                                                    str,
                                                                                                ),
                                                                                                (
                                                                                                    "ip_address_pool_ranges",
                                                                                                    List[
                                                                                                        make_dataclass(
                                                                                                            "ip_address_pool_ranges",
                                                                                                            [
                                                                                                                (
                                                                                                                    "end",
                                                                                                                    str,
                                                                                                                ),
                                                                                                                (
                                                                                                                    "start",
                                                                                                                    str,
                                                                                                                ),
                                                                                                            ],
                                                                                                        )
                                                                                                    ],
                                                                                                ),
                                                                                            ],
                                                                                        )
                                                                                    ],
                                                                                    field(
                                                                                        default=None
                                                                                    ),
                                                                                ),
                                                                            ],
                                                                        )
                                                                    ],
                                                                    field(default=None),
                                                                ),
                                                                (
                                                                    "uplink_profiles",
                                                                    List[
                                                                        make_dataclass(
                                                                            "uplink_profiles",
                                                                            [
                                                                                (
                                                                                    "name",
                                                                                    str,
                                                                                    field(
                                                                                        default=None
                                                                                    ),
                                                                                ),
                                                                                (
                                                                                    "supported_teaming_policies",
                                                                                    Dict,
                                                                                    field(
                                                                                        default=None
                                                                                    ),
                                                                                ),
                                                                                (
                                                                                    "teamings",
                                                                                    List[
                                                                                        make_dataclass(
                                                                                            "teamings",
                                                                                            [
                                                                                                (
                                                                                                    "active_uplinks",
                                                                                                    List[
                                                                                                        str
                                                                                                    ],
                                                                                                    field(
                                                                                                        default=None
                                                                                                    ),
                                                                                                ),
                                                                                                (
                                                                                                    "policy",
                                                                                                    str,
                                                                                                    field(
                                                                                                        default=None
                                                                                                    ),
                                                                                                ),
                                                                                                (
                                                                                                    "stand_by_uplinks",
                                                                                                    List[
                                                                                                        str
                                                                                                    ],
                                                                                                    field(
                                                                                                        default=None
                                                                                                    ),
                                                                                                ),
                                                                                            ],
                                                                                        )
                                                                                    ],
                                                                                    field(
                                                                                        default=None
                                                                                    ),
                                                                                ),
                                                                                (
                                                                                    "transport_vlan",
                                                                                    int,
                                                                                    field(
                                                                                        default=None
                                                                                    ),
                                                                                ),
                                                                            ],
                                                                        )
                                                                    ],
                                                                    field(default=None),
                                                                ),
                                                            ],
                                                        ),
                                                        field(default=None),
                                                    )
                                                ],
                                            ),
                                        ),
                                        (
                                            "vds_specs",
                                            List[
                                                make_dataclass(
                                                    "vds_specs",
                                                    [
                                                        ("name", str),
                                                        (
                                                            "is_used_by_nsxt",
                                                            bool,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "nsxt_switch_config",
                                                            make_dataclass(
                                                                "nsxt_switch_config",
                                                                [
                                                                    (
                                                                        "transport_zones",
                                                                        List[
                                                                            make_dataclass(
                                                                                "transport_zones",
                                                                                [
                                                                                    (
                                                                                        "transport_type",
                                                                                        str,
                                                                                    ),
                                                                                    (
                                                                                        "name",
                                                                                        str,
                                                                                        field(
                                                                                            default=None
                                                                                        ),
                                                                                    ),
                                                                                ],
                                                                            )
                                                                        ],
                                                                        field(
                                                                            default=None
                                                                        ),
                                                                    ),
                                                                    (
                                                                        "host_switch_operational_mode",
                                                                        str,
                                                                        field(
                                                                            default=None
                                                                        ),
                                                                    ),
                                                                ],
                                                            ),
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "mtu",
                                                            int,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "nioc_bandwidth_allocation_specs",
                                                            List[
                                                                make_dataclass(
                                                                    "nioc_bandwidth_allocation_specs",
                                                                    [
                                                                        ("type", str),
                                                                        (
                                                                            "nioc_traffic_resource_allocation",
                                                                            make_dataclass(
                                                                                "nioc_traffic_resource_allocation",
                                                                                [
                                                                                    (
                                                                                        "limit",
                                                                                        int,
                                                                                    ),
                                                                                    (
                                                                                        "reservation",
                                                                                        int,
                                                                                    ),
                                                                                    (
                                                                                        "shares_info",
                                                                                        make_dataclass(
                                                                                            "shares_info",
                                                                                            [
                                                                                                (
                                                                                                    "level",
                                                                                                    str,
                                                                                                    field(
                                                                                                        default=None
                                                                                                    ),
                                                                                                ),
                                                                                                (
                                                                                                    "shares",
                                                                                                    int,
                                                                                                    field(
                                                                                                        default=None
                                                                                                    ),
                                                                                                ),
                                                                                            ],
                                                                                        ),
                                                                                    ),
                                                                                ],
                                                                            ),
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                    ],
                                                                )
                                                            ],
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "port_group_specs",
                                                            List[
                                                                make_dataclass(
                                                                    "port_group_specs",
                                                                    [
                                                                        ("name", str),
                                                                        (
                                                                            "transport_type",
                                                                            str,
                                                                        ),
                                                                        (
                                                                            "active_uplinks",
                                                                            List[str],
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                        (
                                                                            "stand_by_uplinks",
                                                                            List[str],
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                        (
                                                                            "teaming_policy",
                                                                            str,
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                    ],
                                                                )
                                                            ],
                                                            field(default=None),
                                                        ),
                                                    ],
                                                )
                                            ],
                                        ),
                                        (
                                            "network_profiles",
                                            List[
                                                make_dataclass(
                                                    "network_profiles",
                                                    [
                                                        ("name", str),
                                                        (
                                                            "nsxt_host_switch_configs",
                                                            List[
                                                                make_dataclass(
                                                                    "nsxt_host_switch_configs",
                                                                    [
                                                                        (
                                                                            "ip_address_pool_name",
                                                                            str,
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                        (
                                                                            "uplink_profile_name",
                                                                            str,
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                        (
                                                                            "vds_name",
                                                                            str,
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                        (
                                                                            "vds_uplink_to_nsx_uplink",
                                                                            List[
                                                                                make_dataclass(
                                                                                    "vds_uplink_to_nsx_uplink",
                                                                                    [
                                                                                        (
                                                                                            "nsx_uplink_name",
                                                                                            str,
                                                                                        ),
                                                                                        (
                                                                                            "vds_uplink_name",
                                                                                            str,
                                                                                        ),
                                                                                    ],
                                                                                )
                                                                            ],
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                    ],
                                                                )
                                                            ],
                                                        ),
                                                        (
                                                            "description",
                                                            str,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "is_default",
                                                            bool,
                                                            field(default=None),
                                                        ),
                                                    ],
                                                )
                                            ],
                                            field(default=None),
                                        ),
                                    ],
                                ),
                            ),
                            (
                                "advanced_options",
                                make_dataclass(
                                    "advanced_options",
                                    [
                                        ("evc_mode", str, field(default=None)),
                                        (
                                            "high_availability",
                                            make_dataclass(
                                                "high_availability", [("enabled", bool)]
                                            ),
                                            field(default=None),
                                        ),
                                    ],
                                ),
                                field(default=None),
                            ),
                            ("cluster_image_id", str, field(default=None)),
                            ("skip_thumbprint_validation", bool, field(default=None)),
                            (
                                "vx_rail_details",
                                make_dataclass(
                                    "vx_rail_details",
                                    [
                                        (
                                            "admin_credentials",
                                            make_dataclass(
                                                "admin_credentials",
                                                [
                                                    ("credential_type", str),
                                                    ("username", str),
                                                    (
                                                        "password",
                                                        str,
                                                        field(default=None),
                                                    ),
                                                ],
                                            ),
                                            field(default=None),
                                        ),
                                        (
                                            "array_context_with_key_value_pair",
                                            Dict,
                                            field(default=None),
                                        ),
                                        (
                                            "context_with_key_value_pair",
                                            Dict,
                                            field(default=None),
                                        ),
                                        ("dns_name", str, field(default=None)),
                                        ("ip_address", str, field(default=None)),
                                        (
                                            "networks",
                                            List[
                                                make_dataclass(
                                                    "networks",
                                                    [
                                                        (
                                                            "free_ips",
                                                            List[str],
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "gateway",
                                                            str,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "id",
                                                            str,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "ip_pools",
                                                            List[
                                                                make_dataclass(
                                                                    "ip_pools",
                                                                    [
                                                                        (
                                                                            "end",
                                                                            str,
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                        (
                                                                            "start",
                                                                            str,
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                    ],
                                                                )
                                                            ],
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "mask",
                                                            str,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "mtu",
                                                            int,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "subnet",
                                                            str,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "type",
                                                            str,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "used_ips",
                                                            List[str],
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "vlan_id",
                                                            int,
                                                            field(default=None),
                                                        ),
                                                    ],
                                                )
                                            ],
                                            field(default=None),
                                        ),
                                        ("nic_profile", str, field(default=None)),
                                        (
                                            "root_credentials",
                                            make_dataclass(
                                                "root_credentials",
                                                [
                                                    ("credential_type", str),
                                                    ("username", str),
                                                    (
                                                        "password",
                                                        str,
                                                        field(default=None),
                                                    ),
                                                ],
                                            ),
                                            field(default=None),
                                        ),
                                        ("ssh_thumbprint", str, field(default=None)),
                                        ("ssl_thumbprint", str, field(default=None)),
                                    ],
                                ),
                                field(default=None),
                            ),
                        ],
                    )
                ],
            ),
            ("skip_failed_hosts", bool, field(default=None)),
        ],
    ),
    domain_id: str,
    resource_id: str = None,
    name: str = None,
) -> Dict[str, Any]:
    """
    Create a cluster and add to an existing domain
        None

    Args:
        compute_spec(dict[str, Any]):
            computeSpec.

            * cluster_specs (List[dict[str, Any]]):
                List of clusters to be added to workload domain.

                * advanced_options (dict[str, Any], Optional):
                    advancedOptions. Defaults to None.

                    * evc_mode (str, Optional):
                        EVC mode for new cluster, if needed. Defaults to None.

                    * high_availability (dict[str, Any], Optional):
                        highAvailability. Defaults to None.

                        * enabled (bool):
                            enabled.

                * cluster_image_id (str, Optional):
                    ID of the Cluster Image to be used with the Cluster. Defaults to None.

                * datastore_spec (dict[str, Any]):
                    datastoreSpec.

                    * nfs_datastore_specs (List[dict[str, Any]], Optional):
                        Cluster storage configuration for NFS. Defaults to None.

                        * datastore_name (str):
                            Datastore name used for cluster creation.

                        * nas_volume (dict[str, Any], Optional):
                            nasVolume. Defaults to None.

                            * path (str):
                                Shared directory path used for NFS based cluster creation.

                            * read_only (bool):
                                Readonly is used to identify whether to mount the directory as readOnly or not.

                            * server_name (List[str]):
                                NFS Server name used for cluster creation.

                            * user_tag (str, Optional):
                                User tag used to annotate NFS share. Defaults to None.

                    * vmfs_datastore_spec (dict[str, Any], Optional):
                        vmfsDatastoreSpec. Defaults to None.

                        * fc_spec (List[dict[str, Any]], Optional):
                            Cluster storage configuration for VMFS on FC. Defaults to None.

                            * datastore_name (str):
                                Datastore name used for cluster creation.

                    * vsan_datastore_spec (dict[str, Any], Optional):
                        vsanDatastoreSpec. Defaults to None.

                        * datastore_name (str):
                            Datastore name used for cluster creation.

                        * dedup_and_compression_enabled (bool, Optional):
                            Enable vSAN deduplication and compression. Defaults to None.

                        * esa_config (dict[str, Any], Optional):
                            esaConfig. Defaults to None.

                            * enabled (bool):
                                vSAN ESA enablement status.

                        * failures_to_tolerate (int, Optional):
                            Number of vSphere host failures to tolerate in the vSAN cluster. Defaults to None.

                        * license_key (str, Optional):
                            License key for the vSAN data store to be applied in vCenter. Defaults to None.

                    * vsan_remote_datastore_cluster_spec (dict[str, Any], Optional):
                        vsanRemoteDatastoreClusterSpec. Defaults to None.

                        * vsan_remote_datastore_spec (List[dict[str, Any]], Optional):
                            List of Remote vSAN datastore configuration for HCI Mesh compute client cluster. Defaults to None.

                            * datastore_uuid (str):
                                vSAN Remote Datastore UUID.

                    * vvol_datastore_specs (List[dict[str, Any]], Optional):
                        Cluster storage configuration for VVOL. Defaults to None.

                        * name (str):
                            Name of the datastore.

                        * vasa_provider_spec (dict[str, Any]):
                            vasaProviderSpec.

                            * storage_container_id (str):
                                UUID of the VASA storage container.

                            * storage_protocol_type (str):
                                Type of the VASA storage protocol.

                            * user_id (str):
                                UUID of the VASA storage user.

                            * vasa_provider_id (str):
                                UUID of the VASA storage provider.

                * host_specs (List[dict[str, Any]]):
                    List of vSphere host information from the free pool to consume in the workload domain.

                    * az_name (str, Optional):
                        Availability Zone Name
                        (This is required while performing a stretched cluster expand operation). Defaults to None.

                    * host_name (str, Optional):
                        Host name of the vSphere host. Defaults to None.

                    * host_network_spec (dict[str, Any], Optional):
                        hostNetworkSpec. Defaults to None.

                        * network_profile_name (str, Optional):
                            Network profile name. Defaults to None.

                        * vm_nics (List[dict[str, Any]], Optional):
                            List of the vSphere host vmNics. Defaults to None.

                            * id (str, Optional):
                                VmNic ID of vSphere host to be associated with VDS, once added to cluster. Defaults to None.

                            * move_to_nvds (bool, Optional):
                                This flag determines if the vmnic must be on N-VDS. Defaults to None.

                            * uplink (str, Optional):
                                Uplink to be associated with vmnic. Defaults to None.

                            * vds_name (str, Optional):
                                VDS name to associate with vSphere host. Defaults to None.

                    * id (str):
                        ID of a vSphere host in the free pool.

                    * ip_address (str, Optional):
                        IP address of the vSphere host. Defaults to None.

                    * license_key (str, Optional):
                        License key of a vSphere host in the free pool
                        (This is required except in cases where the ESXi host has already been licensed outside of the VMware Cloud Foundation system). Defaults to None.

                    * password (str, Optional):
                        SSH password of the vSphere host Defaults to None.

                    * serial_number (str, Optional):
                        Serial Number of the vSphere host. Defaults to None.

                    * ssh_thumbprint (str, Optional):
                        SSH thumbprint(fingerprint) of the vSphere host
                        Note:This field will be mandatory in future releases. Defaults to None.

                    * username (str, Optional):
                        Username of the vSphere host. Defaults to None.

                * name (str):
                    Name of the new cluster that will be added to the specified workload domain.

                * network_spec (dict[str, Any]):
                    networkSpec.

                    * nsx_cluster_spec (dict[str, Any]):
                        nsxClusterSpec.

                        * nsx_t_cluster_spec (dict[str, Any], Optional):
                            nsxTClusterSpec. Defaults to None.

                            * geneve_vlan_id (int, Optional):
                                Vlan id of Geneve. Defaults to None.

                            * ip_address_pool_spec (dict[str, Any], Optional):
                                ipAddressPoolSpec. Defaults to None.

                                * description (str, Optional):
                                    Description of the IP address pool. Defaults to None.

                                * ignore_unavailable_nsxt_cluster (bool, Optional):
                                    Ignore unavailable NSX cluster(s) during IP pool spec validation. Defaults to None.

                                * name (str):
                                    Name of the IP address pool.

                                * subnets (List[dict[str, Any]], Optional):
                                    List of IP address pool subnet specification. Defaults to None.

                                    * cidr (str):
                                        The subnet representation, contains the network address and the prefix length.

                                    * gateway (str):
                                        The default gateway address of the network.

                                    * ip_address_pool_ranges (List[dict[str, Any]]):
                                        List of the IP allocation ranges. Atleast 1 IP address range has to be specified.

                                        * end (str):
                                            The last IP Address of the IP Address Range.

                                        * start (str):
                                            The first IP Address of the IP Address Range.

                            * ip_address_pools_spec (List[dict[str, Any]], Optional):
                                The list of IP address pools specification. Defaults to None.

                                * description (str, Optional):
                                    Description of the IP address pool. Defaults to None.

                                * ignore_unavailable_nsxt_cluster (bool, Optional):
                                    Ignore unavailable NSX cluster(s) during IP pool spec validation. Defaults to None.

                                * name (str):
                                    Name of the IP address pool.

                                * subnets (List[dict[str, Any]], Optional):
                                    List of IP address pool subnet specification. Defaults to None.

                                    * cidr (str):
                                        The subnet representation, contains the network address and the prefix length.

                                    * gateway (str):
                                        The default gateway address of the network.

                                    * ip_address_pool_ranges (List[dict[str, Any]]):
                                        List of the IP allocation ranges. Atleast 1 IP address range has to be specified.

                                        * end (str):
                                            The last IP Address of the IP Address Range.

                                        * start (str):
                                            The first IP Address of the IP Address Range.

                            * uplink_profiles (List[dict[str, Any]], Optional):
                                The list of uplink profile specifications. Defaults to None.

                                * name (str, Optional):
                                    The uplink profile name. Defaults to None.

                                * supported_teaming_policies (Dict, Optional):
                                    List of supported teaming policies in NSX. Defaults to None.

                                * teamings (List[dict[str, Any]], Optional):
                                    The teaming policies to be associated with the uplink profile. Defaults to None.

                                    * active_uplinks (List[str], Optional):
                                        The list of active uplinks. Defaults to None.

                                    * policy (str, Optional):
                                        The teaming policy associated with the uplink profile. Defaults to None.

                                    * stand_by_uplinks (List[str], Optional):
                                        The list of stand by uplinks. Defaults to None.

                                * transport_vlan (int, Optional):
                                    The VLAN used for tagging overlay traffic of the associated Host Switch. Defaults to None.

                    * vds_specs (List[dict[str, Any]]):
                        Distributed switches to add to the cluster.

                        * is_used_by_nsxt (bool, Optional):
                            Boolean to identify if the vSphere distributed switch is used by NSX. This property is deprecated in favor of nsxtSwitchConfig field. Defaults to None.

                        * nsxt_switch_config (dict[str, Any], Optional):
                            nsxtSwitchConfig. Defaults to None.

                            * transport_zones (List[dict[str, Any]], Optional):
                                The list of transport zones to be associated with the vSphere Distributed Switch managed by NSX. Defaults to None.

                                * name (str, Optional):
                                    The name of the transport zone. Defaults to None.

                                * transport_type (str):
                                    The type of the transport zone.

                            * host_switch_operational_mode (str, Optional):
                                vSphere Distributed Switch name. Defaults to None.

                        * mtu (int, Optional):
                            The maximum transmission unit (MTU) configured for the uplinks. Defaults to None.

                        * name (str):
                            vSphere Distributed Switch name.

                        * nioc_bandwidth_allocation_specs (List[dict[str, Any]], Optional):
                            List of Network I/O Control Bandwidth Allocations for System Traffic. Defaults to None.

                            * nioc_traffic_resource_allocation (dict[str, Any], Optional):
                                niocTrafficResourceAllocation. Defaults to None.

                                * limit (int):
                                    limit.

                                * reservation (int):
                                    reservation.

                                * shares_info (dict[str, Any]):
                                    sharesInfo.

                                    * level (str, Optional):
                                        The allocation level. Defaults to None.

                                    * shares (int, Optional):
                                        The number of shares allocated. Defaults to None.

                            * type (str):
                                DvsHostInfrastructureTrafficResource resource type.

                        * port_group_specs (List[dict[str, Any]], Optional):
                            List of portgroups to be associated with the vSphere Distributed Switch. Defaults to None.

                            * active_uplinks (List[str], Optional):
                                The list of active uplinks associated with portgroup. Defaults to None.

                            * stand_by_uplinks (List[str], Optional):
                                The list of standby uplinks associated with portgroup. Defaults to None.

                            * teaming_policy (str, Optional):
                                The teaming policy associated with the portgroup. Defaults to None.

                            * name (str):
                                Port group name.

                            * transport_type (str):
                                Port group transport type.

                    * network_profiles (List[dict[str, Any]], Optional):
                        The list of network profiles. Defaults to None.

                        * name (str):
                            The network profile name.

                        * description (str, Optional):
                            The network profile description. Defaults to None.

                        * is_default (bool, Optional):
                            Designates the network profile as a Global Network Config or Sub Network Config. Defaults to None.

                        * nsxt_host_switch_configs (List[dict[str, Any]]):
                            The list of NSX host switch configurations.

                            * ip_address_pool_name (str, Optional):
                                The IP address pool name. Defaults to None.

                            * uplink_profile_name (str, Optional):
                                The name of the uplink profile. Defaults to None.

                            * vds_name (str, Optional):
                                The name of the vSphere Distributed Switch. Defaults to None.

                            * vds_uplink_to_nsx_uplink (List[dict[str, Any]], Optional):
                                The map of vSphere Distributed Switch uplinks to the NSX switch uplinks. Defaults to None.

                                * nsx_uplink_name (str):
                                    The uplink name of the NSX switch.

                                * vds_uplink_name (str):
                                    The uplink name of the vSphere Distributed Switch.

                * skip_thumbprint_validation (bool, Optional):
                    Skip thumbprint validation for ESXi and VxRail Manager during add cluster/host operation.
                    This property is deprecated. Defaults to None.

                * vx_rail_details (dict[str, Any], Optional):
                    vxRailDetails. Defaults to None.

                    * admin_credentials (dict[str, Any], Optional):
                        adminCredentials. Defaults to None.

                        * credential_type (str):
                            Credential type.

                        * password (str, Optional):
                            Password. Defaults to None.

                        * username (str):
                            Username.

                    * array_context_with_key_value_pair (Dict, Optional):
                        Map of Context class with list of key and value pairs for array objects. Defaults to None.

                    * context_with_key_value_pair (Dict, Optional):
                        Map of Context class with list of key and value pairs. Defaults to None.

                    * dns_name (str, Optional):
                        DNS Name/Hostname of the VxRail Manager. Defaults to None.

                    * ip_address (str, Optional):
                        IP Address of the VxRail Manager. Defaults to None.

                    * networks (List[dict[str, Any]], Optional):
                        Network details of the VxRail Manager. Defaults to None.

                        * free_ips (List[str], Optional):
                            List of free IPs to use. Defaults to None.

                        * gateway (str, Optional):
                            Gateway for the network. Defaults to None.

                        * id (str, Optional):
                            The ID of the network. Defaults to None.

                        * ip_pools (List[dict[str, Any]], Optional):
                            List of IP pool ranges to use. Defaults to None.

                            * end (str, Optional):
                                End IP address of the IP pool. Defaults to None.

                            * start (str, Optional):
                                Start IP address of the IP pool. Defaults to None.

                        * mask (str, Optional):
                            Subnet mask for the subnet of the network. Defaults to None.

                        * mtu (int, Optional):
                            MTU of the network. Defaults to None.

                        * subnet (str, Optional):
                            Subnet associated with the network. Defaults to None.

                        * type (str, Optional):
                            Network Type of the network. Defaults to None.

                        * used_ips (List[str], Optional):
                            List of used IPs. Defaults to None.

                        * vlan_id (int, Optional):
                            VLAN ID associated with the network. Defaults to None.

                    * nic_profile (str, Optional):
                        Nic Profile Type. Defaults to None.

                    * root_credentials (dict[str, Any], Optional):
                        rootCredentials. Defaults to None.

                        * credential_type (str):
                            Credential type.

                        * password (str, Optional):
                            Password. Defaults to None.

                        * username (str):
                            Username.

                    * ssh_thumbprint (str, Optional):
                        SSH thumbprint of the VxRail Manager. Defaults to None.

                    * ssl_thumbprint (str, Optional):
                        SSL thumbprint of the VxRail Manager. Defaults to None.

            * skip_failed_hosts (bool, Optional):
                Skip failed ESXi Hosts and proceed with the rest of the ESXi Hosts during add Cluster. This is not supported for VCF VxRail. Defaults to None.

        domain_id(str):
            ID of the domain to which the clusters will be added.

        resource_id(str, Optional):
            Clusters unique ID. Defaults to None.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.clusters.present:
                - compute_spec: value
                - domain_id: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.clusters.create compute_spec=value, domain_id=value
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {
        "compute_spec": "computeSpec",
        "domain_id": "domainId",
    }

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    create = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/clusters",
        query_params={},
        data=payload,
        headers={},
    )

    if not create["result"]:
        result["comment"].append(create["comment"])
        result["result"] = False
        return result

    result["comment"].append(
        f"Created vcf.clusters '{name}'",
    )

    result["ret"] = create["ret"]
    # TODO: add "resource_id" to returned response by mapping to correct resource identifier
    return result


async def update(
    hub,
    ctx,
    resource_id: str,
    id_: str,
    name: str = None,
    cluster_compaction_spec: make_dataclass(
        "cluster_compaction_spec",
        [
            ("force", bool, field(default=None)),
            ("force_by_passing_safe_min_size", bool, field(default=None)),
            (
                "hosts",
                List[
                    make_dataclass(
                        "hosts",
                        [
                            ("az_name", str, field(default=None)),
                            ("fqdn", str, field(default=None)),
                            ("id", str, field(default=None)),
                            ("ip_address", str, field(default=None)),
                            (
                                "vm_nics",
                                List[
                                    make_dataclass(
                                        "vm_nics",
                                        [
                                            ("is_active", bool, field(default=None)),
                                            (
                                                "is_auto_negotiate_supported",
                                                bool,
                                                field(default=None),
                                            ),
                                            ("is_in_use", bool, field(default=None)),
                                            ("link_speed_mb", int, field(default=None)),
                                            ("name", str, field(default=None)),
                                        ],
                                    )
                                ],
                                field(default=None),
                            ),
                        ],
                    )
                ],
                field(default=None),
            ),
        ],
    ) = None,
    cluster_compliance_check_spec: make_dataclass(
        "cluster_compliance_check_spec",
        [("cluster_image_id", str, field(default=None))],
    ) = None,
    cluster_compliance_cleanup_spec: {} = None,
    cluster_expansion_spec: make_dataclass(
        "cluster_expansion_spec",
        [
            (
                "host_specs",
                List[
                    make_dataclass(
                        "host_specs",
                        [
                            ("id", str),
                            ("az_name", str, field(default=None)),
                            ("host_name", str, field(default=None)),
                            (
                                "host_network_spec",
                                make_dataclass(
                                    "host_network_spec",
                                    [
                                        (
                                            "network_profile_name",
                                            str,
                                            field(default=None),
                                        ),
                                        (
                                            "vm_nics",
                                            List[
                                                make_dataclass(
                                                    "vm_nics",
                                                    [
                                                        (
                                                            "id",
                                                            str,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "move_to_nvds",
                                                            bool,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "uplink",
                                                            str,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "vds_name",
                                                            str,
                                                            field(default=None),
                                                        ),
                                                    ],
                                                )
                                            ],
                                            field(default=None),
                                        ),
                                    ],
                                ),
                                field(default=None),
                            ),
                            ("ip_address", str, field(default=None)),
                            ("license_key", str, field(default=None)),
                            ("password", str, field(default=None)),
                            ("serial_number", str, field(default=None)),
                            ("ssh_thumbprint", str, field(default=None)),
                            ("username", str, field(default=None)),
                        ],
                    )
                ],
            ),
            ("force_host_addition_in_presenceof_dead_hosts", bool, field(default=None)),
            ("inter_rack_expansion", bool, field(default=None)),
            (
                "network_spec",
                make_dataclass(
                    "network_spec",
                    [
                        (
                            "network_profiles",
                            List[
                                make_dataclass(
                                    "network_profiles",
                                    [
                                        ("description", str, field(default=None)),
                                        ("name", str, field(default=None)),
                                        (
                                            "nsxt_host_switch_configs",
                                            List[
                                                make_dataclass(
                                                    "nsxt_host_switch_configs",
                                                    [
                                                        (
                                                            "ip_address_pool_name",
                                                            str,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "uplink_profile_name",
                                                            str,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "vds_name",
                                                            str,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "vds_uplink_to_nsx_uplink",
                                                            List[
                                                                make_dataclass(
                                                                    "vds_uplink_to_nsx_uplink",
                                                                    [
                                                                        (
                                                                            "nsx_uplink_name",
                                                                            str,
                                                                        ),
                                                                        (
                                                                            "vds_uplink_name",
                                                                            str,
                                                                        ),
                                                                    ],
                                                                )
                                                            ],
                                                            field(default=None),
                                                        ),
                                                    ],
                                                )
                                            ],
                                            field(default=None),
                                        ),
                                    ],
                                )
                            ],
                            field(default=None),
                        ),
                        (
                            "nsx_cluster_spec",
                            make_dataclass(
                                "nsx_cluster_spec",
                                [
                                    (
                                        "ip_address_pools_spec",
                                        List[
                                            make_dataclass(
                                                "ip_address_pools_spec",
                                                [
                                                    ("name", str),
                                                    (
                                                        "description",
                                                        str,
                                                        field(default=None),
                                                    ),
                                                    (
                                                        "ignore_unavailable_nsxt_cluster",
                                                        bool,
                                                        field(default=None),
                                                    ),
                                                    (
                                                        "subnets",
                                                        List[
                                                            make_dataclass(
                                                                "subnets",
                                                                [
                                                                    ("cidr", str),
                                                                    ("gateway", str),
                                                                    (
                                                                        "ip_address_pool_ranges",
                                                                        List[
                                                                            make_dataclass(
                                                                                "ip_address_pool_ranges",
                                                                                [
                                                                                    (
                                                                                        "end",
                                                                                        str,
                                                                                    ),
                                                                                    (
                                                                                        "start",
                                                                                        str,
                                                                                    ),
                                                                                ],
                                                                            )
                                                                        ],
                                                                    ),
                                                                ],
                                                            )
                                                        ],
                                                        field(default=None),
                                                    ),
                                                ],
                                            )
                                        ],
                                        field(default=None),
                                    ),
                                    (
                                        "uplink_profiles",
                                        List[
                                            make_dataclass(
                                                "uplink_profiles",
                                                [
                                                    ("name", str, field(default=None)),
                                                    (
                                                        "supported_teaming_policies",
                                                        Dict,
                                                        field(default=None),
                                                    ),
                                                    (
                                                        "teamings",
                                                        List[
                                                            make_dataclass(
                                                                "teamings",
                                                                [
                                                                    (
                                                                        "active_uplinks",
                                                                        List[str],
                                                                        field(
                                                                            default=None
                                                                        ),
                                                                    ),
                                                                    (
                                                                        "policy",
                                                                        str,
                                                                        field(
                                                                            default=None
                                                                        ),
                                                                    ),
                                                                    (
                                                                        "stand_by_uplinks",
                                                                        List[str],
                                                                        field(
                                                                            default=None
                                                                        ),
                                                                    ),
                                                                ],
                                                            )
                                                        ],
                                                        field(default=None),
                                                    ),
                                                    (
                                                        "transport_vlan",
                                                        int,
                                                        field(default=None),
                                                    ),
                                                ],
                                            )
                                        ],
                                        field(default=None),
                                    ),
                                ],
                            ),
                            field(default=None),
                        ),
                    ],
                ),
                field(default=None),
            ),
            ("skip_thumbprint_validation", bool, field(default=None)),
            (
                "vsan_network_specs",
                List[
                    make_dataclass(
                        "vsan_network_specs",
                        [
                            ("vsan_cidr", str, field(default=None)),
                            ("vsan_gateway_ip", str, field(default=None)),
                        ],
                    )
                ],
                field(default=None),
            ),
            (
                "witness_spec",
                make_dataclass(
                    "witness_spec",
                    [("fqdn", str), ("vsan_cidr", str), ("vsan_ip", str)],
                ),
                field(default=None),
            ),
            ("witness_traffic_shared_with_vsan_traffic", bool, field(default=None)),
        ],
    ) = None,
    cluster_remediation_spec: {} = None,
    cluster_stretch_spec: make_dataclass(
        "cluster_stretch_spec",
        [
            (
                "host_specs",
                List[
                    make_dataclass(
                        "host_specs",
                        [
                            ("id", str),
                            ("az_name", str, field(default=None)),
                            ("host_name", str, field(default=None)),
                            (
                                "host_network_spec",
                                make_dataclass(
                                    "host_network_spec",
                                    [
                                        (
                                            "network_profile_name",
                                            str,
                                            field(default=None),
                                        ),
                                        (
                                            "vm_nics",
                                            List[
                                                make_dataclass(
                                                    "vm_nics",
                                                    [
                                                        (
                                                            "id",
                                                            str,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "move_to_nvds",
                                                            bool,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "uplink",
                                                            str,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "vds_name",
                                                            str,
                                                            field(default=None),
                                                        ),
                                                    ],
                                                )
                                            ],
                                            field(default=None),
                                        ),
                                    ],
                                ),
                                field(default=None),
                            ),
                            ("ip_address", str, field(default=None)),
                            ("license_key", str, field(default=None)),
                            ("password", str, field(default=None)),
                            ("serial_number", str, field(default=None)),
                            ("ssh_thumbprint", str, field(default=None)),
                            ("username", str, field(default=None)),
                        ],
                    )
                ],
            ),
            (
                "witness_spec",
                make_dataclass(
                    "witness_spec",
                    [("fqdn", str), ("vsan_cidr", str), ("vsan_ip", str)],
                ),
            ),
            ("is_edge_cluster_configured_for_multi_az", bool, field(default=None)),
            (
                "network_profiles",
                List[
                    make_dataclass(
                        "network_profiles",
                        [
                            ("name", str),
                            (
                                "nsxt_host_switch_configs",
                                List[
                                    make_dataclass(
                                        "nsxt_host_switch_configs",
                                        [
                                            (
                                                "ip_address_pool_name",
                                                str,
                                                field(default=None),
                                            ),
                                            (
                                                "uplink_profile_name",
                                                str,
                                                field(default=None),
                                            ),
                                            ("vds_name", str, field(default=None)),
                                            (
                                                "vds_uplink_to_nsx_uplink",
                                                List[
                                                    make_dataclass(
                                                        "vds_uplink_to_nsx_uplink",
                                                        [
                                                            ("nsx_uplink_name", str),
                                                            ("vds_uplink_name", str),
                                                        ],
                                                    )
                                                ],
                                                field(default=None),
                                            ),
                                        ],
                                    )
                                ],
                            ),
                            ("description", str, field(default=None)),
                        ],
                    )
                ],
                field(default=None),
            ),
            (
                "nsx_stretch_cluster_spec",
                make_dataclass(
                    "nsx_stretch_cluster_spec",
                    [
                        (
                            "uplink_profiles",
                            List[
                                make_dataclass(
                                    "uplink_profiles",
                                    [
                                        ("name", str, field(default=None)),
                                        (
                                            "supported_teaming_policies",
                                            Dict,
                                            field(default=None),
                                        ),
                                        (
                                            "teamings",
                                            List[
                                                make_dataclass(
                                                    "teamings",
                                                    [
                                                        (
                                                            "active_uplinks",
                                                            List[str],
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "policy",
                                                            str,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "stand_by_uplinks",
                                                            List[str],
                                                            field(default=None),
                                                        ),
                                                    ],
                                                )
                                            ],
                                            field(default=None),
                                        ),
                                        ("transport_vlan", int, field(default=None)),
                                    ],
                                )
                            ],
                        ),
                        (
                            "ip_address_pools_spec",
                            List[
                                make_dataclass(
                                    "ip_address_pools_spec",
                                    [
                                        ("name", str),
                                        ("description", str, field(default=None)),
                                        (
                                            "ignore_unavailable_nsxt_cluster",
                                            bool,
                                            field(default=None),
                                        ),
                                        (
                                            "subnets",
                                            List[
                                                make_dataclass(
                                                    "subnets",
                                                    [
                                                        ("cidr", str),
                                                        ("gateway", str),
                                                        (
                                                            "ip_address_pool_ranges",
                                                            List[
                                                                make_dataclass(
                                                                    "ip_address_pool_ranges",
                                                                    [
                                                                        ("end", str),
                                                                        ("start", str),
                                                                    ],
                                                                )
                                                            ],
                                                        ),
                                                    ],
                                                )
                                            ],
                                            field(default=None),
                                        ),
                                    ],
                                )
                            ],
                            field(default=None),
                        ),
                    ],
                ),
                field(default=None),
            ),
            ("secondary_az_overlay_vlan_id", int, field(default=None)),
            (
                "vsan_network_specs",
                List[
                    make_dataclass(
                        "vsan_network_specs",
                        [
                            ("vsan_cidr", str, field(default=None)),
                            ("vsan_gateway_ip", str, field(default=None)),
                        ],
                    )
                ],
                field(default=None),
            ),
            ("witness_traffic_shared_with_vsan_traffic", bool, field(default=None)),
        ],
    ) = None,
    cluster_transition_spec: {} = None,
    cluster_unstretch_spec: {} = None,
    mark_for_deletion: bool = None,
    prepare_for_stretch: bool = None,
) -> Dict[str, Any]:
    """
    Update a Cluster by adding or removing Hosts, Stretching a standard vSAN cluster, Unstretching a stretched cluster or by marking for deletion
        None

    Args:
        resource_id(str):
            Clusters unique ID.

        id_(str):
            Cluster ID.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

        cluster_compaction_spec(dict[str, Any], Optional):
            clusterCompactionSpec. Defaults to None.

            * force (bool, Optional):
                Force removal of vSphere host. Defaults to None.

            * force_by_passing_safe_min_size (bool, Optional):
                Remove dead hosts from cluster, bypassing validations. Forced removal may result in permanent data loss. Review recovery plan with VMware Support before using. Defaults to None.

            * hosts (List[dict[str, Any]], Optional):
                List of vSphere hosts to be removed. Defaults to None.

                * az_name (str, Optional):
                    Fault domain name of the host. Defaults to None.

                * fqdn (str, Optional):
                    FQDN of the host. Defaults to None.

                * id (str, Optional):
                    ID of the host. Defaults to None.

                * ip_address (str, Optional):
                    IP address of the host. Defaults to None.

                * vm_nics (List[dict[str, Any]], Optional):
                    VM NICs of the host. Defaults to None.

                    * is_active (bool, Optional):
                        Status of VMNic if active or inactive. Defaults to None.

                    * is_auto_negotiate_supported (bool, Optional):
                        Status of VMNic if auto negotiate is supported or not. Defaults to None.

                    * is_in_use (bool, Optional):
                        Status of VMNic if in use or available. Defaults to None.

                    * link_speed_mb (int, Optional):
                        VMNic link speed in MB. Defaults to None.

                    * name (str, Optional):
                        Name of the VMNic. Defaults to None.

        cluster_compliance_check_spec(dict[str, Any], Optional):
            clusterComplianceCheckSpec. Defaults to None.

            * cluster_image_id (str, Optional):
                ID of the Cluster Image to be used with the Cluster. Defaults to None.

        cluster_compliance_cleanup_spec({}, Optional):
            clusterComplianceCleanupSpec. Defaults to None.

        cluster_expansion_spec(dict[str, Any], Optional):
            clusterExpansionSpec. Defaults to None.

            * force_host_addition_in_presenceof_dead_hosts (bool, Optional):
                Use to add host to a cluster with dead host(s). Bypasses validation of disconnected hosts and vSAN cluster health. Review recovery plan VMware Support before using. False if omitted. This property is deprecated and it has no effect when using it. Defaults to None.

            * host_specs (List[dict[str, Any]]):
                List of vSphere host information from the free pool to consume in the workload domain.

                * az_name (str, Optional):
                    Availability Zone Name
                    (This is required while performing a stretched cluster expand operation). Defaults to None.

                * host_name (str, Optional):
                    Host name of the vSphere host. Defaults to None.

                * host_network_spec (dict[str, Any], Optional):
                    hostNetworkSpec. Defaults to None.

                    * network_profile_name (str, Optional):
                        Network profile name. Defaults to None.

                    * vm_nics (List[dict[str, Any]], Optional):
                        List of the vSphere host vmNics. Defaults to None.

                        * id (str, Optional):
                            VmNic ID of vSphere host to be associated with VDS, once added to cluster. Defaults to None.

                        * move_to_nvds (bool, Optional):
                            This flag determines if the vmnic must be on N-VDS. Defaults to None.

                        * uplink (str, Optional):
                            Uplink to be associated with vmnic. Defaults to None.

                        * vds_name (str, Optional):
                            VDS name to associate with vSphere host. Defaults to None.

                * id (str):
                    ID of a vSphere host in the free pool.

                * ip_address (str, Optional):
                    IP address of the vSphere host. Defaults to None.

                * license_key (str, Optional):
                    License key of a vSphere host in the free pool
                    (This is required except in cases where the ESXi host has already been licensed outside of the VMware Cloud Foundation system). Defaults to None.

                * password (str, Optional):
                    SSH password of the vSphere host Defaults to None.

                * serial_number (str, Optional):
                    Serial Number of the vSphere host. Defaults to None.

                * ssh_thumbprint (str, Optional):
                    SSH thumbprint(fingerprint) of the vSphere host
                    Note:This field will be mandatory in future releases. Defaults to None.

                * username (str, Optional):
                    Username of the vSphere host. Defaults to None.

            * inter_rack_expansion (bool, Optional):
                Is inter-rack cluster(true for L2 non-uniform and L3 : At least one of management, uplink, Edge and host TEP networks is different for hosts of the cluster, false for L2 uniform :  All hosts in cluster have identical management, uplink, Edge and host TEP networks) expansion. Required, only if Cluster contains NSX Edge Cluster. Defaults to None.

            * network_spec (dict[str, Any], Optional):
                networkSpec. Defaults to None.

                * network_profiles (List[dict[str, Any]], Optional):
                    The list of network profiles. Defaults to None.

                    * description (str, Optional):
                        The network profile description. Defaults to None.

                    * name (str, Optional):
                        The network profile name. Defaults to None.

                    * nsxt_host_switch_configs (List[dict[str, Any]], Optional):
                        The list of NSX host switch configurations. Defaults to None.

                        * ip_address_pool_name (str, Optional):
                            The IP address pool name. Defaults to None.

                        * uplink_profile_name (str, Optional):
                            The name of the uplink profile. Defaults to None.

                        * vds_name (str, Optional):
                            The name of the vSphere Distributed Switch. Defaults to None.

                        * vds_uplink_to_nsx_uplink (List[dict[str, Any]], Optional):
                            The map of vSphere Distributed Switch uplinks to the NSX switch uplinks. Defaults to None.

                            * nsx_uplink_name (str):
                                The uplink name of the NSX switch.

                            * vds_uplink_name (str):
                                The uplink name of the vSphere Distributed Switch.

                * nsx_cluster_spec (dict[str, Any], Optional):
                    nsxClusterSpec. Defaults to None.

                    * ip_address_pools_spec (List[dict[str, Any]], Optional):
                        The IP address pools specification. Defaults to None.

                        * description (str, Optional):
                            Description of the IP address pool. Defaults to None.

                        * ignore_unavailable_nsxt_cluster (bool, Optional):
                            Ignore unavailable NSX cluster(s) during IP pool spec validation. Defaults to None.

                        * name (str):
                            Name of the IP address pool.

                        * subnets (List[dict[str, Any]], Optional):
                            List of IP address pool subnet specification. Defaults to None.

                            * cidr (str):
                                The subnet representation, contains the network address and the prefix length.

                            * gateway (str):
                                The default gateway address of the network.

                            * ip_address_pool_ranges (List[dict[str, Any]]):
                                List of the IP allocation ranges. Atleast 1 IP address range has to be specified.

                                * end (str):
                                    The last IP Address of the IP Address Range.

                                * start (str):
                                    The first IP Address of the IP Address Range.

                    * uplink_profiles (List[dict[str, Any]], Optional):
                        The list of uplink profile specifications. Defaults to None.

                        * name (str, Optional):
                            The uplink profile name. Defaults to None.

                        * supported_teaming_policies (Dict, Optional):
                            List of supported teaming policies in NSX. Defaults to None.

                        * teamings (List[dict[str, Any]], Optional):
                            The teaming policies to be associated with the uplink profile. Defaults to None.

                            * active_uplinks (List[str], Optional):
                                The list of active uplinks. Defaults to None.

                            * policy (str, Optional):
                                The teaming policy associated with the uplink profile. Defaults to None.

                            * stand_by_uplinks (List[str], Optional):
                                The list of stand by uplinks. Defaults to None.

                        * transport_vlan (int, Optional):
                            The VLAN used for tagging overlay traffic of the associated Host Switch. Defaults to None.

            * skip_thumbprint_validation (bool, Optional):
                Skip thumbprint validation for ESXi hosts during add host operation.
                This property is deprecated. Defaults to None.

            * vsan_network_specs (List[dict[str, Any]], Optional):
                vSAN Network Pool Spec. Defaults to None.

                * vsan_cidr (str, Optional):
                    vSAN subnet cidr of the ESXi host. Defaults to None.

                * vsan_gateway_ip (str, Optional):
                    vSAN Gateway IP of the ESXi host. Defaults to None.

            * witness_spec (dict[str, Any], Optional):
                witnessSpec. Defaults to None.

                * fqdn (str):
                    Management ip of the witness host.

                * vsan_cidr (str):
                    vSAN subnet cidr of the witness host.

                * vsan_ip (str):
                    vSAN IP of the witness host.

            * witness_traffic_shared_with_vsan_traffic (bool, Optional):
                Witness traffic to be shared with vSAN traffic. Defaults to None.

        cluster_remediation_spec({}, Optional):
            clusterRemediationSpec. Defaults to None.

        cluster_stretch_spec(dict[str, Any], Optional):
            clusterStretchSpec. Defaults to None.

            * host_specs (List[dict[str, Any]]):
                List of vSphere host information from the free pool to consume in the workload domain.

                * az_name (str, Optional):
                    Availability Zone Name
                    (This is required while performing a stretched cluster expand operation). Defaults to None.

                * host_name (str, Optional):
                    Host name of the vSphere host. Defaults to None.

                * host_network_spec (dict[str, Any], Optional):
                    hostNetworkSpec. Defaults to None.

                    * network_profile_name (str, Optional):
                        Network profile name. Defaults to None.

                    * vm_nics (List[dict[str, Any]], Optional):
                        List of the vSphere host vmNics. Defaults to None.

                        * id (str, Optional):
                            VmNic ID of vSphere host to be associated with VDS, once added to cluster. Defaults to None.

                        * move_to_nvds (bool, Optional):
                            This flag determines if the vmnic must be on N-VDS. Defaults to None.

                        * uplink (str, Optional):
                            Uplink to be associated with vmnic. Defaults to None.

                        * vds_name (str, Optional):
                            VDS name to associate with vSphere host. Defaults to None.

                * id (str):
                    ID of a vSphere host in the free pool.

                * ip_address (str, Optional):
                    IP address of the vSphere host. Defaults to None.

                * license_key (str, Optional):
                    License key of a vSphere host in the free pool
                    (This is required except in cases where the ESXi host has already been licensed outside of the VMware Cloud Foundation system). Defaults to None.

                * password (str, Optional):
                    SSH password of the vSphere host Defaults to None.

                * serial_number (str, Optional):
                    Serial Number of the vSphere host. Defaults to None.

                * ssh_thumbprint (str, Optional):
                    SSH thumbprint(fingerprint) of the vSphere host
                    Note:This field will be mandatory in future releases. Defaults to None.

                * username (str, Optional):
                    Username of the vSphere host. Defaults to None.

            * is_edge_cluster_configured_for_multi_az (bool, Optional):
                This parameter is required for stretching the clusters that host Edge Cluster VMs. It is an acknowledgement, that the necessary network configurations are considered for the edge cluster to work with vSAN stretched cluster during a failover. Defaults to None.

            * network_profiles (List[dict[str, Any]], Optional):
                The network profile to be associated with Secondary AZ Hosts in NSX. Defaults to None.

                * description (str, Optional):
                    The network profile description. Defaults to None.

                * name (str):
                    The network profile name.

                * nsxt_host_switch_configs (List[dict[str, Any]]):
                    The list of NSX host switch configurations.

                    * ip_address_pool_name (str, Optional):
                        The IP address pool name. Defaults to None.

                    * uplink_profile_name (str, Optional):
                        The name of the uplink profile. Defaults to None.

                    * vds_name (str, Optional):
                        The name of the vSphere Distributed Switch. Defaults to None.

                    * vds_uplink_to_nsx_uplink (List[dict[str, Any]], Optional):
                        The map of vSphere Distributed Switch uplinks to the NSX switch uplinks. Defaults to None.

                        * nsx_uplink_name (str):
                            The uplink name of the NSX switch.

                        * vds_uplink_name (str):
                            The uplink name of the vSphere Distributed Switch.

            * nsx_stretch_cluster_spec (dict[str, Any], Optional):
                nsxStretchClusterSpec. Defaults to None.

                * ip_address_pools_spec (List[dict[str, Any]], Optional):
                    The IP address pool specifications. Defaults to None.

                    * description (str, Optional):
                        Description of the IP address pool. Defaults to None.

                    * ignore_unavailable_nsxt_cluster (bool, Optional):
                        Ignore unavailable NSX cluster(s) during IP pool spec validation. Defaults to None.

                    * name (str):
                        Name of the IP address pool.

                    * subnets (List[dict[str, Any]], Optional):
                        List of IP address pool subnet specification. Defaults to None.

                        * cidr (str):
                            The subnet representation, contains the network address and the prefix length.

                        * gateway (str):
                            The default gateway address of the network.

                        * ip_address_pool_ranges (List[dict[str, Any]]):
                            List of the IP allocation ranges. Atleast 1 IP address range has to be specified.

                            * end (str):
                                The last IP Address of the IP Address Range.

                            * start (str):
                                The first IP Address of the IP Address Range.

                * uplink_profiles (List[dict[str, Any]]):
                    The list of uplink profile specifications.

                    * name (str, Optional):
                        The uplink profile name. Defaults to None.

                    * supported_teaming_policies (Dict, Optional):
                        List of supported teaming policies in NSX. Defaults to None.

                    * teamings (List[dict[str, Any]], Optional):
                        The teaming policies to be associated with the uplink profile. Defaults to None.

                        * active_uplinks (List[str], Optional):
                            The list of active uplinks. Defaults to None.

                        * policy (str, Optional):
                            The teaming policy associated with the uplink profile. Defaults to None.

                        * stand_by_uplinks (List[str], Optional):
                            The list of stand by uplinks. Defaults to None.

                    * transport_vlan (int, Optional):
                        The VLAN used for tagging overlay traffic of the associated Host Switch. Defaults to None.

            * secondary_az_overlay_vlan_id (int, Optional):
                Secondary AZ Overlay Vlan Id. This field is deprecated. The secondary AZ overlay vlan id should be mentioned in the uplinkProfile field instead. Defaults to None.

            * vsan_network_specs (List[dict[str, Any]], Optional):
                vSAN Network Pool Specs. Defaults to None.

                * vsan_cidr (str, Optional):
                    vSAN subnet cidr of the ESXi host. Defaults to None.

                * vsan_gateway_ip (str, Optional):
                    vSAN Gateway IP of the ESXi host. Defaults to None.

            * witness_spec (dict[str, Any]):
                witnessSpec.

                * fqdn (str):
                    Management ip of the witness host.

                * vsan_cidr (str):
                    vSAN subnet cidr of the witness host.

                * vsan_ip (str):
                    vSAN IP of the witness host.

            * witness_traffic_shared_with_vsan_traffic (bool, Optional):
                Witness traffic to be shared with vSAN traffic. Defaults to None.

        cluster_transition_spec({}, Optional):
            clusterTransitionSpec. Defaults to None.

        cluster_unstretch_spec({}, Optional):
            clusterUnstretchSpec. Defaults to None.

        mark_for_deletion(bool, Optional):
            Prepare the cluster for deletion. Defaults to None.

        prepare_for_stretch(bool, Optional):
            Prepare the cluster for stretch. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcf.clusters.present:
                - resource_id: value
                - id_: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.clusters.update resource_id=value, id_=value
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    # TODO: Change request param mapping as necessary
    resource_to_raw_input_mapping = {
        "cluster_compaction_spec": "clusterCompactionSpec",
        "cluster_compliance_check_spec": "clusterComplianceCheckSpec",
        "cluster_compliance_cleanup_spec": "clusterComplianceCleanupSpec",
        "cluster_expansion_spec": "clusterExpansionSpec",
        "cluster_remediation_spec": "clusterRemediationSpec",
        "cluster_stretch_spec": "clusterStretchSpec",
        "cluster_transition_spec": "clusterTransitionSpec",
        "cluster_unstretch_spec": "clusterUnstretchSpec",
        "mark_for_deletion": "markForDeletion",
        "name": "name",
        "prepare_for_stretch": "prepareForStretch",
    }

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    if payload:
        update = await hub.tool.vcf.session.request(
            ctx,
            method="patch",
            path="/v1/clusters/{id}".format(**{"id": id_}),
            query_params={},
            data=payload,
            headers={},
        )

        if not update["result"]:
            result["comment"].append(update["comment"])
            result["result"] = False
            return result

        result["ret"] = update["ret"]
        result["comment"].append(
            f"Updated vcf.clusters '{name}'",
        )

    return result


async def delete(
    hub, ctx, resource_id: str, id_: str, name: str = None
) -> Dict[str, Any]:
    """
    Delete a cluster from a domain if it has been previously initialized for deletion
        None

    Args:
        resource_id(str):
            Clusters unique ID.

        id_(str):
            Cluster ID.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            resource_is_absent:
              vcf.clusters.absent:
                - resource_id: value
                - id_: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcf.clusters.delete resource_id=value, id_=value
    """

    result = dict(comment=[], ret=[], result=True)

    delete = await hub.tool.vcf.session.request(
        ctx,
        method="delete",
        path="/v1/clusters/{id}".format(**{"id": id_}),
        query_params={},
        data={},
        headers={},
    )

    if not delete["result"]:
        result["comment"].append(delete["comment"])
        result["result"] = False
        return result

    result["comment"].append(f"Deleted '{name}'")
    return result

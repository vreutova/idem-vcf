"""Utility functions for Personalitieses. """
from typing import Any
from typing import Dict


async def delete_personality(
    hub, ctx, personality_id: str = None, personality_name: str = None
) -> Dict[str, Any]:
    r"""

    Delete the personality based on either name or id
        Delete the personality based on either name or id

    Args:
        personality_id(str, Optional):
            The personality id. Defaults to None.

        personality_name(str, Optional):
            The personality name. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="delete",
        path="/v1/personalities".format(**{}),
        query_params={
            "personalityId": personality_id,
            "personalityName": personality_name,
        },
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result

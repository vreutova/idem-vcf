"""Utility functions for Check Setss. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List


async def query_check_sets(
    hub,
    ctx,
    check_set_type: str = None,
    domains: List[
        make_dataclass(
            "domains",
            [
                ("domain_id", str),
                (
                    "resources",
                    List[
                        make_dataclass(
                            "resources",
                            [
                                ("resource_type", str),
                                ("resource_id", str, field(default=None)),
                                ("resource_target_version", str, field(default=None)),
                            ],
                        )
                    ],
                    field(default=None),
                ),
            ],
        )
    ] = None,
) -> Dict[str, Any]:
    r"""

    Query for check-sets for the given resources
        Query for check-sets for the given resources

    Args:
        check_set_type(str, Optional):
            Type of the check sets to query for. e.g. 'UPGRADE'. Defaults to None.

        domains(List[dict[str, Any]], Optional):
            List of domain resources to query check-sets of the given type for. Defaults to None.

            * domain_id (str):
                Id of the domain to search for resources in.

            * resources (List[dict[str, Any]], Optional):
                Resources in the domain to generate check-set candidates for. Defaults to None.

                * resource_id (str, Optional):
                    UUID of the resource, if used adds additional filtering. Defaults to None.

                * resource_target_version (str, Optional):
                    Upgrade target version for the resources. Defaults to None.

                * resource_type (str):
                    Type of the resource.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"check_set_type": "checkSetType", "domains": "domains"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/system/check-sets/queries".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict({"queryId": "query_id", "resources": "resources"})

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result

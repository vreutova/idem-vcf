"""Utility functions for Tokenss. """
from typing import Any
from typing import Dict


async def refresh_access_token(hub, ctx) -> Dict[str, Any]:
    r"""

    Refresh Access Token
        Refresh the access token associated with the given refresh token


    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="patch",
        path="/v1/tokens/access-token/refresh".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result

"""Utility functions for Config Reconcilers. """
from collections import OrderedDict
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List


async def reconcile_configs(
    hub,
    ctx,
    reconciliation_for_resources: List[
        make_dataclass(
            "reconciliation_for_resources", [("apply_all", bool), ("resource_id", str)]
        )
    ] = None,
) -> Dict[str, Any]:
    r"""

    Reconcile configs
        For selective reconciliation, provide a config spec.

    Args:
        reconciliation_for_resources(List[dict[str, Any]], Optional):
            List of Configuration Reconciliation for resource, mutually exclusive with reconciliationForDrifts. Defaults to None.

            * apply_all (bool):
                All application flag.

            * resource_id (str):
                Target resource ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"reconciliation_for_resources": "reconciliationForResources"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/config-drift-reconciliations".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "errors": "errors",
            "id": "id",
            "isCancellable": "is_cancellable",
            "isRetryable": "is_retryable",
            "localizableDescriptionPack": "localizable_description_pack",
            "name": "name",
            "resolutionStatus": "resolution_status",
            "resources": "resources",
            "status": "status",
            "subTasks": "sub_tasks",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_reconciliation_task(hub, ctx, task_id: str) -> Dict[str, Any]:
    r"""

    Get config reconciliation task associated with the given task Id
        Get config reconciliation task associated with the given task Id

    Args:
        task_id(str):
            Task Id.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/config-drift-reconciliations/{taskId}".format(**{"taskId": task_id}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "errors": "errors",
            "id": "id",
            "isCancellable": "is_cancellable",
            "isRetryable": "is_retryable",
            "localizableDescriptionPack": "localizable_description_pack",
            "name": "name",
            "resolutionStatus": "resolution_status",
            "resources": "resources",
            "status": "status",
            "subTasks": "sub_tasks",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_configs(
    hub,
    ctx,
    resource_id: str = None,
    resource_type: str = None,
    config_id: str = None,
    drift_type: str = None,
    size: int = None,
    page: int = None,
) -> Dict[str, Any]:
    r"""

    Get configs associated with the given criteria
        Get configs associated with the given criteria, all if no criteria is provided

    Args:
        resource_id(str, Optional):
            Resource Id. Defaults to None.

        resource_type(str, Optional):
            Resource Type. Defaults to None.

        config_id(str, Optional):
            Config Id. Defaults to None.

        drift_type(str, Optional):
            Drift Type. Defaults to None.

        size(int, Optional):
            Size of the page to retrieve. Default page size is 10. Optional. Defaults to None.

        page(int, Optional):
            Page number to retrieve. Default page 0 will retrieve all elements. Optional. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/config-drifts".format(**{}),
        query_params={
            "resourceId": resource_id,
            "resourceType": resource_type,
            "configId": config_id,
            "driftType": drift_type,
            "size": size,
            "page": page,
        },
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result

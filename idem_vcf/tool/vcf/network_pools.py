"""Utility functions for Network Poolss. """
from collections import OrderedDict
from typing import Any
from typing import Dict


async def get_networks_of_network_pool(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Get the Networks of a Network Pool
        Get the Networks that are part of a Network Pool

    Args:
        id_(str):
            ID for Networkpool to get the networks from.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/network-pools/{id}/networks".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_network_of_network_pool(
    hub, ctx, id_: str, network_id: str
) -> Dict[str, Any]:
    r"""

    Get a Network of a Network Pool
        Get a Network that is part of a Network Pool

    Args:
        id_(str):
            Id of the Network pool.

        network_id(str):
            Id of the Network.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/network-pools/{id}/networks/{networkId}".format(
            **{"id": id_, "networkId": network_id}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "freeIps": "free_ips",
            "gateway": "gateway",
            "id": "id",
            "ipPools": "ip_pools",
            "mask": "mask",
            "mtu": "mtu",
            "subnet": "subnet",
            "type": "type",
            "usedIps": "used_ips",
            "vlanId": "vlan_id",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def add_ip_pool_to_network_of_network_pool(
    hub, ctx, id_: str, network_id: str, end: str = None, start: str = None
) -> Dict[str, Any]:
    r"""

    Add an IP Pool to a Network of a Network Pool
        Add an IP Pool to a Network of a Network Pool

    Args:
        id_(str):
            Id of the networkpoolk.

        network_id(str):
            Id of the network.

        end(str, Optional):
            End IP address of the IP pool. Defaults to None.

        start(str, Optional):
            Start IP address of the IP pool. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"end": "end", "start": "start"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/network-pools/{id}/networks/{networkId}/ip-pools".format(
            **{"id": id_, "networkId": network_id}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "freeIps": "free_ips",
            "gateway": "gateway",
            "id": "id",
            "ipPools": "ip_pools",
            "mask": "mask",
            "mtu": "mtu",
            "subnet": "subnet",
            "type": "type",
            "usedIps": "used_ips",
            "vlanId": "vlan_id",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def delete_ip_pool_from_network_of_network_pool(
    hub, ctx, id_: str, network_id: str, end: str = None, start: str = None
) -> Dict[str, Any]:
    r"""

    Delete an IP Pool from a Network of a Network Pool
        Delete an IP Pool from a Network of a Network Pool

    Args:
        id_(str):
            ID of the networkpool.

        network_id(str):
            ID of the network.

        end(str, Optional):
            End IP address of the IP pool. Defaults to None.

        start(str, Optional):
            Start IP address of the IP pool. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"end": "end", "start": "start"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="delete",
        path="/v1/network-pools/{id}/networks/{networkId}/ip-pools".format(
            **{"id": id_, "networkId": network_id}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result

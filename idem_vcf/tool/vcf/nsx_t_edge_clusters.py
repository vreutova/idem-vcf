"""Utility functions for Nsx T Edge Clusterss. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List


async def get_edge_clusters(hub, ctx, cluster_id: str = None) -> Dict[str, Any]:
    r"""

    Retrieve a list of NSX Edge Clusters
        None

    Args:
        cluster_id(str, Optional):
            Pass an optional vSphere Cluster ID to fetch edge clusters associated with the vSphere Cluster. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/edge-clusters".format(**{}),
        query_params={"clusterId": cluster_id},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def create_edge_cluster(
    hub,
    ctx,
    edge_admin_password: str,
    edge_audit_password: str,
    edge_cluster_name: str,
    edge_cluster_profile_spec: make_dataclass(
        "edge_cluster_profile_spec",
        [
            ("bfd_allowed_hop", int),
            ("bfd_declare_dead_multiple", int),
            ("bfd_probe_interval", int),
            ("edge_cluster_profile_name", str),
            ("standby_relocation_threshold", int),
        ],
    ),
    edge_cluster_profile_type: str,
    edge_cluster_type: str,
    edge_form_factor: str,
    edge_node_specs: List[
        make_dataclass(
            "edge_node_specs",
            [
                ("cluster_id", str),
                ("edge_node_name", str),
                ("edge_tep_vlan", int),
                ("inter_rack_cluster", bool),
                ("management_gateway", str),
                ("management_ip", str),
                ("edge_tep1_ip", str, field(default=None)),
                ("edge_tep2_ip", str, field(default=None)),
                ("edge_tep_gateway", str, field(default=None)),
                (
                    "edge_tep_ip_address_pool",
                    make_dataclass(
                        "edge_tep_ip_address_pool",
                        [
                            ("name", str, field(default=None)),
                            ("nsx_id", str, field(default=None)),
                        ],
                    ),
                    field(default=None),
                ),
                ("first_nsx_vds_uplink", str, field(default=None)),
                ("vm_management_portgroup_vlan", int, field(default=None)),
                ("vm_management_portgroup_name", str, field(default=None)),
                ("second_nsx_vds_uplink", str, field(default=None)),
                (
                    "uplink_network",
                    List[
                        make_dataclass(
                            "uplink_network",
                            [
                                ("uplink_interface_ip", str),
                                ("uplink_vlan", int),
                                ("asn_peer", int, field(default=None)),
                                ("bgp_peer_password", str, field(default=None)),
                                (
                                    "bgp_peers",
                                    List[
                                        make_dataclass(
                                            "bgp_peers",
                                            [
                                                ("asn", int),
                                                ("ip", str),
                                                ("password", str),
                                            ],
                                        )
                                    ],
                                    field(default=None),
                                ),
                                ("peer_ip", str, field(default=None)),
                            ],
                        )
                    ],
                    field(default=None),
                ),
            ],
        )
    ],
    edge_root_password: str,
    mtu: int,
    asn: int = None,
    new_ip_address_pool_specs: List[
        make_dataclass(
            "new_ip_address_pool_specs",
            [
                ("name", str),
                ("description", str, field(default=None)),
                ("ignore_unavailable_nsxt_cluster", bool, field(default=None)),
                (
                    "subnets",
                    List[
                        make_dataclass(
                            "subnets",
                            [
                                ("cidr", str),
                                ("gateway", str),
                                (
                                    "ip_address_pool_ranges",
                                    List[
                                        make_dataclass(
                                            "ip_address_pool_ranges",
                                            [("end", str), ("start", str)],
                                        )
                                    ],
                                ),
                            ],
                        )
                    ],
                    field(default=None),
                ),
            ],
        )
    ] = None,
    internal_transit_subnets: List[str] = None,
    skip_tep_routability_check: bool = None,
    tier0_name: str = None,
    tier0_routing_type: str = None,
    tier0_services_high_availability: str = None,
    tier1_name: str = None,
    tier1_unhosted: bool = None,
    transit_subnets: List[str] = None,
) -> Dict[str, Any]:
    r"""

    Create an NSX Edge Cluster
        None

    Args:
        edge_admin_password(str):
            Edge Password for admin user.

        edge_audit_password(str):
            Edge Password for audit.

        edge_cluster_name(str):
            Name for the edge cluster.

        edge_cluster_profile_spec(dict[str, Any]):
            edgeClusterProfileSpec.

            * bfd_allowed_hop (int):
                BFD allowed Hop.

            * bfd_declare_dead_multiple (int):
                BFD Declare Dead Multiple.

            * bfd_probe_interval (int):
                BFD Probe.

            * edge_cluster_profile_name (str):
                Name for the edge cluster profile.

            * standby_relocation_threshold (int):
                Standby Relocation Threshold.

        edge_cluster_profile_type(str):
            Type of edge cluster profile.

        edge_cluster_type(str):
            Type of edge cluster.

        edge_form_factor(str):
            Edge Form Factor.

        edge_node_specs(List[dict[str, Any]]):
            Specifications for Edge Node. Number of Edge Nodes cannot exceed 8 if HA mode is ACTIVE-ACTIVE and can not exceed 2 if HA mode is ACTIVE-STANDBY.

            * cluster_id (str):
                Cluster on which the edge needs to be deployed.

            * edge_node_name (str):
                Edge Node Name.

            * edge_tep1_ip (str, Optional):
                Edge TEP 1 IP. Defaults to None.

            * edge_tep2_ip (str, Optional):
                Edge TEP 2 IP. Defaults to None.

            * edge_tep_gateway (str, Optional):
                Edge TEP Gateway IP. Defaults to None.

            * edge_tep_ip_address_pool (dict[str, Any], Optional):
                edgeTepIpAddressPool. Defaults to None.

                * name (str, Optional):
                    NSX IP Pool Name. Defaults to None.

                * nsx_id (str, Optional):
                    NSX IP Pool ID. Defaults to None.

            * edge_tep_vlan (int):
                Edge TEP VLAN.

            * first_nsx_vds_uplink (str, Optional):
                First NSX enabled VDS uplink for the Edge node. Defaults to None.

            * inter_rack_cluster (bool):
                Is inter-rack cluster(true for L2 non-uniform and L3 : At least one of management, uplink, Edge and host TEP networks is different for hosts of the cluster, false for L2 uniform :   All hosts in cluster have identical management, uplink, Edge and host TEP networks).

            * management_gateway (str):
                Management Gateway IP.

            * management_ip (str):
                Management Interface IP.

            * vm_management_portgroup_vlan (int, Optional):
                Management Vlan Id. Defaults to None.

            * vm_management_portgroup_name (str, Optional):
                Management Network Name. Defaults to None.

            * second_nsx_vds_uplink (str, Optional):
                Second NSX enabled VDS uplink for the Edge node. Defaults to None.

            * uplink_network (List[dict[str, Any]], Optional):
                Specifications of Tier0 uplinks for the Edge Node. Defaults to None.

                * asn_peer (int, Optional):
                    [Deprecated] ASN of Peer (please use bgpPeers instead). Defaults to None.

                * bgp_peer_password (str, Optional):
                    [Deprecated] BGP Peer Password (please use bgpPeers instead). Defaults to None.

                * bgp_peers (List[dict[str, Any]], Optional):
                    List of BGP Peer configuration. Defaults to None.

                    * asn (int):
                        ASN of Peer.

                    * ip (str):
                        BGP Peer IP.

                    * password (str):
                        BGP Peer Password.

                * peer_ip (str, Optional):
                    [Deprecated] BGP Peer IP (please use bgpPeers instead). Defaults to None.

                * uplink_interface_ip (str):
                    Uplink IP.

                * uplink_vlan (int):
                    Uplink Vlan.

        edge_root_password(str):
            Edge Password for root user.

        mtu(int):
            Maximum transmission unit.

        asn(int, Optional):
            ASN to be used for the edge cluster. Defaults to None.

        new_ip_address_pool_specs(List[dict[str, Any]], Optional):
            Specifications for new NSX IP address pool(s). Defaults to None.

            * description (str, Optional):
                Description of the IP address pool. Defaults to None.

            * ignore_unavailable_nsxt_cluster (bool, Optional):
                Ignore unavailable NSX cluster(s) during IP pool spec validation. Defaults to None.

            * name (str):
                Name of the IP address pool.

            * subnets (List[dict[str, Any]], Optional):
                List of IP address pool subnet specification. Defaults to None.

                * cidr (str):
                    The subnet representation, contains the network address and the prefix length.

                * gateway (str):
                    The default gateway address of the network.

                * ip_address_pool_ranges (List[dict[str, Any]]):
                    List of the IP allocation ranges. Atleast 1 IP address range has to be specified.

                    * end (str):
                        The last IP Address of the IP Address Range.

                    * start (str):
                        The first IP Address of the IP Address Range.

        internal_transit_subnets(List[str], Optional):
            Subnet addresses in CIDR notation that are used to assign addresses to logical links connecting service routers and distributed routers. Defaults to None.

        skip_tep_routability_check(bool, Optional):
            Set to true to bypass normal ICMP-based check of Edge TEP / host TEP routability (default is false, meaning do check). Defaults to None.

        tier0_name(str, Optional):
            Name for the Tier-0. Defaults to None.

        tier0_routing_type(str, Optional):
            Tier 0 Routing type -eg eBGP, Static. Defaults to None.

        tier0_services_high_availability(str, Optional):
            High-availability Mode for Tier-0. Defaults to None.

        tier1_name(str, Optional):
            Name for the Tier-1. Defaults to None.

        tier1_unhosted(bool, Optional):
            Select whether Tier-1 being created per this spec is hosted on the new Edge cluster or not (default value is false, meaning hosted). Defaults to None.

        transit_subnets(List[str], Optional):
            Transit subnet addresses in CIDR notation that are used to assign addresses to logical links connecting Tier-0 and Tier-1s. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "asn": "asn",
        "edge_admin_password": "edgeAdminPassword",
        "edge_audit_password": "edgeAuditPassword",
        "edge_cluster_name": "edgeClusterName",
        "edge_cluster_profile_spec": "edgeClusterProfileSpec",
        "edge_cluster_profile_type": "edgeClusterProfileType",
        "edge_cluster_type": "edgeClusterType",
        "edge_form_factor": "edgeFormFactor",
        "edge_node_specs": "edgeNodeSpecs",
        "new_ip_address_pool_specs": "newIpAddressPoolSpecs",
        "edge_root_password": "edgeRootPassword",
        "internal_transit_subnets": "internalTransitSubnets",
        "mtu": "mtu",
        "skip_tep_routability_check": "skipTepRoutabilityCheck",
        "tier0_name": "tier0Name",
        "tier0_routing_type": "tier0RoutingType",
        "tier0_services_high_availability": "tier0ServicesHighAvailability",
        "tier1_name": "tier1Name",
        "tier1_unhosted": "tier1Unhosted",
        "transit_subnets": "transitSubnets",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/edge-clusters".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "errors": "errors",
            "id": "id",
            "isCancellable": "is_cancellable",
            "isRetryable": "is_retryable",
            "localizableDescriptionPack": "localizable_description_pack",
            "name": "name",
            "resolutionStatus": "resolution_status",
            "resources": "resources",
            "status": "status",
            "subTasks": "sub_tasks",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def validate_edge_cluster_creation_spec(
    hub,
    ctx,
    edge_admin_password: str,
    edge_audit_password: str,
    edge_cluster_name: str,
    edge_cluster_profile_spec: make_dataclass(
        "edge_cluster_profile_spec",
        [
            ("bfd_allowed_hop", int),
            ("bfd_declare_dead_multiple", int),
            ("bfd_probe_interval", int),
            ("edge_cluster_profile_name", str),
            ("standby_relocation_threshold", int),
        ],
    ),
    edge_cluster_profile_type: str,
    edge_cluster_type: str,
    edge_form_factor: str,
    edge_node_specs: List[
        make_dataclass(
            "edge_node_specs",
            [
                ("cluster_id", str),
                ("edge_node_name", str),
                ("edge_tep_vlan", int),
                ("inter_rack_cluster", bool),
                ("management_gateway", str),
                ("management_ip", str),
                ("edge_tep1_ip", str, field(default=None)),
                ("edge_tep2_ip", str, field(default=None)),
                ("edge_tep_gateway", str, field(default=None)),
                (
                    "edge_tep_ip_address_pool",
                    make_dataclass(
                        "edge_tep_ip_address_pool",
                        [
                            ("name", str, field(default=None)),
                            ("nsx_id", str, field(default=None)),
                        ],
                    ),
                    field(default=None),
                ),
                ("first_nsx_vds_uplink", str, field(default=None)),
                ("vm_management_portgroup_vlan", int, field(default=None)),
                ("vm_management_portgroup_name", str, field(default=None)),
                ("second_nsx_vds_uplink", str, field(default=None)),
                (
                    "uplink_network",
                    List[
                        make_dataclass(
                            "uplink_network",
                            [
                                ("uplink_interface_ip", str),
                                ("uplink_vlan", int),
                                ("asn_peer", int, field(default=None)),
                                ("bgp_peer_password", str, field(default=None)),
                                (
                                    "bgp_peers",
                                    List[
                                        make_dataclass(
                                            "bgp_peers",
                                            [
                                                ("asn", int),
                                                ("ip", str),
                                                ("password", str),
                                            ],
                                        )
                                    ],
                                    field(default=None),
                                ),
                                ("peer_ip", str, field(default=None)),
                            ],
                        )
                    ],
                    field(default=None),
                ),
            ],
        )
    ],
    edge_root_password: str,
    mtu: int,
    asn: int = None,
    new_ip_address_pool_specs: List[
        make_dataclass(
            "new_ip_address_pool_specs",
            [
                ("name", str),
                ("description", str, field(default=None)),
                ("ignore_unavailable_nsxt_cluster", bool, field(default=None)),
                (
                    "subnets",
                    List[
                        make_dataclass(
                            "subnets",
                            [
                                ("cidr", str),
                                ("gateway", str),
                                (
                                    "ip_address_pool_ranges",
                                    List[
                                        make_dataclass(
                                            "ip_address_pool_ranges",
                                            [("end", str), ("start", str)],
                                        )
                                    ],
                                ),
                            ],
                        )
                    ],
                    field(default=None),
                ),
            ],
        )
    ] = None,
    internal_transit_subnets: List[str] = None,
    skip_tep_routability_check: bool = None,
    tier0_name: str = None,
    tier0_routing_type: str = None,
    tier0_services_high_availability: str = None,
    tier1_name: str = None,
    tier1_unhosted: bool = None,
    transit_subnets: List[str] = None,
) -> Dict[str, Any]:
    r"""

    Perform validiation of the EdgeClusterCreationSpec specification
        None

    Args:
        edge_admin_password(str):
            Edge Password for admin user.

        edge_audit_password(str):
            Edge Password for audit.

        edge_cluster_name(str):
            Name for the edge cluster.

        edge_cluster_profile_spec(dict[str, Any]):
            edgeClusterProfileSpec.

            * bfd_allowed_hop (int):
                BFD allowed Hop.

            * bfd_declare_dead_multiple (int):
                BFD Declare Dead Multiple.

            * bfd_probe_interval (int):
                BFD Probe.

            * edge_cluster_profile_name (str):
                Name for the edge cluster profile.

            * standby_relocation_threshold (int):
                Standby Relocation Threshold.

        edge_cluster_profile_type(str):
            Type of edge cluster profile.

        edge_cluster_type(str):
            Type of edge cluster.

        edge_form_factor(str):
            Edge Form Factor.

        edge_node_specs(List[dict[str, Any]]):
            Specifications for Edge Node. Number of Edge Nodes cannot exceed 8 if HA mode is ACTIVE-ACTIVE and can not exceed 2 if HA mode is ACTIVE-STANDBY.

            * cluster_id (str):
                Cluster on which the edge needs to be deployed.

            * edge_node_name (str):
                Edge Node Name.

            * edge_tep1_ip (str, Optional):
                Edge TEP 1 IP. Defaults to None.

            * edge_tep2_ip (str, Optional):
                Edge TEP 2 IP. Defaults to None.

            * edge_tep_gateway (str, Optional):
                Edge TEP Gateway IP. Defaults to None.

            * edge_tep_ip_address_pool (dict[str, Any], Optional):
                edgeTepIpAddressPool. Defaults to None.

                * name (str, Optional):
                    NSX IP Pool Name. Defaults to None.

                * nsx_id (str, Optional):
                    NSX IP Pool ID. Defaults to None.

            * edge_tep_vlan (int):
                Edge TEP VLAN.

            * first_nsx_vds_uplink (str, Optional):
                First NSX enabled VDS uplink for the Edge node. Defaults to None.

            * inter_rack_cluster (bool):
                Is inter-rack cluster(true for L2 non-uniform and L3 : At least one of management, uplink, Edge and host TEP networks is different for hosts of the cluster, false for L2 uniform :   All hosts in cluster have identical management, uplink, Edge and host TEP networks).

            * management_gateway (str):
                Management Gateway IP.

            * management_ip (str):
                Management Interface IP.

            * vm_management_portgroup_vlan (int, Optional):
                Management Vlan Id. Defaults to None.

            * vm_management_portgroup_name (str, Optional):
                Management Network Name. Defaults to None.

            * second_nsx_vds_uplink (str, Optional):
                Second NSX enabled VDS uplink for the Edge node. Defaults to None.

            * uplink_network (List[dict[str, Any]], Optional):
                Specifications of Tier0 uplinks for the Edge Node. Defaults to None.

                * asn_peer (int, Optional):
                    [Deprecated] ASN of Peer (please use bgpPeers instead). Defaults to None.

                * bgp_peer_password (str, Optional):
                    [Deprecated] BGP Peer Password (please use bgpPeers instead). Defaults to None.

                * bgp_peers (List[dict[str, Any]], Optional):
                    List of BGP Peer configuration. Defaults to None.

                    * asn (int):
                        ASN of Peer.

                    * ip (str):
                        BGP Peer IP.

                    * password (str):
                        BGP Peer Password.

                * peer_ip (str, Optional):
                    [Deprecated] BGP Peer IP (please use bgpPeers instead). Defaults to None.

                * uplink_interface_ip (str):
                    Uplink IP.

                * uplink_vlan (int):
                    Uplink Vlan.

        edge_root_password(str):
            Edge Password for root user.

        mtu(int):
            Maximum transmission unit.

        asn(int, Optional):
            ASN to be used for the edge cluster. Defaults to None.

        new_ip_address_pool_specs(List[dict[str, Any]], Optional):
            Specifications for new NSX IP address pool(s). Defaults to None.

            * description (str, Optional):
                Description of the IP address pool. Defaults to None.

            * ignore_unavailable_nsxt_cluster (bool, Optional):
                Ignore unavailable NSX cluster(s) during IP pool spec validation. Defaults to None.

            * name (str):
                Name of the IP address pool.

            * subnets (List[dict[str, Any]], Optional):
                List of IP address pool subnet specification. Defaults to None.

                * cidr (str):
                    The subnet representation, contains the network address and the prefix length.

                * gateway (str):
                    The default gateway address of the network.

                * ip_address_pool_ranges (List[dict[str, Any]]):
                    List of the IP allocation ranges. Atleast 1 IP address range has to be specified.

                    * end (str):
                        The last IP Address of the IP Address Range.

                    * start (str):
                        The first IP Address of the IP Address Range.

        internal_transit_subnets(List[str], Optional):
            Subnet addresses in CIDR notation that are used to assign addresses to logical links connecting service routers and distributed routers. Defaults to None.

        skip_tep_routability_check(bool, Optional):
            Set to true to bypass normal ICMP-based check of Edge TEP / host TEP routability (default is false, meaning do check). Defaults to None.

        tier0_name(str, Optional):
            Name for the Tier-0. Defaults to None.

        tier0_routing_type(str, Optional):
            Tier 0 Routing type -eg eBGP, Static. Defaults to None.

        tier0_services_high_availability(str, Optional):
            High-availability Mode for Tier-0. Defaults to None.

        tier1_name(str, Optional):
            Name for the Tier-1. Defaults to None.

        tier1_unhosted(bool, Optional):
            Select whether Tier-1 being created per this spec is hosted on the new Edge cluster or not (default value is false, meaning hosted). Defaults to None.

        transit_subnets(List[str], Optional):
            Transit subnet addresses in CIDR notation that are used to assign addresses to logical links connecting Tier-0 and Tier-1s. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "asn": "asn",
        "edge_admin_password": "edgeAdminPassword",
        "edge_audit_password": "edgeAuditPassword",
        "edge_cluster_name": "edgeClusterName",
        "edge_cluster_profile_spec": "edgeClusterProfileSpec",
        "edge_cluster_profile_type": "edgeClusterProfileType",
        "edge_cluster_type": "edgeClusterType",
        "edge_form_factor": "edgeFormFactor",
        "edge_node_specs": "edgeNodeSpecs",
        "new_ip_address_pool_specs": "newIpAddressPoolSpecs",
        "edge_root_password": "edgeRootPassword",
        "internal_transit_subnets": "internalTransitSubnets",
        "mtu": "mtu",
        "skip_tep_routability_check": "skipTepRoutabilityCheck",
        "tier0_name": "tier0Name",
        "tier0_routing_type": "tier0RoutingType",
        "tier0_services_high_availability": "tier0ServicesHighAvailability",
        "tier1_name": "tier1Name",
        "tier1_unhosted": "tier1Unhosted",
        "transit_subnets": "transitSubnets",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/edge-clusters/validations".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "additionalProperties": "additional_properties",
            "description": "description",
            "executionStatus": "execution_status",
            "id": "id",
            "resultStatus": "result_status",
            "validationChecks": "validation_checks",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_edge_cluster_validation_by_id(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Retrieve the results of a NSX Edge Cluster validation by its ID
        None

    Args:
        id_(str):
            The validation ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/edge-clusters/validations/{id}".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "additionalProperties": "additional_properties",
            "description": "description",
            "executionStatus": "execution_status",
            "id": "id",
            "resultStatus": "result_status",
            "validationChecks": "validation_checks",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_edge_cluster(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Retrieve an NSX Edge Cluster by its ID
        None

    Args:
        id_(str):
            Edge Cluster ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/edge-clusters/{id}".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "clusters": "clusters",
            "edgeNodes": "edge_nodes",
            "id": "id",
            "isTier0ManagedBySystem": "is_tier0_managed_by_system",
            "name": "name",
            "nsxtCluster": "nsxt_cluster",
            "skipTepRoutabilityCheck": "skip_tep_routability_check",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def update_edge_cluster(
    hub,
    ctx,
    id_: str,
    operation: str,
    edge_cluster_expansion_spec: make_dataclass(
        "edge_cluster_expansion_spec",
        [
            ("edge_node_admin_password", str),
            ("edge_node_audit_password", str),
            ("edge_node_root_password", str),
            (
                "edge_node_specs",
                List[
                    make_dataclass(
                        "edge_node_specs",
                        [
                            ("cluster_id", str),
                            ("edge_node_name", str),
                            ("edge_tep_vlan", int),
                            ("inter_rack_cluster", bool),
                            ("management_gateway", str),
                            ("management_ip", str),
                            ("edge_tep1_ip", str, field(default=None)),
                            ("edge_tep2_ip", str, field(default=None)),
                            ("edge_tep_gateway", str, field(default=None)),
                            (
                                "edge_tep_ip_address_pool",
                                make_dataclass(
                                    "edge_tep_ip_address_pool",
                                    [
                                        ("name", str, field(default=None)),
                                        ("nsx_id", str, field(default=None)),
                                    ],
                                ),
                                field(default=None),
                            ),
                            ("first_nsx_vds_uplink", str, field(default=None)),
                            ("vm_management_portgroup_vlan", int, field(default=None)),
                            ("vm_management_portgroup_name", str, field(default=None)),
                            ("second_nsx_vds_uplink", str, field(default=None)),
                            (
                                "uplink_network",
                                List[
                                    make_dataclass(
                                        "uplink_network",
                                        [
                                            ("uplink_interface_ip", str),
                                            ("uplink_vlan", int),
                                            ("asn_peer", int, field(default=None)),
                                            (
                                                "bgp_peer_password",
                                                str,
                                                field(default=None),
                                            ),
                                            (
                                                "bgp_peers",
                                                List[
                                                    make_dataclass(
                                                        "bgp_peers",
                                                        [
                                                            ("asn", int),
                                                            ("ip", str),
                                                            ("password", str),
                                                        ],
                                                    )
                                                ],
                                                field(default=None),
                                            ),
                                            ("peer_ip", str, field(default=None)),
                                        ],
                                    )
                                ],
                                field(default=None),
                            ),
                        ],
                    )
                ],
            ),
            ("additional_tier1_names", List[str], field(default=None)),
            (
                "new_ip_address_pool_specs",
                List[
                    make_dataclass(
                        "new_ip_address_pool_specs",
                        [
                            ("name", str),
                            ("description", str, field(default=None)),
                            (
                                "ignore_unavailable_nsxt_cluster",
                                bool,
                                field(default=None),
                            ),
                            (
                                "subnets",
                                List[
                                    make_dataclass(
                                        "subnets",
                                        [
                                            ("cidr", str),
                                            ("gateway", str),
                                            (
                                                "ip_address_pool_ranges",
                                                List[
                                                    make_dataclass(
                                                        "ip_address_pool_ranges",
                                                        [("end", str), ("start", str)],
                                                    )
                                                ],
                                            ),
                                        ],
                                    )
                                ],
                                field(default=None),
                            ),
                        ],
                    )
                ],
                field(default=None),
            ),
            ("skip_tep_routability_check", bool, field(default=None)),
            ("tier1_unhosted", bool, field(default=None)),
        ],
    ) = None,
    edge_cluster_shrinkage_spec: make_dataclass(
        "edge_cluster_shrinkage_spec", [("edge_node_ids", List[str])]
    ) = None,
) -> Dict[str, Any]:
    r"""

    Expand or shrink an NSX Edge Cluster
        None

    Args:
        id_(str):
            Edge Cluster ID.

        operation(str):
            Edge cluster operation Type.

        edge_cluster_expansion_spec(dict[str, Any], Optional):
            edgeClusterExpansionSpec. Defaults to None.

            * additional_tier1_names (List[str], Optional):
                List of names for the additional Tier-1(s) to be created during expansion. Defaults to None.

            * edge_node_admin_password (str):
                Edge Password for admin user.

            * edge_node_audit_password (str):
                Edge Password for audit user.

            * edge_node_root_password (str):
                Edge Password for root user.

            * edge_node_specs (List[dict[str, Any]]):
                Specifications for Edge Node.

                * cluster_id (str):
                    Cluster on which the edge needs to be deployed.

                * edge_node_name (str):
                    Edge Node Name.

                * edge_tep1_ip (str, Optional):
                    Edge TEP 1 IP. Defaults to None.

                * edge_tep2_ip (str, Optional):
                    Edge TEP 2 IP. Defaults to None.

                * edge_tep_gateway (str, Optional):
                    Edge TEP Gateway IP. Defaults to None.

                * edge_tep_ip_address_pool (dict[str, Any], Optional):
                    edgeTepIpAddressPool. Defaults to None.

                    * name (str, Optional):
                        NSX IP Pool Name. Defaults to None.

                    * nsx_id (str, Optional):
                        NSX IP Pool ID. Defaults to None.

                * edge_tep_vlan (int):
                    Edge TEP VLAN.

                * first_nsx_vds_uplink (str, Optional):
                    First NSX enabled VDS uplink for the Edge node. Defaults to None.

                * inter_rack_cluster (bool):
                    Is inter-rack cluster(true for L2 non-uniform and L3 : At least one of management, uplink, Edge and host TEP networks is different for hosts of the cluster, false for L2 uniform :   All hosts in cluster have identical management, uplink, Edge and host TEP networks).

                * management_gateway (str):
                    Management Gateway IP.

                * management_ip (str):
                    Management Interface IP.

                * vm_management_portgroup_vlan (int, Optional):
                    Management Vlan Id. Defaults to None.

                * vm_management_portgroup_name (str, Optional):
                    Management Network Name. Defaults to None.

                * second_nsx_vds_uplink (str, Optional):
                    Second NSX enabled VDS uplink for the Edge node. Defaults to None.

                * uplink_network (List[dict[str, Any]], Optional):
                    Specifications of Tier0 uplinks for the Edge Node. Defaults to None.

                    * asn_peer (int, Optional):
                        [Deprecated] ASN of Peer (please use bgpPeers instead). Defaults to None.

                    * bgp_peer_password (str, Optional):
                        [Deprecated] BGP Peer Password (please use bgpPeers instead). Defaults to None.

                    * bgp_peers (List[dict[str, Any]], Optional):
                        List of BGP Peer configuration. Defaults to None.

                        * asn (int):
                            ASN of Peer.

                        * ip (str):
                            BGP Peer IP.

                        * password (str):
                            BGP Peer Password.

                    * peer_ip (str, Optional):
                        [Deprecated] BGP Peer IP (please use bgpPeers instead). Defaults to None.

                    * uplink_interface_ip (str):
                        Uplink IP.

                    * uplink_vlan (int):
                        Uplink Vlan.

            * new_ip_address_pool_specs (List[dict[str, Any]], Optional):
                Specifications for new NSX IP address pool(s). Defaults to None.

                * description (str, Optional):
                    Description of the IP address pool. Defaults to None.

                * ignore_unavailable_nsxt_cluster (bool, Optional):
                    Ignore unavailable NSX cluster(s) during IP pool spec validation. Defaults to None.

                * name (str):
                    Name of the IP address pool.

                * subnets (List[dict[str, Any]], Optional):
                    List of IP address pool subnet specification. Defaults to None.

                    * cidr (str):
                        The subnet representation, contains the network address and the prefix length.

                    * gateway (str):
                        The default gateway address of the network.

                    * ip_address_pool_ranges (List[dict[str, Any]]):
                        List of the IP allocation ranges. Atleast 1 IP address range has to be specified.

                        * end (str):
                            The last IP Address of the IP Address Range.

                        * start (str):
                            The first IP Address of the IP Address Range.

            * skip_tep_routability_check (bool, Optional):
                Set to true to bypass normal ICMP-based check of Edge TEP / host TEP routability (default is false, meaning do check). Defaults to None.

            * tier1_unhosted (bool, Optional):
                Select whether all Tier-1(s) being created per this spec are hosted on the Edge cluster or not (default is false, meaning hosted). Defaults to None.

        edge_cluster_shrinkage_spec(dict[str, Any], Optional):
            edgeClusterShrinkageSpec. Defaults to None.

            * edge_node_ids (List[str]):
                List of VCF Edge Node ID's to be removed for shrinkage.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "edge_cluster_expansion_spec": "edgeClusterExpansionSpec",
        "edge_cluster_shrinkage_spec": "edgeClusterShrinkageSpec",
        "operation": "operation",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="patch",
        path="/v1/edge-clusters/{id}".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "errors": "errors",
            "id": "id",
            "isCancellable": "is_cancellable",
            "isRetryable": "is_retryable",
            "localizableDescriptionPack": "localizable_description_pack",
            "name": "name",
            "resolutionStatus": "resolution_status",
            "resources": "resources",
            "status": "status",
            "subTasks": "sub_tasks",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def validate_edge_cluster_update_spec(
    hub,
    ctx,
    id_: str,
    operation: str,
    edge_cluster_expansion_spec: make_dataclass(
        "edge_cluster_expansion_spec",
        [
            ("edge_node_admin_password", str),
            ("edge_node_audit_password", str),
            ("edge_node_root_password", str),
            (
                "edge_node_specs",
                List[
                    make_dataclass(
                        "edge_node_specs",
                        [
                            ("cluster_id", str),
                            ("edge_node_name", str),
                            ("edge_tep_vlan", int),
                            ("inter_rack_cluster", bool),
                            ("management_gateway", str),
                            ("management_ip", str),
                            ("edge_tep1_ip", str, field(default=None)),
                            ("edge_tep2_ip", str, field(default=None)),
                            ("edge_tep_gateway", str, field(default=None)),
                            (
                                "edge_tep_ip_address_pool",
                                make_dataclass(
                                    "edge_tep_ip_address_pool",
                                    [
                                        ("name", str, field(default=None)),
                                        ("nsx_id", str, field(default=None)),
                                    ],
                                ),
                                field(default=None),
                            ),
                            ("first_nsx_vds_uplink", str, field(default=None)),
                            ("vm_management_portgroup_vlan", int, field(default=None)),
                            ("vm_management_portgroup_name", str, field(default=None)),
                            ("second_nsx_vds_uplink", str, field(default=None)),
                            (
                                "uplink_network",
                                List[
                                    make_dataclass(
                                        "uplink_network",
                                        [
                                            ("uplink_interface_ip", str),
                                            ("uplink_vlan", int),
                                            ("asn_peer", int, field(default=None)),
                                            (
                                                "bgp_peer_password",
                                                str,
                                                field(default=None),
                                            ),
                                            (
                                                "bgp_peers",
                                                List[
                                                    make_dataclass(
                                                        "bgp_peers",
                                                        [
                                                            ("asn", int),
                                                            ("ip", str),
                                                            ("password", str),
                                                        ],
                                                    )
                                                ],
                                                field(default=None),
                                            ),
                                            ("peer_ip", str, field(default=None)),
                                        ],
                                    )
                                ],
                                field(default=None),
                            ),
                        ],
                    )
                ],
            ),
            ("additional_tier1_names", List[str], field(default=None)),
            (
                "new_ip_address_pool_specs",
                List[
                    make_dataclass(
                        "new_ip_address_pool_specs",
                        [
                            ("name", str),
                            ("description", str, field(default=None)),
                            (
                                "ignore_unavailable_nsxt_cluster",
                                bool,
                                field(default=None),
                            ),
                            (
                                "subnets",
                                List[
                                    make_dataclass(
                                        "subnets",
                                        [
                                            ("cidr", str),
                                            ("gateway", str),
                                            (
                                                "ip_address_pool_ranges",
                                                List[
                                                    make_dataclass(
                                                        "ip_address_pool_ranges",
                                                        [("end", str), ("start", str)],
                                                    )
                                                ],
                                            ),
                                        ],
                                    )
                                ],
                                field(default=None),
                            ),
                        ],
                    )
                ],
                field(default=None),
            ),
            ("skip_tep_routability_check", bool, field(default=None)),
            ("tier1_unhosted", bool, field(default=None)),
        ],
    ) = None,
    edge_cluster_shrinkage_spec: make_dataclass(
        "edge_cluster_shrinkage_spec", [("edge_node_ids", List[str])]
    ) = None,
) -> Dict[str, Any]:
    r"""

    Perform validation of the EdgeClusterUpdateSpec specification
        None

    Args:
        id_(str):
            NSX Edge cluster id.

        operation(str):
            Edge cluster operation Type.

        edge_cluster_expansion_spec(dict[str, Any], Optional):
            edgeClusterExpansionSpec. Defaults to None.

            * additional_tier1_names (List[str], Optional):
                List of names for the additional Tier-1(s) to be created during expansion. Defaults to None.

            * edge_node_admin_password (str):
                Edge Password for admin user.

            * edge_node_audit_password (str):
                Edge Password for audit user.

            * edge_node_root_password (str):
                Edge Password for root user.

            * edge_node_specs (List[dict[str, Any]]):
                Specifications for Edge Node.

                * cluster_id (str):
                    Cluster on which the edge needs to be deployed.

                * edge_node_name (str):
                    Edge Node Name.

                * edge_tep1_ip (str, Optional):
                    Edge TEP 1 IP. Defaults to None.

                * edge_tep2_ip (str, Optional):
                    Edge TEP 2 IP. Defaults to None.

                * edge_tep_gateway (str, Optional):
                    Edge TEP Gateway IP. Defaults to None.

                * edge_tep_ip_address_pool (dict[str, Any], Optional):
                    edgeTepIpAddressPool. Defaults to None.

                    * name (str, Optional):
                        NSX IP Pool Name. Defaults to None.

                    * nsx_id (str, Optional):
                        NSX IP Pool ID. Defaults to None.

                * edge_tep_vlan (int):
                    Edge TEP VLAN.

                * first_nsx_vds_uplink (str, Optional):
                    First NSX enabled VDS uplink for the Edge node. Defaults to None.

                * inter_rack_cluster (bool):
                    Is inter-rack cluster(true for L2 non-uniform and L3 : At least one of management, uplink, Edge and host TEP networks is different for hosts of the cluster, false for L2 uniform :   All hosts in cluster have identical management, uplink, Edge and host TEP networks).

                * management_gateway (str):
                    Management Gateway IP.

                * management_ip (str):
                    Management Interface IP.

                * vm_management_portgroup_vlan (int, Optional):
                    Management Vlan Id. Defaults to None.

                * vm_management_portgroup_name (str, Optional):
                    Management Network Name. Defaults to None.

                * second_nsx_vds_uplink (str, Optional):
                    Second NSX enabled VDS uplink for the Edge node. Defaults to None.

                * uplink_network (List[dict[str, Any]], Optional):
                    Specifications of Tier0 uplinks for the Edge Node. Defaults to None.

                    * asn_peer (int, Optional):
                        [Deprecated] ASN of Peer (please use bgpPeers instead). Defaults to None.

                    * bgp_peer_password (str, Optional):
                        [Deprecated] BGP Peer Password (please use bgpPeers instead). Defaults to None.

                    * bgp_peers (List[dict[str, Any]], Optional):
                        List of BGP Peer configuration. Defaults to None.

                        * asn (int):
                            ASN of Peer.

                        * ip (str):
                            BGP Peer IP.

                        * password (str):
                            BGP Peer Password.

                    * peer_ip (str, Optional):
                        [Deprecated] BGP Peer IP (please use bgpPeers instead). Defaults to None.

                    * uplink_interface_ip (str):
                        Uplink IP.

                    * uplink_vlan (int):
                        Uplink Vlan.

            * new_ip_address_pool_specs (List[dict[str, Any]], Optional):
                Specifications for new NSX IP address pool(s). Defaults to None.

                * description (str, Optional):
                    Description of the IP address pool. Defaults to None.

                * ignore_unavailable_nsxt_cluster (bool, Optional):
                    Ignore unavailable NSX cluster(s) during IP pool spec validation. Defaults to None.

                * name (str):
                    Name of the IP address pool.

                * subnets (List[dict[str, Any]], Optional):
                    List of IP address pool subnet specification. Defaults to None.

                    * cidr (str):
                        The subnet representation, contains the network address and the prefix length.

                    * gateway (str):
                        The default gateway address of the network.

                    * ip_address_pool_ranges (List[dict[str, Any]]):
                        List of the IP allocation ranges. Atleast 1 IP address range has to be specified.

                        * end (str):
                            The last IP Address of the IP Address Range.

                        * start (str):
                            The first IP Address of the IP Address Range.

            * skip_tep_routability_check (bool, Optional):
                Set to true to bypass normal ICMP-based check of Edge TEP / host TEP routability (default is false, meaning do check). Defaults to None.

            * tier1_unhosted (bool, Optional):
                Select whether all Tier-1(s) being created per this spec are hosted on the Edge cluster or not (default is false, meaning hosted). Defaults to None.

        edge_cluster_shrinkage_spec(dict[str, Any], Optional):
            edgeClusterShrinkageSpec. Defaults to None.

            * edge_node_ids (List[str]):
                List of VCF Edge Node ID's to be removed for shrinkage.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "edge_cluster_expansion_spec": "edgeClusterExpansionSpec",
        "edge_cluster_shrinkage_spec": "edgeClusterShrinkageSpec",
        "operation": "operation",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/edge-clusters/{id}/validations".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "additionalProperties": "additional_properties",
            "description": "description",
            "executionStatus": "execution_status",
            "id": "id",
            "resultStatus": "result_status",
            "validationChecks": "validation_checks",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result

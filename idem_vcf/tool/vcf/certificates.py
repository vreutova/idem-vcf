"""Utility functions for Certificateses. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List


async def get_certificate_authorities(hub, ctx) -> Dict[str, Any]:
    r"""

    Retrieve a list of Certificate Authorities
        Get certificate authorities information


    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/certificate-authorities".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def create_certificate_authority(
    hub,
    ctx,
    microsoft_certificate_authority_spec: make_dataclass(
        "microsoft_certificate_authority_spec",
        [
            ("secret", str),
            ("server_url", str),
            ("template_name", str),
            ("username", str),
        ],
    ) = None,
    open_ssl_certificate_authority_spec: make_dataclass(
        "open_ssl_certificate_authority_spec",
        [
            ("common_name", str),
            ("country", str),
            ("locality", str),
            ("organization", str),
            ("organization_unit", str),
            ("state", str),
        ],
    ) = None,
) -> Dict[str, Any]:
    r"""

    Configure integration with a Certificate Authority
        Creates a certificate authority. This is required to generate signed certificates by supporting CAs.

    Args:
        microsoft_certificate_authority_spec(dict[str, Any], Optional):
            microsoftCertificateAuthoritySpec. Defaults to None.

            * secret (str):
                CA server password.

            * server_url (str):
                CA server url.

            * template_name (str):
                CA server template name.

            * username (str):
                CA server username.

        open_ssl_certificate_authority_spec(dict[str, Any], Optional):
            openSSLCertificateAuthoritySpec. Defaults to None.

            * common_name (str):
                OpenSSL CA domain name.

            * country (str):
                ISO 3166 country code where company is legally registered.

            * locality (str):
                The city or locality where company is legally registered.

            * organization (str):
                The name under which company is legally registered.

            * organization_unit (str):
                Organization with which the certificate is associated.

            * state (str):
                The full name of the state where company is legally registered.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "microsoft_certificate_authority_spec": "microsoftCertificateAuthoritySpec",
        "open_ssl_certificate_authority_spec": "openSSLCertificateAuthoritySpec",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="put",
        path="/v1/certificate-authorities".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def configure_certificate_authority(
    hub,
    ctx,
    microsoft_certificate_authority_spec: make_dataclass(
        "microsoft_certificate_authority_spec",
        [
            ("secret", str),
            ("server_url", str),
            ("template_name", str),
            ("username", str),
        ],
    ) = None,
    open_ssl_certificate_authority_spec: make_dataclass(
        "open_ssl_certificate_authority_spec",
        [
            ("common_name", str),
            ("country", str),
            ("locality", str),
            ("organization", str),
            ("organization_unit", str),
            ("state", str),
        ],
    ) = None,
) -> Dict[str, Any]:
    r"""

    Update the configuration of a Certificate Authority
        Update the configuration of a Certificate Authority

    Args:
        microsoft_certificate_authority_spec(dict[str, Any], Optional):
            microsoftCertificateAuthoritySpec. Defaults to None.

            * secret (str):
                CA server password.

            * server_url (str):
                CA server url.

            * template_name (str):
                CA server template name.

            * username (str):
                CA server username.

        open_ssl_certificate_authority_spec(dict[str, Any], Optional):
            openSSLCertificateAuthoritySpec. Defaults to None.

            * common_name (str):
                OpenSSL CA domain name.

            * country (str):
                ISO 3166 country code where company is legally registered.

            * locality (str):
                The city or locality where company is legally registered.

            * organization (str):
                The name under which company is legally registered.

            * organization_unit (str):
                Organization with which the certificate is associated.

            * state (str):
                The full name of the state where company is legally registered.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "microsoft_certificate_authority_spec": "microsoftCertificateAuthoritySpec",
        "open_ssl_certificate_authority_spec": "openSSLCertificateAuthoritySpec",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="patch",
        path="/v1/certificate-authorities".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def get_certificate_authority_by_id(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Retrive the details of a Certificate Authority by ID
        Get certificate authority information

    Args:
        id_(str):
            The CA type.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/certificate-authorities/{id}".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "commonName": "common_name",
            "country": "country",
            "id": "id",
            "locality": "locality",
            "organization": "organization",
            "organizationUnit": "organization_unit",
            "serverUrl": "server_url",
            "state": "state",
            "templateName": "template_name",
            "username": "username",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def remove_certificate_authority(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Remove the configuration of a Certificate Authority
        Deletes CA configuration file

    Args:
        id_(str):
            The CA type.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="delete",
        path="/v1/certificate-authorities/{id}".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def replace_certificates(
    hub,
    ctx,
    id_: str,
    operation_type: str,
    resources: List[
        make_dataclass(
            "resources",
            [
                ("resource_id", str),
                ("type", str),
                ("fqdn", str, field(default=None)),
                ("name", str, field(default=None)),
                ("sans", List[str], field(default=None)),
            ],
        )
    ] = None,
) -> Dict[str, Any]:
    r"""

    Replace certificate(s) for the selected resource(s) in a domain
        Replace certificate(s) for the selected resource(s) in a domain
    DEPRECATED

    Args:
        id_(str):
            Domain ID or Name.

        operation_type(str):
            The Certificates operation type.

        resources(List[dict[str, Any]], Optional):
            Resource(s) in a domain. Defaults to None.

            * fqdn (str, Optional):
                Resource FQDN. Defaults to None.

            * name (str, Optional):
                Name of the resource. Defaults to None.

            * resource_id (str):
                Resource ID.

            * sans (List[str], Optional):
                Subject alternative name(s). Defaults to None.

            * type (str):
                Resource type.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"operation_type": "operationType", "resources": "resources"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="patch",
        path="/v1/domains/{id}/certificates".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "errors": "errors",
            "id": "id",
            "isCancellable": "is_cancellable",
            "isRetryable": "is_retryable",
            "localizableDescriptionPack": "localizable_description_pack",
            "name": "name",
            "resolutionStatus": "resolution_status",
            "resources": "resources",
            "status": "status",
            "subTasks": "sub_tasks",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def upload_certificates(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Import certificate(s) to the certificate store for a domain
        Upload certificates to the certificate store
    DEPRECATED

    Args:
        id_(str):
            Domain ID or Name.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="put",
        path="/v1/domains/{id}/certificates/uploads".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def get_cs_rs(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Request available CSR(s) in JSON format for a domain
        Get available CSR(s) in json format

    Args:
        id_(str):
            Domain ID or Name.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/domains/{id}/csrs".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def generates_cs_rs(
    hub,
    ctx,
    id_: str,
    csr_generation_spec: make_dataclass(
        "csr_generation_spec",
        [
            ("country", str),
            ("key_algorithm", str),
            ("key_size", str),
            ("locality", str),
            ("organization", str),
            ("organization_unit", str),
            ("state", str),
            ("email", str, field(default=None)),
        ],
    ),
    resources: List[
        make_dataclass(
            "resources",
            [
                ("resource_id", str),
                ("type", str),
                ("fqdn", str, field(default=None)),
                ("name", str, field(default=None)),
                ("sans", List[str], field(default=None)),
            ],
        )
    ] = None,
) -> Dict[str, Any]:
    r"""

    Request the creation of certificate signing request (CSR) files for resources of a domain
        Generate CSR(s) for the selected resource(s) in the domain.

    *Warning:*
    _Avoid using wildcard certificates. Instead, use subdomain-specific certificates that are rotated often. A compromised wildcard certificate can lead to security repercussions_

    Args:
        id_(str):
            Domain ID or Name.

        csr_generation_spec(dict[str, Any]):
            csrGenerationSpec.

            * country (str):
                ISO 3166 country code where company is legally registered.

            * email (str, Optional):
                Contact email address. Defaults to None.

            * key_algorithm (str):
                The public key algorithm of the certificate.

            * key_size (str):
                Certificate public key size.

            * locality (str):
                The city or locality where company is legally registered.

            * organization (str):
                The name under which company is legally registered.

            * organization_unit (str):
                Organization with which the certificate is associated.

            * state (str):
                The full name of the state where company is legally registered.

        resources(List[dict[str, Any]], Optional):
            Resource(s) in a domain. Defaults to None.

            * fqdn (str, Optional):
                Resource FQDN. Defaults to None.

            * name (str, Optional):
                Name of the resource. Defaults to None.

            * resource_id (str):
                Resource ID.

            * sans (List[str], Optional):
                Subject alternative name(s). Defaults to None.

            * type (str):
                Resource type.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"csr_generation_spec": "csrGenerationSpec", "resources": "resources"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="put",
        path="/v1/domains/{id}/csrs".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "errors": "errors",
            "id": "id",
            "isCancellable": "is_cancellable",
            "isRetryable": "is_retryable",
            "localizableDescriptionPack": "localizable_description_pack",
            "name": "name",
            "resolutionStatus": "resolution_status",
            "resources": "resources",
            "status": "status",
            "subTasks": "sub_tasks",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def download_csr(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Request the download of CSR(s) for a domain in tar.gz format
        Download available CSR(s) in tar.gz format
    DEPRECATED

    Args:
        id_(str):
            Domain ID or Name.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/domains/{id}/csrs/downloads".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def get_resource_certificates_validation_by_id(
    hub, ctx, id_: str, validation_id: str
) -> Dict[str, Any]:
    r"""

    Retrieve the results of a certificate validation by its ID
        Get the resource certificate validation result

    Args:
        id_(str):
            Domain ID.

        validation_id(str):
            Validation ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/domains/{id}/resource-certificates/validations/{validationId}".format(
            **{"id": id_, "validationId": validation_id}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "completed": "completed",
            "endTimestamp": "end_timestamp",
            "startTimestamp": "start_timestamp",
            "validationId": "validation_id",
            "validations": "validations",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result

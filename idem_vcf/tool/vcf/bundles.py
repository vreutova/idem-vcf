"""Utility functions for Bundleses. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict


async def update_bundle_compatibility_sets(
    hub,
    ctx,
    bundle_download_spec: make_dataclass(
        "bundle_download_spec",
        [
            ("download_now", bool, field(default=None)),
            ("scheduled_timestamp", str, field(default=None)),
        ],
    ) = None,
    compatibility_sets_file_path: str = None,
) -> Dict[str, Any]:
    r"""

    Update the software compatability set for all bundles
        Update software compatibility sets for Bundles
    DEPRECATED

    Args:
        bundle_download_spec(dict[str, Any], Optional):
            bundleDownloadSpec. Defaults to None.

            * download_now (bool, Optional):
                Flag for enabling Download Now. If true, scheduledTimestamp is ignored. Defaults to None.

            * scheduled_timestamp (str, Optional):
                Bundle Download Scheduled Time. Defaults to None.

        compatibility_sets_file_path(str, Optional):
            [Deprecated] Path to the software compatibility sets file. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "bundle_download_spec": "bundleDownloadSpec",
        "compatibility_sets_file_path": "compatibilitySetsFilePath",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="patch",
        path="/v1/bundles/".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def get_bundles_for_skip_upgrade(
    hub, ctx, id_: str, target_version: str = None
) -> Dict[str, Any]:
    r"""

    Retrieve a list of bundles for skip upgrade by domain ID
        Get bundles for skip upgrade a domain from current version to target version.

    Args:
        id_(str):
            Domain ID.

        target_version(str, Optional):
            [Deprecated] Target domain VCF version. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/bundles/domains/{id}".format(**{"id": id_}),
        query_params={"targetVersion": target_version},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result

"""Utility functions for Version Aliases For Bundle Component Types. """
from collections import OrderedDict
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List


async def get_version_alias_configuration(hub, ctx) -> Dict[str, Any]:
    r"""

    Retrieve Version Alias Configuration
        Get the Version Alias Configuration.


    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/system/settings/version-aliases".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def update_version_alias_configurations(
    hub,
    ctx,
    force_update: bool = None,
    target_vcf_version: str = None,
    version_aliases_for_bundle_component_types: List[
        make_dataclass(
            "version_aliases_for_bundle_component_types",
            [
                ("bundle_component_type", str),
                (
                    "version_aliases",
                    List[
                        make_dataclass(
                            "version_aliases",
                            [("aliases", List[str]), ("version", str)],
                        )
                    ],
                ),
            ],
        )
    ] = None,
) -> Dict[str, Any]:
    r"""

    Update Version Alias Configurations
        Update Version Alias Configurations.

    Args:
        force_update(bool, Optional):
            Flag for force update version alias configuration. Defaults to None.

        target_vcf_version(str, Optional):
            If this field is set, LCM will calculate and update version aliases based on current deployment VCF version and the provided target VCF version. Defaults to None.

        version_aliases_for_bundle_component_types(List[dict[str, Any]], Optional):
            List of version aliases for bundle component types. Defaults to None.

            * bundle_component_type (str):
                Bundle Component Type.

            * version_aliases (List[dict[str, Any]]):
                Version Aliases.

                * aliases (List[str]):
                    List of Alias versions.

                * version (str):
                    Base version.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "force_update": "forceUpdate",
        "target_vcf_version": "targetVcfVersion",
        "version_aliases_for_bundle_component_types": "versionAliasesForBundleComponentTypes",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="put",
        path="/v1/system/settings/version-aliases".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def delete_version_alias_by_software_type(
    hub, ctx, bundle_component_type: str
) -> Dict[str, Any]:
    r"""

    Removes Version Alias Configuration by component type
        Removes Version Alias Configuration by component type

    Args:
        bundle_component_type(str):
            Bundle Component Type.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="delete",
        path="/v1/system/settings/version-aliases/{bundleComponentType}".format(
            **{"bundleComponentType": bundle_component_type}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def update_version_alias_configuration(
    hub,
    ctx,
    bundle_component_type: str,
    version: str,
    aliases: List[str],
    force_update: bool = None,
) -> Dict[str, Any]:
    r"""

    Update the Version Alias Configuration by component type
        Update Version Alias Configuration.

    Args:
        bundle_component_type(str):
            Bundle Component Type.

        version(str):
            Version.

        aliases(List[str]):
            Aliases for bundle component type and version.

        force_update(bool, Optional):
            Flag for force update version alias configuration. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"aliases": "aliases", "force_update": "forceUpdate"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="put",
        path="/v1/system/settings/version-aliases/{bundleComponentType}/{version}".format(
            **{"bundleComponentType": bundle_component_type, "version": version}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def delete_alias_versions_by_software_type_and_base_version(
    hub, ctx, bundle_component_type: str, version: str
) -> Dict[str, Any]:
    r"""

    Removes Version Alias Configuration by component type and version
        Delete Version Alias Configuration by bundle component type, version, and aliases.

    Args:
        bundle_component_type(str):
            Bundle Component Type.

        version(str):
            Version.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="delete",
        path="/v1/system/settings/version-aliases/{bundleComponentType}/{version}".format(
            **{"bundleComponentType": bundle_component_type, "version": version}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result

"""Utility functions for Domainss. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List


async def validate_domain_creation_spec(
    hub,
    ctx,
    compute_spec: make_dataclass(
        "compute_spec",
        [
            (
                "cluster_specs",
                List[
                    make_dataclass(
                        "cluster_specs",
                        [
                            (
                                "datastore_spec",
                                make_dataclass(
                                    "datastore_spec",
                                    [
                                        (
                                            "nfs_datastore_specs",
                                            List[
                                                make_dataclass(
                                                    "nfs_datastore_specs",
                                                    [
                                                        ("datastore_name", str),
                                                        (
                                                            "nas_volume",
                                                            make_dataclass(
                                                                "nas_volume",
                                                                [
                                                                    ("path", str),
                                                                    ("read_only", bool),
                                                                    (
                                                                        "server_name",
                                                                        List[str],
                                                                    ),
                                                                    (
                                                                        "user_tag",
                                                                        str,
                                                                        field(
                                                                            default=None
                                                                        ),
                                                                    ),
                                                                ],
                                                            ),
                                                            field(default=None),
                                                        ),
                                                    ],
                                                )
                                            ],
                                            field(default=None),
                                        ),
                                        (
                                            "vmfs_datastore_spec",
                                            make_dataclass(
                                                "vmfs_datastore_spec",
                                                [
                                                    (
                                                        "fc_spec",
                                                        List[
                                                            make_dataclass(
                                                                "fc_spec",
                                                                [
                                                                    (
                                                                        "datastore_name",
                                                                        str,
                                                                    )
                                                                ],
                                                            )
                                                        ],
                                                        field(default=None),
                                                    )
                                                ],
                                            ),
                                            field(default=None),
                                        ),
                                        (
                                            "vsan_datastore_spec",
                                            make_dataclass(
                                                "vsan_datastore_spec",
                                                [
                                                    ("datastore_name", str),
                                                    (
                                                        "dedup_and_compression_enabled",
                                                        bool,
                                                        field(default=None),
                                                    ),
                                                    (
                                                        "esa_config",
                                                        make_dataclass(
                                                            "esa_config",
                                                            [("enabled", bool)],
                                                        ),
                                                        field(default=None),
                                                    ),
                                                    (
                                                        "failures_to_tolerate",
                                                        int,
                                                        field(default=None),
                                                    ),
                                                    (
                                                        "license_key",
                                                        str,
                                                        field(default=None),
                                                    ),
                                                ],
                                            ),
                                            field(default=None),
                                        ),
                                        (
                                            "vsan_remote_datastore_cluster_spec",
                                            make_dataclass(
                                                "vsan_remote_datastore_cluster_spec",
                                                [
                                                    (
                                                        "vsan_remote_datastore_spec",
                                                        List[
                                                            make_dataclass(
                                                                "vsan_remote_datastore_spec",
                                                                [
                                                                    (
                                                                        "datastore_uuid",
                                                                        str,
                                                                    )
                                                                ],
                                                            )
                                                        ],
                                                        field(default=None),
                                                    )
                                                ],
                                            ),
                                            field(default=None),
                                        ),
                                        (
                                            "vvol_datastore_specs",
                                            List[
                                                make_dataclass(
                                                    "vvol_datastore_specs",
                                                    [
                                                        ("name", str),
                                                        (
                                                            "vasa_provider_spec",
                                                            make_dataclass(
                                                                "vasa_provider_spec",
                                                                [
                                                                    (
                                                                        "storage_container_id",
                                                                        str,
                                                                    ),
                                                                    (
                                                                        "storage_protocol_type",
                                                                        str,
                                                                    ),
                                                                    ("user_id", str),
                                                                    (
                                                                        "vasa_provider_id",
                                                                        str,
                                                                    ),
                                                                ],
                                                            ),
                                                        ),
                                                    ],
                                                )
                                            ],
                                            field(default=None),
                                        ),
                                    ],
                                ),
                            ),
                            (
                                "host_specs",
                                List[
                                    make_dataclass(
                                        "host_specs",
                                        [
                                            ("id", str),
                                            ("az_name", str, field(default=None)),
                                            ("host_name", str, field(default=None)),
                                            (
                                                "host_network_spec",
                                                make_dataclass(
                                                    "host_network_spec",
                                                    [
                                                        (
                                                            "network_profile_name",
                                                            str,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "vm_nics",
                                                            List[
                                                                make_dataclass(
                                                                    "vm_nics",
                                                                    [
                                                                        (
                                                                            "id",
                                                                            str,
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                        (
                                                                            "move_to_nvds",
                                                                            bool,
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                        (
                                                                            "uplink",
                                                                            str,
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                        (
                                                                            "vds_name",
                                                                            str,
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                    ],
                                                                )
                                                            ],
                                                            field(default=None),
                                                        ),
                                                    ],
                                                ),
                                                field(default=None),
                                            ),
                                            ("ip_address", str, field(default=None)),
                                            ("license_key", str, field(default=None)),
                                            ("password", str, field(default=None)),
                                            ("serial_number", str, field(default=None)),
                                            (
                                                "ssh_thumbprint",
                                                str,
                                                field(default=None),
                                            ),
                                            ("username", str, field(default=None)),
                                        ],
                                    )
                                ],
                            ),
                            ("name", str),
                            (
                                "network_spec",
                                make_dataclass(
                                    "network_spec",
                                    [
                                        (
                                            "nsx_cluster_spec",
                                            make_dataclass(
                                                "nsx_cluster_spec",
                                                [
                                                    (
                                                        "nsx_t_cluster_spec",
                                                        make_dataclass(
                                                            "nsx_t_cluster_spec",
                                                            [
                                                                (
                                                                    "geneve_vlan_id",
                                                                    int,
                                                                    field(default=None),
                                                                ),
                                                                (
                                                                    "ip_address_pool_spec",
                                                                    make_dataclass(
                                                                        "ip_address_pool_spec",
                                                                        [
                                                                            (
                                                                                "name",
                                                                                str,
                                                                            ),
                                                                            (
                                                                                "description",
                                                                                str,
                                                                                field(
                                                                                    default=None
                                                                                ),
                                                                            ),
                                                                            (
                                                                                "ignore_unavailable_nsxt_cluster",
                                                                                bool,
                                                                                field(
                                                                                    default=None
                                                                                ),
                                                                            ),
                                                                            (
                                                                                "subnets",
                                                                                List[
                                                                                    make_dataclass(
                                                                                        "subnets",
                                                                                        [
                                                                                            (
                                                                                                "cidr",
                                                                                                str,
                                                                                            ),
                                                                                            (
                                                                                                "gateway",
                                                                                                str,
                                                                                            ),
                                                                                            (
                                                                                                "ip_address_pool_ranges",
                                                                                                List[
                                                                                                    make_dataclass(
                                                                                                        "ip_address_pool_ranges",
                                                                                                        [
                                                                                                            (
                                                                                                                "end",
                                                                                                                str,
                                                                                                            ),
                                                                                                            (
                                                                                                                "start",
                                                                                                                str,
                                                                                                            ),
                                                                                                        ],
                                                                                                    )
                                                                                                ],
                                                                                            ),
                                                                                        ],
                                                                                    )
                                                                                ],
                                                                                field(
                                                                                    default=None
                                                                                ),
                                                                            ),
                                                                        ],
                                                                    ),
                                                                    field(default=None),
                                                                ),
                                                                (
                                                                    "ip_address_pools_spec",
                                                                    List[
                                                                        make_dataclass(
                                                                            "ip_address_pools_spec",
                                                                            [
                                                                                (
                                                                                    "name",
                                                                                    str,
                                                                                ),
                                                                                (
                                                                                    "description",
                                                                                    str,
                                                                                    field(
                                                                                        default=None
                                                                                    ),
                                                                                ),
                                                                                (
                                                                                    "ignore_unavailable_nsxt_cluster",
                                                                                    bool,
                                                                                    field(
                                                                                        default=None
                                                                                    ),
                                                                                ),
                                                                                (
                                                                                    "subnets",
                                                                                    List[
                                                                                        make_dataclass(
                                                                                            "subnets",
                                                                                            [
                                                                                                (
                                                                                                    "cidr",
                                                                                                    str,
                                                                                                ),
                                                                                                (
                                                                                                    "gateway",
                                                                                                    str,
                                                                                                ),
                                                                                                (
                                                                                                    "ip_address_pool_ranges",
                                                                                                    List[
                                                                                                        make_dataclass(
                                                                                                            "ip_address_pool_ranges",
                                                                                                            [
                                                                                                                (
                                                                                                                    "end",
                                                                                                                    str,
                                                                                                                ),
                                                                                                                (
                                                                                                                    "start",
                                                                                                                    str,
                                                                                                                ),
                                                                                                            ],
                                                                                                        )
                                                                                                    ],
                                                                                                ),
                                                                                            ],
                                                                                        )
                                                                                    ],
                                                                                    field(
                                                                                        default=None
                                                                                    ),
                                                                                ),
                                                                            ],
                                                                        )
                                                                    ],
                                                                    field(default=None),
                                                                ),
                                                                (
                                                                    "uplink_profiles",
                                                                    List[
                                                                        make_dataclass(
                                                                            "uplink_profiles",
                                                                            [
                                                                                (
                                                                                    "name",
                                                                                    str,
                                                                                    field(
                                                                                        default=None
                                                                                    ),
                                                                                ),
                                                                                (
                                                                                    "supported_teaming_policies",
                                                                                    Dict,
                                                                                    field(
                                                                                        default=None
                                                                                    ),
                                                                                ),
                                                                                (
                                                                                    "teamings",
                                                                                    List[
                                                                                        make_dataclass(
                                                                                            "teamings",
                                                                                            [
                                                                                                (
                                                                                                    "active_uplinks",
                                                                                                    List[
                                                                                                        str
                                                                                                    ],
                                                                                                    field(
                                                                                                        default=None
                                                                                                    ),
                                                                                                ),
                                                                                                (
                                                                                                    "policy",
                                                                                                    str,
                                                                                                    field(
                                                                                                        default=None
                                                                                                    ),
                                                                                                ),
                                                                                                (
                                                                                                    "stand_by_uplinks",
                                                                                                    List[
                                                                                                        str
                                                                                                    ],
                                                                                                    field(
                                                                                                        default=None
                                                                                                    ),
                                                                                                ),
                                                                                            ],
                                                                                        )
                                                                                    ],
                                                                                    field(
                                                                                        default=None
                                                                                    ),
                                                                                ),
                                                                                (
                                                                                    "transport_vlan",
                                                                                    int,
                                                                                    field(
                                                                                        default=None
                                                                                    ),
                                                                                ),
                                                                            ],
                                                                        )
                                                                    ],
                                                                    field(default=None),
                                                                ),
                                                            ],
                                                        ),
                                                        field(default=None),
                                                    )
                                                ],
                                            ),
                                        ),
                                        (
                                            "vds_specs",
                                            List[
                                                make_dataclass(
                                                    "vds_specs",
                                                    [
                                                        ("name", str),
                                                        (
                                                            "is_used_by_nsxt",
                                                            bool,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "nsxt_switch_config",
                                                            make_dataclass(
                                                                "nsxt_switch_config",
                                                                [
                                                                    (
                                                                        "transport_zones",
                                                                        List[
                                                                            make_dataclass(
                                                                                "transport_zones",
                                                                                [
                                                                                    (
                                                                                        "transport_type",
                                                                                        str,
                                                                                    ),
                                                                                    (
                                                                                        "name",
                                                                                        str,
                                                                                        field(
                                                                                            default=None
                                                                                        ),
                                                                                    ),
                                                                                ],
                                                                            )
                                                                        ],
                                                                        field(
                                                                            default=None
                                                                        ),
                                                                    ),
                                                                    (
                                                                        "host_switch_operational_mode",
                                                                        str,
                                                                        field(
                                                                            default=None
                                                                        ),
                                                                    ),
                                                                ],
                                                            ),
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "mtu",
                                                            int,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "nioc_bandwidth_allocation_specs",
                                                            List[
                                                                make_dataclass(
                                                                    "nioc_bandwidth_allocation_specs",
                                                                    [
                                                                        ("type", str),
                                                                        (
                                                                            "nioc_traffic_resource_allocation",
                                                                            make_dataclass(
                                                                                "nioc_traffic_resource_allocation",
                                                                                [
                                                                                    (
                                                                                        "limit",
                                                                                        int,
                                                                                    ),
                                                                                    (
                                                                                        "reservation",
                                                                                        int,
                                                                                    ),
                                                                                    (
                                                                                        "shares_info",
                                                                                        make_dataclass(
                                                                                            "shares_info",
                                                                                            [
                                                                                                (
                                                                                                    "level",
                                                                                                    str,
                                                                                                    field(
                                                                                                        default=None
                                                                                                    ),
                                                                                                ),
                                                                                                (
                                                                                                    "shares",
                                                                                                    int,
                                                                                                    field(
                                                                                                        default=None
                                                                                                    ),
                                                                                                ),
                                                                                            ],
                                                                                        ),
                                                                                    ),
                                                                                ],
                                                                            ),
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                    ],
                                                                )
                                                            ],
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "port_group_specs",
                                                            List[
                                                                make_dataclass(
                                                                    "port_group_specs",
                                                                    [
                                                                        ("name", str),
                                                                        (
                                                                            "transport_type",
                                                                            str,
                                                                        ),
                                                                        (
                                                                            "active_uplinks",
                                                                            List[str],
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                        (
                                                                            "stand_by_uplinks",
                                                                            List[str],
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                        (
                                                                            "teaming_policy",
                                                                            str,
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                    ],
                                                                )
                                                            ],
                                                            field(default=None),
                                                        ),
                                                    ],
                                                )
                                            ],
                                        ),
                                        (
                                            "network_profiles",
                                            List[
                                                make_dataclass(
                                                    "network_profiles",
                                                    [
                                                        ("name", str),
                                                        (
                                                            "nsxt_host_switch_configs",
                                                            List[
                                                                make_dataclass(
                                                                    "nsxt_host_switch_configs",
                                                                    [
                                                                        (
                                                                            "ip_address_pool_name",
                                                                            str,
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                        (
                                                                            "uplink_profile_name",
                                                                            str,
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                        (
                                                                            "vds_name",
                                                                            str,
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                        (
                                                                            "vds_uplink_to_nsx_uplink",
                                                                            List[
                                                                                make_dataclass(
                                                                                    "vds_uplink_to_nsx_uplink",
                                                                                    [
                                                                                        (
                                                                                            "nsx_uplink_name",
                                                                                            str,
                                                                                        ),
                                                                                        (
                                                                                            "vds_uplink_name",
                                                                                            str,
                                                                                        ),
                                                                                    ],
                                                                                )
                                                                            ],
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                    ],
                                                                )
                                                            ],
                                                        ),
                                                        (
                                                            "description",
                                                            str,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "is_default",
                                                            bool,
                                                            field(default=None),
                                                        ),
                                                    ],
                                                )
                                            ],
                                            field(default=None),
                                        ),
                                    ],
                                ),
                            ),
                            (
                                "advanced_options",
                                make_dataclass(
                                    "advanced_options",
                                    [
                                        ("evc_mode", str, field(default=None)),
                                        (
                                            "high_availability",
                                            make_dataclass(
                                                "high_availability", [("enabled", bool)]
                                            ),
                                            field(default=None),
                                        ),
                                    ],
                                ),
                                field(default=None),
                            ),
                            ("cluster_image_id", str, field(default=None)),
                            ("skip_thumbprint_validation", bool, field(default=None)),
                            (
                                "vx_rail_details",
                                make_dataclass(
                                    "vx_rail_details",
                                    [
                                        (
                                            "admin_credentials",
                                            make_dataclass(
                                                "admin_credentials",
                                                [
                                                    ("credential_type", str),
                                                    ("username", str),
                                                    (
                                                        "password",
                                                        str,
                                                        field(default=None),
                                                    ),
                                                ],
                                            ),
                                            field(default=None),
                                        ),
                                        (
                                            "array_context_with_key_value_pair",
                                            Dict,
                                            field(default=None),
                                        ),
                                        (
                                            "context_with_key_value_pair",
                                            Dict,
                                            field(default=None),
                                        ),
                                        ("dns_name", str, field(default=None)),
                                        ("ip_address", str, field(default=None)),
                                        (
                                            "networks",
                                            List[
                                                make_dataclass(
                                                    "networks",
                                                    [
                                                        (
                                                            "free_ips",
                                                            List[str],
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "gateway",
                                                            str,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "id",
                                                            str,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "ip_pools",
                                                            List[
                                                                make_dataclass(
                                                                    "ip_pools",
                                                                    [
                                                                        (
                                                                            "end",
                                                                            str,
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                        (
                                                                            "start",
                                                                            str,
                                                                            field(
                                                                                default=None
                                                                            ),
                                                                        ),
                                                                    ],
                                                                )
                                                            ],
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "mask",
                                                            str,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "mtu",
                                                            int,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "subnet",
                                                            str,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "type",
                                                            str,
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "used_ips",
                                                            List[str],
                                                            field(default=None),
                                                        ),
                                                        (
                                                            "vlan_id",
                                                            int,
                                                            field(default=None),
                                                        ),
                                                    ],
                                                )
                                            ],
                                            field(default=None),
                                        ),
                                        ("nic_profile", str, field(default=None)),
                                        (
                                            "root_credentials",
                                            make_dataclass(
                                                "root_credentials",
                                                [
                                                    ("credential_type", str),
                                                    ("username", str),
                                                    (
                                                        "password",
                                                        str,
                                                        field(default=None),
                                                    ),
                                                ],
                                            ),
                                            field(default=None),
                                        ),
                                        ("ssh_thumbprint", str, field(default=None)),
                                        ("ssl_thumbprint", str, field(default=None)),
                                    ],
                                ),
                                field(default=None),
                            ),
                        ],
                    )
                ],
            ),
            ("skip_failed_hosts", bool, field(default=None)),
        ],
    ),
    domain_name: str,
    vcenter_spec: make_dataclass(
        "vcenter_spec",
        [
            ("datacenter_name", str),
            ("name", str),
            (
                "network_details_spec",
                make_dataclass(
                    "network_details_spec",
                    [
                        ("ip_address", str),
                        ("dns_name", str, field(default=None)),
                        ("gateway", str, field(default=None)),
                        ("subnet_mask", str, field(default=None)),
                    ],
                ),
            ),
            ("root_password", str),
            ("storage_size", str, field(default=None)),
            ("vm_size", str, field(default=None)),
        ],
    ),
    network_separation_spec: make_dataclass(
        "network_separation_spec",
        [
            (
                "segment_spec",
                make_dataclass(
                    "segment_spec",
                    [("name", str), ("vlan_id", str, field(default=None))],
                ),
            ),
            ("enable_security", bool, field(default=None)),
        ],
    ) = None,
    nsx_t_spec: make_dataclass(
        "nsx_t_spec",
        [
            (
                "nsx_manager_specs",
                List[
                    make_dataclass(
                        "nsx_manager_specs",
                        [
                            ("name", str),
                            (
                                "network_details_spec",
                                make_dataclass(
                                    "network_details_spec",
                                    [
                                        ("ip_address", str),
                                        ("dns_name", str, field(default=None)),
                                        ("gateway", str, field(default=None)),
                                        ("subnet_mask", str, field(default=None)),
                                    ],
                                ),
                            ),
                        ],
                    )
                ],
            ),
            ("vip", str),
            ("vip_fqdn", str),
            ("form_factor", str, field(default=None)),
            (
                "ip_address_pool_spec",
                make_dataclass(
                    "ip_address_pool_spec",
                    [
                        ("name", str),
                        ("description", str, field(default=None)),
                        ("ignore_unavailable_nsxt_cluster", bool, field(default=None)),
                        (
                            "subnets",
                            List[
                                make_dataclass(
                                    "subnets",
                                    [
                                        ("cidr", str),
                                        ("gateway", str),
                                        (
                                            "ip_address_pool_ranges",
                                            List[
                                                make_dataclass(
                                                    "ip_address_pool_ranges",
                                                    [("end", str), ("start", str)],
                                                )
                                            ],
                                        ),
                                    ],
                                )
                            ],
                            field(default=None),
                        ),
                    ],
                ),
                field(default=None),
            ),
            ("license_key", str, field(default=None)),
            ("nsx_manager_admin_password", str, field(default=None)),
            ("nsx_manager_audit_password", str, field(default=None)),
        ],
    ) = None,
    org_name: str = None,
    sso_domain_spec: make_dataclass(
        "sso_domain_spec",
        [
            ("sso_domain_name", str, field(default=None)),
            ("sso_domain_password", str, field(default=None)),
        ],
    ) = None,
) -> Dict[str, Any]:
    r"""

    Perform validation of the DomainCreationSpec specification
        None

    Args:
        compute_spec(dict[str, Any]):
            computeSpec.

            * cluster_specs (List[dict[str, Any]]):
                List of clusters to be added to workload domain.

                * advanced_options (dict[str, Any], Optional):
                    advancedOptions. Defaults to None.

                    * evc_mode (str, Optional):
                        EVC mode for new cluster, if needed. Defaults to None.

                    * high_availability (dict[str, Any], Optional):
                        highAvailability. Defaults to None.

                        * enabled (bool):
                            enabled.

                * cluster_image_id (str, Optional):
                    ID of the Cluster Image to be used with the Cluster. Defaults to None.

                * datastore_spec (dict[str, Any]):
                    datastoreSpec.

                    * nfs_datastore_specs (List[dict[str, Any]], Optional):
                        Cluster storage configuration for NFS. Defaults to None.

                        * datastore_name (str):
                            Datastore name used for cluster creation.

                        * nas_volume (dict[str, Any], Optional):
                            nasVolume. Defaults to None.

                            * path (str):
                                Shared directory path used for NFS based cluster creation.

                            * read_only (bool):
                                Readonly is used to identify whether to mount the directory as readOnly or not.

                            * server_name (List[str]):
                                NFS Server name used for cluster creation.

                            * user_tag (str, Optional):
                                User tag used to annotate NFS share. Defaults to None.

                    * vmfs_datastore_spec (dict[str, Any], Optional):
                        vmfsDatastoreSpec. Defaults to None.

                        * fc_spec (List[dict[str, Any]], Optional):
                            Cluster storage configuration for VMFS on FC. Defaults to None.

                            * datastore_name (str):
                                Datastore name used for cluster creation.

                    * vsan_datastore_spec (dict[str, Any], Optional):
                        vsanDatastoreSpec. Defaults to None.

                        * datastore_name (str):
                            Datastore name used for cluster creation.

                        * dedup_and_compression_enabled (bool, Optional):
                            Enable vSAN deduplication and compression. Defaults to None.

                        * esa_config (dict[str, Any], Optional):
                            esaConfig. Defaults to None.

                            * enabled (bool):
                                vSAN ESA enablement status.

                        * failures_to_tolerate (int, Optional):
                            Number of vSphere host failures to tolerate in the vSAN cluster. Defaults to None.

                        * license_key (str, Optional):
                            License key for the vSAN data store to be applied in vCenter. Defaults to None.

                    * vsan_remote_datastore_cluster_spec (dict[str, Any], Optional):
                        vsanRemoteDatastoreClusterSpec. Defaults to None.

                        * vsan_remote_datastore_spec (List[dict[str, Any]], Optional):
                            List of Remote vSAN datastore configuration for HCI Mesh compute client cluster. Defaults to None.

                            * datastore_uuid (str):
                                vSAN Remote Datastore UUID.

                    * vvol_datastore_specs (List[dict[str, Any]], Optional):
                        Cluster storage configuration for VVOL. Defaults to None.

                        * name (str):
                            Name of the datastore.

                        * vasa_provider_spec (dict[str, Any]):
                            vasaProviderSpec.

                            * storage_container_id (str):
                                UUID of the VASA storage container.

                            * storage_protocol_type (str):
                                Type of the VASA storage protocol.

                            * user_id (str):
                                UUID of the VASA storage user.

                            * vasa_provider_id (str):
                                UUID of the VASA storage provider.

                * host_specs (List[dict[str, Any]]):
                    List of vSphere host information from the free pool to consume in the workload domain.

                    * az_name (str, Optional):
                        Availability Zone Name
                        (This is required while performing a stretched cluster expand operation). Defaults to None.

                    * host_name (str, Optional):
                        Host name of the vSphere host. Defaults to None.

                    * host_network_spec (dict[str, Any], Optional):
                        hostNetworkSpec. Defaults to None.

                        * network_profile_name (str, Optional):
                            Network profile name. Defaults to None.

                        * vm_nics (List[dict[str, Any]], Optional):
                            List of the vSphere host vmNics. Defaults to None.

                            * id (str, Optional):
                                VmNic ID of vSphere host to be associated with VDS, once added to cluster. Defaults to None.

                            * move_to_nvds (bool, Optional):
                                This flag determines if the vmnic must be on N-VDS. Defaults to None.

                            * uplink (str, Optional):
                                Uplink to be associated with vmnic. Defaults to None.

                            * vds_name (str, Optional):
                                VDS name to associate with vSphere host. Defaults to None.

                    * id (str):
                        ID of a vSphere host in the free pool.

                    * ip_address (str, Optional):
                        IP address of the vSphere host. Defaults to None.

                    * license_key (str, Optional):
                        License key of a vSphere host in the free pool
                        (This is required except in cases where the ESXi host has already been licensed outside of the VMware Cloud Foundation system). Defaults to None.

                    * password (str, Optional):
                        SSH password of the vSphere host Defaults to None.

                    * serial_number (str, Optional):
                        Serial Number of the vSphere host. Defaults to None.

                    * ssh_thumbprint (str, Optional):
                        SSH thumbprint(fingerprint) of the vSphere host
                        Note:This field will be mandatory in future releases. Defaults to None.

                    * username (str, Optional):
                        Username of the vSphere host. Defaults to None.

                * name (str):
                    Name of the new cluster that will be added to the specified workload domain.

                * network_spec (dict[str, Any]):
                    networkSpec.

                    * nsx_cluster_spec (dict[str, Any]):
                        nsxClusterSpec.

                        * nsx_t_cluster_spec (dict[str, Any], Optional):
                            nsxTClusterSpec. Defaults to None.

                            * geneve_vlan_id (int, Optional):
                                Vlan id of Geneve. Defaults to None.

                            * ip_address_pool_spec (dict[str, Any], Optional):
                                ipAddressPoolSpec. Defaults to None.

                                * description (str, Optional):
                                    Description of the IP address pool. Defaults to None.

                                * ignore_unavailable_nsxt_cluster (bool, Optional):
                                    Ignore unavailable NSX cluster(s) during IP pool spec validation. Defaults to None.

                                * name (str):
                                    Name of the IP address pool.

                                * subnets (List[dict[str, Any]], Optional):
                                    List of IP address pool subnet specification. Defaults to None.

                                    * cidr (str):
                                        The subnet representation, contains the network address and the prefix length.

                                    * gateway (str):
                                        The default gateway address of the network.

                                    * ip_address_pool_ranges (List[dict[str, Any]]):
                                        List of the IP allocation ranges. Atleast 1 IP address range has to be specified.

                                        * end (str):
                                            The last IP Address of the IP Address Range.

                                        * start (str):
                                            The first IP Address of the IP Address Range.

                            * ip_address_pools_spec (List[dict[str, Any]], Optional):
                                The list of IP address pools specification. Defaults to None.

                                * description (str, Optional):
                                    Description of the IP address pool. Defaults to None.

                                * ignore_unavailable_nsxt_cluster (bool, Optional):
                                    Ignore unavailable NSX cluster(s) during IP pool spec validation. Defaults to None.

                                * name (str):
                                    Name of the IP address pool.

                                * subnets (List[dict[str, Any]], Optional):
                                    List of IP address pool subnet specification. Defaults to None.

                                    * cidr (str):
                                        The subnet representation, contains the network address and the prefix length.

                                    * gateway (str):
                                        The default gateway address of the network.

                                    * ip_address_pool_ranges (List[dict[str, Any]]):
                                        List of the IP allocation ranges. Atleast 1 IP address range has to be specified.

                                        * end (str):
                                            The last IP Address of the IP Address Range.

                                        * start (str):
                                            The first IP Address of the IP Address Range.

                            * uplink_profiles (List[dict[str, Any]], Optional):
                                The list of uplink profile specifications. Defaults to None.

                                * name (str, Optional):
                                    The uplink profile name. Defaults to None.

                                * supported_teaming_policies (Dict, Optional):
                                    List of supported teaming policies in NSX. Defaults to None.

                                * teamings (List[dict[str, Any]], Optional):
                                    The teaming policies to be associated with the uplink profile. Defaults to None.

                                    * active_uplinks (List[str], Optional):
                                        The list of active uplinks. Defaults to None.

                                    * policy (str, Optional):
                                        The teaming policy associated with the uplink profile. Defaults to None.

                                    * stand_by_uplinks (List[str], Optional):
                                        The list of stand by uplinks. Defaults to None.

                                * transport_vlan (int, Optional):
                                    The VLAN used for tagging overlay traffic of the associated Host Switch. Defaults to None.

                    * vds_specs (List[dict[str, Any]]):
                        Distributed switches to add to the cluster.

                        * is_used_by_nsxt (bool, Optional):
                            Boolean to identify if the vSphere distributed switch is used by NSX. This property is deprecated in favor of nsxtSwitchConfig field. Defaults to None.

                        * nsxt_switch_config (dict[str, Any], Optional):
                            nsxtSwitchConfig. Defaults to None.

                            * transport_zones (List[dict[str, Any]], Optional):
                                The list of transport zones to be associated with the vSphere Distributed Switch managed by NSX. Defaults to None.

                                * name (str, Optional):
                                    The name of the transport zone. Defaults to None.

                                * transport_type (str):
                                    The type of the transport zone.

                            * host_switch_operational_mode (str, Optional):
                                vSphere Distributed Switch name. Defaults to None.

                        * mtu (int, Optional):
                            The maximum transmission unit (MTU) configured for the uplinks. Defaults to None.

                        * name (str):
                            vSphere Distributed Switch name.

                        * nioc_bandwidth_allocation_specs (List[dict[str, Any]], Optional):
                            List of Network I/O Control Bandwidth Allocations for System Traffic. Defaults to None.

                            * nioc_traffic_resource_allocation (dict[str, Any], Optional):
                                niocTrafficResourceAllocation. Defaults to None.

                                * limit (int):
                                    limit.

                                * reservation (int):
                                    reservation.

                                * shares_info (dict[str, Any]):
                                    sharesInfo.

                                    * level (str, Optional):
                                        The allocation level. Defaults to None.

                                    * shares (int, Optional):
                                        The number of shares allocated. Defaults to None.

                            * type (str):
                                DvsHostInfrastructureTrafficResource resource type.

                        * port_group_specs (List[dict[str, Any]], Optional):
                            List of portgroups to be associated with the vSphere Distributed Switch. Defaults to None.

                            * active_uplinks (List[str], Optional):
                                The list of active uplinks associated with portgroup. Defaults to None.

                            * stand_by_uplinks (List[str], Optional):
                                The list of standby uplinks associated with portgroup. Defaults to None.

                            * teaming_policy (str, Optional):
                                The teaming policy associated with the portgroup. Defaults to None.

                            * name (str):
                                Port group name.

                            * transport_type (str):
                                Port group transport type.

                    * network_profiles (List[dict[str, Any]], Optional):
                        The list of network profiles. Defaults to None.

                        * name (str):
                            The network profile name.

                        * description (str, Optional):
                            The network profile description. Defaults to None.

                        * is_default (bool, Optional):
                            Designates the network profile as a Global Network Config or Sub Network Config. Defaults to None.

                        * nsxt_host_switch_configs (List[dict[str, Any]]):
                            The list of NSX host switch configurations.

                            * ip_address_pool_name (str, Optional):
                                The IP address pool name. Defaults to None.

                            * uplink_profile_name (str, Optional):
                                The name of the uplink profile. Defaults to None.

                            * vds_name (str, Optional):
                                The name of the vSphere Distributed Switch. Defaults to None.

                            * vds_uplink_to_nsx_uplink (List[dict[str, Any]], Optional):
                                The map of vSphere Distributed Switch uplinks to the NSX switch uplinks. Defaults to None.

                                * nsx_uplink_name (str):
                                    The uplink name of the NSX switch.

                                * vds_uplink_name (str):
                                    The uplink name of the vSphere Distributed Switch.

                * skip_thumbprint_validation (bool, Optional):
                    Skip thumbprint validation for ESXi and VxRail Manager during add cluster/host operation.
                    This property is deprecated. Defaults to None.

                * vx_rail_details (dict[str, Any], Optional):
                    vxRailDetails. Defaults to None.

                    * admin_credentials (dict[str, Any], Optional):
                        adminCredentials. Defaults to None.

                        * credential_type (str):
                            Credential type.

                        * password (str, Optional):
                            Password. Defaults to None.

                        * username (str):
                            Username.

                    * array_context_with_key_value_pair (Dict, Optional):
                        Map of Context class with list of key and value pairs for array objects. Defaults to None.

                    * context_with_key_value_pair (Dict, Optional):
                        Map of Context class with list of key and value pairs. Defaults to None.

                    * dns_name (str, Optional):
                        DNS Name/Hostname of the VxRail Manager. Defaults to None.

                    * ip_address (str, Optional):
                        IP Address of the VxRail Manager. Defaults to None.

                    * networks (List[dict[str, Any]], Optional):
                        Network details of the VxRail Manager. Defaults to None.

                        * free_ips (List[str], Optional):
                            List of free IPs to use. Defaults to None.

                        * gateway (str, Optional):
                            Gateway for the network. Defaults to None.

                        * id (str, Optional):
                            The ID of the network. Defaults to None.

                        * ip_pools (List[dict[str, Any]], Optional):
                            List of IP pool ranges to use. Defaults to None.

                            * end (str, Optional):
                                End IP address of the IP pool. Defaults to None.

                            * start (str, Optional):
                                Start IP address of the IP pool. Defaults to None.

                        * mask (str, Optional):
                            Subnet mask for the subnet of the network. Defaults to None.

                        * mtu (int, Optional):
                            MTU of the network. Defaults to None.

                        * subnet (str, Optional):
                            Subnet associated with the network. Defaults to None.

                        * type (str, Optional):
                            Network Type of the network. Defaults to None.

                        * used_ips (List[str], Optional):
                            List of used IPs. Defaults to None.

                        * vlan_id (int, Optional):
                            VLAN ID associated with the network. Defaults to None.

                    * nic_profile (str, Optional):
                        Nic Profile Type. Defaults to None.

                    * root_credentials (dict[str, Any], Optional):
                        rootCredentials. Defaults to None.

                        * credential_type (str):
                            Credential type.

                        * password (str, Optional):
                            Password. Defaults to None.

                        * username (str):
                            Username.

                    * ssh_thumbprint (str, Optional):
                        SSH thumbprint of the VxRail Manager. Defaults to None.

                    * ssl_thumbprint (str, Optional):
                        SSL thumbprint of the VxRail Manager. Defaults to None.

            * skip_failed_hosts (bool, Optional):
                Skip failed ESXi Hosts and proceed with the rest of the ESXi Hosts during add Cluster. This is not supported for VCF VxRail. Defaults to None.

        domain_name(str):
            Name of the workload domain.

        vcenter_spec(dict[str, Any]):
            vcenterSpec.

            * datacenter_name (str):
                vCenter datacenter name.

            * name (str):
                Name of the vCenter virtual machine.

            * network_details_spec (dict[str, Any]):
                networkDetailsSpec.

                * dns_name (str, Optional):
                    DNS name of the virtual machine, e.g., vc-1.domain1.vsphere.local. Defaults to None.

                * gateway (str, Optional):
                    IPv4 gateway the VM can use to connect to the outside world. Defaults to None.

                * ip_address (str):
                    IPv4 address of the virtual machine.

                * subnet_mask (str, Optional):
                    Subnet mask. Defaults to None.

            * root_password (str):
                vCenter root shell password.

            * storage_size (str, Optional):
                VCenter storage size. Defaults to None.

            * vm_size (str, Optional):
                VCenter VM size. Defaults to None.

        network_separation_spec(dict[str, Any], Optional):
            networkSeparationSpec. Defaults to None.

            * enable_security (bool, Optional):
                enable/disable distributed firewall rules for the Isolated WLD. Defaults to None.

            * segment_spec (dict[str, Any]):
                segmentSpec.

                * name (str):
                    The name of the NSX segment.

                * vlan_id (str, Optional):
                    The VLAN Id to be used by the segment. Defaults to None.

        nsx_t_spec(dict[str, Any], Optional):
            nsxTSpec. Defaults to None.

            * form_factor (str, Optional):
                NSX manager form factor. Defaults to None.

            * ip_address_pool_spec (dict[str, Any], Optional):
                ipAddressPoolSpec. Defaults to None.

                * description (str, Optional):
                    Description of the IP address pool. Defaults to None.

                * ignore_unavailable_nsxt_cluster (bool, Optional):
                    Ignore unavailable NSX cluster(s) during IP pool spec validation. Defaults to None.

                * name (str):
                    Name of the IP address pool.

                * subnets (List[dict[str, Any]], Optional):
                    List of IP address pool subnet specification. Defaults to None.

                    * cidr (str):
                        The subnet representation, contains the network address and the prefix length.

                    * gateway (str):
                        The default gateway address of the network.

                    * ip_address_pool_ranges (List[dict[str, Any]]):
                        List of the IP allocation ranges. Atleast 1 IP address range has to be specified.

                        * end (str):
                            The last IP Address of the IP Address Range.

                        * start (str):
                            The first IP Address of the IP Address Range.

            * license_key (str, Optional):
                NSX license value. Defaults to None.

            * nsx_manager_admin_password (str, Optional):
                NSX manager admin password (basic auth and SSH). Defaults to None.

            * nsx_manager_audit_password (str, Optional):
                NSX manager Audit password. Defaults to None.

            * nsx_manager_specs (List[dict[str, Any]]):
                Specification details of the NSX Manager virtual machine.

                * name (str):
                    Name of the NSX Manager virtual machine.

                * network_details_spec (dict[str, Any]):
                    networkDetailsSpec.

                    * dns_name (str, Optional):
                        DNS name of the virtual machine, e.g., vc-1.domain1.vsphere.local. Defaults to None.

                    * gateway (str, Optional):
                        IPv4 gateway the VM can use to connect to the outside world. Defaults to None.

                    * ip_address (str):
                        IPv4 address of the virtual machine.

                    * subnet_mask (str, Optional):
                        Subnet mask. Defaults to None.

            * vip (str):
                Virtual IP address which would act as proxy/alias for NSX Managers.

            * vip_fqdn (str):
                FQDN for VIP so that common SSL certificates can be installed across all managers.

        org_name(str, Optional):
            Organization name of the workload domain. Defaults to None.

        sso_domain_spec(dict[str, Any], Optional):
            ssoDomainSpec. Defaults to None.

            * sso_domain_name (str, Optional):
                SSO domain name to be created. Defaults to None.

            * sso_domain_password (str, Optional):
                New SSO domain password. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "compute_spec": "computeSpec",
        "domain_name": "domainName",
        "network_separation_spec": "networkSeparationSpec",
        "nsx_t_spec": "nsxTSpec",
        "org_name": "orgName",
        "sso_domain_spec": "ssoDomainSpec",
        "vcenter_spec": "vcenterSpec",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/domains/validations".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "additionalProperties": "additional_properties",
            "description": "description",
            "executionStatus": "execution_status",
            "id": "id",
            "resultStatus": "result_status",
            "validationChecks": "validation_checks",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_cluster_criteria(hub, ctx, domain_id: str) -> Dict[str, Any]:
    r"""

    Get all cluster criteria
        None

    Args:
        domain_id(str):
            Domain ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/domains/{domainId}/clusters/criteria".format(
            **{"domainId": domain_id}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_cluster_criterion(hub, ctx, domain_id: str) -> Dict[str, Any]:
    r"""

    Get a criterion to query for cluster
        None

    Args:
        domain_id(str):
            Domain ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/domains/{domainId}/clusters/criteria/{name}".format(
            **{"domainId": domain_id}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"arguments": "arguments", "description": "description", "name": "name"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def post_clusters_query(
    hub, ctx, domain_id: str, arguments: Dict = None, description: str = None
) -> Dict[str, Any]:
    r"""

    Post clusters query
        None

    Args:
        domain_id(str):
            Domain ID.

        arguments(Dict, Optional):
            Arguments required for a particular criterion. Defaults to None.

        description(str, Optional):
            Description of the criterion. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"arguments": "arguments", "description": "description", "name": "name"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/domains/{domainId}/clusters/queries".format(
            **{"domainId": domain_id}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict({"queryInfo": "query_info", "result": "result"})

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_clusters_query_response(
    hub, ctx, domain_id: str, query_id: str
) -> Dict[str, Any]:
    r"""

    Get clusters query response
        None

    Args:
        domain_id(str):
            Domain ID.

        query_id(str):
            Query ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/domains/{domainId}/clusters/queries/{queryId}".format(
            **{"domainId": domain_id, "queryId": query_id}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict({"queryInfo": "query_info", "result": "result"})

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def post_cluster_query(
    hub,
    ctx,
    domain_id: str,
    cluster_name: str,
    arguments: Dict = None,
    description: str = None,
) -> Dict[str, Any]:
    r"""

    Post a cluster query
        None

    Args:
        domain_id(str):
            Domain ID.

        cluster_name(str):
            Cluster Name.

        arguments(Dict, Optional):
            Arguments required for a particular criterion. Defaults to None.

        description(str, Optional):
            Description of the criterion. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"arguments": "arguments", "description": "description", "name": "name"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/domains/{domainId}/clusters/{clusterName}/queries".format(
            **{"domainId": domain_id, "clusterName": cluster_name}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict({"queryInfo": "query_info", "result": "result"})

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_cluster_query_response(
    hub, ctx, domain_id: str, cluster_name: str, query_id: str
) -> Dict[str, Any]:
    r"""

    Get cluster query response
        None

    Args:
        domain_id(str):
            Domain ID.

        cluster_name(str):
            Cluster Name.

        query_id(str):
            Query ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/domains/{domainId}/clusters/{clusterName}/queries/{queryId}".format(
            **{"domainId": domain_id, "clusterName": cluster_name, "queryId": query_id}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict({"queryInfo": "query_info", "result": "result"})

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_datastores_criteria_1(hub, ctx, domain_id: str) -> Dict[str, Any]:
    r"""

    Get all datastore criteria
        None

    Args:
        domain_id(str):
            Domain ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/domains/{domainId}/datastores/criteria".format(
            **{"domainId": domain_id}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_datastore_criterion_1(hub, ctx, domain_id: str) -> Dict[str, Any]:
    r"""

    Get a criterion to query for datastore
        None

    Args:
        domain_id(str):
            Domain ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/domains/{domainId}/datastores/criteria/{name}".format(
            **{"domainId": domain_id}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"arguments": "arguments", "description": "description", "name": "name"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def post_datastore_query_1(
    hub, ctx, domain_id: str, arguments: Dict = None, description: str = None
) -> Dict[str, Any]:
    r"""

    Post a datastore query
        None

    Args:
        domain_id(str):
            Domain ID.

        arguments(Dict, Optional):
            Arguments required for a particular criterion. Defaults to None.

        description(str, Optional):
            Description of the criterion. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"arguments": "arguments", "description": "description", "name": "name"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/domains/{domainId}/datastores/queries".format(
            **{"domainId": domain_id}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict({"queryInfo": "query_info", "result": "result"})

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_datastore_query_response_1(
    hub, ctx, domain_id: str, query_id: str
) -> Dict[str, Any]:
    r"""

    Get Datastore query response
        None

    Args:
        domain_id(str):
            Domain ID.

        query_id(str):
            Query ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/domains/{domainId}/datastores/queries/{queryId}".format(
            **{"domainId": domain_id, "queryId": query_id}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict({"queryInfo": "query_info", "result": "result"})

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_domain_endpoints(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Retrieve a list of endpoints or URLs for a domain by its ID
        None

    Args:
        id_(str):
            Domain ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/domains/{id}/endpoints".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_tags_assigned_to_domain(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Get tags assigned to a domain
        None

    Args:
        id_(str):
            Domain ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/domains/{id}/tags".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def assign_tags_to_domain(
    hub, ctx, id_: str, tag_ids: List[str]
) -> Dict[str, Any]:
    r"""

    Assign tags to a domain
        None

    Args:
        id_(str):
            Domain ID.

        tag_ids(List[str]):
            Tag IDs.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"tag_ids": "tagIds"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="put",
        path="/v1/domains/{id}/tags".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"defaultErrorMessages": "default_error_messages", "success": "success"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def remove_tags_from_domain(
    hub, ctx, id_: str, tag_ids: List[str]
) -> Dict[str, Any]:
    r"""

    remove Tags From Domain
        None

    Args:
        id_(str):
            Domain ID.

        tag_ids(List[str]):
            Tag IDs.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"tag_ids": "tagIds"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="delete",
        path="/v1/domains/{id}/tags".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"defaultErrorMessages": "default_error_messages", "success": "success"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_assignable_tags_for_domain(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Get tags assignable to a domain
        None

    Args:
        id_(str):
            Domain ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/domains/{id}/tags/assignable-tags".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_domain_tag_manager_url(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Get Domain Tag Manager Url
        None

    Args:
        id_(str):
            Domain ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/domains/{id}/tags/tag-manager".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict({"tagManagerUrl": "tag_manager_url"})

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result

"""Utility functions for Upgradeses. """
from collections import OrderedDict
from typing import Any
from typing import Dict


async def start_upgrade_precheck(hub, ctx, upgrade_id: str) -> Dict[str, Any]:
    r"""

    Start an upgrade precheck operation
        Perform Upgrade Prechecks

    Args:
        upgrade_id(str):
            upgradeId.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/upgrades/{upgradeId}/prechecks".format(**{"upgradeId": upgrade_id}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "errors": "errors",
            "id": "id",
            "isCancellable": "is_cancellable",
            "isRetryable": "is_retryable",
            "localizableDescriptionPack": "localizable_description_pack",
            "name": "name",
            "resolutionStatus": "resolution_status",
            "resources": "resources",
            "status": "status",
            "subTasks": "sub_tasks",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_upgrade_precheck_by_id(
    hub, ctx, upgrade_id: str, precheck_id: str
) -> Dict[str, Any]:
    r"""

    Retrieve an upgrade precheck task by ID
        Gets upgrade precheck details

    Args:
        upgrade_id(str):
            upgradeId.

        precheck_id(str):
            precheckId.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/upgrades/{upgradeId}/prechecks/{precheckId}".format(
            **{"upgradeId": upgrade_id, "precheckId": precheck_id}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "errors": "errors",
            "id": "id",
            "isCancellable": "is_cancellable",
            "isRetryable": "is_retryable",
            "localizableDescriptionPack": "localizable_description_pack",
            "name": "name",
            "resolutionStatus": "resolution_status",
            "resources": "resources",
            "status": "status",
            "subTasks": "sub_tasks",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result

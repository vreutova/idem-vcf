"""Utility functions for Nsx Alb Clusterss. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List


async def validate_alb_cluster(
    hub,
    ctx,
    domain_id: str = None,
    cluster_fqdn: str = None,
    cluster_ip_address: str = None,
    form_factor: str = None,
    admin_password: str = None,
    nodes: List[
        make_dataclass("nodes", [("ip_address", str, field(default=None))])
    ] = None,
    alb_bundle_id: str = None,
) -> Dict[str, Any]:
    r"""

    Perform validiation of the NsxAlbControllerClusterSpec specification
        None

    Args:
        domain_id(str, Optional):
            Associated VCF Domain ID. Defaults to None.

        cluster_fqdn(str, Optional):
            NSX ALB Cluster's FQDN. Defaults to None.

        cluster_ip_address(str, Optional):
            NSX ALB Cluster's VIP. Defaults to None.

        form_factor(str, Optional):
            Size of NSX ALB cluster. Defaults to None.

        admin_password(str, Optional):
            Admin user password of NSX ALB cluster. Defaults to None.

        nodes(List[dict[str, Any]], Optional):
            NSX ALB nodes. Defaults to None.

            * ip_address (str, Optional):
                IPv4 address of NSX ALB Node. Defaults to None.

        alb_bundle_id(str, Optional):
            NSX ALB bundle id. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "domain_id": "domainId",
        "name": "name",
        "cluster_fqdn": "clusterFqdn",
        "cluster_ip_address": "clusterIpAddress",
        "form_factor": "formFactor",
        "admin_password": "adminPassword",
        "nodes": "nodes",
        "alb_bundle_id": "albBundleId",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/nsx-alb-clusters/validations".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "additionalProperties": "additional_properties",
            "description": "description",
            "executionStatus": "execution_status",
            "id": "id",
            "resultStatus": "result_status",
            "validationChecks": "validation_checks",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def validate_version(
    hub, ctx, domain_id: str = None, alb_bundle_id: str = None
) -> Dict[str, Any]:
    r"""

    Validate an NSX ALB version compatibility with the domain
        None

    Args:
        domain_id(str, Optional):
            ID of the WorkLoadDomain where NSX ALB Cluster will be deployed. Defaults to None.

        alb_bundle_id(str, Optional):
            NSX ALB bundle id. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"domain_id": "domainId", "alb_bundle_id": "albBundleId"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/nsx-alb-clusters/validations/version".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result

"""Utility functions for License Keyss. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List


async def get_license_information(
    hub, ctx, resource_type: str = None, resource_ids: List[str] = None
) -> Dict[str, Any]:
    r"""

    Retrieve licensing information
        None

    Args:
        resource_type(str, Optional):
            Resource type. Defaults to None.

        resource_ids(List[str], Optional):
            Resource IDs. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/licensing-info".format(**{}),
        query_params={"resourceType": resource_type, "resourceIds": resource_ids},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def update_license_information(
    hub,
    ctx,
    resource_type: str,
    days_remaining_to_subscribe: int = None,
    is_registered: bool = None,
    is_subscribed: bool = None,
    licensing_mode: str = None,
    resource_id: str = None,
    subscription_status: str = None,
    subscribed_state: str = None,
    entitlements: make_dataclass(
        "entitlements", [("has_nsx_entitlement", bool, field(default=None))]
    ) = None,
) -> Dict[str, Any]:
    r"""

    Update the licensing information
        None

    Args:
        resource_type(str):
            Type of the resource.

        days_remaining_to_subscribe(int, Optional):
            Number of days remaining to subscribe. Defaults to None.

        is_registered(bool, Optional):
            Flag indicating the resource is registered for subscription or not. Defaults to None.

        is_subscribed(bool, Optional):
            Flag indicating the resource is subscribed or not. Defaults to None.

        licensing_mode(str, Optional):
            Licensing mode. Defaults to None.

        resource_id(str, Optional):
            ID of the resource, need not be set for the resource of the type : SYSTEM. Defaults to None.

        subscription_status(str, Optional):
            Status of the subscription mode. Defaults to None.

        subscribed_state(str, Optional):
            Subscribed state. Defaults to None.

        entitlements(dict[str, Any], Optional):
            entitlements. Defaults to None.

            * has_nsx_entitlement (bool, Optional):
                Flag indicating whether the resource has NSX entitlement or not. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "days_remaining_to_subscribe": "daysRemainingToSubscribe",
        "is_registered": "isRegistered",
        "is_subscribed": "isSubscribed",
        "licensing_mode": "licensingMode",
        "resource_id": "resourceId",
        "resource_type": "resourceType",
        "subscription_status": "subscriptionStatus",
        "subscribed_state": "subscribedState",
        "entitlements": "entitlements",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="patch",
        path="/v1/licensing-info".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "errors": "errors",
            "id": "id",
            "isCancellable": "is_cancellable",
            "isRetryable": "is_retryable",
            "localizableDescriptionPack": "localizable_description_pack",
            "name": "name",
            "resolutionStatus": "resolution_status",
            "resources": "resources",
            "status": "status",
            "subTasks": "sub_tasks",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def start_license_check_by_resource(
    hub, ctx, for_all_domains: bool = None
) -> Dict[str, Any]:
    r"""

    Start a license check for a resource
        None

    Args:
        for_all_domains(bool, Optional):
            Value that defines whether to get license check for all domains or not. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/resources/license-checks".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "endTimestamp": "end_timestamp",
            "id": "id",
            "resourceLicensingInfos": "resource_licensing_infos",
            "startTimestamp": "start_timestamp",
            "status": "status",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_license_check_result_by_id(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Retrieve the results of a license check by its ID
        None

    Args:
        id_(str):
            The resources license check ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/resources/license-checks/{id}".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "endTimestamp": "end_timestamp",
            "id": "id",
            "resourceLicensingInfos": "resource_licensing_infos",
            "startTimestamp": "start_timestamp",
            "status": "status",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def set_license_key_for_resource(hub, ctx) -> Dict[str, Any]:
    r"""

    Update a license key for a resource
        None


    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="put",
        path="/v1/resources/licensing-infos".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "errors": "errors",
            "id": "id",
            "isCancellable": "is_cancellable",
            "isRetryable": "is_retryable",
            "localizableDescriptionPack": "localizable_description_pack",
            "name": "name",
            "resolutionStatus": "resolution_status",
            "resources": "resources",
            "status": "status",
            "subTasks": "sub_tasks",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result

"""Utility functions for Target Upgrade Versions. """
from collections import OrderedDict
from typing import Any
from typing import Dict


async def get_release_by_domain_1(hub, ctx) -> Dict[str, Any]:
    r"""

    Retrieve a release view for all domains
        Get last selected upgrade version for WLDs.


    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/releases/domains".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_release_by_domain(hub, ctx, domain_id: str) -> Dict[str, Any]:
    r"""

    Retrieve a release view for a domain by its ID
        Get last selected upgrade version for the domain.

    Args:
        domain_id(str):
            Domain ID to get target version of the domain.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/releases/domains/{domainId}".format(**{"domainId": domain_id}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "domainId": "domain_id",
            "targetVersion": "target_version",
            "targetVxRailVersion": "target_vx_rail_version",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def update_release_by_domain_id(
    hub, ctx, domain_id: str, target_version: str, target_vx_rail_version: str = None
) -> Dict[str, Any]:
    r"""

    Modify the target upgrade release for a domain by its ID
        Update last selected upgrade version for the domain

    Args:
        domain_id(str):
            Domain ID.

        target_version(str):
            Target version.

        target_vx_rail_version(str, Optional):
            Target VxRail version. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "target_version": "targetVersion",
        "target_vx_rail_version": "targetVxRailVersion",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="patch",
        path="/v1/releases/domains/{domainId}".format(**{"domainId": domain_id}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result

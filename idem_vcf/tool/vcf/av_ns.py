"""Utility functions for Av Nss. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List


async def get_avns(hub, ctx, region_type: str = None) -> Dict[str, Any]:
    r"""

    Retrieve Application Virtual Network (AVN) details
        Returns all matching AVNs

    Args:
        region_type(str, Optional):
            Pass an optional AVN region type matching either Region-A or X-Region. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/avns".format(**{}),
        query_params={"regionType": region_type},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def create_avns(
    hub,
    ctx,
    avns: List[
        make_dataclass(
            "avns",
            [
                ("gateway", str),
                ("mtu", int),
                ("name", str),
                ("region_type", str),
                ("subnet", str),
                ("subnet_mask", str),
                ("domain_name", str, field(default=None)),
                ("id", str, field(default=None)),
                ("port_group_name", str, field(default=None)),
                ("router_name", str, field(default=None)),
                ("vlan_id", int, field(default=None)),
            ],
        )
    ],
    edge_cluster_id: str,
) -> Dict[str, Any]:
    r"""

    Create Application Virtual Networks (AVN)
        None

    Args:
        avns(List[dict[str, Any]]):
            The list of AVNs to be created.

            * domain_name (str, Optional):
                AVN search domain. Defaults to None.

            * gateway (str):
                Gateway of the AVN subnet.

            * id (str, Optional):
                The UUID of the AVN. Defaults to None.

            * mtu (int):
                MTU for the AVN.

            * name (str):
                User provided name of the AVN.

            * port_group_name (str, Optional):
                Port group name identifying the AVN on the distributed switch. Defaults to None.

            * region_type (str):
                The region type of AVN; X_REGION or REGION_A.

            * router_name (str, Optional):
                Name of Tier-1 Gateway AVN is connected to. Defaults to None.

            * subnet (str):
                Subnet assigned to the AVN.

            * subnet_mask (str):
                Subnet mask for the network assigned to the AVN.

            * vlan_id (int, Optional):
                VLAN ID for the VLAN backed AVN. Defaults to None.

        edge_cluster_id(str):
            The UUID of the Edge Cluster to associate AVNs.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"avns": "avns", "edge_cluster_id": "edgeClusterId"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/avns".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "errors": "errors",
            "id": "id",
            "isCancellable": "is_cancellable",
            "isRetryable": "is_retryable",
            "localizableDescriptionPack": "localizable_description_pack",
            "name": "name",
            "resolutionStatus": "resolution_status",
            "resources": "resources",
            "status": "status",
            "subTasks": "sub_tasks",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def validate_avn_spec(
    hub,
    ctx,
    avns: List[
        make_dataclass(
            "avns",
            [
                ("gateway", str),
                ("mtu", int),
                ("name", str),
                ("region_type", str),
                ("subnet", str),
                ("subnet_mask", str),
                ("domain_name", str, field(default=None)),
                ("id", str, field(default=None)),
                ("port_group_name", str, field(default=None)),
                ("router_name", str, field(default=None)),
                ("vlan_id", int, field(default=None)),
            ],
        )
    ],
    edge_cluster_id: str,
) -> Dict[str, Any]:
    r"""

    Perform validation of the AvnsCreationSpec specification
        Returns Validation report

    Args:
        avns(List[dict[str, Any]]):
            The list of AVNs to be created.

            * domain_name (str, Optional):
                AVN search domain. Defaults to None.

            * gateway (str):
                Gateway of the AVN subnet.

            * id (str, Optional):
                The UUID of the AVN. Defaults to None.

            * mtu (int):
                MTU for the AVN.

            * name (str):
                User provided name of the AVN.

            * port_group_name (str, Optional):
                Port group name identifying the AVN on the distributed switch. Defaults to None.

            * region_type (str):
                The region type of AVN; X_REGION or REGION_A.

            * router_name (str, Optional):
                Name of Tier-1 Gateway AVN is connected to. Defaults to None.

            * subnet (str):
                Subnet assigned to the AVN.

            * subnet_mask (str):
                Subnet mask for the network assigned to the AVN.

            * vlan_id (int, Optional):
                VLAN ID for the VLAN backed AVN. Defaults to None.

        edge_cluster_id(str):
            The UUID of the Edge Cluster to associate AVNs.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"avns": "avns", "edge_cluster_id": "edgeClusterId"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/avns/validations".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "additionalProperties": "additional_properties",
            "description": "description",
            "executionStatus": "execution_status",
            "id": "id",
            "resultStatus": "result_status",
            "validationChecks": "validation_checks",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result

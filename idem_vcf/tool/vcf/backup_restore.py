"""Utility functions for Backup Restores. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List


async def start_backup(
    hub, ctx, elements: List[make_dataclass("elements", [("resource_type", str)])]
) -> Dict[str, Any]:
    r"""

    Start a backup operation
        None

    Args:
        elements(List[dict[str, Any]]):
            List of elements to be backed up.

            * resource_type (str):
                Resource type.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"elements": "elements"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/backups/tasks".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "errors": "errors",
            "id": "id",
            "name": "name",
            "resources": "resources",
            "status": "status",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def start_restore(
    hub,
    ctx,
    backup_file: str,
    elements: List[make_dataclass("elements", [("resource_type", str)])],
    encryption: make_dataclass("encryption", [("passphrase", str)]),
) -> Dict[str, Any]:
    r"""

    Start a restore operation
        None

    Args:
        backup_file(str):
            Backup file name.

        elements(List[dict[str, Any]]):
            List of elements to be restored.

            * resource_type (str):
                Resource type.

        encryption(dict[str, Any]):
            encryption.

            * passphrase (str):
                Passphrase.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "backup_file": "backupFile",
        "elements": "elements",
        "encryption": "encryption",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/restores/tasks".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "errors": "errors",
            "id": "id",
            "isCancellable": "is_cancellable",
            "isRetryable": "is_retryable",
            "localizableDescriptionPack": "localizable_description_pack",
            "name": "name",
            "resolutionStatus": "resolution_status",
            "resources": "resources",
            "status": "status",
            "subTasks": "sub_tasks",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_restore_task(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Retrieve the restore task
        None

    Args:
        id_(str):
            The restore task ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/restores/tasks/{id}".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "errors": "errors",
            "id": "id",
            "isCancellable": "is_cancellable",
            "isRetryable": "is_retryable",
            "localizableDescriptionPack": "localizable_description_pack",
            "name": "name",
            "resolutionStatus": "resolution_status",
            "resources": "resources",
            "status": "status",
            "subTasks": "sub_tasks",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_backup_configuration(hub, ctx) -> Dict[str, Any]:
    r"""

    Retrieve the backup configuration for SDDC Manager and NSX Manager
        None


    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/system/backup-configuration".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "backupLocations": "backup_locations",
            "backupSchedules": "backup_schedules",
            "encryption": "encryption",
            "isConfigured": "is_configured",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def set_backup_configuration(
    hub,
    ctx,
    backup_locations: List[
        make_dataclass(
            "backup_locations",
            [
                ("directory_path", str),
                ("port", int),
                ("protocol", str),
                ("server", str),
                ("username", str),
                ("password", str, field(default=None)),
                ("ssh_fingerprint", str, field(default=None)),
            ],
        )
    ] = None,
    backup_schedules: List[
        make_dataclass(
            "backup_schedules",
            [
                ("frequency", str),
                ("resource_type", str),
                ("days_of_week", List[str], field(default=None)),
                ("hour_of_day", int, field(default=None)),
                ("minute_of_hour", int, field(default=None)),
                (
                    "retention_policy",
                    make_dataclass(
                        "retention_policy",
                        [
                            ("number_of_most_recent_backups", int),
                            (
                                "number_of_days_of_daily_backups",
                                int,
                                field(default=None),
                            ),
                            (
                                "number_of_days_of_hourly_backups",
                                int,
                                field(default=None),
                            ),
                        ],
                    ),
                    field(default=None),
                ),
                ("take_backup_on_state_change", bool, field(default=None)),
                ("take_scheduled_backups", bool, field(default=None)),
            ],
        )
    ] = None,
    encryption: make_dataclass("encryption", [("passphrase", str)]) = None,
) -> Dict[str, Any]:
    r"""

    Configure the backup configuration for SDDC Manager and NSX Manager
        None

    Args:
        backup_locations(List[dict[str, Any]], Optional):
            List of backup locations. Defaults to None.

            * directory_path (str):
                Full directory path to save the backup files.

            * password (str, Optional):
                Backup server password. Defaults to None.

            * port (int):
                Port number for the backup server to connect.

            * protocol (str):
                Protocol to be used for transferring files.

            * server (str):
                IP address or FQDN  of the backup server.

            * ssh_fingerprint (str, Optional):
                SSH fingerprint of the backup server. Defaults to None.

            * username (str):
                Password for backup server username.

        backup_schedules(List[dict[str, Any]], Optional):
            List of backup schedules. Defaults to None.

            * days_of_week (List[str], Optional):
                List of days of the week to schedule backup. Defaults to None.

            * frequency (str):
                Backup frequency.

            * hour_of_day (int, Optional):
                Hour of the day to schedule backup. Defaults to None.

            * minute_of_hour (int, Optional):
                Minute of the hour to schedule backup. Defaults to None.

            * resource_type (str):
                Resource type to configure backup schedule.

            * retention_policy (dict[str, Any], Optional):
                retentionPolicy. Defaults to None.

                * number_of_days_of_daily_backups (int, Optional):
                    This attribute controls the number of daily backup files to retain, measured in days. Range 0 to 30 days. The system will filter the existing backup files, and retain one for every day for the specified number of days, counting back from the most recent backup. Defaults to None.

                * number_of_days_of_hourly_backups (int, Optional):
                    This attribute controls the number of hourly backup files to retain, measured in days. Range 0 to 14 days. The system will filter the existing backup files, and retain one for every hour for the specified number of days, counting back from the most recent backup. Defaults to None.

                * number_of_most_recent_backups (int):
                    This attribute controls the number of recent backup files to retain. Range 1 to 600 backup files.

            * take_backup_on_state_change (bool, Optional):
                Enable/disable backups on state change. If enabled, SDDC Manager will take a backup after the successful completion of an operation that changes its state. This mode requires that scheduled backups be enabled. Defaults to None.

            * take_scheduled_backups (bool, Optional):
                Enable/disable scheduled backups. Defaults to None.

        encryption(dict[str, Any], Optional):
            encryption. Defaults to None.

            * passphrase (str):
                Passphrase.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "backup_locations": "backupLocations",
        "backup_schedules": "backupSchedules",
        "encryption": "encryption",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="put",
        path="/v1/system/backup-configuration".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "errors": "errors",
            "id": "id",
            "isCancellable": "is_cancellable",
            "isRetryable": "is_retryable",
            "localizableDescriptionPack": "localizable_description_pack",
            "name": "name",
            "resolutionStatus": "resolution_status",
            "resources": "resources",
            "status": "status",
            "subTasks": "sub_tasks",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def update_backup_configuration(
    hub,
    ctx,
    backup_locations: List[
        make_dataclass(
            "backup_locations",
            [
                ("directory_path", str),
                ("port", int),
                ("protocol", str),
                ("server", str),
                ("username", str),
                ("password", str, field(default=None)),
                ("ssh_fingerprint", str, field(default=None)),
            ],
        )
    ] = None,
    backup_schedules: List[
        make_dataclass(
            "backup_schedules",
            [
                ("frequency", str),
                ("resource_type", str),
                ("days_of_week", List[str], field(default=None)),
                ("hour_of_day", int, field(default=None)),
                ("minute_of_hour", int, field(default=None)),
                (
                    "retention_policy",
                    make_dataclass(
                        "retention_policy",
                        [
                            ("number_of_most_recent_backups", int),
                            (
                                "number_of_days_of_daily_backups",
                                int,
                                field(default=None),
                            ),
                            (
                                "number_of_days_of_hourly_backups",
                                int,
                                field(default=None),
                            ),
                        ],
                    ),
                    field(default=None),
                ),
                ("take_backup_on_state_change", bool, field(default=None)),
                ("take_scheduled_backups", bool, field(default=None)),
            ],
        )
    ] = None,
    encryption: make_dataclass("encryption", [("passphrase", str)]) = None,
) -> Dict[str, Any]:
    r"""

    Update the backup configuration for SDDC Manager and NSX Manager
        None

    Args:
        backup_locations(List[dict[str, Any]], Optional):
            List of backup locations. Defaults to None.

            * directory_path (str):
                Full directory path to save the backup files.

            * password (str, Optional):
                Backup server password. Defaults to None.

            * port (int):
                Port number for the backup server to connect.

            * protocol (str):
                Protocol to be used for transferring files.

            * server (str):
                IP address or FQDN  of the backup server.

            * ssh_fingerprint (str, Optional):
                SSH fingerprint of the backup server. Defaults to None.

            * username (str):
                Password for backup server username.

        backup_schedules(List[dict[str, Any]], Optional):
            List of backup schedules. Defaults to None.

            * days_of_week (List[str], Optional):
                List of days of the week to schedule backup. Defaults to None.

            * frequency (str):
                Backup frequency.

            * hour_of_day (int, Optional):
                Hour of the day to schedule backup. Defaults to None.

            * minute_of_hour (int, Optional):
                Minute of the hour to schedule backup. Defaults to None.

            * resource_type (str):
                Resource type to configure backup schedule.

            * retention_policy (dict[str, Any], Optional):
                retentionPolicy. Defaults to None.

                * number_of_days_of_daily_backups (int, Optional):
                    This attribute controls the number of daily backup files to retain, measured in days. Range 0 to 30 days. The system will filter the existing backup files, and retain one for every day for the specified number of days, counting back from the most recent backup. Defaults to None.

                * number_of_days_of_hourly_backups (int, Optional):
                    This attribute controls the number of hourly backup files to retain, measured in days. Range 0 to 14 days. The system will filter the existing backup files, and retain one for every hour for the specified number of days, counting back from the most recent backup. Defaults to None.

                * number_of_most_recent_backups (int):
                    This attribute controls the number of recent backup files to retain. Range 1 to 600 backup files.

            * take_backup_on_state_change (bool, Optional):
                Enable/disable backups on state change. If enabled, SDDC Manager will take a backup after the successful completion of an operation that changes its state. This mode requires that scheduled backups be enabled. Defaults to None.

            * take_scheduled_backups (bool, Optional):
                Enable/disable scheduled backups. Defaults to None.

        encryption(dict[str, Any], Optional):
            encryption. Defaults to None.

            * passphrase (str):
                Passphrase.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "backup_locations": "backupLocations",
        "backup_schedules": "backupSchedules",
        "encryption": "encryption",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="patch",
        path="/v1/system/backup-configuration".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "errors": "errors",
            "id": "id",
            "isCancellable": "is_cancellable",
            "isRetryable": "is_retryable",
            "localizableDescriptionPack": "localizable_description_pack",
            "name": "name",
            "resolutionStatus": "resolution_status",
            "resources": "resources",
            "status": "status",
            "subTasks": "sub_tasks",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result

"""Utility functions for Soss. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List


async def get_health_check_task(hub, ctx) -> Dict[str, Any]:
    r"""

    Retieve a list of Health Check tasks
        None


    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/system/health-summary".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def start_health_check(
    hub,
    ctx,
    health_checks: make_dataclass(
        "health_checks",
        [
            ("certificate_health", bool, field(default=None)),
            ("composability_health", bool, field(default=None)),
            ("compute_health", bool, field(default=None)),
            ("connectivity_health", bool, field(default=None)),
            ("dns_health", bool, field(default=None)),
            ("general_health", bool, field(default=None)),
            ("hardware_compatibility_health", bool, field(default=None)),
            ("ntp_health", bool, field(default=None)),
            ("password_health", bool, field(default=None)),
            ("services_health", bool, field(default=None)),
            ("storage_health", bool, field(default=None)),
            ("version_health", bool, field(default=None)),
        ],
    ) = None,
    options: make_dataclass(
        "options",
        [
            (
                "config",
                make_dataclass(
                    "config",
                    [
                        ("force", bool, field(default=None)),
                        ("skip_known_host_check", bool, field(default=None)),
                    ],
                ),
                field(default=None),
            ),
            (
                "include",
                make_dataclass(
                    "include",
                    [
                        ("precheck_report", bool, field(default=None)),
                        ("summary_report", bool, field(default=None)),
                    ],
                ),
                field(default=None),
            ),
        ],
    ) = None,
    scope: make_dataclass(
        "scope",
        [
            (
                "domains",
                List[
                    make_dataclass(
                        "domains",
                        [
                            ("cluster_names", List[str], field(default=None)),
                            ("domain_name", str, field(default=None)),
                        ],
                    )
                ],
                field(default=None),
            ),
            ("include_all_domains", bool, field(default=None)),
            ("include_free_hosts", bool, field(default=None)),
        ],
    ) = None,
) -> Dict[str, Any]:
    r"""

    Start a Health Check operation using SoS
        None

    Args:
        health_checks(dict[str, Any], Optional):
            healthChecks. Defaults to None.

            * certificate_health (bool, Optional):
                Performs Certificate health checks. Defaults to None.

            * composability_health (bool, Optional):
                Performs Composability Health checks. Defaults to None.

            * compute_health (bool, Optional):
                Performs Compute Health checks. Defaults to None.

            * connectivity_health (bool, Optional):
                Performs Connectivity health checks. Defaults to None.

            * dns_health (bool, Optional):
                Performs DNS Health checks. Defaults to None.

            * general_health (bool, Optional):
                Performs some generic health checks. Defaults to None.

            * hardware_compatibility_health (bool, Optional):
                Performs HardwareCompatibilityHealth Health checks. Defaults to None.

            * ntp_health (bool, Optional):
                Performs NTP checks for components. Defaults to None.

            * password_health (bool, Optional):
                Performs Password Expiry checks. Defaults to None.

            * services_health (bool, Optional):
                Performs checks on Services health. Defaults to None.

            * storage_health (bool, Optional):
                Performs Storage Health checks. Defaults to None.

            * version_health (bool, Optional):
                Perform version checks for VCF components. Defaults to None.

        options(dict[str, Any], Optional):
            options. Defaults to None.

            * config (dict[str, Any], Optional):
                config. Defaults to None.

                * force (bool, Optional):
                    Run SOS operations, even if there is a Workload running. Defaults to None.

                * skip_known_host_check (bool, Optional):
                    Skip known_hosts file for HealthSummary. Defaults to None.

            * include (dict[str, Any], Optional):
                include. Defaults to None.

                * precheck_report (bool, Optional):
                    Collect VCF LCM Upgrade Pre-check Reports, Default value is False. Defaults to None.

                * summary_report (bool, Optional):
                    Collect Vcf Summary Reports. Defaults to None.

        scope(dict[str, Any], Optional):
            scope. Defaults to None.

            * domains (List[dict[str, Any]], Optional):
                Domains and Clusters for SOS operation. Defaults to None.

                * cluster_names (List[str], Optional):
                    Clusters for the operation. Defaults to None.

                * domain_name (str, Optional):
                    Domain name. Defaults to None.

            * include_all_domains (bool, Optional):
                Include all domains for SOS operation. Defaults to None.

            * include_free_hosts (bool, Optional):
                Include free hosts. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"health_checks": "healthChecks", "options": "options", "scope": "scope"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/system/health-summary".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "bundleAvailable": "bundle_available",
            "bundleName": "bundle_name",
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "description": "description",
            "id": "id",
            "status": "status",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_health_check_status(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Retrieve the status of the Health Check operations
        None

    Args:
        id_(str):
            The Health Summary Id.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/system/health-summary/{id}".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "bundleAvailable": "bundle_available",
            "bundleName": "bundle_name",
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "description": "description",
            "id": "id",
            "status": "status",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def export_health_check_by_id(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Download a Health Check tar.gz by its ID
        None

    Args:
        id_(str):
            The Health Summary Id.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/system/health-summary/{id}/data".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def get_support_bundle_task(hub, ctx) -> Dict[str, Any]:
    r"""

    Retrieve a list of Support Bundle tasks
        None


    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/system/support-bundles".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def start_support_bundle(
    hub,
    ctx,
    logs: make_dataclass(
        "logs",
        [
            ("api_logs", bool, field(default=None)),
            ("automation_logs", bool, field(default=None)),
            ("esx_logs", bool, field(default=None)),
            ("lifecycle_logs", bool, field(default=None)),
            ("nsx_logs", bool, field(default=None)),
            ("operations_for_logs", bool, field(default=None)),
            ("operations_logs", bool, field(default=None)),
            ("sddc_manager_logs", bool, field(default=None)),
            ("system_debug_logs", bool, field(default=None)),
            ("vc_logs", bool, field(default=None)),
            ("vm_screenshots", bool, field(default=None)),
            ("vra_logs", bool, field(default=None)),
            ("vrli_logs", bool, field(default=None)),
            ("vrops_logs", bool, field(default=None)),
            ("vrslcm_logs", bool, field(default=None)),
            ("vxrail_manager_logs", bool, field(default=None)),
            ("wcp_logs", bool, field(default=None)),
        ],
    ) = None,
    options: make_dataclass(
        "options",
        [
            (
                "config",
                make_dataclass(
                    "config",
                    [
                        ("force", bool, field(default=None)),
                        ("skip_known_host_check", bool, field(default=None)),
                    ],
                ),
                field(default=None),
            ),
            (
                "include",
                make_dataclass(
                    "include",
                    [
                        ("health_check", bool, field(default=None)),
                        ("summary_report", bool, field(default=None)),
                    ],
                ),
                field(default=None),
            ),
        ],
    ) = None,
    scope: make_dataclass(
        "scope",
        [
            (
                "domains",
                List[
                    make_dataclass(
                        "domains",
                        [
                            ("cluster_names", List[str], field(default=None)),
                            ("domain_name", str, field(default=None)),
                        ],
                    )
                ],
                field(default=None),
            ),
            ("include_free_hosts", bool, field(default=None)),
        ],
    ) = None,
) -> Dict[str, Any]:
    r"""

    Start a Support Bundle operation using SoS
        None

    Args:
        logs(dict[str, Any], Optional):
            logs. Defaults to None.

            * api_logs (bool, Optional):
                Collect API Logs. Defaults to None.

            * automation_logs (bool, Optional):
                Collect VMware Aria Automation support logs. Defaults to None.

            * esx_logs (bool, Optional):
                Collect ESX Logs. Defaults to None.

            * lifecycle_logs (bool, Optional):
                Collect VMware Aria Suite Lifecycle support logs. Defaults to None.

            * nsx_logs (bool, Optional):
                Collect NSX Logs. Defaults to None.

            * operations_for_logs (bool, Optional):
                Collect VMware Aria Operations For Logs support logs. Defaults to None.

            * operations_logs (bool, Optional):
                Collect VMware Aria Operations support logs. Defaults to None.

            * sddc_manager_logs (bool, Optional):
                Collect SDDC Manager Logs. Defaults to None.

            * system_debug_logs (bool, Optional):
                Collect SystemDebug Logs. Defaults to None.

            * vc_logs (bool, Optional):
                Collect vCenter Logs. Defaults to None.

            * vm_screenshots (bool, Optional):
                Collect VMScreenshots. Defaults to None.

            * vra_logs (bool, Optional):
                [Deprecated] Collect VMware Aria Automation Logs. Defaults to None.

            * vrli_logs (bool, Optional):
                [Deprecated] Collect VMware Aria Operations For Logs Logs. Defaults to None.

            * vrops_logs (bool, Optional):
                [Deprecated] Collect VMware Aria Operations Logs. Defaults to None.

            * vrslcm_logs (bool, Optional):
                [Deprecated] Collect VMware Aria Suite Lifecycle Logs. Defaults to None.

            * vxrail_manager_logs (bool, Optional):
                Collect VXRailManager Logs. Defaults to None.

            * wcp_logs (bool, Optional):
                Collect WCP Logs. Defaults to None.

        options(dict[str, Any], Optional):
            options. Defaults to None.

            * config (dict[str, Any], Optional):
                config. Defaults to None.

                * force (bool, Optional):
                    Run SOS operations, even if there is a Workload running. Defaults to None.

                * skip_known_host_check (bool, Optional):
                    Skip known_hosts file for SupportBundle collection. Defaults to None.

            * include (dict[str, Any], Optional):
                include. Defaults to None.

                * health_check (bool, Optional):
                    Perform SOS Health checks. Defaults to None.

                * summary_report (bool, Optional):
                    Collect Vcf Summary Reports. Defaults to None.

        scope(dict[str, Any], Optional):
            scope. Defaults to None.

            * domains (List[dict[str, Any]], Optional):
                Domains and Clusters for SOS operation. Defaults to None.

                * cluster_names (List[str], Optional):
                    Clusters for the operation. Defaults to None.

                * domain_name (str, Optional):
                    Domain name. Defaults to None.

            * include_free_hosts (bool, Optional):
                Include free hosts. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"logs": "logs", "options": "options", "scope": "scope"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/system/support-bundles".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "bundleAvailable": "bundle_available",
            "bundleName": "bundle_name",
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "description": "description",
            "id": "id",
            "status": "status",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_support_bundle_status(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Retrive the status of the Support Bundle operation
        None

    Args:
        id_(str):
            The Support Bundle ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/system/support-bundles/{id}".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "bundleAvailable": "bundle_available",
            "bundleName": "bundle_name",
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "description": "description",
            "id": "id",
            "status": "status",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def export_support_bundle_by_id(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Download a Support Bundle tar.gz by ID
        None

    Args:
        id_(str):
            The Support Bundle ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/system/support-bundles/{id}/data".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result

"""Utility functions for Vasa Providerss. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List


async def validate_vasa_provider_spec(
    hub,
    ctx,
    storage_containers: List[
        make_dataclass(
            "storage_containers",
            [
                ("name", str),
                ("protocol_type", str),
                ("cluster_id", str, field(default=None)),
                ("id", str, field(default=None)),
            ],
        )
    ],
    url: str,
    users: List[
        make_dataclass(
            "users",
            [("password", str), ("username", str), ("id", str, field(default=None))],
        )
    ],
    id_: str = None,
) -> Dict[str, Any]:
    r"""

    Validate VasaProvider input specification
        None

    Args:
        storage_containers(List[dict[str, Any]]):
            List of storage containers associated with the VASA Provider.

            * cluster_id (str, Optional):
                ID of the cluster which is using the storage container. Defaults to None.

            * id (str, Optional):
                ID of the storage container. Defaults to None.

            * name (str):
                Name of the storage container.

            * protocol_type (str):
                Storage protocol type.

        url(str):
            URL of the VASA Provider.

        users(List[dict[str, Any]]):
            List of users associated with the VASA Provider.

            * id (str, Optional):
                ID of the VASA User. Defaults to None.

            * password (str):
                Password.

            * username (str):
                VASA User name.

        id_(str, Optional):
            ID of the VASA Provider. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "id": "id",
        "name": "name",
        "storage_containers": "storageContainers",
        "url": "url",
        "users": "users",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/vasa-providers/validations".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "additionalProperties": "additional_properties",
            "description": "description",
            "executionStatus": "execution_status",
            "id": "id",
            "resultStatus": "result_status",
            "validationChecks": "validation_checks",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_vasa_provider_validation(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Get the status of the validation of the VASA Provider
        None

    Args:
        id_(str):
            The validation ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/vasa-providers/validations/{id}".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "additionalProperties": "additional_properties",
            "description": "description",
            "executionStatus": "execution_status",
            "id": "id",
            "resultStatus": "result_status",
            "validationChecks": "validation_checks",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_vasa_provider_storage_containers(
    hub, ctx, id_: str, protocol_type: str = None
) -> Dict[str, Any]:
    r"""

    Get the storage containers of a VASA Provider
        None

    Args:
        id_(str):
            VASA Provider ID.

        protocol_type(str, Optional):
            Pass an optional Storage Protocol type. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/vasa-providers/{id}/storage-containers".format(**{"id": id_}),
        query_params={"protocolType": protocol_type},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def add_vasa_provider_storage_container(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Add the storage containers to a VASA Provider
        None

    Args:
        id_(str):
            VASA Provider ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/vasa-providers/{id}/storage-containers".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def remove_vasa_provider_storage_container(
    hub, ctx, id_: str, storage_container_id: str
) -> Dict[str, Any]:
    r"""

    Delete a storage container of a VASA Provider
        None

    Args:
        id_(str):
            VASA Provider ID.

        storage_container_id(str):
            Storage container ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="delete",
        path="/v1/vasa-providers/{id}/storage-containers/{storageContainerId}".format(
            **{"id": id_, "storageContainerId": storage_container_id}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def update_vasa_provider_storage_container(
    hub, ctx, id_: str, storage_container_id: str
) -> Dict[str, Any]:
    r"""

    Update the storage container of a VASA Provider
        None

    Args:
        id_(str):
            VASA Provider ID.

        storage_container_id(str):
            Storage container ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"name": "name"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="patch",
        path="/v1/vasa-providers/{id}/storage-containers/{storageContainerId}".format(
            **{"id": id_, "storageContainerId": storage_container_id}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "id": "id",
            "name": "name",
            "storageContainers": "storage_containers",
            "url": "url",
            "users": "users",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_vasa_provider_user(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Get the users of a VASA Provider
        None

    Args:
        id_(str):
            VASA Provider ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/vasa-providers/{id}/users".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def add_vasa_provider_user(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Add the users to a VASA Provider
        None

    Args:
        id_(str):
            VASA Provider ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/vasa-providers/{id}/users".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def update_vasa_provider_user(
    hub, ctx, id_: str, user_id: str, password: str = None, username: str = None
) -> Dict[str, Any]:
    r"""

    Update the user of a VASA Provider
        None

    Args:
        id_(str):
            VASA Provider ID.

        user_id(str):
            User ID.

        password(str, Optional):
            Password. Defaults to None.

        username(str, Optional):
            VASA User name. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"password": "password", "username": "username"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="patch",
        path="/v1/vasa-providers/{id}/users/{userId}".format(
            **{"id": id_, "userId": user_id}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "id": "id",
            "name": "name",
            "storageContainers": "storage_containers",
            "url": "url",
            "users": "users",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result

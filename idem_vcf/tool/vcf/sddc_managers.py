"""Utility functions for Sddc Managerss. """
from typing import Any
from typing import Dict


async def get_local_os_user_accounts(hub, ctx) -> Dict[str, Any]:
    r"""

    Get local OS user accounts from the SDDC Manager appliance
        None


    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/sddc-manager/local-os-user-accounts".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result

"""Utility functions for Identity Providerss. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List


async def add_embedded_identity_source(
    hub,
    ctx,
    id_: str,
    ldap: make_dataclass(
        "ldap",
        [
            ("domain_name", str),
            ("password", str),
            (
                "source_details",
                make_dataclass(
                    "source_details",
                    [
                        ("groups_base_dn", str),
                        ("server_endpoints", List[str]),
                        ("users_base_dn", str),
                        ("cert_chain", List[str], field(default=None)),
                    ],
                ),
            ),
            ("type", str),
            ("username", str),
            ("domain_alias", str, field(default=None)),
        ],
    ),
) -> Dict[str, Any]:
    r"""

    Add a new identity source to the embedded identity provider
        Add an identity source to an embedded IDP by id, if it exists

    Args:
        id_(str):
            ID of Identity Provider.

        ldap(dict[str, Any]):
            ldap.

            * domain_alias (str, Optional):
                The optional alias to associate the domain name. Defaults to None.

            * domain_name (str):
                The name to associate with the created domain.

            * password (str):
                Password to connect to the ldap(s) server.

            * source_details (dict[str, Any]):
                sourceDetails.

                * cert_chain (List[str], Optional):
                    SSL certificate chain in base64 encoding. This field can be unset only, if all the active directory server endpoints use the LDAP (not LDAPS) protocol. Defaults to None.

                * groups_base_dn (str):
                    Base distinguished name for groups.

                * server_endpoints (List[str]):
                    Active directory server endpoints. At least one active directory server endpoint must be set.

                * users_base_dn (str):
                    Base distinguished name for users.

            * type (str):
                The type of the LDAP Server.

            * username (str):
                User name to connect to ldap(s) server.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"ldap": "ldap", "name": "name"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/identity-providers/{id}/identity-sources".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def delete_identity_source(
    hub, ctx, id_: str, domain_name: str
) -> Dict[str, Any]:
    r"""

    Delete an Identity Source
        Delete an Identity Source by domain name, if it exists

    Args:
        id_(str):
            ID of Identity Provider.

        domain_name(str):
            Domain Name associated with the identity source.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="delete",
        path="/v1/identity-providers/{id}/identity-sources/{domainName}".format(
            **{"id": id_, "domainName": domain_name}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def update_embedded_identity_source(
    hub,
    ctx,
    id_: str,
    domain_name: str,
    ldap: make_dataclass(
        "ldap",
        [
            ("domain_name", str),
            ("password", str),
            (
                "source_details",
                make_dataclass(
                    "source_details",
                    [
                        ("groups_base_dn", str),
                        ("server_endpoints", List[str]),
                        ("users_base_dn", str),
                        ("cert_chain", List[str], field(default=None)),
                    ],
                ),
            ),
            ("type", str),
            ("username", str),
            ("domain_alias", str, field(default=None)),
        ],
    ),
) -> Dict[str, Any]:
    r"""

    Update an identity source
        Update the identity source associated with the embedded IDP by name, if it exists

    Args:
        id_(str):
            ID of Identity Provider.

        domain_name(str):
            Domain Name associated with the identity source.

        ldap(dict[str, Any]):
            ldap.

            * domain_alias (str, Optional):
                The optional alias to associate the domain name. Defaults to None.

            * domain_name (str):
                The name to associate with the created domain.

            * password (str):
                Password to connect to the ldap(s) server.

            * source_details (dict[str, Any]):
                sourceDetails.

                * cert_chain (List[str], Optional):
                    SSL certificate chain in base64 encoding. This field can be unset only, if all the active directory server endpoints use the LDAP (not LDAPS) protocol. Defaults to None.

                * groups_base_dn (str):
                    Base distinguished name for groups.

                * server_endpoints (List[str]):
                    Active directory server endpoints. At least one active directory server endpoint must be set.

                * users_base_dn (str):
                    Base distinguished name for users.

            * type (str):
                The type of the LDAP Server.

            * username (str):
                User name to connect to ldap(s) server.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"ldap": "ldap", "name": "name"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="patch",
        path="/v1/identity-providers/{id}/identity-sources/{domainName}".format(
            **{"id": id_, "domainName": domain_name}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def generate_sync_client_token(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Generate new sync client token
        Generates a new sync client token

    Args:
        id_(str):
            ID of Identity Provider.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/identity-providers/{id}/sync-client".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"expireAt": "expire_at", "scimUrl": "scim_url", "token": "token"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result

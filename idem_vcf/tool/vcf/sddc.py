"""Utility functions for Sddcs. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List


async def validate_bringup_spec(
    hub,
    ctx,
    cluster_spec: make_dataclass(
        "cluster_spec",
        [
            ("cluster_name", str),
            ("cluster_evc_mode", str, field(default=None)),
            ("cluster_image_enabled", bool, field(default=None)),
            ("host_failures_to_tolerate", int, field(default=None)),
            ("personality_name", str, field(default=None)),
            (
                "resource_pool_specs",
                List[
                    make_dataclass(
                        "resource_pool_specs",
                        [
                            ("memory_reservation_expandable", bool),
                            ("name", str),
                            ("cpu_limit", int, field(default=None)),
                            ("cpu_reservation_expandable", bool, field(default=None)),
                            ("cpu_reservation_mhz", int, field(default=None)),
                            ("cpu_reservation_percentage", int, field(default=None)),
                            ("cpu_shares_level", str, field(default=None)),
                            ("cpu_shares_value", int, field(default=None)),
                            ("memory_limit", int, field(default=None)),
                            ("memory_reservation_mb", int, field(default=None)),
                            ("memory_reservation_percentage", int, field(default=None)),
                            ("memory_shares_level", str, field(default=None)),
                            ("memory_shares_value", int, field(default=None)),
                            ("type", str, field(default=None)),
                        ],
                    )
                ],
                field(default=None),
            ),
            ("vm_folders", Dict, field(default=None)),
        ],
    ),
    dns_spec: make_dataclass(
        "dns_spec",
        [
            ("domain", str),
            ("subdomain", str),
            ("nameserver", str, field(default=None)),
            ("secondary_nameserver", str, field(default=None)),
        ],
    ),
    dvs_specs: List[
        make_dataclass(
            "dvs_specs",
            [
                ("dvs_name", str),
                ("networks", List[str]),
                ("is_used_by_nsxt", bool, field(default=None)),
                ("mtu", int, field(default=None)),
                (
                    "nioc_specs",
                    List[
                        make_dataclass(
                            "nioc_specs", [("traffic_type", str), ("value", str)]
                        )
                    ],
                    field(default=None),
                ),
                ("vmnics", List[str], field(default=None)),
                (
                    "nsxt_switch_config",
                    make_dataclass(
                        "nsxt_switch_config",
                        [
                            (
                                "transport_zones",
                                List[
                                    make_dataclass(
                                        "transport_zones",
                                        [
                                            ("transport_type", str),
                                            ("name", str, field(default=None)),
                                        ],
                                    )
                                ],
                                field(default=None),
                            ),
                            ("host_switch_operational_mode", str, field(default=None)),
                        ],
                    ),
                    field(default=None),
                ),
                (
                    "vmnics_to_uplinks",
                    List[
                        make_dataclass(
                            "vmnics_to_uplinks",
                            [("nsx_uplink_name", str), ("vds_uplink_name", str)],
                        )
                    ],
                    field(default=None),
                ),
            ],
        )
    ],
    host_specs: List[
        make_dataclass(
            "host_specs",
            [
                ("association", str),
                (
                    "credentials",
                    make_dataclass(
                        "credentials", [("password", str), ("username", str)]
                    ),
                ),
                ("hostname", str),
                (
                    "ip_address_private",
                    make_dataclass(
                        "ip_address_private",
                        [
                            ("ip_address", str),
                            ("cidr", str, field(default=None)),
                            ("gateway", str, field(default=None)),
                            ("subnet", str, field(default=None)),
                        ],
                    ),
                ),
                ("v_switch", str),
                ("ssh_thumbprint", str, field(default=None)),
                ("ssl_thumbprint", str, field(default=None)),
                ("vswitch", str, field(default=None)),
            ],
        )
    ],
    network_specs: List[
        make_dataclass(
            "network_specs",
            [
                ("network_type", str),
                ("vlan_id", str),
                ("active_uplinks", List[str], field(default=None)),
                ("exclude_ip_address_ranges", List[str], field(default=None)),
                ("exclude_ipaddresses", List[str], field(default=None)),
                ("gateway", str, field(default=None)),
                ("include_ip_address", List[str], field(default=None)),
                (
                    "include_ip_address_ranges",
                    List[
                        make_dataclass(
                            "include_ip_address_ranges",
                            [("end_ip_address", str), ("start_ip_address", str)],
                        )
                    ],
                    field(default=None),
                ),
                ("mtu", str, field(default=None)),
                ("port_group_key", str, field(default=None)),
                ("standby_uplinks", List[str], field(default=None)),
                ("subnet", str, field(default=None)),
                ("subnet_mask", str, field(default=None)),
                ("teaming_policy", str, field(default=None)),
            ],
        )
    ],
    ntp_servers: List[str],
    sddc_id: str,
    task_name: str,
    vcenter_spec: make_dataclass(
        "vcenter_spec",
        [
            ("root_vcenter_password", str),
            ("vcenter_hostname", str),
            ("license_file", str, field(default=None)),
            ("ssh_thumbprint", str, field(default=None)),
            ("ssl_thumbprint", str, field(default=None)),
            ("storage_size", str, field(default=None)),
            ("vcenter_ip", str, field(default=None)),
            ("vm_size", str, field(default=None)),
        ],
    ),
    redo: bool = None,
    ceip_enabled: bool = None,
    dv_switch_version: str = None,
    esx_license: str = None,
    excluded_components: List[str] = None,
    fips_enabled: bool = None,
    management_pool_name: str = None,
    nsxt_spec: make_dataclass(
        "nsxt_spec",
        [
            ("nsxt_manager_size", str),
            (
                "nsxt_managers",
                List[
                    make_dataclass(
                        "nsxt_managers",
                        [
                            ("hostname", str, field(default=None)),
                            ("ip", str, field(default=None)),
                        ],
                    )
                ],
            ),
            ("root_nsxt_manager_password", str),
            ("vip", str),
            ("vip_fqdn", str),
            (
                "ip_address_pool_spec",
                make_dataclass(
                    "ip_address_pool_spec",
                    [
                        ("name", str),
                        ("description", str, field(default=None)),
                        ("ignore_unavailable_nsxt_cluster", bool, field(default=None)),
                        (
                            "subnets",
                            List[
                                make_dataclass(
                                    "subnets",
                                    [
                                        ("cidr", str),
                                        ("gateway", str),
                                        (
                                            "ip_address_pool_ranges",
                                            List[
                                                make_dataclass(
                                                    "ip_address_pool_ranges",
                                                    [("end", str), ("start", str)],
                                                )
                                            ],
                                        ),
                                    ],
                                )
                            ],
                            field(default=None),
                        ),
                    ],
                ),
                field(default=None),
            ),
            ("nsxt_admin_password", str, field(default=None)),
            ("nsxt_audit_password", str, field(default=None)),
            ("nsxt_license", str, field(default=None)),
            (
                "over_lay_transport_zone",
                make_dataclass(
                    "over_lay_transport_zone",
                    [("network_name", str), ("zone_name", str)],
                ),
                field(default=None),
            ),
            ("transport_vlan_id", int, field(default=None)),
        ],
    ) = None,
    proxy_spec: make_dataclass(
        "proxy_spec",
        [("host", str, field(default=None)), ("port", int, field(default=None))],
    ) = None,
    psc_specs: List[
        make_dataclass(
            "psc_specs",
            [
                ("admin_user_sso_password", str),
                (
                    "psc_sso_spec",
                    make_dataclass(
                        "psc_sso_spec", [("sso_domain", str, field(default=None))]
                    ),
                    field(default=None),
                ),
            ],
        )
    ] = None,
    sddc_manager_spec: make_dataclass(
        "sddc_manager_spec",
        [
            ("hostname", str),
            ("ip_address", str),
            (
                "root_user_credentials",
                make_dataclass(
                    "root_user_credentials", [("password", str), ("username", str)]
                ),
            ),
            (
                "second_user_credentials",
                make_dataclass(
                    "second_user_credentials", [("password", str), ("username", str)]
                ),
            ),
            ("local_user_password", str, field(default=None)),
        ],
    ) = None,
    security_spec: make_dataclass(
        "security_spec",
        [
            ("esxi_certs_mode", str, field(default=None)),
            (
                "root_ca_certs",
                List[
                    make_dataclass(
                        "root_ca_certs",
                        [
                            ("alias", str, field(default=None)),
                            ("cert_chain", List[str], field(default=None)),
                        ],
                    )
                ],
                field(default=None),
            ),
        ],
    ) = None,
    skip_esx_thumbprint_validation: bool = None,
    skip_gateway_ping_validation: bool = None,
    vsan_spec: make_dataclass(
        "vsan_spec",
        [
            ("datastore_name", str),
            (
                "esa_config",
                make_dataclass("esa_config", [("enabled", bool, field(default=None))]),
                field(default=None),
            ),
            ("hcl_file", str, field(default=None)),
            ("license_file", str, field(default=None)),
            ("vsan_dedup", bool, field(default=None)),
        ],
    ) = None,
    vx_manager_spec: make_dataclass(
        "vx_manager_spec",
        [
            (
                "default_admin_user_credentials",
                make_dataclass(
                    "default_admin_user_credentials",
                    [("password", str), ("username", str)],
                ),
            ),
            (
                "default_root_user_credentials",
                make_dataclass(
                    "default_root_user_credentials",
                    [("password", str), ("username", str)],
                ),
            ),
            ("vx_manager_host_name", str),
            ("ssh_thumbprint", str, field(default=None)),
            ("ssl_thumbprint", str, field(default=None)),
        ],
    ) = None,
) -> Dict[str, Any]:
    r"""

    Perform validation of the SddcSpec specification
        SDDC specification incorporates all the client inputs regarding VMW component parameters constituting the SDDC: NTP, DNS spec, ESXi, VC, VSAN, NSX spec et al.

    Args:
        cluster_spec(dict[str, Any]):
            clusterSpec.

            * cluster_evc_mode (str, Optional):
                vCenter cluster EVC mode. Defaults to None.

            * cluster_image_enabled (bool, Optional):
                Enable vSphere Lifecycle Manager Images for cluster creation. Defaults to None.

            * cluster_name (str):
                vCenter Cluster Name.

            * host_failures_to_tolerate (int, Optional):
                Host failures to tolerate. Defaults to None.

            * personality_name (str, Optional):
                Cluster Personality Name. Defaults to None.

            * resource_pool_specs (List[dict[str, Any]], Optional):
                Resource Pool Specs. Defaults to None.

                * cpu_limit (int, Optional):
                    CPU limit, default -1 (unlimited). Defaults to None.

                * cpu_reservation_expandable (bool, Optional):
                    Is CPU reservation expandable, default true. Defaults to None.

                * cpu_reservation_mhz (int, Optional):
                    CPU reservation in Mhz. Defaults to None.

                * cpu_reservation_percentage (int, Optional):
                    CPU reservation percentage, from 0 to 100, default 0. Defaults to None.

                * cpu_shares_level (str, Optional):
                    CPU shares level, default 'normal'. Defaults to None.

                * cpu_shares_value (int, Optional):
                    CPU shares value, only required when shares level is 'normal'. Defaults to None.

                * memory_limit (int, Optional):
                    Memory limit, default -1 (unlimited). Defaults to None.

                * memory_reservation_expandable (bool):
                    Is Memory reservation expandable, default true.

                * memory_reservation_mb (int, Optional):
                    Memory reservation in MB. Defaults to None.

                * memory_reservation_percentage (int, Optional):
                    Memory reservation percentage, from 0 to 100, default 0. Defaults to None.

                * memory_shares_level (str, Optional):
                    Memory shares level. default 'normal'. Defaults to None.

                * memory_shares_value (int, Optional):
                    Memory shares value, only required when shares level is '0'. Defaults to None.

                * name (str):
                    Resource Pool name.

                * type (str, Optional):
                    Type of resource pool. Defaults to None.

            * vm_folders (Dict, Optional):
                Virtual Machine folders map. Defaults to None.

        dns_spec(dict[str, Any]):
            dnsSpec.

            * domain (str):
                Tenant domain.

            * nameserver (str, Optional):
                Primary nameserver to be configured for vCenter/PSC/ESXi's/NSX. Defaults to None.

            * secondary_nameserver (str, Optional):
                Secondary nameserver to be configured for vCenter/PSC/ESXi's/NSX. Defaults to None.

            * subdomain (str):
                Tenant Sub domain.

        dvs_specs(List[dict[str, Any]]):
            List of Dvs Specs.

            * dvs_name (str):
                DVS Name.

            * is_used_by_nsxt (bool, Optional):
                Flag indicating whether the DVS is used by NSX.
                 This property is deprecated in favor of nsxtSwitchConfig field. Defaults to None.

            * mtu (int, Optional):
                DVS MTU (default value is 9000). Defaults to None.

            * networks (List[str]):
                Types of networks in this portgroup.

            * nioc_specs (List[dict[str, Any]], Optional):
                List of NIOC specs for networks. Defaults to None.

                * traffic_type (str):
                    Traffic Type.

                * value (str):
                    NIOC Value.

            * vmnics (List[str], Optional):
                Vmnics to be attached to the DVS.
                 This property is deprecated in favor of vmnicsToUplinks fields. Defaults to None.

            * nsxt_switch_config (dict[str, Any], Optional):
                nsxtSwitchConfig. Defaults to None.

                * transport_zones (List[dict[str, Any]], Optional):
                    The list of transport zones to be associated with the vSphere Distributed Switch managed by NSX. Defaults to None.

                    * name (str, Optional):
                        The name of the transport zone. Defaults to None.

                    * transport_type (str):
                        The type of the transport zone.

                * host_switch_operational_mode (str, Optional):
                    vSphere Distributed Switch name. Defaults to None.

            * vmnics_to_uplinks (List[dict[str, Any]], Optional):
                The map of vSphere Distributed Switch uplinks to the NSX switch uplinks. Defaults to None.

                * nsx_uplink_name (str):
                    The uplink name of the NSX switch.

                * vds_uplink_name (str):
                    The uplink name of the vSphere Distributed Switch.

        host_specs(List[dict[str, Any]]):
            List of Host Specs.

            * association (str):
                Host Association: Location/Datacenter.

            * credentials (dict[str, Any]):
                credentials.

                * password (str):
                    Password.

                * username (str):
                    Username.

            * hostname (str):
                Host Hostname.

            * ip_address_private (dict[str, Any]):
                ipAddressPrivate.

                * cidr (str, Optional):
                    Classless Inter-Domain Routing (CIDR). Defaults to None.

                * gateway (str, Optional):
                    Gateway. Defaults to None.

                * ip_address (str):
                    IP Address.

                * subnet (str, Optional):
                    Subnet. Defaults to None.

            * ssh_thumbprint (str, Optional):
                Host SSH thumbprint (RSA SHA256). Defaults to None.

            * ssl_thumbprint (str, Optional):
                Host SSL thumbprint (SHA256). Defaults to None.

            * v_switch (str):
                vSwitch.

            * vswitch (str, Optional):
                vswitch. Defaults to None.

        network_specs(List[dict[str, Any]]):
            List of Network Specs.

            * active_uplinks (List[str], Optional):
                Active Uplinks for teaming policy, specify uplink1 for failover_explicit VSAN Teaming Policy. Defaults to None.

            * exclude_ip_address_ranges (List[str], Optional):
                IP Addresse ranges to be excluded. Defaults to None.

            * exclude_ipaddresses (List[str], Optional):
                IP Addresses to be excluded. Defaults to None.

            * gateway (str, Optional):
                Gateway. Defaults to None.

            * include_ip_address (List[str], Optional):
                IP Addresses to be included. Defaults to None.

            * include_ip_address_ranges (List[dict[str, Any]], Optional):
                IP Addresse ranges to be included. Defaults to None.

                * end_ip_address (str):
                    End IP Address.

                * start_ip_address (str):
                    Start IP Address.

            * mtu (str, Optional):
                MTU size. Defaults to None.

            * network_type (str):
                Network Type.

            * port_group_key (str, Optional):
                Portgroup key name. Defaults to None.

            * standby_uplinks (List[str], Optional):
                Standby Uplinks for teaming policy, specify uplink2 for failover_explicit VSAN Teaming Policy. Defaults to None.

            * subnet (str, Optional):
                Subnet. Defaults to None.

            * subnet_mask (str, Optional):
                Subnet Mask. Defaults to None.

            * teaming_policy (str, Optional):
                Teaming Policy for VSAN and VMOTION network types, Default is loadbalance_loadbased. Defaults to None.

            * vlan_id (str):
                VLAN Id.

        ntp_servers(List[str]):
            List of NTP servers.

        sddc_id(str):
            Client string that identifies an SDDC by name or instance name. Used for management domain name. Can contain only letters, numbers and the following symbols: '-'.

        task_name(str):
            Name of the task to execute.

        vcenter_spec(dict[str, Any]):
            vcenterSpec.

            * license_file (str, Optional):
                License File. Defaults to None.

            * root_vcenter_password (str):
                vCenter root password. The password must be between 8 characters and 20 characters long. It must also contain at least one uppercase and lowercase letter, one number, and one character from '! " # $ % & ' ( ) * + , - . / : ; < = > ? @ [ \ ] ^ _ ` { &Iota; } ~' and all characters must be ASCII. Space is not allowed in password.

            * ssh_thumbprint (str, Optional):
                vCenter Server SSH thumbprint (RSA SHA256). Defaults to None.

            * ssl_thumbprint (str, Optional):
                vCenter Server SSL thumbprint (SHA256). Defaults to None.

            * storage_size (str, Optional):
                vCenter VM storage size. Defaults to None.

            * vcenter_hostname (str):
                vCenter hostname address.

            * vcenter_ip (str, Optional):
                vCenter IP address. Defaults to None.

            * vm_size (str, Optional):
                vCenter VM size. Defaults to None.

        redo(bool, Optional):
            redo. Defaults to None.

        ceip_enabled(bool, Optional):
            Enable VCF Customer Experience Improvement Program. Defaults to None.

        dv_switch_version(str, Optional):
            The version of the distributed virtual switches to be used. Defaults to None.

        esx_license(str, Optional):
            License for the ESXi hosts. Defaults to None.

        excluded_components(List[str], Optional):
            Components to be excluded. Defaults to None.

        fips_enabled(bool, Optional):
            Enable Federal Information Processing Standards. Defaults to None.

        management_pool_name(str, Optional):
            A String identifying the network pool associated with the management domain. Defaults to None.

        nsxt_spec(dict[str, Any], Optional):
            nsxtSpec. Defaults to None.

            * ip_address_pool_spec (dict[str, Any], Optional):
                ipAddressPoolSpec. Defaults to None.

                * description (str, Optional):
                    Description of the IP address pool. Defaults to None.

                * ignore_unavailable_nsxt_cluster (bool, Optional):
                    Ignore unavailable NSX cluster(s) during IP pool spec validation. Defaults to None.

                * name (str):
                    Name of the IP address pool.

                * subnets (List[dict[str, Any]], Optional):
                    List of IP address pool subnet specification. Defaults to None.

                    * cidr (str):
                        The subnet representation, contains the network address and the prefix length.

                    * gateway (str):
                        The default gateway address of the network.

                    * ip_address_pool_ranges (List[dict[str, Any]]):
                        List of the IP allocation ranges. Atleast 1 IP address range has to be specified.

                        * end (str):
                            The last IP Address of the IP Address Range.

                        * start (str):
                            The first IP Address of the IP Address Range.

            * nsxt_admin_password (str, Optional):
                NSX admin password. The password must be at least 12 characters long. Must contain at-least 1 uppercase, 1 lowercase, 1 special character and 1 digit. In addition, a character cannot be repeated 3 or more times consectively. Defaults to None.

            * nsxt_audit_password (str, Optional):
                NSX audit password. The password must be at least 12 characters long. Must contain at-least 1 uppercase, 1 lowercase, 1 special character and 1 digit. In addition, a character cannot be repeated 3 or more times consectively. Defaults to None.

            * nsxt_license (str, Optional):
                NSX Manager license. Defaults to None.

            * nsxt_manager_size (str):
                NSX Manager size.

            * nsxt_managers (List[dict[str, Any]]):
                NSX Managers.

                * hostname (str, Optional):
                    NSX Manager hostname. Defaults to None.

                * ip (str, Optional):
                    NSX Manager IP Address. Defaults to None.

            * over_lay_transport_zone (dict[str, Any], Optional):
                overLayTransportZone. Defaults to None.

                * network_name (str):
                    Transport zone network name.

                * zone_name (str):
                    Transport zone name.

            * root_nsxt_manager_password (str):
                NSX Manager root password. Password should have 1) At least eight characters, 2) At least one lower-case letter, 3) At least one upper-case letter 4) At least one digit 5) At least one special character, 6) At least five different characters , 7) No dictionary words, 6) No palindromes.

            * transport_vlan_id (int, Optional):
                Transport VLAN ID. Defaults to None.

            * vip (str):
                Virtual IP address which would act as proxy/alias for NSX Managers.

            * vip_fqdn (str):
                FQDN for VIP so that common SSL certificates can be installed across all managers.

        proxy_spec(dict[str, Any], Optional):
            proxySpec. Defaults to None.

            * host (str, Optional):
                IP address/FQDN of proxy server. Defaults to None.

            * port (int, Optional):
                Port of proxy server. Defaults to None.

        psc_specs(List[dict[str, Any]], Optional):
            PSC VM spec. Defaults to None.

            * admin_user_sso_password (str):
                Admin user sso passwordPassword needs to be a strong password with at least one Uppercase alphabet, one lowercase alphabet, one digit and one special character specified in braces [!$%^] and 8-20 characters in length,and 3 maximum identical adjacent characters!.

            * psc_sso_spec (dict[str, Any], Optional):
                pscSsoSpec. Defaults to None.

                * sso_domain (str, Optional):
                    PSC SSO Domain. Defaults to None.

        sddc_manager_spec(dict[str, Any], Optional):
            sddcManagerSpec. Defaults to None.

            * hostname (str):
                SDDC Manager Hostname.

            * ip_address (str):
                SDDC Manager ip address.

            * local_user_password (str, Optional):
                The local account is a built-in admin account in VCF that can be used in emergency scenarios. The password of this account must be at least 12 characters long. It also must contain at-least 1 uppercase, 1 lowercase, 1 special character specified in braces [!%@$^#?] and 1 digit. In addition, a character cannot be repeated more than 3 times consecutively. Defaults to None.

            * root_user_credentials (dict[str, Any]):
                rootUserCredentials.

                * password (str):
                    Password.

                * username (str):
                    Username.

            * second_user_credentials (dict[str, Any]):
                secondUserCredentials.

                * password (str):
                    Password.

                * username (str):
                    Username.

        security_spec(dict[str, Any], Optional):
            securitySpec. Defaults to None.

            * esxi_certs_mode (str, Optional):
                ESXi certificates mode. Defaults to None.

            * root_ca_certs (List[dict[str, Any]], Optional):
                Root Certificate Authority certificate list. Defaults to None.

                * alias (str, Optional):
                    Certificate alias. Defaults to None.

                * cert_chain (List[str], Optional):
                    List of Base64 encoded certificates. Defaults to None.

        skip_esx_thumbprint_validation(bool, Optional):
            Skip ESXi thumbprint validation. Defaults to None.

        skip_gateway_ping_validation(bool, Optional):
            Skip networks gateway connectivity validation. Defaults to None.

        vsan_spec(dict[str, Any], Optional):
            vsanSpec. Defaults to None.

            * datastore_name (str):
                Datastore Name.

            * esa_config (dict[str, Any], Optional):
                esaConfig. Defaults to None.

                * enabled (bool, Optional):
                    Whether the vSAN ESA is enabled. Defaults to None.

            * hcl_file (str, Optional):
                HCL File. Defaults to None.

            * license_file (str, Optional):
                License File. Defaults to None.

            * vsan_dedup (bool, Optional):
                VSAN feature Deduplication and Compression flag, one flag for both features. Defaults to None.

        vx_manager_spec(dict[str, Any], Optional):
            vxManagerSpec. Defaults to None.

            * default_admin_user_credentials (dict[str, Any]):
                defaultAdminUserCredentials.

                * password (str):
                    Password.

                * username (str):
                    Username.

            * default_root_user_credentials (dict[str, Any]):
                defaultRootUserCredentials.

                * password (str):
                    Password.

                * username (str):
                    Username.

            * ssh_thumbprint (str, Optional):
                VxRail Manager SSH thumbprint (RSA SHA256). Defaults to None.

            * ssl_thumbprint (str, Optional):
                VxRail Manager SSL thumbprint (SHA256). Defaults to None.

            * vx_manager_host_name (str):
                VxManager host name.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "ceip_enabled": "ceipEnabled",
        "cluster_spec": "clusterSpec",
        "dns_spec": "dnsSpec",
        "dv_switch_version": "dvSwitchVersion",
        "dvs_specs": "dvsSpecs",
        "esx_license": "esxLicense",
        "excluded_components": "excludedComponents",
        "fips_enabled": "fipsEnabled",
        "host_specs": "hostSpecs",
        "management_pool_name": "managementPoolName",
        "network_specs": "networkSpecs",
        "nsxt_spec": "nsxtSpec",
        "ntp_servers": "ntpServers",
        "proxy_spec": "proxySpec",
        "psc_specs": "pscSpecs",
        "sddc_id": "sddcId",
        "sddc_manager_spec": "sddcManagerSpec",
        "security_spec": "securitySpec",
        "skip_esx_thumbprint_validation": "skipEsxThumbprintValidation",
        "skip_gateway_ping_validation": "skipGatewayPingValidation",
        "task_name": "taskName",
        "vcenter_spec": "vcenterSpec",
        "vsan_spec": "vsanSpec",
        "vx_manager_spec": "vxManagerSpec",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/sddcs/validations".format(**{}),
        query_params={"redo": redo},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "additionalProperties": "additional_properties",
            "description": "description",
            "executionStatus": "execution_status",
            "id": "id",
            "resultStatus": "result_status",
            "validationChecks": "validation_checks",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_bringup_validation(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Retrieve the results of a bringup validation by its ID
        None

    Args:
        id_(str):
            SDDC validation ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/sddcs/validations/{id}".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "additionalProperties": "additional_properties",
            "description": "description",
            "executionStatus": "execution_status",
            "id": "id",
            "resultStatus": "result_status",
            "validationChecks": "validation_checks",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def retry_bringup_validation(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Retry bringup validation
        Retry a completed SDDC validation

    Args:
        id_(str):
            SDDC validation ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="patch",
        path="/v1/sddcs/validations/{id}".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "additionalProperties": "additional_properties",
            "description": "description",
            "executionStatus": "execution_status",
            "id": "id",
            "resultStatus": "result_status",
            "validationChecks": "validation_checks",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def export_bringup_validation_report(
    hub, ctx, validation_id: str, start_time: str = None, cur_client_time: str = None
) -> Dict[str, Any]:
    r"""

    Get validation report by ID
        Returns the bringup report for a validation. Reports are generated in PDF format.

    Args:
        validation_id(str):
            Bringup validation ID.

        start_time(str, Optional):
            Start time of validation to be put in the report. Defaults to None.

        cur_client_time(str, Optional):
            Current client local time of the the report generation. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/sddcs/validations/{validationId}/report".format(
            **{"validationId": validation_id}
        ),
        query_params={"startTime": start_time, "curClientTime": cur_client_time},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def export_bringup_detail_report(
    hub, ctx, id_: str, format_: str = None
) -> Dict[str, Any]:
    r"""

    Get bringup report by ID
        Returns the bringup report. Reports are generated in PDF and CSV formats.

    Args:
        id_(str):
            SDDC ID.

        format_(str, Optional):
            One among: PDF, CSV. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/sddcs/{id}/detail-report".format(**{"id": id_}),
        query_params={"format": format_},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def get_sddc_manager_info(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Retrieve SDDC Manager VM details
        Retrieves the details of SDDC Manager VM

    Args:
        id_(str):
            SDDC ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/sddcs/{id}/sddc-manager".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"fqdn": "fqdn", "id": "id", "ipAddress": "ip_address", "version": "version"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def start_bringup_spec_conversion(hub, ctx, design: str = None) -> Dict[str, Any]:
    r"""

    Convert SDDC specification Json/Excel File
        SDDC specification incorporates all the client inputs regarding VMW component parameters constituting the SDDC: NTP, DNS spec, ESXi, VC, VSAN, NSX spec et al.

    Args:
        design(str, Optional):
            Supported bringup designs - EMS, VXRAIL. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/system/sddc-spec-converter".format(**{}),
        query_params={"design": design},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "ceipEnabled": "ceip_enabled",
            "clusterSpec": "cluster_spec",
            "dnsSpec": "dns_spec",
            "dvSwitchVersion": "dv_switch_version",
            "dvsSpecs": "dvs_specs",
            "esxLicense": "esx_license",
            "excludedComponents": "excluded_components",
            "fipsEnabled": "fips_enabled",
            "hostSpecs": "host_specs",
            "managementPoolName": "management_pool_name",
            "networkSpecs": "network_specs",
            "nsxtSpec": "nsxt_spec",
            "ntpServers": "ntp_servers",
            "proxySpec": "proxy_spec",
            "pscSpecs": "psc_specs",
            "sddcId": "sddc_id",
            "sddcManagerSpec": "sddc_manager_spec",
            "securitySpec": "security_spec",
            "skipEsxThumbprintValidation": "skip_esx_thumbprint_validation",
            "skipGatewayPingValidation": "skip_gateway_ping_validation",
            "taskName": "task_name",
            "vcenterSpec": "vcenter_spec",
            "vsanSpec": "vsan_spec",
            "vxManagerSpec": "vx_manager_spec",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result

"""Utility functions for System Precheckss. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List


async def start_precheck(
    hub,
    ctx,
    resources: List[
        make_dataclass(
            "resources",
            [
                ("resource_id", str),
                ("type", str),
                ("fqdn", str, field(default=None)),
                ("name", str, field(default=None)),
                ("sans", List[str], field(default=None)),
            ],
        )
    ],
    bundle_id: str = None,
    mode: str = None,
) -> Dict[str, Any]:
    r"""

    Start a system precheck
        Perform precheck of resource(ex: Domain, Cluster). If only resource is specified, all resources/software components under it are included. If resource(Domain, Cluster etc) and specific resources/software components are provided, only those are included in precheck. As this API is deprecated, please use the new LCM Prechecks API - /v1/system/check-sets/queries and /v1/system/check-sets
    DEPRECATED

    Args:
        resources(List[dict[str, Any]]):
            List of resources for Precheck supported type DOMAIN, CLUSTER.

            * fqdn (str, Optional):
                Resource FQDN. Defaults to None.

            * name (str, Optional):
                Name of the resource. Defaults to None.

            * resource_id (str):
                Resource ID.

            * sans (List[str], Optional):
                Subject alternative name(s). Defaults to None.

            * type (str):
                Resource type.

        bundle_id(str, Optional):
             If specified, indicate the bundle applicability in the result. Defaults to None.

        mode(str, Optional):
             If specified, indicates the mode of the execution. Supported modes are UPGRADE and RECOVERY. UPGRADE is default mode. Specify RECOVERY for the Inventory Consistency Checks. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"bundle_id": "bundleId", "mode": "mode", "resources": "resources"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/system/prechecks".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "errors": "errors",
            "id": "id",
            "isCancellable": "is_cancellable",
            "isRetryable": "is_retryable",
            "localizableDescriptionPack": "localizable_description_pack",
            "name": "name",
            "resolutionStatus": "resolution_status",
            "resources": "resources",
            "status": "status",
            "subTasks": "sub_tasks",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_precheck_task(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Retrieve a precheck task by its id
        Monitor the progress of precheck task by the precheck task ID. As this API is deprecated, please use the new LCM Prechecks API - /v1/system/check-sets/{taskId}
    DEPRECATED

    Args:
        id_(str):
            Precheck Task ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/system/prechecks/tasks/{id}".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "errors": "errors",
            "id": "id",
            "isCancellable": "is_cancellable",
            "isRetryable": "is_retryable",
            "localizableDescriptionPack": "localizable_description_pack",
            "name": "name",
            "resolutionStatus": "resolution_status",
            "resources": "resources",
            "status": "status",
            "subTasks": "sub_tasks",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result

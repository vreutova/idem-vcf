"""Utility functions for V Centerss. """
from collections import OrderedDict
from typing import Any
from typing import Dict


async def get_vcenters(hub, ctx, domain_id: str = None) -> Dict[str, Any]:
    r"""

    Retrieve a list of vCenter Servers
        None

    Args:
        domain_id(str, Optional):
            ID of the domain. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/vcenters".format(**{}),
        query_params={"domainId": domain_id},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_vcenter(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Retrieve a vCenter Server by its ID
        None

    Args:
        id_(str):
            vCenter ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/vcenters/{id}".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "domain": "domain",
            "fqdn": "fqdn",
            "id": "id",
            "ipAddress": "ip_address",
            "version": "version",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result

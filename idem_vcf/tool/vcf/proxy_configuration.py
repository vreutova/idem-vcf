"""Utility functions for Proxy Configurations. """
from collections import OrderedDict
from typing import Any
from typing import Dict


async def update_proxy_configuration(
    hub,
    ctx,
    host: str = None,
    is_configured: bool = None,
    is_enabled: bool = None,
    port: int = None,
) -> Dict[str, Any]:
    r"""

    Update Proxy configuration
        Update Proxy configuration

    Args:
        host(str, Optional):
            IP address/FQDN of proxy server. Defaults to None.

        is_configured(bool, Optional):
            Is proxy configured. Defaults to None.

        is_enabled(bool, Optional):
            Is proxy enabled. Defaults to None.

        port(int, Optional):
            Port of proxy server. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "host": "host",
        "is_configured": "isConfigured",
        "is_enabled": "isEnabled",
        "port": "port",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="patch",
        path="/v1/system/proxy-configuration".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "errors": "errors",
            "id": "id",
            "isCancellable": "is_cancellable",
            "isRetryable": "is_retryable",
            "localizableDescriptionPack": "localizable_description_pack",
            "name": "name",
            "resolutionStatus": "resolution_status",
            "resources": "resources",
            "status": "status",
            "subTasks": "sub_tasks",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result

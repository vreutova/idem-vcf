"""Utility functions for Hostss. """
from collections import OrderedDict
from typing import Any
from typing import Dict
from typing import List


async def decommission_hosts(hub, ctx) -> Dict[str, Any]:
    r"""

    Decommission the Hosts
        None


    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="delete",
        path="/v1/hosts".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "errors": "errors",
            "id": "id",
            "isCancellable": "is_cancellable",
            "isRetryable": "is_retryable",
            "localizableDescriptionPack": "localizable_description_pack",
            "name": "name",
            "resolutionStatus": "resolution_status",
            "resources": "resources",
            "status": "status",
            "subTasks": "sub_tasks",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_criterion(hub, ctx) -> Dict[str, Any]:
    r"""

    Get a criterion
        None


    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/hosts/criteria/{name}".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"arguments": "arguments", "description": "description", "name": "name"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def post_query(
    hub, ctx, arguments: Dict = None, description: str = None
) -> Dict[str, Any]:
    r"""

    Post a query
        None

    Args:
        arguments(Dict, Optional):
            Arguments required for a particular criterion. Defaults to None.

        description(str, Optional):
            Description of the criterion. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"arguments": "arguments", "description": "description", "name": "name"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/hosts/queries".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict({"queryInfo": "query_info", "result": "result"})

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_host_query_response_1(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Get query response
        None

    Args:
        id_(str):
            id.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/hosts/queries/{id}".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict({"queryInfo": "query_info", "result": "result"})

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def validate_host_commission_spec(hub, ctx) -> Dict[str, Any]:
    r"""

    Perform validation of the  HostCommissionSpec specification
        None


    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/hosts/validations".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "additionalProperties": "additional_properties",
            "description": "description",
            "executionStatus": "execution_status",
            "id": "id",
            "resultStatus": "result_status",
            "validationChecks": "validation_checks",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_host_commission_validation_by_id(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Retrieve the results of a host commission validation by its ID
        None

    Args:
        id_(str):
            The validation ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/hosts/validations/{id}".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "additionalProperties": "additional_properties",
            "description": "description",
            "executionStatus": "execution_status",
            "id": "id",
            "resultStatus": "result_status",
            "validationChecks": "validation_checks",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_tags_assigned_to_host(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Get Tags assigned to Host
        None

    Args:
        id_(str):
            Host ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/hosts/{id}/tags".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def assign_tags_to_host(hub, ctx, id_: str, tag_ids: List[str]) -> Dict[str, Any]:
    r"""

    Assign tags to a host
        None

    Args:
        id_(str):
            Host ID.

        tag_ids(List[str]):
            Tag IDs.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"tag_ids": "tagIds"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="put",
        path="/v1/hosts/{id}/tags".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"defaultErrorMessages": "default_error_messages", "success": "success"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def remove_tags_from_host(
    hub, ctx, id_: str, tag_ids: List[str]
) -> Dict[str, Any]:
    r"""

    Remove Tags From Host
        None

    Args:
        id_(str):
            Host ID.

        tag_ids(List[str]):
            Tag IDs.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"tag_ids": "tagIds"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="delete",
        path="/v1/hosts/{id}/tags".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"defaultErrorMessages": "default_error_messages", "success": "success"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_assignable_tag_for_host(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Get tags assignable to a host
        None

    Args:
        id_(str):
            Host ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/hosts/{id}/tags/assignable-tags".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_host_tag_manager_url(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Get Host Tag Manager Url
        None

    Args:
        id_(str):
            Host ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/hosts/{id}/tags/tag-manager".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict({"tagManagerUrl": "tag_manager_url"})

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result

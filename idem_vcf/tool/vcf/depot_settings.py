"""Utility functions for Depot Settingss. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict


async def get_depot_settings(hub, ctx) -> Dict[str, Any]:
    r"""

    Retrieve the depot configuration
        Get the depot configuration. In a fresh setup, this would be empty.


    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/system/settings/depot".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "dellEmcSupportAccount": "dell_emc_support_account",
            "depotConfiguration": "depot_configuration",
            "offlineAccount": "offline_account",
            "vmwareAccount": "vmware_account",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def update_depot_settings(
    hub,
    ctx,
    dell_emc_support_account: make_dataclass(
        "dell_emc_support_account",
        [
            ("password", str),
            ("username", str),
            ("message", str, field(default=None)),
            ("status", str, field(default=None)),
        ],
    ) = None,
    vmware_account: make_dataclass(
        "vmware_account",
        [
            ("password", str),
            ("username", str),
            ("message", str, field(default=None)),
            ("status", str, field(default=None)),
        ],
    ) = None,
    offline_account: make_dataclass(
        "offline_account",
        [
            ("password", str),
            ("username", str),
            ("message", str, field(default=None)),
            ("status", str, field(default=None)),
        ],
    ) = None,
    depot_configuration: make_dataclass(
        "depot_configuration",
        [
            ("is_offline_depot", bool, field(default=None)),
            ("hostname", str, field(default=None)),
            ("port", int, field(default=None)),
        ],
    ) = None,
) -> Dict[str, Any]:
    r"""

    Configure the depot credentials
        Update depot settings. Depot settings can be updated with VMware Depot account

    Args:
        dell_emc_support_account(dict[str, Any], Optional):
            dellEmcSupportAccount. Defaults to None.

            * message (str, Optional):
                Message explaining depot status. Defaults to None.

            * password (str):
                Depot Password for Access.

            * status (str, Optional):
                Depot Status. Defaults to None.

            * username (str):
                Depot Username for Access.

        vmware_account(dict[str, Any], Optional):
            vmwareAccount. Defaults to None.

            * message (str, Optional):
                Message explaining depot status. Defaults to None.

            * password (str):
                Depot Password for Access.

            * status (str, Optional):
                Depot Status. Defaults to None.

            * username (str):
                Depot Username for Access.

        offline_account(dict[str, Any], Optional):
            offlineAccount. Defaults to None.

            * message (str, Optional):
                Message explaining depot status. Defaults to None.

            * password (str):
                Depot Password for Access.

            * status (str, Optional):
                Depot Status. Defaults to None.

            * username (str):
                Depot Username for Access.

        depot_configuration(dict[str, Any], Optional):
            depotConfiguration. Defaults to None.

            * is_offline_depot (bool, Optional):
                Flag indicating if the depot is in offline mode. Defaults to None.

            * hostname (str, Optional):
                IP/Hostname of the depot. Defaults to None.

            * port (int, Optional):
                Port of the depot. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "dell_emc_support_account": "dellEmcSupportAccount",
        "vmware_account": "vmwareAccount",
        "offline_account": "offlineAccount",
        "depot_configuration": "depotConfiguration",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="put",
        path="/v1/system/settings/depot".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "dellEmcSupportAccount": "dell_emc_support_account",
            "depotConfiguration": "depot_configuration",
            "offlineAccount": "offline_account",
            "vmwareAccount": "vmware_account",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result

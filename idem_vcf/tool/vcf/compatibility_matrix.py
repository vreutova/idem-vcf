"""Utility functions for Compatibility Matrixes. """
from collections import OrderedDict
from typing import Any
from typing import Dict


async def get_compatibility_matrices(hub, ctx) -> Dict[str, Any]:
    r"""

    Get Compatibility Matrices
        Get Compatibility Matrices


    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/compatibility-matrices".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def update_compatibility_matrix(
    hub, ctx, compatibility_matrix_source: str
) -> Dict[str, Any]:
    r"""

    Update CompatibilityMatrix for a source with JSON file
        Update CompatibilityMatrix content for a source using a JSON file

    Args:
        compatibility_matrix_source(str):
            Compatibility Matrix Source (VMWARE_COMPAT, VXRAIL_COMPAT).

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="put",
        path="/v1/compatibility-matrices".format(**{}),
        query_params={"compatibilityMatrixSource": compatibility_matrix_source},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def get_compatibility_matrix(
    hub, ctx, compatibility_matrix_source: str
) -> Dict[str, Any]:
    r"""

    Get Compatibility Matrix
        Get Compatibility Matrix

    Args:
        compatibility_matrix_source(str):
            compatibilityMatrixSource of compatibility data.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/compatibility-matrices/{compatibilityMatrixSource}".format(
            **{"compatibilityMatrixSource": compatibility_matrix_source}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "compatibilityMatrixContent": "compatibility_matrix_content",
            "compatibilityMatrixMetadata": "compatibility_matrix_metadata",
            "compatibilityMatrixSource": "compatibility_matrix_source",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_compatibility_matrix_content(
    hub, ctx, compatibility_matrix_source: str
) -> Dict[str, Any]:
    r"""

    Get Compatibility Matrix content
        Get Compatibility Matrix content

    Args:
        compatibility_matrix_source(str):
            compatibilityMatrixSource of compatibility data.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/compatibility-matrices/{compatibilityMatrixSource}/content".format(
            **{"compatibilityMatrixSource": compatibility_matrix_source}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def get_compatibility_matrix_metadata(
    hub, ctx, compatibility_matrix_source: str
) -> Dict[str, Any]:
    r"""

    Get Compatibility Matrix Metadata
        Get Compatibility Matrix Metadata

    Args:
        compatibility_matrix_source(str):
            compatibilityMatrixSource of compatibility data.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/compatibility-matrices/{compatibilityMatrixSource}/metadata".format(
            **{"compatibilityMatrixSource": compatibility_matrix_source}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "errorMessage": "error_message",
            "isMissing": "is_missing",
            "isStale": "is_stale",
            "lastModifiedDate": "last_modified_date",
            "message": "message",
            "warningMessage": "warning_message",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result

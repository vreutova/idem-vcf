"""Utility functions for V San Health Checks. """
from collections import OrderedDict
from typing import Any
from typing import Dict


async def get_vsan_health_check_by_domain(
    hub, ctx, domain_id: str, status: str = None
) -> Dict[str, Any]:
    r"""

    Retrieve vSAN Health Check status for a domain by its ID
        Get vSAN health check status for all cluster on the domain

    Args:
        domain_id(str):
            Domain ID.

        status(str, Optional):
            Status of health check to filter by. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/domains/{domainId}/health-checks".format(**{"domainId": domain_id}),
        query_params={"status": status},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"result": "result", "resultId": "result_id", "status": "status"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def update_vsan_health_check_by_domain(
    hub, ctx, domain_id: str
) -> Dict[str, Any]:
    r"""

    Update the vSAN Health Check status for a domain
        Update vSAN health check status for domain

    Args:
        domain_id(str):
            Domain ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="patch",
        path="/v1/domains/{domainId}/health-checks".format(**{"domainId": domain_id}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"id": "id", "resourceStatus": "resource_status", "status": "status"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_vsan_health_check_by_query_id(
    hub, ctx, domain_id: str, query_id: str
) -> Dict[str, Any]:
    r"""

    Retrieve vSAN Health Check status for a domain and query ID
        Get vSAN health check status for a given Query Id

    Args:
        domain_id(str):
            Domain ID.

        query_id(str):
            Query ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/domains/{domainId}/health-checks/queries/{queryId}".format(
            **{"domainId": domain_id, "queryId": query_id}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"result": "result", "resultId": "result_id", "status": "status"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_vsan_health_check_by_task_id(
    hub, ctx, domain_id: str, task_id: str
) -> Dict[str, Any]:
    r"""

    Retrieve a vSAN Health Check by task ID
        Get vSAN health check update task status for a given task Id

    Args:
        domain_id(str):
            Domain ID.

        task_id(str):
            Health check task id.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/domains/{domainId}/health-checks/tasks/{taskId}".format(
            **{"domainId": domain_id, "taskId": task_id}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"id": "id", "resourceStatus": "resource_status", "status": "status"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result

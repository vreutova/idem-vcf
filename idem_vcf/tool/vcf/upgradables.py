"""Utility functions for Upgradableses. """
from collections import OrderedDict
from typing import Any
from typing import Dict


async def get_upgradables_by_domain(
    hub, ctx, domain_id: str, target_version: str = None
) -> Dict[str, Any]:
    r"""

    Retrieve a list of all upgradable resources for a domain by its ID
        Fetches the list of Upgradables for a given domain. If a target version is provided, Upgradables that are required for given target version become Available. The Upgradables providesinformation that can be use for Precheck API and also in the actual Upgrade API call.This API is used only for management domain, for all cases please use v1/system/upgradables.

    Args:
        domain_id(str):
            Domain ID.

        target_version(str, Optional):
            Target Version to get Upgradables for a given Target Release. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/upgradables/domains/{domainId}".format(**{"domainId": domain_id}),
        query_params={"targetVersion": target_version},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_upgradables_clusters(hub, ctx, domain_id: str) -> Dict[str, Any]:
    r"""

    Retrieve a list of upgradable packages details from HSM for a domain by its ID
        Fetches the list of available hardware support managers and configured hardware support managers for the give resource along with the hardware support packages and Software details.

    Args:
        domain_id(str):
            Domain ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/upgradables/domains/{domainId}/clusters".format(
            **{"domainId": domain_id}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict({"elements": "elements"})

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_nsx_upgrade_resources(
    hub, ctx, domain_id: str, bundle_id: str = None
) -> Dict[str, Any]:
    r"""

    Retrieve a list of all upgradable NSX resources for a domain by its ID
        Get the list NSX upgradable reosurce with resource metadata info

    Args:
        domain_id(str):
            Domain ID.

        bundle_id(str, Optional):
            bundle Id of the upgrade bundle applicable on the domain. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/upgradables/domains/{domainId}/nsxt".format(
            **{"domainId": domain_id}
        ),
        query_params={"bundleId": bundle_id},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "bundleId": "bundle_id",
            "domainId": "domain_id",
            "nsxtEdgeClusters": "nsxt_edge_clusters",
            "nsxtHostClusters": "nsxt_host_clusters",
            "nsxtManagerCluster": "nsxt_manager_cluster",
            "nsxtUpgradeCoordinator": "nsxt_upgrade_coordinator",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result

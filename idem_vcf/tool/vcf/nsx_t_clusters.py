"""Utility functions for Nsx T Clusterss. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List


async def get_nsx_clusters(hub, ctx, is_shareable: bool = None) -> Dict[str, Any]:
    r"""

    Retrieve a list of NSX clusters
        None

    Args:
        is_shareable(bool, Optional):
            filter NSX clusters which can be shared for domain creation. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/nsxt-clusters".format(**{}),
        query_params={"isShareable": is_shareable},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_nsx_criteria(hub, ctx) -> Dict[str, Any]:
    r"""

    Get all NSX criteria
        None


    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/nsxt-clusters/criteria".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_nsx_criterion(hub, ctx) -> Dict[str, Any]:
    r"""

    Get a NSX criterion
        None


    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/nsxt-clusters/criteria/{name}".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"arguments": "arguments", "description": "description", "name": "name"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def validate_ip_pool_using_post(
    hub,
    ctx,
    ip_address_pool_spec: make_dataclass(
        "ip_address_pool_spec",
        [
            ("name", str),
            ("description", str, field(default=None)),
            ("ignore_unavailable_nsxt_cluster", bool, field(default=None)),
            (
                "subnets",
                List[
                    make_dataclass(
                        "subnets",
                        [
                            ("cidr", str),
                            ("gateway", str),
                            (
                                "ip_address_pool_ranges",
                                List[
                                    make_dataclass(
                                        "ip_address_pool_ranges",
                                        [("end", str), ("start", str)],
                                    )
                                ],
                            ),
                        ],
                    )
                ],
                field(default=None),
            ),
        ],
    ),
    nsxt_cluster_id: str,
) -> Dict[str, Any]:
    r"""

    validateIpPool
        None

    Args:
        ip_address_pool_spec(dict[str, Any]):
            ipAddressPoolSpec.

            * description (str, Optional):
                Description of the IP address pool. Defaults to None.

            * ignore_unavailable_nsxt_cluster (bool, Optional):
                Ignore unavailable NSX cluster(s) during IP pool spec validation. Defaults to None.

            * name (str):
                Name of the IP address pool.

            * subnets (List[dict[str, Any]], Optional):
                List of IP address pool subnet specification. Defaults to None.

                * cidr (str):
                    The subnet representation, contains the network address and the prefix length.

                * gateway (str):
                    The default gateway address of the network.

                * ip_address_pool_ranges (List[dict[str, Any]]):
                    List of the IP allocation ranges. Atleast 1 IP address range has to be specified.

                    * end (str):
                        The last IP Address of the IP Address Range.

                    * start (str):
                        The first IP Address of the IP Address Range.

        nsxt_cluster_id(str):
            nsxtClusterId.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "ip_address_pool_spec": "ipAddressPoolSpec",
        "nsxt_cluster_id": "nsxtClusterId",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/nsxt-clusters/ip-address-pools/validations".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "additionalProperties": "additional_properties",
            "description": "description",
            "executionStatus": "execution_status",
            "id": "id",
            "resultStatus": "result_status",
            "validationChecks": "validation_checks",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_validation_result_using_get(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    getValidationResult
        None

    Args:
        id_(str):
            id.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/nsxt-clusters/ip-address-pools/validations/{id}".format(
            **{"id": id_}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "additionalProperties": "additional_properties",
            "description": "description",
            "executionStatus": "execution_status",
            "id": "id",
            "resultStatus": "result_status",
            "validationChecks": "validation_checks",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def start_nsx_criteria_query(
    hub, ctx, arguments: Dict = None, description: str = None
) -> Dict[str, Any]:
    r"""

    Start a query with NSX Criteria
        None

    Args:
        arguments(Dict, Optional):
            Arguments required for a particular criterion. Defaults to None.

        description(str, Optional):
            Description of the criterion. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"arguments": "arguments", "description": "description", "name": "name"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/nsxt-clusters/queries".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict({"queryInfo": "query_info", "result": "result"})

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_nsx_cluster_query_response(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Get NSX cluster query response
        None

    Args:
        id_(str):
            id.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/nsxt-clusters/queries/{id}".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict({"queryInfo": "query_info", "result": "result"})

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_nsx_cluster(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Retrieve the details of an NSX cluster by its ID
        None

    Args:
        id_(str):
            NSX cluster ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/nsxt-clusters/{id}".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "domains": "domains",
            "id": "id",
            "isShareable": "is_shareable",
            "isShared": "is_shared",
            "isVlcmCompatible": "is_vlcm_compatible",
            "nativeId": "native_id",
            "nodes": "nodes",
            "status": "status",
            "version": "version",
            "vip": "vip",
            "vipFqdn": "vip_fqdn",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_nsx_ip_address_pools(hub, ctx, nsxt_cluster_id: str) -> Dict[str, Any]:
    r"""

    Retrive a list of NSX IP Address Pools by the NSX cluster ID
        None

    Args:
        nsxt_cluster_id(str):
            NSX cluster ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/nsxt-clusters/{nsxt-cluster-id}/ip-address-pools".format(
            **{"nsxt-cluster-id": nsxt_cluster_id}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "availableIpAddresses": "available_ip_addresses",
            "blockSubnets": "block_subnets",
            "description": "description",
            "name": "name",
            "staticSubnets": "static_subnets",
            "totalIpAddresses": "total_ip_addresses",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_nsx_ip_address_pool(hub, ctx, nsxt_cluster_id: str) -> Dict[str, Any]:
    r"""

    Retrieve an NSX IP Address Pool by its name
        None

    Args:
        nsxt_cluster_id(str):
            NSX cluster ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/nsxt-clusters/{nsxt-cluster-id}/ip-address-pools/{name}".format(
            **{"nsxt-cluster-id": nsxt_cluster_id}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "availableIpAddresses": "available_ip_addresses",
            "blockSubnets": "block_subnets",
            "description": "description",
            "name": "name",
            "staticSubnets": "static_subnets",
            "totalIpAddresses": "total_ip_addresses",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_nsx_transport_zones(hub, ctx, nsxt_cluster_id: str) -> Dict[str, Any]:
    r"""

    Retrieve list of all NSX transport zones
        None

    Args:
        nsxt_cluster_id(str):
            NSX cluster ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/nsxt-clusters/{nsxt-cluster-id}/transport-zones".format(
            **{"nsxt-cluster-id": nsxt_cluster_id}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result

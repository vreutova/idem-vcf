"""Utility functions for Credentialss. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List


async def update_or_rotate_passwords(
    hub,
    ctx,
    elements: List[
        make_dataclass(
            "elements",
            [
                (
                    "credentials",
                    List[
                        make_dataclass(
                            "credentials",
                            [
                                ("username", str),
                                ("account_type", str, field(default=None)),
                                ("credential_type", str, field(default=None)),
                                ("password", str, field(default=None)),
                            ],
                        )
                    ],
                ),
                ("resource_type", str),
                ("resource_id", str, field(default=None)),
                ("resource_name", str, field(default=None)),
            ],
        )
    ],
    operation_type: str,
    auto_rotate_policy: make_dataclass(
        "auto_rotate_policy",
        [
            ("enable_auto_rotate_policy", bool, field(default=None)),
            ("frequency_in_days", int, field(default=None)),
        ],
    ) = None,
) -> Dict[str, Any]:
    r"""

    Update or rotate passwords for a list of resources
        Update passwords for given list of resources by supplying new passwords or rotate the passwords using system generated passwords

    Args:
        elements(List[dict[str, Any]]):
            List of resource credentials to be changed.

            * credentials (List[dict[str, Any]]):
                Credentials of the resource.

                * account_type (str, Optional):
                    Account type. Defaults to None.

                * credential_type (str, Optional):
                    Credential type. Defaults to None.

                * password (str, Optional):
                    Password. Defaults to None.

                * username (str):
                    Username.

            * resource_id (str, Optional):
                Resource ID. Defaults to None.

            * resource_name (str, Optional):
                Resource name. Defaults to None.

            * resource_type (str):
                Authenticated Resource Type.

        operation_type(str):
            Operation type.

        auto_rotate_policy(dict[str, Any], Optional):
            autoRotatePolicy. Defaults to None.

            * enable_auto_rotate_policy (bool, Optional):
                 Enable or disable  auto rotate policy. Defaults to None.

            * frequency_in_days (int, Optional):
                Frequency in days. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "auto_rotate_policy": "autoRotatePolicy",
        "elements": "elements",
        "operation_type": "operationType",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="patch",
        path="/v1/credentials".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "errors": "errors",
            "id": "id",
            "isCancellable": "is_cancellable",
            "isRetryable": "is_retryable",
            "localizableDescriptionPack": "localizable_description_pack",
            "name": "name",
            "resolutionStatus": "resolution_status",
            "resources": "resources",
            "status": "status",
            "subTasks": "sub_tasks",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_password_expiration(
    hub,
    ctx,
    resource_type: str,
    credential_ids: List[str] = None,
    domain_name: str = None,
) -> Dict[str, Any]:
    r"""

    Fetch expiration details of passwords for a list of credentials
        Fetch expiration details of passwords for a list of credentials

    Args:
        resource_type(str):
            Authenticated Resource Type.

        credential_ids(List[str], Optional):
            List of credential ids. Defaults to None.

        domain_name(str, Optional):
            Domain name. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "credential_ids": "credentialIds",
        "domain_name": "domainName",
        "resource_type": "resourceType",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/credentials/expirations".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "elements": "elements",
            "id": "id",
            "status": "status",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_password_expiration_by_task_id(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Retrive a password expiration task by ID
        Retrive a password expiration task by ID

    Args:
        id_(str):
            The expiration fetch workflow ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/credentials/expirations/{id}".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "description": "description",
            "executionStatus": "execution_status",
            "id": "id",
            "validationChecks": "validation_checks",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_credentials_task(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Retrieve a credential task by ID
        Retrieve a credential task by ID

    Args:
        id_(str):
            The ID of the credentials task.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/credentials/tasks/{id}".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "creationTimestamp": "creation_timestamp",
            "errors": "errors",
            "id": "id",
            "isAutoRotate": "is_auto_rotate",
            "name": "name",
            "status": "status",
            "subTasks": "sub_tasks",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def cancel_credentials_task(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Cancel a failed credential task by its ID
        Cancel a failed credential task by its ID

    Args:
        id_(str):
            Task ID of the failed operation required to be cancelled.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="delete",
        path="/v1/credentials/tasks/{id}".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "errors": "errors",
            "id": "id",
            "isCancellable": "is_cancellable",
            "isRetryable": "is_retryable",
            "localizableDescriptionPack": "localizable_description_pack",
            "name": "name",
            "resolutionStatus": "resolution_status",
            "resources": "resources",
            "status": "status",
            "subTasks": "sub_tasks",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def retry_credentials_task(
    hub,
    ctx,
    id_: str,
    elements: List[
        make_dataclass(
            "elements",
            [
                (
                    "credentials",
                    List[
                        make_dataclass(
                            "credentials",
                            [
                                ("username", str),
                                ("account_type", str, field(default=None)),
                                ("credential_type", str, field(default=None)),
                                ("password", str, field(default=None)),
                            ],
                        )
                    ],
                ),
                ("resource_type", str),
                ("resource_id", str, field(default=None)),
                ("resource_name", str, field(default=None)),
            ],
        )
    ],
    operation_type: str,
    auto_rotate_policy: make_dataclass(
        "auto_rotate_policy",
        [
            ("enable_auto_rotate_policy", bool, field(default=None)),
            ("frequency_in_days", int, field(default=None)),
        ],
    ) = None,
) -> Dict[str, Any]:
    r"""

    Retry a failed credentials task for a given ID
        Retry a failed credentials task for a given ID

    Args:
        id_(str):
            Task ID of the failed operation that is to be retried.

        elements(List[dict[str, Any]]):
            List of resource credentials to be changed.

            * credentials (List[dict[str, Any]]):
                Credentials of the resource.

                * account_type (str, Optional):
                    Account type. Defaults to None.

                * credential_type (str, Optional):
                    Credential type. Defaults to None.

                * password (str, Optional):
                    Password. Defaults to None.

                * username (str):
                    Username.

            * resource_id (str, Optional):
                Resource ID. Defaults to None.

            * resource_name (str, Optional):
                Resource name. Defaults to None.

            * resource_type (str):
                Authenticated Resource Type.

        operation_type(str):
            Operation type.

        auto_rotate_policy(dict[str, Any], Optional):
            autoRotatePolicy. Defaults to None.

            * enable_auto_rotate_policy (bool, Optional):
                 Enable or disable  auto rotate policy. Defaults to None.

            * frequency_in_days (int, Optional):
                Frequency in days. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "auto_rotate_policy": "autoRotatePolicy",
        "elements": "elements",
        "operation_type": "operationType",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="patch",
        path="/v1/credentials/tasks/{id}".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "completionTimestamp": "completion_timestamp",
            "creationTimestamp": "creation_timestamp",
            "errors": "errors",
            "id": "id",
            "isCancellable": "is_cancellable",
            "isRetryable": "is_retryable",
            "localizableDescriptionPack": "localizable_description_pack",
            "name": "name",
            "resolutionStatus": "resolution_status",
            "resources": "resources",
            "status": "status",
            "subTasks": "sub_tasks",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_credentials_sub_task(
    hub, ctx, id_: str, subtask_id: str
) -> Dict[str, Any]:
    r"""

    Retrieve a credential sub task by its ID
        Retrieve a credential sub task by its ID

    Args:
        id_(str):
            The ID of the credentials task.

        subtask_id(str):
            The ID of the credentials sub-task.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/credentials/tasks/{id}/subtasks/{subtaskId}".format(
            **{"id": id_, "subtaskId": subtask_id}
        ),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "creationTimestamp": "creation_timestamp",
            "errors": "errors",
            "id": "id",
            "isAutoRotate": "is_auto_rotate",
            "name": "name",
            "status": "status",
            "subTasks": "sub_tasks",
            "type": "type",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result

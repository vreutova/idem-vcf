"""Utility functions for Suite Lifecycles. """
from collections import OrderedDict
from typing import Any
from typing import Dict


async def get_vrslcm(hub, ctx) -> Dict[str, Any]:
    r"""

    Retrive information about VMware Aria Suite Lifecycle
        Gets the complete information about the existing VMware Aria Suite Lifecycle instance.


    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/vrslcm".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "fqdn": "fqdn",
            "id": "id",
            "ipAddress": "ip_address",
            "status": "status",
            "version": "version",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def update_vrslcm_version_in_inventory(
    hub, ctx, fqdn: str, id_: str, ip_address: str, status: str, version: str
) -> Dict[str, Any]:
    r"""

    Update the version of VMware Aria Suite Lifecycle
        Updates VMware Aria Suite Lifecycle version in VCF inventory.

    Args:
        fqdn(str):
            Fully Qualified Domain Name.

        id_(str):
            The ID of the VMware Aria Suite Lifecycle instance.

        ip_address(str):
            IP Address of VMware Aria Suite Lifecycle appliance.

        status(str):
            The state of the current product instance.

        version(str):
            Version of the product that is currently running on the environment.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "fqdn": "fqdn",
        "id": "id",
        "ip_address": "ipAddress",
        "status": "status",
        "version": "version",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="put",
        path="/v1/vrslcm".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def rollback_vrslcm(hub, ctx) -> Dict[str, Any]:
    r"""

    Remove VMware Aria Suite Lifecycle after an unsuccessful deployment
        Triggers the VMware Aria Suite Lifecycle rollback operation and returns an URL in the headers to track the operation status.


    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="delete",
        path="/v1/vrslcm".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def get_vrslcms(hub, ctx) -> Dict[str, Any]:
    r"""

    Get all existing VMware Aria Suite Lifecycle instances
        None


    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/vrslcms".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {"elements": "elements", "pageMetadata": "page_metadata"}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def deploy_vrslcm(
    hub,
    ctx,
    api_password: str,
    fqdn: str,
    nsxt_standalone_tier1_ip: str,
    ssh_password: str,
) -> Dict[str, Any]:
    r"""

    Deploy VMware Aria Suite Lifecycle
        Triggers the VMware Aria Suite Lifecycle deployment operation and returns an URL in the headers to track the operation status.

    Args:
        api_password(str):
            The password for an admin API/UI user of VMware Aria Suite Lifecycle.

        fqdn(str):
            Fully Qualified Domain Name.

        nsxt_standalone_tier1_ip(str):
            The IP to use for deploying a new standalone Tier-1 router in NSX. This router will be used subsequently for VMware Aria load balancers.

        ssh_password(str):
            The password for a root user of VMware Aria Suite Lifecycle appliance.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "api_password": "apiPassword",
        "fqdn": "fqdn",
        "nsxt_standalone_tier1_ip": "nsxtStandaloneTier1Ip",
        "ssh_password": "sshPassword",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/vrslcms".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def validate_vrslcm(
    hub,
    ctx,
    api_password: str,
    fqdn: str,
    nsxt_standalone_tier1_ip: str,
    ssh_password: str,
) -> Dict[str, Any]:
    r"""

    Perform validation of the VrslcmDeploymentSpec specification
        Triggers VMware Aria Suite Lifecycle deployment specification validation workflow

    Args:
        api_password(str):
            The password for an admin API/UI user of VMware Aria Suite Lifecycle.

        fqdn(str):
            Fully Qualified Domain Name.

        nsxt_standalone_tier1_ip(str):
            The IP to use for deploying a new standalone Tier-1 router in NSX. This router will be used subsequently for VMware Aria load balancers.

        ssh_password(str):
            The password for a root user of VMware Aria Suite Lifecycle appliance.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "api_password": "apiPassword",
        "fqdn": "fqdn",
        "nsxt_standalone_tier1_ip": "nsxtStandaloneTier1Ip",
        "ssh_password": "sshPassword",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/vrslcms/validations".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def get_vrslcm_validation(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Retrieve the results of a VMware Aria Suite Lifecycle validation by its ID
        Gets the status of given VMware Aria Suite Lifecycle validation workflow by given validation id

    Args:
        id_(str):
            VMware Aria Suite Lifecycle validation id.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/vrslcms/validations/{id}".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "additionalProperties": "additional_properties",
            "description": "description",
            "executionStatus": "execution_status",
            "id": "id",
            "resultStatus": "result_status",
            "validationChecks": "validation_checks",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def update_vrslcm_version_by_id_in_inventory(
    hub, ctx, id_: str, fqdn: str, ip_address: str, status: str, version: str
) -> Dict[str, Any]:
    r"""

    Update the version of VMware Aria Suite Lifecycle based on its ID
        Updates VMware Aria Suite Lifecycle version

    Args:
        id_(str):
            The ID of the VMware Aria Suite Lifecycle instance.

        fqdn(str):
            Fully Qualified Domain Name.

        ip_address(str):
            IP Address of VMware Aria Suite Lifecycle appliance.

        status(str):
            The state of the current product instance.

        version(str):
            Version of the product that is currently running on the environment.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {
        "fqdn": "fqdn",
        "id": "id",
        "ip_address": "ipAddress",
        "status": "status",
        "version": "version",
    }

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="put",
        path="/v1/vrslcms/{id}".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result

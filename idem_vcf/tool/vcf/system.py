"""Utility functions for Systems. """
from collections import OrderedDict
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List


async def update_system_configuration(
    hub, ctx, max_allowed_domains_in_subscription: int = None
) -> Dict[str, Any]:
    r"""

    Updates paramters of the system configuration
        Updates paramters of the system configuration

    Args:
        max_allowed_domains_in_subscription(int, Optional):
            new limit of amount of domains in subscription mode. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"max_allowed_domains_in_subscription": "maxAllowedDomainsInSubscription"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="patch",
        path="/v1/system".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    result["ret"] = ret["ret"]

    return result


async def get_validations_of_dns_configuration(
    hub, ctx, execution_status: str = None
) -> Dict[str, Any]:
    r"""

    Retrieve a list of DNS configuation validations
        None

    Args:
        execution_status(str, Optional):
            executionStatus. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/system/dns-configuration/validations".format(**{}),
        query_params={"executionStatus": execution_status},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "additionalProperties": "additional_properties",
            "description": "description",
            "executionStatus": "execution_status",
            "id": "id",
            "resultStatus": "result_status",
            "validationChecks": "validation_checks",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def validate_dns_configuration(
    hub,
    ctx,
    dns_servers: List[
        make_dataclass("dns_servers", [("ip_address", str), ("is_primary", bool)])
    ],
) -> Dict[str, Any]:
    r"""

    Perform validation of the DnsConfiguration specification
        None

    Args:
        dns_servers(List[dict[str, Any]]):
            List of DNS servers.

            * ip_address (str):
                IP address/FQDN of DNS server.

            * is_primary (bool):
                Is DNS server primary.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"dns_servers": "dnsServers"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/system/dns-configuration/validations".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "additionalProperties": "additional_properties",
            "description": "description",
            "executionStatus": "execution_status",
            "id": "id",
            "resultStatus": "result_status",
            "validationChecks": "validation_checks",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_validation_of_dns_configuration(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Retrieve the results of a DNS configuration validation by its ID
        None

    Args:
        id_(str):
            The validation ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/system/dns-configuration/validations/{id}".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "additionalProperties": "additional_properties",
            "description": "description",
            "executionStatus": "execution_status",
            "id": "id",
            "resultStatus": "result_status",
            "validationChecks": "validation_checks",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_validations_of_ntp_configuration(
    hub, ctx, execution_status: str = None
) -> Dict[str, Any]:
    r"""

    Retrieve a list of NTP configuation validations
        None

    Args:
        execution_status(str, Optional):
            executionStatus. Defaults to None.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/system/ntp-configuration/validations".format(**{}),
        query_params={"executionStatus": execution_status},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "additionalProperties": "additional_properties",
            "description": "description",
            "executionStatus": "execution_status",
            "id": "id",
            "resultStatus": "result_status",
            "validationChecks": "validation_checks",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def validate_ntp_configuration(
    hub, ctx, ntp_servers: List[make_dataclass("ntp_servers", [("ip_address", str)])]
) -> Dict[str, Any]:
    r"""

    Perform validation of the NtpConfiguration specification
        None

    Args:
        ntp_servers(List[dict[str, Any]]):
            List of NTP servers.

            * ip_address (str):
                IP address/FQDN of NTP server.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {"ntp_servers": "ntpServers"}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="post",
        path="/v1/system/ntp-configuration/validations".format(**{}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "additionalProperties": "additional_properties",
            "description": "description",
            "executionStatus": "execution_status",
            "id": "id",
            "resultStatus": "result_status",
            "validationChecks": "validation_checks",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def get_validation_of_ntp_configuration(hub, ctx, id_: str) -> Dict[str, Any]:
    r"""

    Retrieve the results of a NTP configuration  validation by its ID
        None

    Args:
        id_(str):
            The validation ID.

    Returns:
        Dict[str, Any]

    """

    result = dict(comment=[], ret=None, result=True)

    # TODO: Change request param mapping as necessary
    payload = {}

    ret = await hub.tool.vcf.session.request(
        ctx,
        method="get",
        path="/v1/system/ntp-configuration/validations/{id}".format(**{"id": id_}),
        query_params={},
        data=payload,
        headers={},
    )

    if not ret["result"]:
        result["comment"].append(ret["comment"])
        result["result"] = False
        return result

    # Convert raw response into present format
    raw_resource = ret["ret"]

    resource_in_present_format = {}
    resource_parameters = OrderedDict(
        {
            "additionalProperties": "additional_properties",
            "description": "description",
            "executionStatus": "execution_status",
            "id": "id",
            "resultStatus": "result_status",
            "validationChecks": "validation_checks",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result

"""States module for managing License Keyss. """
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict

import dict_tools.differ as differ

__contracts__ = ["resource"]


async def present(
    hub,
    ctx,
    name: str,
    description: str,
    key: str,
    product_type: str,
    resource_id: str = None,
    id_: str = None,
    is_unlimited: bool = None,
    license_key_usage: make_dataclass(
        "license_key_usage",
        [
            ("license_unit", str, field(default=None)),
            ("remaining", int, field(default=None)),
            ("total", int, field(default=None)),
            ("used", int, field(default=None)),
        ],
    ) = None,
    license_key_validity: make_dataclass(
        "license_key_validity",
        [
            ("expiry_date", str, field(default=None)),
            ("license_key_status", str, field(default=None)),
        ],
    ) = None,
    product_version: str = None,
) -> Dict[str, Any]:
    """
    Add a a new license key
        None

    Args:
        name(str):
            Idem name of the resource.

        description(str):
            Description of the license key given by user.

        key(str):
            The 29 alpha numeric character license key with hyphens.

        product_type(str):
            The type of the product to which the license key is applicable.

        resource_id(str, Optional):
            License_keys unique ID. Defaults to None.

        id_(str, Optional):
            The ID of the license key. Defaults to None.

        is_unlimited(bool, Optional):
            Indicates if the license key has unlimited usage. Defaults to None.

        license_key_usage(dict[str, Any], Optional):
            licenseKeyUsage. Defaults to None.

            * license_unit (str, Optional):
                Units of the license key. Defaults to None.

            * remaining (int, Optional):
                The remaining/free units of the license key. Defaults to None.

            * total (int, Optional):
                The total units of the license key. Defaults to None.

            * used (int, Optional):
                The consumed/used units of the license key. Defaults to None.

        license_key_validity(dict[str, Any], Optional):
            licenseKeyValidity. Defaults to None.

            * expiry_date (str, Optional):
                The license key expiry date. Defaults to None.

            * license_key_status (str, Optional):
                The validity status of the license key. Defaults to None.

        product_version(str, Optional):
            Product version. Defaults to None.

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


          idem_test_vcf.license_keys_is_present:
              vcf.vcf.license_keys.present:
              - description: string
              - id_: string
              - is_unlimited: bool
              - key: string
              - license_key_usage:
                  license_unit: string
                  remaining: int
                  total: int
                  used: int
              - license_key_validity:
                  expiry_date: string
                  license_key_status: string
              - product_type: string
              - product_version: string


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "kwargs", "result") and v is not None
    }

    if resource_id:
        # Possible parameters: **{"resource_id": resource_id, "name": name, "description": description, "id": id_, "is_unlimited": is_unlimited, "key": key, "license_key_usage": license_key_usage, "license_key_validity": license_key_validity, "product_type": product_type, "product_version": product_version}
        before = await hub.exec.vcf.license_keys.get(
            ctx,
            name=name,
            resource_id=resource_id,
        )

        if not before["result"] or not before["ret"]:
            result["result"] = False
            result["comment"] = before["comment"]
            return result

        result["old_state"] = before.ret

        result["comment"].append(f"'vcf.license_keys: {name}' already exists")

        # If there are changes in desired state from existing state
        changes = differ.deep_diff(before.ret if before.ret else {}, desired_state)

        if bool(changes.get("new")):
            if ctx.test:
                result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                    enforced_state={}, desired_state=desired_state
                )
                result["comment"].append(f"Would update vcf.license_keys: {name}")
                return result
            else:
                # Update the resource
                update_ret = await hub.exec.vcf.license_keys.update(
                    ctx,
                    **{
                        "resource_id": resource_id,
                        "name": name,
                        "description": description,
                        "id": id_,
                        "is_unlimited": is_unlimited,
                        "key": key,
                        "license_key_usage": license_key_usage,
                        "license_key_validity": license_key_validity,
                        "product_type": product_type,
                        "product_version": product_version,
                    },
                )
                result["result"] = update_ret["result"]

                if result["result"]:
                    result["comment"].append(f"Updated 'vcf.license_keys: {name}'")
                else:
                    result["comment"].append(update_ret["comment"])
    else:
        if ctx.test:
            result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                enforced_state={}, desired_state=desired_state
            )
            result["comment"] = (f"Would create vcf.license_keys: {name}",)
            return result
        else:
            create_ret = await hub.exec.vcf.license_keys.create(
                ctx,
                **{
                    "resource_id": resource_id,
                    "name": name,
                    "description": description,
                    "id": id_,
                    "is_unlimited": is_unlimited,
                    "key": key,
                    "license_key_usage": license_key_usage,
                    "license_key_validity": license_key_validity,
                    "product_type": product_type,
                    "product_version": product_version,
                },
            )
            result["result"] = create_ret["result"]

            if result["result"]:
                result["comment"].append(f"Created 'vcf.license_keys: {name}'")
                resource_id = create_ret["ret"]["resource_id"]
                # Safeguard for any future errors so that the resource_id is saved in the ESM
                result["new_state"] = dict(name=name, resource_id=resource_id)
            else:
                result["comment"].append(create_ret["comment"])

    if not result["result"]:
        # If there is any failure in create/update, it should reconcile.
        # The type of data is less important here to use default reconciliation
        # If there are no changes for 3 runs with rerun_data, then it will come out of execution
        result["rerun_data"] = dict(name=name, resource_id=resource_id)

    # Possible parameters: **{"resource_id": resource_id, "name": name, "description": description, "id": id_, "is_unlimited": is_unlimited, "key": key, "license_key_usage": license_key_usage, "license_key_validity": license_key_validity, "product_type": product_type, "product_version": product_version}
    after = await hub.exec.vcf.license_keys.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )
    result["new_state"] = after.ret
    return result


async def absent(
    hub,
    ctx,
    name: str,
    resource_id: str = None,
    key: str = None,
) -> Dict[str, Any]:
    """

    Remove a license key
        None

    Args:
        name(str):
            Idem name of the resource.

        key(str):
            The 29 alpha numeric character license key with hyphens.

        resource_id(str, Optional):
            License_keys unique ID. Defaults to None.

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


            idem_test_vcf.license_keys_is_absent:
              vcf.vcf.license_keys.absent:
              - key: string


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    if not resource_id:
        resource_id = (ctx.old_state or {}).get("resource_id")

    if not resource_id:
        result["comment"].append(f"'vcf.license_keys: {name}' already absent")
        return result

    before = await hub.exec.vcf.license_keys.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )

    if before["ret"]:
        if ctx.test:
            result["comment"] = f"Would delete vcf.license_keys: {name}"
            return result

        delete_ret = await hub.exec.vcf.license_keys.delete(
            ctx,
            name=name,
            resource_id=resource_id,
        )
        result["result"] = delete_ret["result"]

        if result["result"]:
            result["comment"].append(f"Deleted vcf.license_keys: {name}")
        else:
            # If there is any failure in delete, it should reconcile.
            # The type of data is less important here to use default reconciliation
            # If there are no changes for 3 runs with rerun_data, then it will come out of execution
            result["rerun_data"] = resource_id
            result["comment"].append(delete_ret["result"])
    else:
        result["comment"].append(f"vcf.license_keys: {name} already absent")
        return result

    result["old_state"] = before.ret
    return result


async def describe(hub, ctx) -> Dict[str, Dict[str, Any]]:
    """
    Describe the resource in a way that can be recreated/managed with the corresponding "present" function


    Retrieve a list of license keys
        None

    Args:
        product_type(List[str], Optional):
            Type of a Product. Defaults to None.

        license_key_status(List[str], Optional):
            Status of a License Key. Defaults to None.

        product_version(str, Optional):
            Product Version, gets the license keys matching the major version of the product version. Defaults to None.

    Returns:
        Dict[str, Any]

    Example:

        .. code-block:: bash

            $ idem describe vcf.license_keys
    """

    result = {}

    # TODO: Add other required parameters from: {}
    ret = await hub.exec.vcf.license_keys.list(ctx)

    if not ret or not ret["result"]:
        hub.log.debug(f"Could not describe vcf.license_keys {ret['comment']}")
        return result

    for resource in ret["ret"]:
        # TODO: Look for respective identifier in **
        resource_id = resource.get("resource_id")
        result[resource_id] = {
            "vcf.license_keys.present": [
                {parameter_key: parameter_value}
                for parameter_key, parameter_value in resource.items()
            ]
        }
    return result

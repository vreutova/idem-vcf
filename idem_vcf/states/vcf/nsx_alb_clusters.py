"""States module for managing Nsx Alb Clusterss. """
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List

import dict_tools.differ as differ

__contracts__ = ["resource"]


async def present(
    hub,
    ctx,
    name: str,
    resource_id: str = None,
    domain_id: str = None,
    cluster_fqdn: str = None,
    cluster_ip_address: str = None,
    form_factor: str = None,
    admin_password: str = None,
    nodes: List[
        make_dataclass("nodes", [("ip_address", str, field(default=None))])
    ] = None,
    alb_bundle_id: str = None,
) -> Dict[str, Any]:
    """
    Create an NSX ALB Cluster
        None

    Args:
        name(str):
            Idem name of the resource.

        resource_id(str, Optional):
            Nsx_alb_clusters unique ID. Defaults to None.

        domain_id(str, Optional):
            Associated VCF Domain ID. Defaults to None.

        cluster_fqdn(str, Optional):
            NSX ALB Cluster's FQDN. Defaults to None.

        cluster_ip_address(str, Optional):
            NSX ALB Cluster's VIP. Defaults to None.

        form_factor(str, Optional):
            Size of NSX ALB cluster. Defaults to None.

        admin_password(str, Optional):
            Admin user password of NSX ALB cluster. Defaults to None.

        nodes(List[dict[str, Any]], Optional):
            NSX ALB nodes. Defaults to None.

            * ip_address (str, Optional):
                IPv4 address of NSX ALB Node. Defaults to None.

        alb_bundle_id(str, Optional):
            NSX ALB bundle id. Defaults to None.

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


          idem_test_vcf.nsx_alb_clusters_is_present:
              vcf.vcf.nsx_alb_clusters.present:
              - domain_id: string
              - cluster_fqdn: string
              - cluster_ip_address: string
              - form_factor: string
              - admin_password: string
              - nodes:
                - ip_address: string
              - alb_bundle_id: string


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "kwargs", "result") and v is not None
    }

    if resource_id:
        # Possible parameters: **{"resource_id": resource_id, "name": name, "domain_id": domain_id, "cluster_fqdn": cluster_fqdn, "cluster_ip_address": cluster_ip_address, "form_factor": form_factor, "admin_password": admin_password, "nodes": nodes, "alb_bundle_id": alb_bundle_id}
        before = await hub.exec.vcf.nsx_alb_clusters.get(
            ctx,
            name=name,
            resource_id=resource_id,
        )

        if not before["result"] or not before["ret"]:
            result["result"] = False
            result["comment"] = before["comment"]
            return result

        result["old_state"] = before.ret

        result["comment"].append(f"'vcf.nsx_alb_clusters: {name}' already exists")

        # If there are changes in desired state from existing state
        changes = differ.deep_diff(before.ret if before.ret else {}, desired_state)

        if bool(changes.get("new")):
            if ctx.test:
                result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                    enforced_state={}, desired_state=desired_state
                )
                result["comment"].append(f"Would update vcf.nsx_alb_clusters: {name}")
                return result
            else:
                # Update the resource
                update_ret = await hub.exec.vcf.nsx_alb_clusters.update(
                    ctx,
                    **{
                        "resource_id": resource_id,
                        "name": name,
                        "domain_id": domain_id,
                        "cluster_fqdn": cluster_fqdn,
                        "cluster_ip_address": cluster_ip_address,
                        "form_factor": form_factor,
                        "admin_password": admin_password,
                        "nodes": nodes,
                        "alb_bundle_id": alb_bundle_id,
                    },
                )
                result["result"] = update_ret["result"]

                if result["result"]:
                    result["comment"].append(f"Updated 'vcf.nsx_alb_clusters: {name}'")
                else:
                    result["comment"].append(update_ret["comment"])
    else:
        if ctx.test:
            result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                enforced_state={}, desired_state=desired_state
            )
            result["comment"] = (f"Would create vcf.nsx_alb_clusters: {name}",)
            return result
        else:
            create_ret = await hub.exec.vcf.nsx_alb_clusters.create(
                ctx,
                **{
                    "resource_id": resource_id,
                    "name": name,
                    "domain_id": domain_id,
                    "cluster_fqdn": cluster_fqdn,
                    "cluster_ip_address": cluster_ip_address,
                    "form_factor": form_factor,
                    "admin_password": admin_password,
                    "nodes": nodes,
                    "alb_bundle_id": alb_bundle_id,
                },
            )
            result["result"] = create_ret["result"]

            if result["result"]:
                result["comment"].append(f"Created 'vcf.nsx_alb_clusters: {name}'")
                resource_id = create_ret["ret"]["resource_id"]
                # Safeguard for any future errors so that the resource_id is saved in the ESM
                result["new_state"] = dict(name=name, resource_id=resource_id)
            else:
                result["comment"].append(create_ret["comment"])

    if not result["result"]:
        # If there is any failure in create/update, it should reconcile.
        # The type of data is less important here to use default reconciliation
        # If there are no changes for 3 runs with rerun_data, then it will come out of execution
        result["rerun_data"] = dict(name=name, resource_id=resource_id)

    # Possible parameters: **{"resource_id": resource_id, "name": name, "domain_id": domain_id, "cluster_fqdn": cluster_fqdn, "cluster_ip_address": cluster_ip_address, "form_factor": form_factor, "admin_password": admin_password, "nodes": nodes, "alb_bundle_id": alb_bundle_id}
    after = await hub.exec.vcf.nsx_alb_clusters.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )
    result["new_state"] = after.ret
    return result


async def absent(
    hub,
    ctx,
    name: str,
    resource_id: str = None,
    id_: str = None,
) -> Dict[str, Any]:
    """

    Delete an NSX ALB Cluster by its ID
        Deletes NSX ALB Cluster

    Args:
        name(str):
            Idem name of the resource.

        id_(str):
            NSX ALB Cluster ID.

        resource_id(str, Optional):
            Nsx_alb_clusters unique ID. Defaults to None.

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


            idem_test_vcf.nsx_alb_clusters_is_absent:
              vcf.vcf.nsx_alb_clusters.absent:
              - id_: string


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    if not resource_id:
        resource_id = (ctx.old_state or {}).get("resource_id")

    if not resource_id:
        result["comment"].append(f"'vcf.nsx_alb_clusters: {name}' already absent")
        return result

    before = await hub.exec.vcf.nsx_alb_clusters.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )

    if before["ret"]:
        if ctx.test:
            result["comment"] = f"Would delete vcf.nsx_alb_clusters: {name}"
            return result

        delete_ret = await hub.exec.vcf.nsx_alb_clusters.delete(
            ctx,
            name=name,
            resource_id=resource_id,
        )
        result["result"] = delete_ret["result"]

        if result["result"]:
            result["comment"].append(f"Deleted vcf.nsx_alb_clusters: {name}")
        else:
            # If there is any failure in delete, it should reconcile.
            # The type of data is less important here to use default reconciliation
            # If there are no changes for 3 runs with rerun_data, then it will come out of execution
            result["rerun_data"] = resource_id
            result["comment"].append(delete_ret["result"])
    else:
        result["comment"].append(f"vcf.nsx_alb_clusters: {name} already absent")
        return result

    result["old_state"] = before.ret
    return result


async def describe(hub, ctx) -> Dict[str, Dict[str, Any]]:
    """
    Describe the resource in a way that can be recreated/managed with the corresponding "present" function


    Retrieve a list of NSX ALB Clusters
        None

    Args:
        domain_id(str, Optional):
            Pass an optional domain ID to fetch ALB clusters associated with the workload domain. Defaults to None.

    Returns:
        Dict[str, Any]

    Example:

        .. code-block:: bash

            $ idem describe vcf.nsx_alb_clusters
    """

    result = {}

    # TODO: Add other required parameters from: {}
    ret = await hub.exec.vcf.nsx_alb_clusters.list(ctx)

    if not ret or not ret["result"]:
        hub.log.debug(f"Could not describe vcf.nsx_alb_clusters {ret['comment']}")
        return result

    for resource in ret["ret"]:
        # TODO: Look for respective identifier in **
        resource_id = resource.get("resource_id")
        result[resource_id] = {
            "vcf.nsx_alb_clusters.present": [
                {parameter_key: parameter_value}
                for parameter_key, parameter_value in resource.items()
            ]
        }
    return result

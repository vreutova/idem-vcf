"""States module for managing Sddcs. """
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List

import dict_tools.differ as differ

__contracts__ = ["resource"]


async def present(
    hub,
    ctx,
    name: str,
    cluster_spec: make_dataclass(
        "cluster_spec",
        [
            ("cluster_name", str),
            ("cluster_evc_mode", str, field(default=None)),
            ("cluster_image_enabled", bool, field(default=None)),
            ("host_failures_to_tolerate", int, field(default=None)),
            ("personality_name", str, field(default=None)),
            (
                "resource_pool_specs",
                List[
                    make_dataclass(
                        "resource_pool_specs",
                        [
                            ("memory_reservation_expandable", bool),
                            ("name", str),
                            ("cpu_limit", int, field(default=None)),
                            ("cpu_reservation_expandable", bool, field(default=None)),
                            ("cpu_reservation_mhz", int, field(default=None)),
                            ("cpu_reservation_percentage", int, field(default=None)),
                            ("cpu_shares_level", str, field(default=None)),
                            ("cpu_shares_value", int, field(default=None)),
                            ("memory_limit", int, field(default=None)),
                            ("memory_reservation_mb", int, field(default=None)),
                            ("memory_reservation_percentage", int, field(default=None)),
                            ("memory_shares_level", str, field(default=None)),
                            ("memory_shares_value", int, field(default=None)),
                            ("type", str, field(default=None)),
                        ],
                    )
                ],
                field(default=None),
            ),
            ("vm_folders", Dict, field(default=None)),
        ],
    ),
    dns_spec: make_dataclass(
        "dns_spec",
        [
            ("domain", str),
            ("subdomain", str),
            ("nameserver", str, field(default=None)),
            ("secondary_nameserver", str, field(default=None)),
        ],
    ),
    dvs_specs: List[
        make_dataclass(
            "dvs_specs",
            [
                ("dvs_name", str),
                ("networks", List[str]),
                ("is_used_by_nsxt", bool, field(default=None)),
                ("mtu", int, field(default=None)),
                (
                    "nioc_specs",
                    List[
                        make_dataclass(
                            "nioc_specs", [("traffic_type", str), ("value", str)]
                        )
                    ],
                    field(default=None),
                ),
                ("vmnics", List[str], field(default=None)),
                (
                    "nsxt_switch_config",
                    make_dataclass(
                        "nsxt_switch_config",
                        [
                            (
                                "transport_zones",
                                List[
                                    make_dataclass(
                                        "transport_zones",
                                        [
                                            ("transport_type", str),
                                            ("name", str, field(default=None)),
                                        ],
                                    )
                                ],
                                field(default=None),
                            ),
                            ("host_switch_operational_mode", str, field(default=None)),
                        ],
                    ),
                    field(default=None),
                ),
                (
                    "vmnics_to_uplinks",
                    List[
                        make_dataclass(
                            "vmnics_to_uplinks",
                            [("nsx_uplink_name", str), ("vds_uplink_name", str)],
                        )
                    ],
                    field(default=None),
                ),
            ],
        )
    ],
    host_specs: List[
        make_dataclass(
            "host_specs",
            [
                ("association", str),
                (
                    "credentials",
                    make_dataclass(
                        "credentials", [("password", str), ("username", str)]
                    ),
                ),
                ("hostname", str),
                (
                    "ip_address_private",
                    make_dataclass(
                        "ip_address_private",
                        [
                            ("ip_address", str),
                            ("cidr", str, field(default=None)),
                            ("gateway", str, field(default=None)),
                            ("subnet", str, field(default=None)),
                        ],
                    ),
                ),
                ("v_switch", str),
                ("ssh_thumbprint", str, field(default=None)),
                ("ssl_thumbprint", str, field(default=None)),
                ("vswitch", str, field(default=None)),
            ],
        )
    ],
    network_specs: List[
        make_dataclass(
            "network_specs",
            [
                ("network_type", str),
                ("vlan_id", str),
                ("active_uplinks", List[str], field(default=None)),
                ("exclude_ip_address_ranges", List[str], field(default=None)),
                ("exclude_ipaddresses", List[str], field(default=None)),
                ("gateway", str, field(default=None)),
                ("include_ip_address", List[str], field(default=None)),
                (
                    "include_ip_address_ranges",
                    List[
                        make_dataclass(
                            "include_ip_address_ranges",
                            [("end_ip_address", str), ("start_ip_address", str)],
                        )
                    ],
                    field(default=None),
                ),
                ("mtu", str, field(default=None)),
                ("port_group_key", str, field(default=None)),
                ("standby_uplinks", List[str], field(default=None)),
                ("subnet", str, field(default=None)),
                ("subnet_mask", str, field(default=None)),
                ("teaming_policy", str, field(default=None)),
            ],
        )
    ],
    ntp_servers: List[str],
    sddc_id: str,
    task_name: str,
    vcenter_spec: make_dataclass(
        "vcenter_spec",
        [
            ("root_vcenter_password", str),
            ("vcenter_hostname", str),
            ("license_file", str, field(default=None)),
            ("ssh_thumbprint", str, field(default=None)),
            ("ssl_thumbprint", str, field(default=None)),
            ("storage_size", str, field(default=None)),
            ("vcenter_ip", str, field(default=None)),
            ("vm_size", str, field(default=None)),
        ],
    ),
    resource_id: str = None,
    ceip_enabled: bool = None,
    dv_switch_version: str = None,
    esx_license: str = None,
    excluded_components: List[str] = None,
    fips_enabled: bool = None,
    management_pool_name: str = None,
    nsxt_spec: make_dataclass(
        "nsxt_spec",
        [
            ("nsxt_manager_size", str),
            (
                "nsxt_managers",
                List[
                    make_dataclass(
                        "nsxt_managers",
                        [
                            ("hostname", str, field(default=None)),
                            ("ip", str, field(default=None)),
                        ],
                    )
                ],
            ),
            ("root_nsxt_manager_password", str),
            ("vip", str),
            ("vip_fqdn", str),
            (
                "ip_address_pool_spec",
                make_dataclass(
                    "ip_address_pool_spec",
                    [
                        ("name", str),
                        ("description", str, field(default=None)),
                        ("ignore_unavailable_nsxt_cluster", bool, field(default=None)),
                        (
                            "subnets",
                            List[
                                make_dataclass(
                                    "subnets",
                                    [
                                        ("cidr", str),
                                        ("gateway", str),
                                        (
                                            "ip_address_pool_ranges",
                                            List[
                                                make_dataclass(
                                                    "ip_address_pool_ranges",
                                                    [("end", str), ("start", str)],
                                                )
                                            ],
                                        ),
                                    ],
                                )
                            ],
                            field(default=None),
                        ),
                    ],
                ),
                field(default=None),
            ),
            ("nsxt_admin_password", str, field(default=None)),
            ("nsxt_audit_password", str, field(default=None)),
            ("nsxt_license", str, field(default=None)),
            (
                "over_lay_transport_zone",
                make_dataclass(
                    "over_lay_transport_zone",
                    [("network_name", str), ("zone_name", str)],
                ),
                field(default=None),
            ),
            ("transport_vlan_id", int, field(default=None)),
        ],
    ) = None,
    proxy_spec: make_dataclass(
        "proxy_spec",
        [("host", str, field(default=None)), ("port", int, field(default=None))],
    ) = None,
    psc_specs: List[
        make_dataclass(
            "psc_specs",
            [
                ("admin_user_sso_password", str),
                (
                    "psc_sso_spec",
                    make_dataclass(
                        "psc_sso_spec", [("sso_domain", str, field(default=None))]
                    ),
                    field(default=None),
                ),
            ],
        )
    ] = None,
    sddc_manager_spec: make_dataclass(
        "sddc_manager_spec",
        [
            ("hostname", str),
            ("ip_address", str),
            (
                "root_user_credentials",
                make_dataclass(
                    "root_user_credentials", [("password", str), ("username", str)]
                ),
            ),
            (
                "second_user_credentials",
                make_dataclass(
                    "second_user_credentials", [("password", str), ("username", str)]
                ),
            ),
            ("local_user_password", str, field(default=None)),
        ],
    ) = None,
    security_spec: make_dataclass(
        "security_spec",
        [
            ("esxi_certs_mode", str, field(default=None)),
            (
                "root_ca_certs",
                List[
                    make_dataclass(
                        "root_ca_certs",
                        [
                            ("alias", str, field(default=None)),
                            ("cert_chain", List[str], field(default=None)),
                        ],
                    )
                ],
                field(default=None),
            ),
        ],
    ) = None,
    skip_esx_thumbprint_validation: bool = None,
    skip_gateway_ping_validation: bool = None,
    vsan_spec: make_dataclass(
        "vsan_spec",
        [
            ("datastore_name", str),
            (
                "esa_config",
                make_dataclass("esa_config", [("enabled", bool, field(default=None))]),
                field(default=None),
            ),
            ("hcl_file", str, field(default=None)),
            ("license_file", str, field(default=None)),
            ("vsan_dedup", bool, field(default=None)),
        ],
    ) = None,
    vx_manager_spec: make_dataclass(
        "vx_manager_spec",
        [
            (
                "default_admin_user_credentials",
                make_dataclass(
                    "default_admin_user_credentials",
                    [("password", str), ("username", str)],
                ),
            ),
            (
                "default_root_user_credentials",
                make_dataclass(
                    "default_root_user_credentials",
                    [("password", str), ("username", str)],
                ),
            ),
            ("vx_manager_host_name", str),
            ("ssh_thumbprint", str, field(default=None)),
            ("ssl_thumbprint", str, field(default=None)),
        ],
    ) = None,
) -> Dict[str, Any]:
    r"""
    Deploy a management domain
        None

    Args:
        name(str):
            Idem name of the resource.

        cluster_spec(dict[str, Any]):
            clusterSpec.

            * cluster_evc_mode (str, Optional):
                vCenter cluster EVC mode. Defaults to None.

            * cluster_image_enabled (bool, Optional):
                Enable vSphere Lifecycle Manager Images for cluster creation. Defaults to None.

            * cluster_name (str):
                vCenter Cluster Name.

            * host_failures_to_tolerate (int, Optional):
                Host failures to tolerate. Defaults to None.

            * personality_name (str, Optional):
                Cluster Personality Name. Defaults to None.

            * resource_pool_specs (List[dict[str, Any]], Optional):
                Resource Pool Specs. Defaults to None.

                * cpu_limit (int, Optional):
                    CPU limit, default -1 (unlimited). Defaults to None.

                * cpu_reservation_expandable (bool, Optional):
                    Is CPU reservation expandable, default true. Defaults to None.

                * cpu_reservation_mhz (int, Optional):
                    CPU reservation in Mhz. Defaults to None.

                * cpu_reservation_percentage (int, Optional):
                    CPU reservation percentage, from 0 to 100, default 0. Defaults to None.

                * cpu_shares_level (str, Optional):
                    CPU shares level, default 'normal'. Defaults to None.

                * cpu_shares_value (int, Optional):
                    CPU shares value, only required when shares level is 'normal'. Defaults to None.

                * memory_limit (int, Optional):
                    Memory limit, default -1 (unlimited). Defaults to None.

                * memory_reservation_expandable (bool):
                    Is Memory reservation expandable, default true.

                * memory_reservation_mb (int, Optional):
                    Memory reservation in MB. Defaults to None.

                * memory_reservation_percentage (int, Optional):
                    Memory reservation percentage, from 0 to 100, default 0. Defaults to None.

                * memory_shares_level (str, Optional):
                    Memory shares level. default 'normal'. Defaults to None.

                * memory_shares_value (int, Optional):
                    Memory shares value, only required when shares level is '0'. Defaults to None.

                * name (str):
                    Resource Pool name.

                * type (str, Optional):
                    Type of resource pool. Defaults to None.

            * vm_folders (Dict, Optional):
                Virtual Machine folders map. Defaults to None.

        dns_spec(dict[str, Any]):
            dnsSpec.

            * domain (str):
                Tenant domain.

            * nameserver (str, Optional):
                Primary nameserver to be configured for vCenter/PSC/ESXi's/NSX. Defaults to None.

            * secondary_nameserver (str, Optional):
                Secondary nameserver to be configured for vCenter/PSC/ESXi's/NSX. Defaults to None.

            * subdomain (str):
                Tenant Sub domain.

        dvs_specs(List[dict[str, Any]]):
            List of Dvs Specs.

            * dvs_name (str):
                DVS Name.

            * is_used_by_nsxt (bool, Optional):
                Flag indicating whether the DVS is used by NSX.
                 This property is deprecated in favor of nsxtSwitchConfig field. Defaults to None.

            * mtu (int, Optional):
                DVS MTU (default value is 9000). Defaults to None.

            * networks (List[str]):
                Types of networks in this portgroup.

            * nioc_specs (List[dict[str, Any]], Optional):
                List of NIOC specs for networks. Defaults to None.

                * traffic_type (str):
                    Traffic Type.

                * value (str):
                    NIOC Value.

            * vmnics (List[str], Optional):
                Vmnics to be attached to the DVS.
                 This property is deprecated in favor of vmnicsToUplinks fields. Defaults to None.

            * nsxt_switch_config (dict[str, Any], Optional):
                nsxtSwitchConfig. Defaults to None.

                * transport_zones (List[dict[str, Any]], Optional):
                    The list of transport zones to be associated with the vSphere Distributed Switch managed by NSX. Defaults to None.

                    * name (str, Optional):
                        The name of the transport zone. Defaults to None.

                    * transport_type (str):
                        The type of the transport zone.

                * host_switch_operational_mode (str, Optional):
                    vSphere Distributed Switch name. Defaults to None.

            * vmnics_to_uplinks (List[dict[str, Any]], Optional):
                The map of vSphere Distributed Switch uplinks to the NSX switch uplinks. Defaults to None.

                * nsx_uplink_name (str):
                    The uplink name of the NSX switch.

                * vds_uplink_name (str):
                    The uplink name of the vSphere Distributed Switch.

        host_specs(List[dict[str, Any]]):
            List of Host Specs.

            * association (str):
                Host Association: Location/Datacenter.

            * credentials (dict[str, Any]):
                credentials.

                * password (str):
                    Password.

                * username (str):
                    Username.

            * hostname (str):
                Host Hostname.

            * ip_address_private (dict[str, Any]):
                ipAddressPrivate.

                * cidr (str, Optional):
                    Classless Inter-Domain Routing (CIDR). Defaults to None.

                * gateway (str, Optional):
                    Gateway. Defaults to None.

                * ip_address (str):
                    IP Address.

                * subnet (str, Optional):
                    Subnet. Defaults to None.

            * ssh_thumbprint (str, Optional):
                Host SSH thumbprint (RSA SHA256). Defaults to None.

            * ssl_thumbprint (str, Optional):
                Host SSL thumbprint (SHA256). Defaults to None.

            * v_switch (str):
                vSwitch.

            * vswitch (str, Optional):
                vswitch. Defaults to None.

        network_specs(List[dict[str, Any]]):
            List of Network Specs.

            * active_uplinks (List[str], Optional):
                Active Uplinks for teaming policy, specify uplink1 for failover_explicit VSAN Teaming Policy. Defaults to None.

            * exclude_ip_address_ranges (List[str], Optional):
                IP Addresse ranges to be excluded. Defaults to None.

            * exclude_ipaddresses (List[str], Optional):
                IP Addresses to be excluded. Defaults to None.

            * gateway (str, Optional):
                Gateway. Defaults to None.

            * include_ip_address (List[str], Optional):
                IP Addresses to be included. Defaults to None.

            * include_ip_address_ranges (List[dict[str, Any]], Optional):
                IP Addresse ranges to be included. Defaults to None.

                * end_ip_address (str):
                    End IP Address.

                * start_ip_address (str):
                    Start IP Address.

            * mtu (str, Optional):
                MTU size. Defaults to None.

            * network_type (str):
                Network Type.

            * port_group_key (str, Optional):
                Portgroup key name. Defaults to None.

            * standby_uplinks (List[str], Optional):
                Standby Uplinks for teaming policy, specify uplink2 for failover_explicit VSAN Teaming Policy. Defaults to None.

            * subnet (str, Optional):
                Subnet. Defaults to None.

            * subnet_mask (str, Optional):
                Subnet Mask. Defaults to None.

            * teaming_policy (str, Optional):
                Teaming Policy for VSAN and VMOTION network types, Default is loadbalance_loadbased. Defaults to None.

            * vlan_id (str):
                VLAN Id.

        ntp_servers(List[str]):
            List of NTP servers.

        sddc_id(str):
            Client string that identifies an SDDC by name or instance name. Used for management domain name. Can contain only letters, numbers and the following symbols: '-'.

        task_name(str):
            Name of the task to execute.

        vcenter_spec(dict[str, Any]):
            vcenterSpec.

            * license_file (str, Optional):
                License File. Defaults to None.

            * root_vcenter_password (str):
                vCenter root password. The password must be between 8 characters and 20 characters long. It must also contain at least one uppercase and lowercase letter, one number, and one character from '! " # $ % & ' ( ) * + , - . / : ; < = > ? @ [ \ ] ^ _ ` { &Iota; } ~' and all characters must be ASCII. Space is not allowed in password.

            * ssh_thumbprint (str, Optional):
                vCenter Server SSH thumbprint (RSA SHA256). Defaults to None.

            * ssl_thumbprint (str, Optional):
                vCenter Server SSL thumbprint (SHA256). Defaults to None.

            * storage_size (str, Optional):
                vCenter VM storage size. Defaults to None.

            * vcenter_hostname (str):
                vCenter hostname address.

            * vcenter_ip (str, Optional):
                vCenter IP address. Defaults to None.

            * vm_size (str, Optional):
                vCenter VM size. Defaults to None.

        resource_id(str, Optional):
            Sddc unique ID. Defaults to None.

        ceip_enabled(bool, Optional):
            Enable VCF Customer Experience Improvement Program. Defaults to None.

        dv_switch_version(str, Optional):
            The version of the distributed virtual switches to be used. Defaults to None.

        esx_license(str, Optional):
            License for the ESXi hosts. Defaults to None.

        excluded_components(List[str], Optional):
            Components to be excluded. Defaults to None.

        fips_enabled(bool, Optional):
            Enable Federal Information Processing Standards. Defaults to None.

        management_pool_name(str, Optional):
            A String identifying the network pool associated with the management domain. Defaults to None.

        nsxt_spec(dict[str, Any], Optional):
            nsxtSpec. Defaults to None.

            * ip_address_pool_spec (dict[str, Any], Optional):
                ipAddressPoolSpec. Defaults to None.

                * description (str, Optional):
                    Description of the IP address pool. Defaults to None.

                * ignore_unavailable_nsxt_cluster (bool, Optional):
                    Ignore unavailable NSX cluster(s) during IP pool spec validation. Defaults to None.

                * name (str):
                    Name of the IP address pool.

                * subnets (List[dict[str, Any]], Optional):
                    List of IP address pool subnet specification. Defaults to None.

                    * cidr (str):
                        The subnet representation, contains the network address and the prefix length.

                    * gateway (str):
                        The default gateway address of the network.

                    * ip_address_pool_ranges (List[dict[str, Any]]):
                        List of the IP allocation ranges. Atleast 1 IP address range has to be specified.

                        * end (str):
                            The last IP Address of the IP Address Range.

                        * start (str):
                            The first IP Address of the IP Address Range.

            * nsxt_admin_password (str, Optional):
                NSX admin password. The password must be at least 12 characters long. Must contain at-least 1 uppercase, 1 lowercase, 1 special character and 1 digit. In addition, a character cannot be repeated 3 or more times consectively. Defaults to None.

            * nsxt_audit_password (str, Optional):
                NSX audit password. The password must be at least 12 characters long. Must contain at-least 1 uppercase, 1 lowercase, 1 special character and 1 digit. In addition, a character cannot be repeated 3 or more times consectively. Defaults to None.

            * nsxt_license (str, Optional):
                NSX Manager license. Defaults to None.

            * nsxt_manager_size (str):
                NSX Manager size.

            * nsxt_managers (List[dict[str, Any]]):
                NSX Managers.

                * hostname (str, Optional):
                    NSX Manager hostname. Defaults to None.

                * ip (str, Optional):
                    NSX Manager IP Address. Defaults to None.

            * over_lay_transport_zone (dict[str, Any], Optional):
                overLayTransportZone. Defaults to None.

                * network_name (str):
                    Transport zone network name.

                * zone_name (str):
                    Transport zone name.

            * root_nsxt_manager_password (str):
                NSX Manager root password. Password should have 1) At least eight characters, 2) At least one lower-case letter, 3) At least one upper-case letter 4) At least one digit 5) At least one special character, 6) At least five different characters , 7) No dictionary words, 6) No palindromes.

            * transport_vlan_id (int, Optional):
                Transport VLAN ID. Defaults to None.

            * vip (str):
                Virtual IP address which would act as proxy/alias for NSX Managers.

            * vip_fqdn (str):
                FQDN for VIP so that common SSL certificates can be installed across all managers.

        proxy_spec(dict[str, Any], Optional):
            proxySpec. Defaults to None.

            * host (str, Optional):
                IP address/FQDN of proxy server. Defaults to None.

            * port (int, Optional):
                Port of proxy server. Defaults to None.

        psc_specs(List[dict[str, Any]], Optional):
            PSC VM spec. Defaults to None.

            * admin_user_sso_password (str):
                Admin user sso passwordPassword needs to be a strong password with at least one Uppercase alphabet, one lowercase alphabet, one digit and one special character specified in braces [!$%^] and 8-20 characters in length,and 3 maximum identical adjacent characters!.

            * psc_sso_spec (dict[str, Any], Optional):
                pscSsoSpec. Defaults to None.

                * sso_domain (str, Optional):
                    PSC SSO Domain. Defaults to None.

        sddc_manager_spec(dict[str, Any], Optional):
            sddcManagerSpec. Defaults to None.

            * hostname (str):
                SDDC Manager Hostname.

            * ip_address (str):
                SDDC Manager ip address.

            * local_user_password (str, Optional):
                The local account is a built-in admin account in VCF that can be used in emergency scenarios. The password of this account must be at least 12 characters long. It also must contain at-least 1 uppercase, 1 lowercase, 1 special character specified in braces [!%@$^#?] and 1 digit. In addition, a character cannot be repeated more than 3 times consecutively. Defaults to None.

            * root_user_credentials (dict[str, Any]):
                rootUserCredentials.

                * password (str):
                    Password.

                * username (str):
                    Username.

            * second_user_credentials (dict[str, Any]):
                secondUserCredentials.

                * password (str):
                    Password.

                * username (str):
                    Username.

        security_spec(dict[str, Any], Optional):
            securitySpec. Defaults to None.

            * esxi_certs_mode (str, Optional):
                ESXi certificates mode. Defaults to None.

            * root_ca_certs (List[dict[str, Any]], Optional):
                Root Certificate Authority certificate list. Defaults to None.

                * alias (str, Optional):
                    Certificate alias. Defaults to None.

                * cert_chain (List[str], Optional):
                    List of Base64 encoded certificates. Defaults to None.

        skip_esx_thumbprint_validation(bool, Optional):
            Skip ESXi thumbprint validation. Defaults to None.

        skip_gateway_ping_validation(bool, Optional):
            Skip networks gateway connectivity validation. Defaults to None.

        vsan_spec(dict[str, Any], Optional):
            vsanSpec. Defaults to None.

            * datastore_name (str):
                Datastore Name.

            * esa_config (dict[str, Any], Optional):
                esaConfig. Defaults to None.

                * enabled (bool, Optional):
                    Whether the vSAN ESA is enabled. Defaults to None.

            * hcl_file (str, Optional):
                HCL File. Defaults to None.

            * license_file (str, Optional):
                License File. Defaults to None.

            * vsan_dedup (bool, Optional):
                VSAN feature Deduplication and Compression flag, one flag for both features. Defaults to None.

        vx_manager_spec(dict[str, Any], Optional):
            vxManagerSpec. Defaults to None.

            * default_admin_user_credentials (dict[str, Any]):
                defaultAdminUserCredentials.

                * password (str):
                    Password.

                * username (str):
                    Username.

            * default_root_user_credentials (dict[str, Any]):
                defaultRootUserCredentials.

                * password (str):
                    Password.

                * username (str):
                    Username.

            * ssh_thumbprint (str, Optional):
                VxRail Manager SSH thumbprint (RSA SHA256). Defaults to None.

            * ssl_thumbprint (str, Optional):
                VxRail Manager SSL thumbprint (SHA256). Defaults to None.

            * vx_manager_host_name (str):
                VxManager host name.

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


          idem_test_vcf.sddc_is_present:
              vcf.vcf.sddc.present:
              - ceip_enabled: bool
              - cluster_spec:
                  cluster_evc_mode: string
                  cluster_image_enabled: bool
                  cluster_name: string
                  host_failures_to_tolerate: int
                  personality_name: string
                  resource_pool_specs:
                  - cpu_limit: int
                    cpu_reservation_expandable: bool
                    cpu_reservation_mhz: int
                    cpu_reservation_percentage: int
                    cpu_shares_level: string
                    cpu_shares_value: int
                    memory_limit: int
                    memory_reservation_expandable: bool
                    memory_reservation_mb: int
                    memory_reservation_percentage: int
                    memory_shares_level: string
                    memory_shares_value: int
                    name: string
                    type_: string
                  vm_folders: Dict
              - dns_spec:
                  domain: string
                  nameserver: string
                  secondary_nameserver: string
                  subdomain: string
              - dv_switch_version: string
              - dvs_specs:
                - dvs_name: string
                  is_used_by_nsxt: bool
                  mtu: int
                  networks:
                  - value
                  nioc_specs:
                  - traffic_type: string
                    value: string
                  nsxt_switch_config:
                    host_switch_operational_mode: string
                    transport_zones:
                    - name: string
                      transport_type: string
                  vmnics:
                  - value
                  vmnics_to_uplinks:
                  - nsx_uplink_name: string
                    vds_uplink_name: string
              - esx_license: string
              - excluded_components:
                - value
              - fips_enabled: bool
              - host_specs:
                - association: string
                  credentials:
                    password: string
                    username: string
                  hostname: string
                  ip_address_private:
                    cidr: string
                    gateway: string
                    ip_address: string
                    subnet: string
                  ssh_thumbprint: string
                  ssl_thumbprint: string
                  v_switch: string
                  vswitch: string
              - management_pool_name: string
              - network_specs:
                - active_uplinks:
                  - value
                  exclude_ip_address_ranges:
                  - value
                  exclude_ipaddresses:
                  - value
                  gateway: string
                  include_ip_address:
                  - value
                  include_ip_address_ranges:
                  - end_ip_address: string
                    start_ip_address: string
                  mtu: string
                  network_type: string
                  port_group_key: string
                  standby_uplinks:
                  - value
                  subnet: string
                  subnet_mask: string
                  teaming_policy: string
                  vlan_id: string
              - nsxt_spec:
                  ip_address_pool_spec:
                    description: string
                    ignore_unavailable_nsxt_cluster: bool
                    name: string
                    subnets:
                    - cidr: string
                      gateway: string
                      ip_address_pool_ranges:
                      - end: string
                        start: string
                  nsxt_admin_password: string
                  nsxt_audit_password: string
                  nsxt_license: string
                  nsxt_manager_size: string
                  nsxt_managers:
                  - hostname: string
                    ip: string
                  over_lay_transport_zone:
                    network_name: string
                    zone_name: string
                  root_nsxt_manager_password: string
                  transport_vlan_id: int
                  vip: string
                  vip_fqdn: string
              - ntp_servers:
                - value
              - proxy_spec:
                  host: string
                  port: int
              - psc_specs:
                - admin_user_sso_password: string
                  psc_sso_spec:
                    sso_domain: string
              - sddc_id: string
              - sddc_manager_spec:
                  hostname: string
                  ip_address: string
                  local_user_password: string
                  root_user_credentials:
                    password: string
                    username: string
                  second_user_credentials:
                    password: string
                    username: string
              - security_spec:
                  esxi_certs_mode: string
                  root_ca_certs:
                  - alias: string
                    cert_chain:
                    - value
              - skip_esx_thumbprint_validation: bool
              - skip_gateway_ping_validation: bool
              - task_name: string
              - vcenter_spec:
                  license_file: string
                  root_vcenter_password: string
                  ssh_thumbprint: string
                  ssl_thumbprint: string
                  storage_size: string
                  vcenter_hostname: string
                  vcenter_ip: string
                  vm_size: string
              - vsan_spec:
                  datastore_name: string
                  esa_config:
                    enabled: bool
                  hcl_file: string
                  license_file: string
                  vsan_dedup: bool
              - vx_manager_spec:
                  default_admin_user_credentials:
                    password: string
                    username: string
                  default_root_user_credentials:
                    password: string
                    username: string
                  ssh_thumbprint: string
                  ssl_thumbprint: string
                  vx_manager_host_name: string


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "kwargs", "result") and v is not None
    }

    if resource_id:
        # Possible parameters: **{"resource_id": resource_id, "name": name, "ceip_enabled": ceip_enabled, "cluster_spec": cluster_spec, "dns_spec": dns_spec, "dv_switch_version": dv_switch_version, "dvs_specs": dvs_specs, "esx_license": esx_license, "excluded_components": excluded_components, "fips_enabled": fips_enabled, "host_specs": host_specs, "management_pool_name": management_pool_name, "network_specs": network_specs, "nsxt_spec": nsxt_spec, "ntp_servers": ntp_servers, "proxy_spec": proxy_spec, "psc_specs": psc_specs, "sddc_id": sddc_id, "sddc_manager_spec": sddc_manager_spec, "security_spec": security_spec, "skip_esx_thumbprint_validation": skip_esx_thumbprint_validation, "skip_gateway_ping_validation": skip_gateway_ping_validation, "task_name": task_name, "vcenter_spec": vcenter_spec, "vsan_spec": vsan_spec, "vx_manager_spec": vx_manager_spec}
        before = await hub.exec.vcf.sddc.get(
            ctx,
            name=name,
            resource_id=resource_id,
        )

        if not before["result"] or not before["ret"]:
            result["result"] = False
            result["comment"] = before["comment"]
            return result

        result["old_state"] = before.ret

        result["comment"].append(f"'vcf.sddc: {name}' already exists")

        # If there are changes in desired state from existing state
        changes = differ.deep_diff(before.ret if before.ret else {}, desired_state)

        if bool(changes.get("new")):
            if ctx.test:
                result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                    enforced_state={}, desired_state=desired_state
                )
                result["comment"].append(f"Would update vcf.sddc: {name}")
                return result
            else:
                # Update the resource
                update_ret = await hub.exec.vcf.sddc.update(
                    ctx,
                    **{
                        "resource_id": resource_id,
                        "name": name,
                        "ceip_enabled": ceip_enabled,
                        "cluster_spec": cluster_spec,
                        "dns_spec": dns_spec,
                        "dv_switch_version": dv_switch_version,
                        "dvs_specs": dvs_specs,
                        "esx_license": esx_license,
                        "excluded_components": excluded_components,
                        "fips_enabled": fips_enabled,
                        "host_specs": host_specs,
                        "management_pool_name": management_pool_name,
                        "network_specs": network_specs,
                        "nsxt_spec": nsxt_spec,
                        "ntp_servers": ntp_servers,
                        "proxy_spec": proxy_spec,
                        "psc_specs": psc_specs,
                        "sddc_id": sddc_id,
                        "sddc_manager_spec": sddc_manager_spec,
                        "security_spec": security_spec,
                        "skip_esx_thumbprint_validation": skip_esx_thumbprint_validation,
                        "skip_gateway_ping_validation": skip_gateway_ping_validation,
                        "task_name": task_name,
                        "vcenter_spec": vcenter_spec,
                        "vsan_spec": vsan_spec,
                        "vx_manager_spec": vx_manager_spec,
                    },
                )
                result["result"] = update_ret["result"]

                if result["result"]:
                    result["comment"].append(f"Updated 'vcf.sddc: {name}'")
                else:
                    result["comment"].append(update_ret["comment"])
    else:
        if ctx.test:
            result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                enforced_state={}, desired_state=desired_state
            )
            result["comment"] = (f"Would create vcf.sddc: {name}",)
            return result
        else:
            create_ret = await hub.exec.vcf.sddc.create(
                ctx,
                **{
                    "resource_id": resource_id,
                    "name": name,
                    "ceip_enabled": ceip_enabled,
                    "cluster_spec": cluster_spec,
                    "dns_spec": dns_spec,
                    "dv_switch_version": dv_switch_version,
                    "dvs_specs": dvs_specs,
                    "esx_license": esx_license,
                    "excluded_components": excluded_components,
                    "fips_enabled": fips_enabled,
                    "host_specs": host_specs,
                    "management_pool_name": management_pool_name,
                    "network_specs": network_specs,
                    "nsxt_spec": nsxt_spec,
                    "ntp_servers": ntp_servers,
                    "proxy_spec": proxy_spec,
                    "psc_specs": psc_specs,
                    "sddc_id": sddc_id,
                    "sddc_manager_spec": sddc_manager_spec,
                    "security_spec": security_spec,
                    "skip_esx_thumbprint_validation": skip_esx_thumbprint_validation,
                    "skip_gateway_ping_validation": skip_gateway_ping_validation,
                    "task_name": task_name,
                    "vcenter_spec": vcenter_spec,
                    "vsan_spec": vsan_spec,
                    "vx_manager_spec": vx_manager_spec,
                },
            )
            result["result"] = create_ret["result"]

            if result["result"]:
                result["comment"].append(f"Created 'vcf.sddc: {name}'")
                resource_id = create_ret["ret"]["resource_id"]
                # Safeguard for any future errors so that the resource_id is saved in the ESM
                result["new_state"] = dict(name=name, resource_id=resource_id)
            else:
                result["comment"].append(create_ret["comment"])

    if not result["result"]:
        # If there is any failure in create/update, it should reconcile.
        # The type of data is less important here to use default reconciliation
        # If there are no changes for 3 runs with rerun_data, then it will come out of execution
        result["rerun_data"] = dict(name=name, resource_id=resource_id)

    # Possible parameters: **{"resource_id": resource_id, "name": name, "ceip_enabled": ceip_enabled, "cluster_spec": cluster_spec, "dns_spec": dns_spec, "dv_switch_version": dv_switch_version, "dvs_specs": dvs_specs, "esx_license": esx_license, "excluded_components": excluded_components, "fips_enabled": fips_enabled, "host_specs": host_specs, "management_pool_name": management_pool_name, "network_specs": network_specs, "nsxt_spec": nsxt_spec, "ntp_servers": ntp_servers, "proxy_spec": proxy_spec, "psc_specs": psc_specs, "sddc_id": sddc_id, "sddc_manager_spec": sddc_manager_spec, "security_spec": security_spec, "skip_esx_thumbprint_validation": skip_esx_thumbprint_validation, "skip_gateway_ping_validation": skip_gateway_ping_validation, "task_name": task_name, "vcenter_spec": vcenter_spec, "vsan_spec": vsan_spec, "vx_manager_spec": vx_manager_spec}
    after = await hub.exec.vcf.sddc.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )
    result["new_state"] = after.ret
    return result


async def absent(
    hub,
    ctx,
) -> Dict[str, Any]:
    """


    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


            idem_test_vcf.sddc_is_absent:
              vcf.vcf.sddc.absent: []


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    if not resource_id:
        resource_id = (ctx.old_state or {}).get("resource_id")

    if not resource_id:
        result["comment"].append(f"'vcf.sddc: {name}' already absent")
        return result

    before = await hub.exec.vcf.sddc.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )

    if before["ret"]:
        if ctx.test:
            result["comment"] = f"Would delete vcf.sddc: {name}"
            return result

        delete_ret = await hub.exec.vcf.sddc.delete(
            ctx,
            name=name,
            resource_id=resource_id,
        )
        result["result"] = delete_ret["result"]

        if result["result"]:
            result["comment"].append(f"Deleted vcf.sddc: {name}")
        else:
            # If there is any failure in delete, it should reconcile.
            # The type of data is less important here to use default reconciliation
            # If there are no changes for 3 runs with rerun_data, then it will come out of execution
            result["rerun_data"] = resource_id
            result["comment"].append(delete_ret["result"])
    else:
        result["comment"].append(f"vcf.sddc: {name} already absent")
        return result

    result["old_state"] = before.ret
    return result


async def describe(hub, ctx) -> Dict[str, Dict[str, Any]]:
    """
    Describe the resource in a way that can be recreated/managed with the corresponding "present" function


    Retrieve all bringup tasks
        None


    Returns:
        Dict[str, Any]

    Example:

        .. code-block:: bash

            $ idem describe vcf.sddc
    """

    result = {}

    # TODO: Add other required parameters from: {}
    ret = await hub.exec.vcf.sddc.list(ctx)

    if not ret or not ret["result"]:
        hub.log.debug(f"Could not describe vcf.sddc {ret['comment']}")
        return result

    for resource in ret["ret"]:
        # TODO: Look for respective identifier in **
        resource_id = resource.get("resource_id")
        result[resource_id] = {
            "vcf.sddc.present": [
                {parameter_key: parameter_value}
                for parameter_key, parameter_value in resource.items()
            ]
        }
    return result

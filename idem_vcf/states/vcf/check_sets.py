"""States module for managing Check Setss. """
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List

import dict_tools.differ as differ

__contracts__ = ["resource"]


async def present(
    hub,
    ctx,
    name: str,
    query_id: str,
    resources: List[
        make_dataclass(
            "resources",
            [
                ("resource_id", str),
                ("resource_name", str),
                ("resource_type", str),
                (
                    "check_sets",
                    List[make_dataclass("check_sets", [("check_set_id", str)])],
                    field(default=None),
                ),
                (
                    "domain",
                    make_dataclass(
                        "domain",
                        [
                            ("domain_id", str, field(default=None)),
                            ("domain_name", str, field(default=None)),
                            ("domain_type", str, field(default=None)),
                        ],
                    ),
                    field(default=None),
                ),
            ],
        )
    ],
    resource_id: str = None,
) -> Dict[str, Any]:
    """
    Trigger a run of the selected checks
        Trigger a run of the selected checks

    Args:
        name(str):
            Idem name of the resource.

        query_id(str):
            Id of the query the selection was based on.

        resources(List[dict[str, Any]]):
            Information about the resource and its selection.

            * check_sets (List[dict[str, Any]], Optional):
                Information about the selected check-set candidates. Defaults to None.

                * check_set_id (str):
                    Id of the selected check-set.

            * domain (dict[str, Any], Optional):
                domain. Defaults to None.

                * domain_id (str, Optional):
                    Id of the domain. Defaults to None.

                * domain_name (str, Optional):
                    Name of the domain. Defaults to None.

                * domain_type (str, Optional):
                    Type of the domain. Defaults to None.

            * resource_id (str):
                Id of the resource.

            * resource_name (str):
                Name of the resource.

            * resource_type (str):
                Type of the resource.

        resource_id(str, Optional):
            Check_sets unique ID. Defaults to None.

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


          idem_test_vcf.check_sets_is_present:
              vcf.vcf.check_sets.present:
              - query_id: string
              - resources:
                - check_sets:
                  - check_set_id: string
                  domain:
                    domain_id: string
                    domain_name: string
                    domain_type: string
                  resource_id: string
                  resource_name: string
                  resource_type: string


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "kwargs", "result") and v is not None
    }

    if resource_id:
        # Possible parameters: **{"resource_id": resource_id, "name": name, "query_id": query_id, "resources": resources}
        before = await hub.exec.vcf.check_sets.get(
            ctx,
            name=name,
            resource_id=resource_id,
        )

        if not before["result"] or not before["ret"]:
            result["result"] = False
            result["comment"] = before["comment"]
            return result

        result["old_state"] = before.ret

        result["comment"].append(f"'vcf.check_sets: {name}' already exists")

        # If there are changes in desired state from existing state
        changes = differ.deep_diff(before.ret if before.ret else {}, desired_state)

        if bool(changes.get("new")):
            if ctx.test:
                result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                    enforced_state={}, desired_state=desired_state
                )
                result["comment"].append(f"Would update vcf.check_sets: {name}")
                return result
            else:
                # Update the resource
                update_ret = await hub.exec.vcf.check_sets.update(
                    ctx,
                    **{
                        "resource_id": resource_id,
                        "name": name,
                        "query_id": query_id,
                        "resources": resources,
                    },
                )
                result["result"] = update_ret["result"]

                if result["result"]:
                    result["comment"].append(f"Updated 'vcf.check_sets: {name}'")
                else:
                    result["comment"].append(update_ret["comment"])
    else:
        if ctx.test:
            result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                enforced_state={}, desired_state=desired_state
            )
            result["comment"] = (f"Would create vcf.check_sets: {name}",)
            return result
        else:
            create_ret = await hub.exec.vcf.check_sets.create(
                ctx,
                **{
                    "resource_id": resource_id,
                    "name": name,
                    "query_id": query_id,
                    "resources": resources,
                },
            )
            result["result"] = create_ret["result"]

            if result["result"]:
                result["comment"].append(f"Created 'vcf.check_sets: {name}'")
                resource_id = create_ret["ret"]["resource_id"]
                # Safeguard for any future errors so that the resource_id is saved in the ESM
                result["new_state"] = dict(name=name, resource_id=resource_id)
            else:
                result["comment"].append(create_ret["comment"])

    if not result["result"]:
        # If there is any failure in create/update, it should reconcile.
        # The type of data is less important here to use default reconciliation
        # If there are no changes for 3 runs with rerun_data, then it will come out of execution
        result["rerun_data"] = dict(name=name, resource_id=resource_id)

    # Possible parameters: **{"resource_id": resource_id, "name": name, "query_id": query_id, "resources": resources}
    after = await hub.exec.vcf.check_sets.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )
    result["new_state"] = after.ret
    return result


async def absent(
    hub,
    ctx,
) -> Dict[str, Any]:
    """


    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


            idem_test_vcf.check_sets_is_absent:
              vcf.vcf.check_sets.absent: []


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    if not resource_id:
        resource_id = (ctx.old_state or {}).get("resource_id")

    if not resource_id:
        result["comment"].append(f"'vcf.check_sets: {name}' already absent")
        return result

    before = await hub.exec.vcf.check_sets.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )

    if before["ret"]:
        if ctx.test:
            result["comment"] = f"Would delete vcf.check_sets: {name}"
            return result

        delete_ret = await hub.exec.vcf.check_sets.delete(
            ctx,
            name=name,
            resource_id=resource_id,
        )
        result["result"] = delete_ret["result"]

        if result["result"]:
            result["comment"].append(f"Deleted vcf.check_sets: {name}")
        else:
            # If there is any failure in delete, it should reconcile.
            # The type of data is less important here to use default reconciliation
            # If there are no changes for 3 runs with rerun_data, then it will come out of execution
            result["rerun_data"] = resource_id
            result["comment"].append(delete_ret["result"])
    else:
        result["comment"].append(f"vcf.check_sets: {name} already absent")
        return result

    result["old_state"] = before.ret
    return result


async def describe(hub, ctx) -> Dict[str, Dict[str, Any]]:
    """
    Describe the resource in a way that can be recreated/managed with the corresponding "present" function


    Get information about the last assessment run
        Get information about the last assessment run

    Args:
        domain_id(str, Optional):
            Id of the domain to filter tasks for. Defaults to None.

    Returns:
        Dict[str, Any]

    Example:

        .. code-block:: bash

            $ idem describe vcf.check_sets
    """

    result = {}

    # TODO: Add other required parameters from: {}
    ret = await hub.exec.vcf.check_sets.list(ctx)

    if not ret or not ret["result"]:
        hub.log.debug(f"Could not describe vcf.check_sets {ret['comment']}")
        return result

    for resource in ret["ret"]:
        # TODO: Look for respective identifier in **
        resource_id = resource.get("resource_id")
        result[resource_id] = {
            "vcf.check_sets.present": [
                {parameter_key: parameter_value}
                for parameter_key, parameter_value in resource.items()
            ]
        }
    return result

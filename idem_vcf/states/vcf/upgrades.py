"""States module for managing Upgradeses. """
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List

import dict_tools.differ as differ

__contracts__ = ["resource"]


async def present(
    hub,
    ctx,
    name: str,
    bundle_id: str,
    resource_type: str,
    resource_upgrade_specs: List[
        make_dataclass(
            "resource_upgrade_specs",
            [
                ("resource_id", str),
                (
                    "custom_iso_spec",
                    make_dataclass(
                        "custom_iso_spec",
                        [("id", str), ("host_ids", List[str], field(default=None))],
                    ),
                    field(default=None),
                ),
                ("enable_quickboot", bool, field(default=None)),
                ("evacuate_offline_vms", bool, field(default=None)),
                (
                    "esx_upgrade_options_spec",
                    make_dataclass(
                        "esx_upgrade_options_spec",
                        [
                            ("enable_quickboot", bool, field(default=None)),
                            ("evacuate_offline_vms", bool, field(default=None)),
                            ("disable_hac", bool, field(default=None)),
                            ("disable_dpm", bool, field(default=None)),
                            ("enforce_hcl_validation", bool, field(default=None)),
                            ("pre_remediation_power_action", str, field(default=None)),
                            (
                                "esx_upgrade_failure_action",
                                make_dataclass(
                                    "esx_upgrade_failure_action",
                                    [
                                        ("retry_delay", int, field(default=None)),
                                        ("retry_count", int, field(default=None)),
                                        ("action", str, field(default=None)),
                                    ],
                                ),
                                field(default=None),
                            ),
                        ],
                    ),
                    field(default=None),
                ),
                ("hosts_to_upgrade", List[str], field(default=None)),
                (
                    "personality_spec",
                    make_dataclass(
                        "personality_spec",
                        [
                            ("personality_id", str),
                            (
                                "hardware_support_specs",
                                List[
                                    make_dataclass(
                                        "hardware_support_specs",
                                        [
                                            ("name", str),
                                            (
                                                "package_spec",
                                                make_dataclass(
                                                    "package_spec",
                                                    [("name", str), ("version", str)],
                                                ),
                                            ),
                                        ],
                                    )
                                ],
                                field(default=None),
                            ),
                        ],
                    ),
                    field(default=None),
                ),
                ("scheduled_timestamp", str, field(default=None)),
                ("shutdown_vms", bool, field(default=None)),
                ("to_version", str, field(default=None)),
                ("upgrade_now", bool, field(default=None)),
            ],
        )
    ],
    resource_id: str = None,
    draft_mode: bool = None,
    nsxt_upgrade_user_input_specs: List[
        make_dataclass(
            "nsxt_upgrade_user_input_specs",
            [
                (
                    "nsxt_edge_cluster_upgrade_specs",
                    List[
                        make_dataclass(
                            "nsxt_edge_cluster_upgrade_specs",
                            [
                                ("edge_cluster_id", str),
                                ("edge_parallel_upgrade", bool, field(default=None)),
                            ],
                        )
                    ],
                    field(default=None),
                ),
                (
                    "nsxt_host_cluster_upgrade_specs",
                    List[
                        make_dataclass(
                            "nsxt_host_cluster_upgrade_specs",
                            [
                                ("host_cluster_id", str),
                                ("host_parallel_upgrade", bool, field(default=None)),
                                ("live_upgrade", bool, field(default=None)),
                            ],
                        )
                    ],
                    field(default=None),
                ),
                ("nsxt_id", str, field(default=None)),
                (
                    "nsxt_upgrade_options",
                    make_dataclass(
                        "nsxt_upgrade_options",
                        [
                            (
                                "is_edge_clusters_upgrade_parallel",
                                bool,
                                field(default=None),
                            ),
                            ("is_edge_only_upgrade", bool, field(default=None)),
                            (
                                "is_host_clusters_upgrade_parallel",
                                bool,
                                field(default=None),
                            ),
                        ],
                    ),
                    field(default=None),
                ),
            ],
        )
    ] = None,
    parallel_upgrade: bool = None,
    vcenter_upgrade_user_input_specs: List[
        make_dataclass(
            "vcenter_upgrade_user_input_specs",
            [
                (
                    "temporary_network",
                    make_dataclass(
                        "temporary_network",
                        [("gateway", str), ("ip_address", str), ("subnet_mask", str)],
                    ),
                )
            ],
        )
    ] = None,
) -> Dict[str, Any]:
    """
    Start an upgrade operation
        Schedule/Trigger Upgrade of a Resource. Ex: Resource can be DOMAIN, CLUSTER, UNMANAGED_HOST etc. Performing upgrades are supported on VMware Cloud Foundation 3.5 BOM resources and above.

    Args:
        name(str):
            Idem name of the resource.

        bundle_id(str):
            Bundle ID for Upgrade.

        resource_type(str):
            Resource Type for Upgrade.

        resource_upgrade_specs(List[dict[str, Any]]):
            Resource Upgrade Specifications.

            * custom_iso_spec (dict[str, Any], Optional):
                customISOSpec. Defaults to None.

                * host_ids (List[str], Optional):
                    List of host IDs. Defaults to None.

                * id (str):
                    Custom ISO Id for VUM Clusters Upgrade.

            * enable_quickboot (bool, Optional):
                [Deprecated] (Please use esxUpgradeOptionsSpec instead) Flag for requesting Quick Boot for ESXi upgrade. Defaults to None.

            * evacuate_offline_vms (bool, Optional):
                [Deprecated] (Please use esxUpgradeOptionsSpec instead) Flag for requesting Evacuation of Offline VMs for ESXi upgrade. Defaults to None.

            * esx_upgrade_options_spec (dict[str, Any], Optional):
                esxUpgradeOptionsSpec. Defaults to None.

                * enable_quickboot (bool, Optional):
                    Flag for requesting Quick Boot for ESXi upgrade. Defaults to None.

                * evacuate_offline_vms (bool, Optional):
                    Flag for requesting Evacuation of Offline VMs for ESXi upgrade. Defaults to None.

                * disable_hac (bool, Optional):
                    Flag for disabling HA admission control on the cluster for ESXi upgrade. This option is only applicable to vLCM based ESXi upgrade. Defaults to None.

                * disable_dpm (bool, Optional):
                    Flag for disabling DPM on the cluster for ESXi upgrade. This option is only applicable to vLCM based ESXi upgrade. Defaults to None.

                * enforce_hcl_validation (bool, Optional):
                    Flag for enforcing HCL validation for ESXi upgrade, when applicable, to prevent remediation if hardware compatibility issues are found. This option is only applicable to vLCM based ESXi upgrade. Defaults to None.

                * pre_remediation_power_action (str, Optional):
                    Flag for setting VM power state before entering maintenance mode during ESXi upgrade. This option is only applicable to vLCM based ESXi upgrade. Defaults to None.

                * esx_upgrade_failure_action (dict[str, Any], Optional):
                    esxUpgradeFailureAction. Defaults to None.

                    * retry_delay (int, Optional):
                        Time to wait before we retry the failed operation in seconds. If action is RETRY, the value should be between 300 and 3600 seconds. If action is FAIL, the value should not be set. Defaults to None.

                    * retry_count (int, Optional):
                        Number of times to retry the failed operation. If the action is RETRY, the value should be between 3 and 100. If the action is FAIL, the value should be 0. Defaults to None.

                    * action (str, Optional):
                        Action to be taken if entering maintenance mode fails on a host within the cluster ESXi upgrade. Defaults to None.

            * hosts_to_upgrade (List[str], Optional):
                Set of hostIDs to be upgraded for ESXi upgrade. Defaults to None.

            * personality_spec (dict[str, Any], Optional):
                personalitySpec. Defaults to None.

                * hardware_support_specs (List[dict[str, Any]], Optional):
                    Hardware Support Specifications for Firmware upgrade. Defaults to None.

                    * name (str):
                        Hardware Support Manager name.

                    * package_spec (dict[str, Any]):
                        packageSpec.

                        * name (str):
                            Package name.

                        * version (str):
                            Package version.

                * personality_id (str):
                    Personality ID for vLCM based Upgrade.

            * resource_id (str):
                Resource ID for Upgrade.

            * scheduled_timestamp (str, Optional):
                Upgrade Scheduled Time. Defaults to None.

            * shutdown_vms (bool, Optional):
                If Resource Type is UNASSIGNED_HOST, set flag for enabling shutting down VM's before Upgrade. Defaults to None.

            * to_version (str, Optional):
                If Resource Type is UNASSIGNED_HOST, set the target version for Upgrade. Defaults to None.

            * upgrade_now (bool, Optional):
                Flag for enabling Upgrade Now. If true, scheduledTimestamp is ignored. Defaults to None.

        resource_id(str, Optional):
            Upgrades unique ID. Defaults to None.

        draft_mode(bool, Optional):
            Boolean to represent upgrade will be created in DRAFT mode. This allows to run prechecks before user confirm/commit the upgrade. Defaults to None.

        nsxt_upgrade_user_input_specs(List[dict[str, Any]], Optional):
            Resource Upgrade Specifications for NSX upgrade. Defaults to None.

            * nsxt_edge_cluster_upgrade_specs (List[dict[str, Any]], Optional):
                List of edge clusters information if explicit selection is to be made. Defaults to None.

                * edge_cluster_id (str):
                    Resource ID of the edge transport node cluster.

                * edge_parallel_upgrade (bool, Optional):
                    disable/enable parallel upgrade of edges within the cluster. Defaults to None.

            * nsxt_host_cluster_upgrade_specs (List[dict[str, Any]], Optional):
                List of host clusters information if explicit selection is to be made. Defaults to None.

                * host_cluster_id (str):
                    Resource ID of the host transport node cluster.

                * host_parallel_upgrade (bool, Optional):
                    Flag for disabling/enabling parallel upgrade within the cluster. Defaults to None.

                * live_upgrade (bool, Optional):
                    Flag for disabling/enabling live upgrade of hosts in the transportnode clusters. Defaults to None.

            * nsxt_id (str, Optional):
                Identifier of the NSX instance. Defaults to None.

            * nsxt_upgrade_options (dict[str, Any], Optional):
                nsxtUpgradeOptions. Defaults to None.

                * is_edge_clusters_upgrade_parallel (bool, Optional):
                    Flag for disabling/enabling parallel upgrade of edge transportnode clusters. Defaults to None.

                * is_edge_only_upgrade (bool, Optional):
                    Flag for performing edge-only upgrade. Defaults to None.

                * is_host_clusters_upgrade_parallel (bool, Optional):
                    Flag for disabling/enabling parallel upgrade of host transportnode clusters. Defaults to None.

        parallel_upgrade(bool, Optional):
            Boolean to represent components will be upgraded in parallel on not. Defaults to None.

        vcenter_upgrade_user_input_specs(List[dict[str, Any]], Optional):
            User Input for vCenter upgrade. Defaults to None.

            * temporary_network (dict[str, Any]):
                temporaryNetwork.

                * gateway (str):
                    Gateway for vCenter Upgrade temporary network.

                * ip_address (str):
                    IP Address for vCenter Upgrade temporary network.

                * subnet_mask (str):
                    Subnet Mask for vCenter Upgrade temporary network.

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


          idem_test_vcf.upgrades_is_present:
              vcf.vcf.upgrades.present:
              - bundle_id: string
              - draft_mode: bool
              - nsxt_upgrade_user_input_specs:
                - nsxt_edge_cluster_upgrade_specs:
                  - edge_cluster_id: string
                    edge_parallel_upgrade: bool
                  nsxt_host_cluster_upgrade_specs:
                  - host_cluster_id: string
                    host_parallel_upgrade: bool
                    live_upgrade: bool
                  nsxt_id: string
                  nsxt_upgrade_options:
                    is_edge_clusters_upgrade_parallel: bool
                    is_edge_only_upgrade: bool
                    is_host_clusters_upgrade_parallel: bool
              - parallel_upgrade: bool
              - resource_type: string
              - resource_upgrade_specs:
                - custom_iso_spec:
                    host_ids:
                    - value
                    id_: string
                  enable_quickboot: bool
                  esx_upgrade_options_spec:
                    disable_dpm: bool
                    disable_hac: bool
                    enable_quickboot: bool
                    enforce_hcl_validation: bool
                    esx_upgrade_failure_action:
                      action: string
                      retry_count: int
                      retry_delay: int
                    evacuate_offline_vms: bool
                    pre_remediation_power_action: string
                  evacuate_offline_vms: bool
                  hosts_to_upgrade:
                  - value
                  personality_spec:
                    hardware_support_specs:
                    - name: string
                      package_spec:
                        name: string
                        version: string
                    personality_id: string
                  resource_id: string
                  scheduled_timestamp: string
                  shutdown_vms: bool
                  to_version: string
                  upgrade_now: bool
              - vcenter_upgrade_user_input_specs:
                - temporary_network:
                    gateway: string
                    ip_address: string
                    subnet_mask: string


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "kwargs", "result") and v is not None
    }

    if resource_id:
        # Possible parameters: **{"resource_id": resource_id, "name": name, "bundle_id": bundle_id, "draft_mode": draft_mode, "nsxt_upgrade_user_input_specs": nsxt_upgrade_user_input_specs, "parallel_upgrade": parallel_upgrade, "resource_type": resource_type, "resource_upgrade_specs": resource_upgrade_specs, "vcenter_upgrade_user_input_specs": vcenter_upgrade_user_input_specs}
        before = await hub.exec.vcf.upgrades.get(
            ctx,
            name=name,
            resource_id=resource_id,
        )

        if not before["result"] or not before["ret"]:
            result["result"] = False
            result["comment"] = before["comment"]
            return result

        result["old_state"] = before.ret

        result["comment"].append(f"'vcf.upgrades: {name}' already exists")

        # If there are changes in desired state from existing state
        changes = differ.deep_diff(before.ret if before.ret else {}, desired_state)

        if bool(changes.get("new")):
            if ctx.test:
                result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                    enforced_state={}, desired_state=desired_state
                )
                result["comment"].append(f"Would update vcf.upgrades: {name}")
                return result
            else:
                # Update the resource
                update_ret = await hub.exec.vcf.upgrades.update(
                    ctx,
                    **{
                        "resource_id": resource_id,
                        "name": name,
                        "bundle_id": bundle_id,
                        "draft_mode": draft_mode,
                        "nsxt_upgrade_user_input_specs": nsxt_upgrade_user_input_specs,
                        "parallel_upgrade": parallel_upgrade,
                        "resource_type": resource_type,
                        "resource_upgrade_specs": resource_upgrade_specs,
                        "vcenter_upgrade_user_input_specs": vcenter_upgrade_user_input_specs,
                    },
                )
                result["result"] = update_ret["result"]

                if result["result"]:
                    result["comment"].append(f"Updated 'vcf.upgrades: {name}'")
                else:
                    result["comment"].append(update_ret["comment"])
    else:
        if ctx.test:
            result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                enforced_state={}, desired_state=desired_state
            )
            result["comment"] = (f"Would create vcf.upgrades: {name}",)
            return result
        else:
            create_ret = await hub.exec.vcf.upgrades.create(
                ctx,
                **{
                    "resource_id": resource_id,
                    "name": name,
                    "bundle_id": bundle_id,
                    "draft_mode": draft_mode,
                    "nsxt_upgrade_user_input_specs": nsxt_upgrade_user_input_specs,
                    "parallel_upgrade": parallel_upgrade,
                    "resource_type": resource_type,
                    "resource_upgrade_specs": resource_upgrade_specs,
                    "vcenter_upgrade_user_input_specs": vcenter_upgrade_user_input_specs,
                },
            )
            result["result"] = create_ret["result"]

            if result["result"]:
                result["comment"].append(f"Created 'vcf.upgrades: {name}'")
                resource_id = create_ret["ret"]["resource_id"]
                # Safeguard for any future errors so that the resource_id is saved in the ESM
                result["new_state"] = dict(name=name, resource_id=resource_id)
            else:
                result["comment"].append(create_ret["comment"])

    if not result["result"]:
        # If there is any failure in create/update, it should reconcile.
        # The type of data is less important here to use default reconciliation
        # If there are no changes for 3 runs with rerun_data, then it will come out of execution
        result["rerun_data"] = dict(name=name, resource_id=resource_id)

    # Possible parameters: **{"resource_id": resource_id, "name": name, "bundle_id": bundle_id, "draft_mode": draft_mode, "nsxt_upgrade_user_input_specs": nsxt_upgrade_user_input_specs, "parallel_upgrade": parallel_upgrade, "resource_type": resource_type, "resource_upgrade_specs": resource_upgrade_specs, "vcenter_upgrade_user_input_specs": vcenter_upgrade_user_input_specs}
    after = await hub.exec.vcf.upgrades.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )
    result["new_state"] = after.ret
    return result


async def absent(
    hub,
    ctx,
) -> Dict[str, Any]:
    """


    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


            idem_test_vcf.upgrades_is_absent:
              vcf.vcf.upgrades.absent: []


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    if not resource_id:
        resource_id = (ctx.old_state or {}).get("resource_id")

    if not resource_id:
        result["comment"].append(f"'vcf.upgrades: {name}' already absent")
        return result

    before = await hub.exec.vcf.upgrades.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )

    if before["ret"]:
        if ctx.test:
            result["comment"] = f"Would delete vcf.upgrades: {name}"
            return result

        delete_ret = await hub.exec.vcf.upgrades.delete(
            ctx,
            name=name,
            resource_id=resource_id,
        )
        result["result"] = delete_ret["result"]

        if result["result"]:
            result["comment"].append(f"Deleted vcf.upgrades: {name}")
        else:
            # If there is any failure in delete, it should reconcile.
            # The type of data is less important here to use default reconciliation
            # If there are no changes for 3 runs with rerun_data, then it will come out of execution
            result["rerun_data"] = resource_id
            result["comment"].append(delete_ret["result"])
    else:
        result["comment"].append(f"vcf.upgrades: {name} already absent")
        return result

    result["old_state"] = before.ret
    return result


async def describe(hub, ctx) -> Dict[str, Dict[str, Any]]:
    """
    Describe the resource in a way that can be recreated/managed with the corresponding "present" function


    Retrieve a list of upgrades
        Retrieve a list of upgrades

    Args:
        status(str, Optional):
            Status of the upgrades you want to retrieve. Defaults to None.

        bundle_id(str, Optional):
            Bundle Id for the upgrade. Defaults to None.

    Returns:
        Dict[str, Any]

    Example:

        .. code-block:: bash

            $ idem describe vcf.upgrades
    """

    result = {}

    # TODO: Add other required parameters from: {}
    ret = await hub.exec.vcf.upgrades.list(ctx)

    if not ret or not ret["result"]:
        hub.log.debug(f"Could not describe vcf.upgrades {ret['comment']}")
        return result

    for resource in ret["ret"]:
        # TODO: Look for respective identifier in **
        resource_id = resource.get("resource_id")
        result[resource_id] = {
            "vcf.upgrades.present": [
                {parameter_key: parameter_value}
                for parameter_key, parameter_value in resource.items()
            ]
        }
    return result

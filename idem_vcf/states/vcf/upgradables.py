"""States module for managing Upgradableses. """
from typing import Any
from typing import Dict

import dict_tools.differ as differ

__contracts__ = ["resource"]


async def present(
    hub,
    ctx,
) -> Dict[str, Any]:
    """

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


          idem_test_vcf.upgradables_is_present:
              vcf.vcf.upgradables.present: []


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "kwargs", "result") and v is not None
    }

    if resource_id:
        # Possible parameters: **{}
        before = await hub.exec.vcf.upgradables.get(
            ctx,
            name=name,
            resource_id=resource_id,
        )

        if not before["result"] or not before["ret"]:
            result["result"] = False
            result["comment"] = before["comment"]
            return result

        result["old_state"] = before.ret

        result["comment"].append(f"'vcf.upgradables: {name}' already exists")

        # If there are changes in desired state from existing state
        changes = differ.deep_diff(before.ret if before.ret else {}, desired_state)

        if bool(changes.get("new")):
            if ctx.test:
                result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                    enforced_state={}, desired_state=desired_state
                )
                result["comment"].append(f"Would update vcf.upgradables: {name}")
                return result
            else:
                # Update the resource
                update_ret = await hub.exec.vcf.upgradables.update(ctx, **{})
                result["result"] = update_ret["result"]

                if result["result"]:
                    result["comment"].append(f"Updated 'vcf.upgradables: {name}'")
                else:
                    result["comment"].append(update_ret["comment"])
    else:
        if ctx.test:
            result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                enforced_state={}, desired_state=desired_state
            )
            result["comment"] = (f"Would create vcf.upgradables: {name}",)
            return result
        else:
            create_ret = await hub.exec.vcf.upgradables.create(ctx, **{})
            result["result"] = create_ret["result"]

            if result["result"]:
                result["comment"].append(f"Created 'vcf.upgradables: {name}'")
                resource_id = create_ret["ret"]["resource_id"]
                # Safeguard for any future errors so that the resource_id is saved in the ESM
                result["new_state"] = dict(name=name, resource_id=resource_id)
            else:
                result["comment"].append(create_ret["comment"])

    if not result["result"]:
        # If there is any failure in create/update, it should reconcile.
        # The type of data is less important here to use default reconciliation
        # If there are no changes for 3 runs with rerun_data, then it will come out of execution
        result["rerun_data"] = dict(name=name, resource_id=resource_id)

    # Possible parameters: **{}
    after = await hub.exec.vcf.upgradables.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )
    result["new_state"] = after.ret
    return result


async def absent(
    hub,
    ctx,
) -> Dict[str, Any]:
    """


    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


            idem_test_vcf.upgradables_is_absent:
              vcf.vcf.upgradables.absent: []


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    if not resource_id:
        resource_id = (ctx.old_state or {}).get("resource_id")

    if not resource_id:
        result["comment"].append(f"'vcf.upgradables: {name}' already absent")
        return result

    before = await hub.exec.vcf.upgradables.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )

    if before["ret"]:
        if ctx.test:
            result["comment"] = f"Would delete vcf.upgradables: {name}"
            return result

        delete_ret = await hub.exec.vcf.upgradables.delete(
            ctx,
            name=name,
            resource_id=resource_id,
        )
        result["result"] = delete_ret["result"]

        if result["result"]:
            result["comment"].append(f"Deleted vcf.upgradables: {name}")
        else:
            # If there is any failure in delete, it should reconcile.
            # The type of data is less important here to use default reconciliation
            # If there are no changes for 3 runs with rerun_data, then it will come out of execution
            result["rerun_data"] = resource_id
            result["comment"].append(delete_ret["result"])
    else:
        result["comment"].append(f"vcf.upgradables: {name} already absent")
        return result

    result["old_state"] = before.ret
    return result


async def describe(hub, ctx) -> Dict[str, Dict[str, Any]]:
    """
    Describe the resource in a way that can be recreated/managed with the corresponding "present" function


    Retrieve a list of all upgradable resources
        Fetches the list of Upgradables in the System. Only one Upgradable becomes AVAILABLE for Upgrade. The Upgradables provides information that can be use for Precheck API and also in the actual Upgrade API call.


    Returns:
        Dict[str, Any]

    Example:

        .. code-block:: bash

            $ idem describe vcf.upgradables
    """

    result = {}

    # TODO: Add other required parameters from: {}
    ret = await hub.exec.vcf.upgradables.list(ctx)

    if not ret or not ret["result"]:
        hub.log.debug(f"Could not describe vcf.upgradables {ret['comment']}")
        return result

    for resource in ret["ret"]:
        # TODO: Look for respective identifier in **
        resource_id = resource.get("resource_id")
        result[resource_id] = {
            "vcf.upgradables.present": [
                {parameter_key: parameter_value}
                for parameter_key, parameter_value in resource.items()
            ]
        }
    return result

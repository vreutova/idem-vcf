"""States module for managing Identity Providerss. """
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List

import dict_tools.differ as differ

__contracts__ = ["resource"]


async def present(
    hub,
    ctx,
    name: str,
    type_: str,
    resource_id: str = None,
    cert_chain: List[str] = None,
    fed_idp_spec: make_dataclass(
        "fed_idp_spec",
        [
            (
                "directory",
                make_dataclass(
                    "directory",
                    [
                        ("default_domain", str),
                        ("domains", List[str]),
                        ("name", str),
                        ("directory_id", str, field(default=None)),
                    ],
                ),
            ),
            ("name", str),
            (
                "oidc_spec",
                make_dataclass(
                    "oidc_spec",
                    [
                        ("client_id", str),
                        ("client_secret", str),
                        ("discovery_endpoint", str),
                    ],
                ),
            ),
            ("sync_client_token_ttl", int, field(default=None)),
        ],
    ) = None,
    ldap: make_dataclass(
        "ldap",
        [
            ("domain_name", str),
            ("password", str),
            (
                "source_details",
                make_dataclass(
                    "source_details",
                    [
                        ("groups_base_dn", str),
                        ("server_endpoints", List[str]),
                        ("users_base_dn", str),
                        ("cert_chain", List[str], field(default=None)),
                    ],
                ),
            ),
            ("type", str),
            ("username", str),
            ("domain_alias", str, field(default=None)),
        ],
    ) = None,
    oidc: make_dataclass(
        "oidc",
        [("client_id", str), ("client_secret", str), ("discovery_endpoint", str)],
    ) = None,
) -> Dict[str, Any]:
    """
    Add a new external identity provider
        Add a new external identity provider

    Args:
        name(str):
            Idem name of the resource.

        type_(str):
            The type of Identity Identity Provider.

        resource_id(str, Optional):
            Identity_providers unique ID. Defaults to None.

        cert_chain(List[str], Optional):
            The root certificate chain required to connect to the external server. Defaults to None.

        fed_idp_spec(dict[str, Any], Optional):
            fedIdpSpec. Defaults to None.

            * directory (dict[str, Any]):
                directory.

                * default_domain (str):
                    The trusted default domain of the directory.

                * directory_id (str, Optional):
                    The id of the directory. Defaults to None.

                * domains (List[str]):
                    The set of trusted domains of the directory.

                * name (str):
                    The user-friendly name for the directory.

            * name (str):
                The user-friendly name for the Identity Provider.

            * oidc_spec (dict[str, Any]):
                oidcSpec.

                * client_id (str):
                    Client identifier to connect to the provider.

                * client_secret (str):
                    The secret shared between the client and the provider.

                * discovery_endpoint (str):
                    Endpoint to retrieve the provider metadata.

            * sync_client_token_ttl (int, Optional):
                The lifetime in seconds of the sync client bear token, default to 3 days if not specified. Defaults to None.

        ldap(dict[str, Any], Optional):
            ldap. Defaults to None.

            * domain_alias (str, Optional):
                The optional alias to associate the domain name. Defaults to None.

            * domain_name (str):
                The name to associate with the created domain.

            * password (str):
                Password to connect to the ldap(s) server.

            * source_details (dict[str, Any]):
                sourceDetails.

                * cert_chain (List[str], Optional):
                    SSL certificate chain in base64 encoding. This field can be unset only, if all the active directory server endpoints use the LDAP (not LDAPS) protocol. Defaults to None.

                * groups_base_dn (str):
                    Base distinguished name for groups.

                * server_endpoints (List[str]):
                    Active directory server endpoints. At least one active directory server endpoint must be set.

                * users_base_dn (str):
                    Base distinguished name for users.

            * type (str):
                The type of the LDAP Server.

            * username (str):
                User name to connect to ldap(s) server.

        oidc(dict[str, Any], Optional):
            oidc. Defaults to None.

            * client_id (str):
                Client identifier to connect to the provider.

            * client_secret (str):
                The secret shared between the client and the provider.

            * discovery_endpoint (str):
                Endpoint to retrieve the provider metadata.

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


          idem_test_vcf.identity_providers_is_present:
              vcf.vcf.identity_providers.present:
              - cert_chain:
                - value
              - fed_idp_spec:
                  directory:
                    default_domain: string
                    directory_id: string
                    domains:
                    - value
                    name: string
                  name: string
                  oidc_spec:
                    client_id: string
                    client_secret: string
                    discovery_endpoint: string
                  sync_client_token_ttl: int
              - ldap:
                  domain_alias: string
                  domain_name: string
                  password: string
                  source_details:
                    cert_chain:
                    - value
                    groups_base_dn: string
                    server_endpoints:
                    - value
                    users_base_dn: string
                  type_: string
                  username: string
              - oidc:
                  client_id: string
                  client_secret: string
                  discovery_endpoint: string
              - type_: string


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "kwargs", "result") and v is not None
    }

    if resource_id:
        # Possible parameters: **{"resource_id": resource_id, "name": name, "cert_chain": cert_chain, "fed_idp_spec": fed_idp_spec, "ldap": ldap, "oidc": oidc, "type": type_}
        before = await hub.exec.vcf.identity_providers.get(
            ctx,
            name=name,
            resource_id=resource_id,
        )

        if not before["result"] or not before["ret"]:
            result["result"] = False
            result["comment"] = before["comment"]
            return result

        result["old_state"] = before.ret

        result["comment"].append(f"'vcf.identity_providers: {name}' already exists")

        # If there are changes in desired state from existing state
        changes = differ.deep_diff(before.ret if before.ret else {}, desired_state)

        if bool(changes.get("new")):
            if ctx.test:
                result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                    enforced_state={}, desired_state=desired_state
                )
                result["comment"].append(f"Would update vcf.identity_providers: {name}")
                return result
            else:
                # Update the resource
                update_ret = await hub.exec.vcf.identity_providers.update(
                    ctx,
                    **{
                        "resource_id": resource_id,
                        "name": name,
                        "cert_chain": cert_chain,
                        "fed_idp_spec": fed_idp_spec,
                        "ldap": ldap,
                        "oidc": oidc,
                        "type": type_,
                    },
                )
                result["result"] = update_ret["result"]

                if result["result"]:
                    result["comment"].append(
                        f"Updated 'vcf.identity_providers: {name}'"
                    )
                else:
                    result["comment"].append(update_ret["comment"])
    else:
        if ctx.test:
            result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                enforced_state={}, desired_state=desired_state
            )
            result["comment"] = (f"Would create vcf.identity_providers: {name}",)
            return result
        else:
            create_ret = await hub.exec.vcf.identity_providers.create(
                ctx,
                **{
                    "resource_id": resource_id,
                    "name": name,
                    "cert_chain": cert_chain,
                    "fed_idp_spec": fed_idp_spec,
                    "ldap": ldap,
                    "oidc": oidc,
                    "type": type_,
                },
            )
            result["result"] = create_ret["result"]

            if result["result"]:
                result["comment"].append(f"Created 'vcf.identity_providers: {name}'")
                resource_id = create_ret["ret"]["resource_id"]
                # Safeguard for any future errors so that the resource_id is saved in the ESM
                result["new_state"] = dict(name=name, resource_id=resource_id)
            else:
                result["comment"].append(create_ret["comment"])

    if not result["result"]:
        # If there is any failure in create/update, it should reconcile.
        # The type of data is less important here to use default reconciliation
        # If there are no changes for 3 runs with rerun_data, then it will come out of execution
        result["rerun_data"] = dict(name=name, resource_id=resource_id)

    # Possible parameters: **{"resource_id": resource_id, "name": name, "cert_chain": cert_chain, "fed_idp_spec": fed_idp_spec, "ldap": ldap, "oidc": oidc, "type": type_}
    after = await hub.exec.vcf.identity_providers.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )
    result["new_state"] = after.ret
    return result


async def absent(
    hub,
    ctx,
    name: str,
    resource_id: str = None,
    id_: str = None,
) -> Dict[str, Any]:
    """

    Remove an identity provider
        Delete an Identity Provider by its identifier, if it exists

    Args:
        name(str):
            Idem name of the resource.

        id_(str):
            ID of Identity Provider.

        resource_id(str, Optional):
            Identity_providers unique ID. Defaults to None.

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


            idem_test_vcf.identity_providers_is_absent:
              vcf.vcf.identity_providers.absent:
              - id_: string


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    if not resource_id:
        resource_id = (ctx.old_state or {}).get("resource_id")

    if not resource_id:
        result["comment"].append(f"'vcf.identity_providers: {name}' already absent")
        return result

    before = await hub.exec.vcf.identity_providers.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )

    if before["ret"]:
        if ctx.test:
            result["comment"] = f"Would delete vcf.identity_providers: {name}"
            return result

        delete_ret = await hub.exec.vcf.identity_providers.delete(
            ctx,
            name=name,
            resource_id=resource_id,
        )
        result["result"] = delete_ret["result"]

        if result["result"]:
            result["comment"].append(f"Deleted vcf.identity_providers: {name}")
        else:
            # If there is any failure in delete, it should reconcile.
            # The type of data is less important here to use default reconciliation
            # If there are no changes for 3 runs with rerun_data, then it will come out of execution
            result["rerun_data"] = resource_id
            result["comment"].append(delete_ret["result"])
    else:
        result["comment"].append(f"vcf.identity_providers: {name} already absent")
        return result

    result["old_state"] = before.ret
    return result


async def describe(hub, ctx) -> Dict[str, Dict[str, Any]]:
    """
    Describe the resource in a way that can be recreated/managed with the corresponding "present" function


    Get all identity providers
        Get a list of all identity providers


    Returns:
        Dict[str, Any]

    Example:

        .. code-block:: bash

            $ idem describe vcf.identity_providers
    """

    result = {}

    # TODO: Add other required parameters from: {}
    ret = await hub.exec.vcf.identity_providers.list(ctx)

    if not ret or not ret["result"]:
        hub.log.debug(f"Could not describe vcf.identity_providers {ret['comment']}")
        return result

    for resource in ret["ret"]:
        # TODO: Look for respective identifier in **
        resource_id = resource.get("resource_id")
        result[resource_id] = {
            "vcf.identity_providers.present": [
                {parameter_key: parameter_value}
                for parameter_key, parameter_value in resource.items()
            ]
        }
    return result

def __init__(hub):
    # This enables acct profiles that begin with "vcf" for states
    hub.states.vcf.ACCT = ["vcf"]

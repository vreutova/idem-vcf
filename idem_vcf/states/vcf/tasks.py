"""States module for managing Taskss. """
from typing import Any
from typing import Dict

import dict_tools.differ as differ

__contracts__ = ["resource"]


async def present(
    hub,
    ctx,
    name: str,
    id_: str,
    resource_id: str = None,
) -> Dict[str, Any]:
    """
    Retry a Task
        Retry a failed Task by ID, if it exists

    Args:
        name(str):
            Idem name of the resource.

        id_(str):
            Task id retry.

        resource_id(str, Optional):
            Tasks unique ID. Defaults to None.

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


          idem_test_vcf.tasks_is_present:
              vcf.vcf.tasks.present:
              - id_: string


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "kwargs", "result") and v is not None
    }

    if resource_id:
        # Possible parameters: **{"resource_id": resource_id, "name": name}
        before = await hub.exec.vcf.tasks.get(
            ctx,
            name=name,
            resource_id=resource_id,
        )

        if not before["result"] or not before["ret"]:
            result["result"] = False
            result["comment"] = before["comment"]
            return result

        result["old_state"] = before.ret

        result["comment"].append(f"'vcf.tasks: {name}' already exists")

        # If there are changes in desired state from existing state
        changes = differ.deep_diff(before.ret if before.ret else {}, desired_state)

        if bool(changes.get("new")):
            if ctx.test:
                result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                    enforced_state={}, desired_state=desired_state
                )
                result["comment"].append(f"Would update vcf.tasks: {name}")
                return result
            else:
                # Update the resource
                update_ret = await hub.exec.vcf.tasks.update(
                    ctx, **{"resource_id": resource_id, "name": name}
                )
                result["result"] = update_ret["result"]

                if result["result"]:
                    result["comment"].append(f"Updated 'vcf.tasks: {name}'")
                else:
                    result["comment"].append(update_ret["comment"])
    else:
        if ctx.test:
            result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                enforced_state={}, desired_state=desired_state
            )
            result["comment"] = (f"Would create vcf.tasks: {name}",)
            return result
        else:
            create_ret = await hub.exec.vcf.tasks.create(
                ctx, **{"resource_id": resource_id, "name": name}
            )
            result["result"] = create_ret["result"]

            if result["result"]:
                result["comment"].append(f"Created 'vcf.tasks: {name}'")
                resource_id = create_ret["ret"]["resource_id"]
                # Safeguard for any future errors so that the resource_id is saved in the ESM
                result["new_state"] = dict(name=name, resource_id=resource_id)
            else:
                result["comment"].append(create_ret["comment"])

    if not result["result"]:
        # If there is any failure in create/update, it should reconcile.
        # The type of data is less important here to use default reconciliation
        # If there are no changes for 3 runs with rerun_data, then it will come out of execution
        result["rerun_data"] = dict(name=name, resource_id=resource_id)

    # Possible parameters: **{"resource_id": resource_id, "name": name}
    after = await hub.exec.vcf.tasks.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )
    result["new_state"] = after.ret
    return result


async def absent(
    hub,
    ctx,
    name: str,
    resource_id: str = None,
    id_: str = None,
) -> Dict[str, Any]:
    """

    Cancel a Task
        Cancel a Task by ID, if it exists

    Args:
        name(str):
            Idem name of the resource.

        id_(str):
            Task id for cancelling.

        resource_id(str, Optional):
            Tasks unique ID. Defaults to None.

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


            idem_test_vcf.tasks_is_absent:
              vcf.vcf.tasks.absent:
              - id_: string


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    if not resource_id:
        resource_id = (ctx.old_state or {}).get("resource_id")

    if not resource_id:
        result["comment"].append(f"'vcf.tasks: {name}' already absent")
        return result

    before = await hub.exec.vcf.tasks.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )

    if before["ret"]:
        if ctx.test:
            result["comment"] = f"Would delete vcf.tasks: {name}"
            return result

        delete_ret = await hub.exec.vcf.tasks.delete(
            ctx,
            name=name,
            resource_id=resource_id,
        )
        result["result"] = delete_ret["result"]

        if result["result"]:
            result["comment"].append(f"Deleted vcf.tasks: {name}")
        else:
            # If there is any failure in delete, it should reconcile.
            # The type of data is less important here to use default reconciliation
            # If there are no changes for 3 runs with rerun_data, then it will come out of execution
            result["rerun_data"] = resource_id
            result["comment"].append(delete_ret["result"])
    else:
        result["comment"].append(f"vcf.tasks: {name} already absent")
        return result

    result["old_state"] = before.ret
    return result


async def describe(hub, ctx) -> Dict[str, Dict[str, Any]]:
    """
    Describe the resource in a way that can be recreated/managed with the corresponding "present" function


    Retrieve a list of all tasks
        Get the tasks

    Args:
        limit(int, Optional):
            The number of elements to be returned in the result. Defaults to None.

        task_status(str, Optional):
            taskStatus. Defaults to None.

        task_type(str, Optional):
            taskType. Defaults to None.

        resource_id(str, Optional):
            resourceId. Defaults to None.

        resource_type(str, Optional):
            resourceType. Defaults to None.

        completed_after(int, Optional):
            A time based filter to get tasks which are completed after the given timestamp. A task is completed if its status is 'Successsful' or 'Failed'. Time is in milliseconds. Defaults to None.

        page_number(int, Optional):
            Page number. Defaults to None.

        page_size(int, Optional):
            Size of the page you want to retrieve. Max page size allowed is 100. Defaults to None.

        order_direction(str, Optional):
            orderDirection. Defaults to None.

        order_by(str, Optional):
            orderBy. Defaults to None.

        task_name(str, Optional):
            Search filter when task name contains text. Defaults to None.

    Returns:
        Dict[str, Any]

    Example:

        .. code-block:: bash

            $ idem describe vcf.tasks
    """

    result = {}

    # TODO: Add other required parameters from: {}
    ret = await hub.exec.vcf.tasks.list(ctx)

    if not ret or not ret["result"]:
        hub.log.debug(f"Could not describe vcf.tasks {ret['comment']}")
        return result

    for resource in ret["ret"]:
        # TODO: Look for respective identifier in **
        resource_id = resource.get("resource_id")
        result[resource_id] = {
            "vcf.tasks.present": [
                {parameter_key: parameter_value}
                for parameter_key, parameter_value in resource.items()
            ]
        }
    return result

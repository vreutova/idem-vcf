"""States module for managing Network Poolss. """
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List

import dict_tools.differ as differ

__contracts__ = ["resource"]


async def present(
    hub,
    ctx,
    name: str,
    resource_id: str = None,
    id_: str = None,
    networks: List[
        make_dataclass(
            "networks",
            [
                ("free_ips", List[str], field(default=None)),
                ("gateway", str, field(default=None)),
                ("id", str, field(default=None)),
                (
                    "ip_pools",
                    List[
                        make_dataclass(
                            "ip_pools",
                            [
                                ("end", str, field(default=None)),
                                ("start", str, field(default=None)),
                            ],
                        )
                    ],
                    field(default=None),
                ),
                ("mask", str, field(default=None)),
                ("mtu", int, field(default=None)),
                ("subnet", str, field(default=None)),
                ("type", str, field(default=None)),
                ("used_ips", List[str], field(default=None)),
                ("vlan_id", int, field(default=None)),
            ],
        )
    ] = None,
) -> Dict[str, Any]:
    """
    Create a Network Pool
        Create a Network Pool

    Args:
        name(str):
            Idem name of the resource.

        resource_id(str, Optional):
            Network_pools unique ID. Defaults to None.

        id_(str, Optional):
            The ID of the network pool. Defaults to None.

        networks(List[dict[str, Any]], Optional):
            Representing the networks associated with the network pool. Defaults to None.

            * free_ips (List[str], Optional):
                List of free IPs to use. Defaults to None.

            * gateway (str, Optional):
                Gateway for the network. Defaults to None.

            * id (str, Optional):
                The ID of the network. Defaults to None.

            * ip_pools (List[dict[str, Any]], Optional):
                List of IP pool ranges to use. Defaults to None.

                * end (str, Optional):
                    End IP address of the IP pool. Defaults to None.

                * start (str, Optional):
                    Start IP address of the IP pool. Defaults to None.

            * mask (str, Optional):
                Subnet mask for the subnet of the network. Defaults to None.

            * mtu (int, Optional):
                MTU of the network. Defaults to None.

            * subnet (str, Optional):
                Subnet associated with the network. Defaults to None.

            * type (str, Optional):
                Network Type of the network. Defaults to None.

            * used_ips (List[str], Optional):
                List of used IPs. Defaults to None.

            * vlan_id (int, Optional):
                VLAN ID associated with the network. Defaults to None.

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


          idem_test_vcf.network_pools_is_present:
              vcf.vcf.network_pools.present:
              - id_: string
              - networks:
                - free_ips:
                  - value
                  gateway: string
                  id_: string
                  ip_pools:
                  - end: string
                    start: string
                  mask: string
                  mtu: int
                  subnet: string
                  type_: string
                  used_ips:
                  - value
                  vlan_id: int


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "kwargs", "result") and v is not None
    }

    if resource_id:
        # Possible parameters: **{"resource_id": resource_id, "name": name, "id": id_, "networks": networks}
        before = await hub.exec.vcf.network_pools.get(
            ctx,
            name=name,
            resource_id=resource_id,
        )

        if not before["result"] or not before["ret"]:
            result["result"] = False
            result["comment"] = before["comment"]
            return result

        result["old_state"] = before.ret

        result["comment"].append(f"'vcf.network_pools: {name}' already exists")

        # If there are changes in desired state from existing state
        changes = differ.deep_diff(before.ret if before.ret else {}, desired_state)

        if bool(changes.get("new")):
            if ctx.test:
                result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                    enforced_state={}, desired_state=desired_state
                )
                result["comment"].append(f"Would update vcf.network_pools: {name}")
                return result
            else:
                # Update the resource
                update_ret = await hub.exec.vcf.network_pools.update(
                    ctx,
                    **{
                        "resource_id": resource_id,
                        "name": name,
                        "id": id_,
                        "networks": networks,
                    },
                )
                result["result"] = update_ret["result"]

                if result["result"]:
                    result["comment"].append(f"Updated 'vcf.network_pools: {name}'")
                else:
                    result["comment"].append(update_ret["comment"])
    else:
        if ctx.test:
            result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                enforced_state={}, desired_state=desired_state
            )
            result["comment"] = (f"Would create vcf.network_pools: {name}",)
            return result
        else:
            create_ret = await hub.exec.vcf.network_pools.create(
                ctx,
                **{
                    "resource_id": resource_id,
                    "name": name,
                    "id": id_,
                    "networks": networks,
                },
            )
            result["result"] = create_ret["result"]

            if result["result"]:
                result["comment"].append(f"Created 'vcf.network_pools: {name}'")
                resource_id = create_ret["ret"]["resource_id"]
                # Safeguard for any future errors so that the resource_id is saved in the ESM
                result["new_state"] = dict(name=name, resource_id=resource_id)
            else:
                result["comment"].append(create_ret["comment"])

    if not result["result"]:
        # If there is any failure in create/update, it should reconcile.
        # The type of data is less important here to use default reconciliation
        # If there are no changes for 3 runs with rerun_data, then it will come out of execution
        result["rerun_data"] = dict(name=name, resource_id=resource_id)

    # Possible parameters: **{"resource_id": resource_id, "name": name, "id": id_, "networks": networks}
    after = await hub.exec.vcf.network_pools.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )
    result["new_state"] = after.ret
    return result


async def absent(
    hub,
    ctx,
    name: str,
    resource_id: str = None,
    id_: str = None,
) -> Dict[str, Any]:
    """

    Delete a Network Pool
        Delete the Network Pool by the ID, if it exists and is unused

    Args:
        name(str):
            Idem name of the resource.

        id_(str):
            ID of the network pool.

        resource_id(str, Optional):
            Network_pools unique ID. Defaults to None.

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


            idem_test_vcf.network_pools_is_absent:
              vcf.vcf.network_pools.absent:
              - id_: string


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    if not resource_id:
        resource_id = (ctx.old_state or {}).get("resource_id")

    if not resource_id:
        result["comment"].append(f"'vcf.network_pools: {name}' already absent")
        return result

    before = await hub.exec.vcf.network_pools.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )

    if before["ret"]:
        if ctx.test:
            result["comment"] = f"Would delete vcf.network_pools: {name}"
            return result

        delete_ret = await hub.exec.vcf.network_pools.delete(
            ctx,
            name=name,
            resource_id=resource_id,
        )
        result["result"] = delete_ret["result"]

        if result["result"]:
            result["comment"].append(f"Deleted vcf.network_pools: {name}")
        else:
            # If there is any failure in delete, it should reconcile.
            # The type of data is less important here to use default reconciliation
            # If there are no changes for 3 runs with rerun_data, then it will come out of execution
            result["rerun_data"] = resource_id
            result["comment"].append(delete_ret["result"])
    else:
        result["comment"].append(f"vcf.network_pools: {name} already absent")
        return result

    result["old_state"] = before.ret
    return result


async def describe(hub, ctx) -> Dict[str, Dict[str, Any]]:
    """
    Describe the resource in a way that can be recreated/managed with the corresponding "present" function


    Get the list of all Network Pools
        Get the Network Pools


    Returns:
        Dict[str, Any]

    Example:

        .. code-block:: bash

            $ idem describe vcf.network_pools
    """

    result = {}

    # TODO: Add other required parameters from: {}
    ret = await hub.exec.vcf.network_pools.list(ctx)

    if not ret or not ret["result"]:
        hub.log.debug(f"Could not describe vcf.network_pools {ret['comment']}")
        return result

    for resource in ret["ret"]:
        # TODO: Look for respective identifier in **
        resource_id = resource.get("resource_id")
        result[resource_id] = {
            "vcf.network_pools.present": [
                {parameter_key: parameter_value}
                for parameter_key, parameter_value in resource.items()
            ]
        }
    return result

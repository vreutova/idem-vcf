"""States module for managing Trusted Certificateses. """
from typing import Any
from typing import Dict

import dict_tools.differ as differ

__contracts__ = ["resource"]


async def present(
    hub,
    ctx,
    name: str,
    certificate: str,
    certificate_usage_type: str,
    resource_id: str = None,
) -> Dict[str, Any]:
    """
    Add a trusted certificate to the SDDC Manager
        Add a trusted certificate to the SDDC Manager

    Args:
        name(str):
            Idem name of the resource.

        certificate(str):
            Certificate in PEM format.

        certificate_usage_type(str):
            Certificate usage.

        resource_id(str, Optional):
            Trusted_certificates unique ID. Defaults to None.

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


          idem_test_vcf.trusted_certificates_is_present:
              vcf.vcf.trusted_certificates.present:
              - certificate: string
              - certificate_usage_type: string


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "kwargs", "result") and v is not None
    }

    if resource_id:
        # Possible parameters: **{"resource_id": resource_id, "name": name, "certificate": certificate, "certificate_usage_type": certificate_usage_type}
        before = await hub.exec.vcf.trusted_certificates.get(
            ctx,
            name=name,
            resource_id=resource_id,
        )

        if not before["result"] or not before["ret"]:
            result["result"] = False
            result["comment"] = before["comment"]
            return result

        result["old_state"] = before.ret

        result["comment"].append(f"'vcf.trusted_certificates: {name}' already exists")

        # If there are changes in desired state from existing state
        changes = differ.deep_diff(before.ret if before.ret else {}, desired_state)

        if bool(changes.get("new")):
            if ctx.test:
                result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                    enforced_state={}, desired_state=desired_state
                )
                result["comment"].append(
                    f"Would update vcf.trusted_certificates: {name}"
                )
                return result
            else:
                # Update the resource
                update_ret = await hub.exec.vcf.trusted_certificates.update(
                    ctx,
                    **{
                        "resource_id": resource_id,
                        "name": name,
                        "certificate": certificate,
                        "certificate_usage_type": certificate_usage_type,
                    },
                )
                result["result"] = update_ret["result"]

                if result["result"]:
                    result["comment"].append(
                        f"Updated 'vcf.trusted_certificates: {name}'"
                    )
                else:
                    result["comment"].append(update_ret["comment"])
    else:
        if ctx.test:
            result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                enforced_state={}, desired_state=desired_state
            )
            result["comment"] = (f"Would create vcf.trusted_certificates: {name}",)
            return result
        else:
            create_ret = await hub.exec.vcf.trusted_certificates.create(
                ctx,
                **{
                    "resource_id": resource_id,
                    "name": name,
                    "certificate": certificate,
                    "certificate_usage_type": certificate_usage_type,
                },
            )
            result["result"] = create_ret["result"]

            if result["result"]:
                result["comment"].append(f"Created 'vcf.trusted_certificates: {name}'")
                resource_id = create_ret["ret"]["resource_id"]
                # Safeguard for any future errors so that the resource_id is saved in the ESM
                result["new_state"] = dict(name=name, resource_id=resource_id)
            else:
                result["comment"].append(create_ret["comment"])

    if not result["result"]:
        # If there is any failure in create/update, it should reconcile.
        # The type of data is less important here to use default reconciliation
        # If there are no changes for 3 runs with rerun_data, then it will come out of execution
        result["rerun_data"] = dict(name=name, resource_id=resource_id)

    # Possible parameters: **{"resource_id": resource_id, "name": name, "certificate": certificate, "certificate_usage_type": certificate_usage_type}
    after = await hub.exec.vcf.trusted_certificates.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )
    result["new_state"] = after.ret
    return result


async def absent(
    hub,
    ctx,
    name: str,
    resource_id: str = None,
    alias: str = None,
) -> Dict[str, Any]:
    """

    Delete a trusted certificate from the SDDC Manager
        Delete a trusted certificate from the SDDC Manager. Restart the services to reflect the changes.

    Args:
        name(str):
            Idem name of the resource.

        alias(str):
            Certificate Alias.

        resource_id(str, Optional):
            Trusted_certificates unique ID. Defaults to None.

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


            idem_test_vcf.trusted_certificates_is_absent:
              vcf.vcf.trusted_certificates.absent:
              - alias: string


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    if not resource_id:
        resource_id = (ctx.old_state or {}).get("resource_id")

    if not resource_id:
        result["comment"].append(f"'vcf.trusted_certificates: {name}' already absent")
        return result

    before = await hub.exec.vcf.trusted_certificates.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )

    if before["ret"]:
        if ctx.test:
            result["comment"] = f"Would delete vcf.trusted_certificates: {name}"
            return result

        delete_ret = await hub.exec.vcf.trusted_certificates.delete(
            ctx,
            name=name,
            resource_id=resource_id,
        )
        result["result"] = delete_ret["result"]

        if result["result"]:
            result["comment"].append(f"Deleted vcf.trusted_certificates: {name}")
        else:
            # If there is any failure in delete, it should reconcile.
            # The type of data is less important here to use default reconciliation
            # If there are no changes for 3 runs with rerun_data, then it will come out of execution
            result["rerun_data"] = resource_id
            result["comment"].append(delete_ret["result"])
    else:
        result["comment"].append(f"vcf.trusted_certificates: {name} already absent")
        return result

    result["old_state"] = before.ret
    return result


async def describe(hub, ctx) -> Dict[str, Dict[str, Any]]:
    """
    Describe the resource in a way that can be recreated/managed with the corresponding "present" function


    Retrieve all trusted certificates from SDDC Manager
        Retrieve all trusted certificates from SDDC Manager


    Returns:
        Dict[str, Any]

    Example:

        .. code-block:: bash

            $ idem describe vcf.trusted_certificates
    """

    result = {}

    # TODO: Add other required parameters from: {}
    ret = await hub.exec.vcf.trusted_certificates.list(ctx)

    if not ret or not ret["result"]:
        hub.log.debug(f"Could not describe vcf.trusted_certificates {ret['comment']}")
        return result

    for resource in ret["ret"]:
        # TODO: Look for respective identifier in **
        resource_id = resource.get("resource_id")
        result[resource_id] = {
            "vcf.trusted_certificates.present": [
                {parameter_key: parameter_value}
                for parameter_key, parameter_value in resource.items()
            ]
        }
    return result

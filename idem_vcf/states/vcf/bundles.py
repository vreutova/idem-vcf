"""States module for managing Bundleses. """
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict

import dict_tools.differ as differ

__contracts__ = ["resource"]


async def present(
    hub,
    ctx,
    name: str,
    bundle_file_path: str,
    manifest_file_path: str,
    resource_id: str = None,
    compatibility_sets_file_path: str = None,
    partner_extension_spec: make_dataclass(
        "partner_extension_spec",
        [
            ("partner_bundle_metadata_file_path", str, field(default=None)),
            ("partner_bundle_version", str, field(default=None)),
        ],
    ) = None,
    signature_file_path: str = None,
) -> Dict[str, Any]:
    """
    Upload a bundle to SDDC Manager
        Upload Bundle to SDDC Manager. Used when you do not have internet connectivity for downloading bundles from VMWare/VxRail to SDDC Manager. The Bundles are manually downloaded from Depot using Bundle Transfer utility

    Args:
        name(str):
            Idem name of the resource.

        bundle_file_path(str):
            Bundle Upload File Path.

        manifest_file_path(str):
            Bundle Upload Manifest File Path.

        resource_id(str, Optional):
            Bundles unique ID. Defaults to None.

        compatibility_sets_file_path(str, Optional):
            [Deprecated] Path to the software compatibility sets file. Defaults to None.

        partner_extension_spec(dict[str, Any], Optional):
            partnerExtensionSpec. Defaults to None.

            * partner_bundle_metadata_file_path (str, Optional):
                Path to the bundle metadata file. The metadata file can have details of multiple bundles. Defaults to None.

            * partner_bundle_version (str, Optional):
                Version of partner bundle to be uploaded. Should match one of the bundle versions available in the partner bundle metadata file. Defaults to None.

        signature_file_path(str, Optional):
            Bundle Upload Signature File Path. Defaults to None.

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


          idem_test_vcf.bundles_is_present:
              vcf.vcf.bundles.present:
              - bundle_file_path: string
              - compatibility_sets_file_path: string
              - manifest_file_path: string
              - partner_extension_spec:
                  partner_bundle_metadata_file_path: string
                  partner_bundle_version: string
              - signature_file_path: string


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "kwargs", "result") and v is not None
    }

    if resource_id:
        # Possible parameters: **{"resource_id": resource_id, "name": name, "bundle_file_path": bundle_file_path, "compatibility_sets_file_path": compatibility_sets_file_path, "manifest_file_path": manifest_file_path, "partner_extension_spec": partner_extension_spec, "signature_file_path": signature_file_path}
        before = await hub.exec.vcf.bundles.get(
            ctx,
            name=name,
            resource_id=resource_id,
        )

        if not before["result"] or not before["ret"]:
            result["result"] = False
            result["comment"] = before["comment"]
            return result

        result["old_state"] = before.ret

        result["comment"].append(f"'vcf.bundles: {name}' already exists")

        # If there are changes in desired state from existing state
        changes = differ.deep_diff(before.ret if before.ret else {}, desired_state)

        if bool(changes.get("new")):
            if ctx.test:
                result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                    enforced_state={}, desired_state=desired_state
                )
                result["comment"].append(f"Would update vcf.bundles: {name}")
                return result
            else:
                # Update the resource
                update_ret = await hub.exec.vcf.bundles.update(
                    ctx,
                    **{
                        "resource_id": resource_id,
                        "name": name,
                        "bundle_file_path": bundle_file_path,
                        "compatibility_sets_file_path": compatibility_sets_file_path,
                        "manifest_file_path": manifest_file_path,
                        "partner_extension_spec": partner_extension_spec,
                        "signature_file_path": signature_file_path,
                    },
                )
                result["result"] = update_ret["result"]

                if result["result"]:
                    result["comment"].append(f"Updated 'vcf.bundles: {name}'")
                else:
                    result["comment"].append(update_ret["comment"])
    else:
        if ctx.test:
            result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                enforced_state={}, desired_state=desired_state
            )
            result["comment"] = (f"Would create vcf.bundles: {name}",)
            return result
        else:
            create_ret = await hub.exec.vcf.bundles.create(
                ctx,
                **{
                    "resource_id": resource_id,
                    "name": name,
                    "bundle_file_path": bundle_file_path,
                    "compatibility_sets_file_path": compatibility_sets_file_path,
                    "manifest_file_path": manifest_file_path,
                    "partner_extension_spec": partner_extension_spec,
                    "signature_file_path": signature_file_path,
                },
            )
            result["result"] = create_ret["result"]

            if result["result"]:
                result["comment"].append(f"Created 'vcf.bundles: {name}'")
                resource_id = create_ret["ret"]["resource_id"]
                # Safeguard for any future errors so that the resource_id is saved in the ESM
                result["new_state"] = dict(name=name, resource_id=resource_id)
            else:
                result["comment"].append(create_ret["comment"])

    if not result["result"]:
        # If there is any failure in create/update, it should reconcile.
        # The type of data is less important here to use default reconciliation
        # If there are no changes for 3 runs with rerun_data, then it will come out of execution
        result["rerun_data"] = dict(name=name, resource_id=resource_id)

    # Possible parameters: **{"resource_id": resource_id, "name": name, "bundle_file_path": bundle_file_path, "compatibility_sets_file_path": compatibility_sets_file_path, "manifest_file_path": manifest_file_path, "partner_extension_spec": partner_extension_spec, "signature_file_path": signature_file_path}
    after = await hub.exec.vcf.bundles.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )
    result["new_state"] = after.ret
    return result


async def absent(
    hub,
    ctx,
) -> Dict[str, Any]:
    """


    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


            idem_test_vcf.bundles_is_absent:
              vcf.vcf.bundles.absent: []


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    if not resource_id:
        resource_id = (ctx.old_state or {}).get("resource_id")

    if not resource_id:
        result["comment"].append(f"'vcf.bundles: {name}' already absent")
        return result

    before = await hub.exec.vcf.bundles.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )

    if before["ret"]:
        if ctx.test:
            result["comment"] = f"Would delete vcf.bundles: {name}"
            return result

        delete_ret = await hub.exec.vcf.bundles.delete(
            ctx,
            name=name,
            resource_id=resource_id,
        )
        result["result"] = delete_ret["result"]

        if result["result"]:
            result["comment"].append(f"Deleted vcf.bundles: {name}")
        else:
            # If there is any failure in delete, it should reconcile.
            # The type of data is less important here to use default reconciliation
            # If there are no changes for 3 runs with rerun_data, then it will come out of execution
            result["rerun_data"] = resource_id
            result["comment"].append(delete_ret["result"])
    else:
        result["comment"].append(f"vcf.bundles: {name} already absent")
        return result

    result["old_state"] = before.ret
    return result


async def describe(hub, ctx) -> Dict[str, Dict[str, Any]]:
    """
    Describe the resource in a way that can be recreated/managed with the corresponding "present" function


    Retrieve a list of bundles
        Get all Bundles i.e uploaded bundles and also bundles available via depot access.

    Args:
        product_type(str, Optional):
            The type of the product. Defaults to None.

        is_compliant(bool, Optional):
            Is compliant with the current VCF version. Defaults to None.

        bundle_type(str, Optional):
            The type of the bundle. Defaults to None.

    Returns:
        Dict[str, Any]

    Example:

        .. code-block:: bash

            $ idem describe vcf.bundles
    """

    result = {}

    # TODO: Add other required parameters from: {}
    ret = await hub.exec.vcf.bundles.list(ctx)

    if not ret or not ret["result"]:
        hub.log.debug(f"Could not describe vcf.bundles {ret['comment']}")
        return result

    for resource in ret["ret"]:
        # TODO: Look for respective identifier in **
        resource_id = resource.get("resource_id")
        result[resource_id] = {
            "vcf.bundles.present": [
                {parameter_key: parameter_value}
                for parameter_key, parameter_value in resource.items()
            ]
        }
    return result

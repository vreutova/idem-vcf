"""States module for managing Manifestss. """
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List

import dict_tools.differ as differ

__contracts__ = ["resource"]


async def present(
    hub,
    ctx,
    name: str,
    creation_time: str,
    published_date: str,
    releases: List[
        make_dataclass(
            "releases",
            [
                (
                    "bom",
                    List[
                        make_dataclass(
                            "bom",
                            [
                                ("name", str),
                                ("public_name", str),
                                ("version", str),
                                ("additional_metadata", str, field(default=None)),
                                ("release_url", str, field(default=None)),
                            ],
                        )
                    ],
                ),
                ("description", str),
                ("min_compatible_vcf_version", str),
                ("product", str),
                ("release_date", str),
                ("version", str),
                ("eol", str, field(default=None)),
                ("is_applicable", bool, field(default=None)),
                ("not_applicable_reason", str, field(default=None)),
                (
                    "patch_bundles",
                    List[
                        make_dataclass(
                            "patch_bundles",
                            [
                                ("bundle_elements", List[str]),
                                ("bundle_id", str),
                                ("bundle_type", str),
                                ("cumulative_from_vcf_version", str),
                            ],
                        )
                    ],
                    field(default=None),
                ),
                (
                    "sku",
                    List[
                        make_dataclass(
                            "sku",
                            [
                                (
                                    "bom",
                                    List[
                                        make_dataclass(
                                            "bom",
                                            [
                                                ("name", str),
                                                ("public_name", str),
                                                ("version", str),
                                                (
                                                    "additional_metadata",
                                                    str,
                                                    field(default=None),
                                                ),
                                                (
                                                    "release_url",
                                                    str,
                                                    field(default=None),
                                                ),
                                            ],
                                        )
                                    ],
                                ),
                                ("description", str, field(default=None)),
                                ("name", str, field(default=None)),
                                (
                                    "sku_specific_patch_bundles",
                                    List[
                                        make_dataclass(
                                            "sku_specific_patch_bundles",
                                            [
                                                ("bundle_type", str),
                                                ("bundle_version", str),
                                                ("version", str),
                                            ],
                                        )
                                    ],
                                    field(default=None),
                                ),
                            ],
                        )
                    ],
                    field(default=None),
                ),
                (
                    "updates",
                    List[
                        make_dataclass(
                            "updates",
                            [
                                ("base_product_version", str),
                                ("description", str),
                                ("id", str),
                                ("product_name", str),
                                ("release_date", str),
                                (
                                    "release_update_url",
                                    make_dataclass(
                                        "release_update_url",
                                        [
                                            ("authority", str, field(default=None)),
                                            ("content", Dict, field(default=None)),
                                            ("default_port", int, field(default=None)),
                                            (
                                                "deserialized_fields",
                                                {},
                                                field(default=None),
                                            ),
                                            ("file", str, field(default=None)),
                                            ("host", str, field(default=None)),
                                            ("path", str, field(default=None)),
                                            ("port", int, field(default=None)),
                                            ("protocol", str, field(default=None)),
                                            ("query", str, field(default=None)),
                                            ("ref", str, field(default=None)),
                                            (
                                                "serialized_hash_code",
                                                int,
                                                field(default=None),
                                            ),
                                            ("user_info", str, field(default=None)),
                                        ],
                                    ),
                                    field(default=None),
                                ),
                            ],
                        )
                    ],
                    field(default=None),
                ),
            ],
        )
    ],
    sequence_number: int,
    version: int,
    resource_id: str = None,
    async_patches: Dict = None,
    recalled_bundles: List[
        make_dataclass(
            "recalled_bundles",
            [
                ("recalled_bundle_ids", List[str]),
                ("replacement_bundle_status", str),
                ("replacement_bundle_ids", List[str], field(default=None)),
            ],
        )
    ] = None,
    vvs_mappings: Dict = None,
) -> Dict[str, Any]:
    """
    Save/Load manifest
        Save/Load manifest. Make sure manifest is a valid one. If manifest already exists, it gets overridden.

    Args:
        name(str):
            Idem name of the resource.

        creation_time(str):
            Creation time for the manifest e.g. 2020-06-08T02:20:15.844Z, in yyyy-MM-dd'T'HH:mm:ss[.SSS]XXX ISO 8601 format.

        published_date(str):
            Date of publish of the manifest e.g. 2020-06-08T02:20:15.844Z, in yyyy-MM-dd'T'HH:mm:ss[.SSS]XXX ISO 8601 format.

        releases(List[dict[str, Any]]):
            Releases of VCF in the ascending order of product version.

            * bom (List[dict[str, Any]]):
                Release bill of materials.

                * additional_metadata (str, Optional):
                    any additional metadata. Defaults to None.

                * name (str):
                    Name of the product. e.g ESX.

                * public_name (str):
                    Public name of the product, e.g VMware ESXi.

                * release_url (str, Optional):
                    URL for the release. Defaults to None.

                * version (str):
                    Version for the product, e.g 6.7.0-11675023.

            * description (str):
                Release description with all major features.

            * eol (str, Optional):
                Release eol information e.g. 2020-06-08T02:20:15.844Z in yyyy-MM-dd'T'HH:mm:ss[.SSS]XXX ISO 8601 format. Defaults to None.

            * is_applicable (bool, Optional):
                [Deprecated] Whether bundle is applicable or not. Defaults to None.

            * min_compatible_vcf_version (str):
                Minimum compatible VCF version, used to represent compatibility of SDDC Manager and VMware BOM components.

            * not_applicable_reason (str, Optional):
                [Deprecated] Incompatibility reason for not applicable. Defaults to None.

            * patch_bundles (List[dict[str, Any]], Optional):
                List of patch bundles in this release. Defaults to None.

                * bundle_elements (List[str]):
                    Bundle elements of the patch bundle.

                * bundle_id (str):
                    Bundle ID of the patch bundle.

                * bundle_type (str):
                    Bundle type of the patch bundle.

                * cumulative_from_vcf_version (str):
                    Minimum VCF version that this patch bundle can be directly applied on.

            * product (str):
                Name of the product e.g. "VCF".

            * release_date (str):
                Release date e.g. 2020-06-08T02:20:15.844Z in yyyy-MM-dd'T'HH:mm:ss[.SSS]XXX ISO 8601 format.

            * sku (List[dict[str, Any]], Optional):
                Release sku specific patch and bill of materials. Defaults to None.

                * bom (List[dict[str, Any]]):
                    Sku specific bill of materials.

                    * additional_metadata (str, Optional):
                        any additional metadata. Defaults to None.

                    * name (str):
                        Name of the product. e.g ESX.

                    * public_name (str):
                        Public name of the product, e.g VMware ESXi.

                    * release_url (str, Optional):
                        URL for the release. Defaults to None.

                    * version (str):
                        Version for the product, e.g 6.7.0-11675023.

                * description (str, Optional):
                    Description to be shown in release page. Defaults to None.

                * name (str, Optional):
                    SKU name. Defaults to None.

                * sku_specific_patch_bundles (List[dict[str, Any]], Optional):
                    List of patch bundles in this release. Defaults to None.

                    * bundle_type (str):
                        Bundle type of the patch bundle.

                    * bundle_version (str):
                        Bundle Version of the product.

                    * version (str):
                        Product version.

            * updates (List[dict[str, Any]], Optional):
                Collection of release updates. Defaults to None.

                * base_product_version (str):
                    Base product version for the release.

                * description (str):
                    Description of the release update.

                * id (str):
                    Release version.

                * product_name (str):
                    Product name for which the release update is provided.

                * release_date (str):
                    Release date e.g. 2020-06-08T02:20:15.844Z in yyyy-MM-dd'T'HH:mm:ss[.SSS]XXX ISO 8601 format.

                * release_update_url (dict[str, Any], Optional):
                    releaseUpdateURL. Defaults to None.

                    * authority (str, Optional):
                        authority. Defaults to None.

                    * content (Dict, Optional):
                        content. Defaults to None.

                    * default_port (int, Optional):
                        defaultPort. Defaults to None.

                    * deserialized_fields ({}, Optional):
                        deserializedFields. Defaults to None.

                    * file (str, Optional):
                        file. Defaults to None.

                    * host (str, Optional):
                        host. Defaults to None.

                    * path (str, Optional):
                        path. Defaults to None.

                    * port (int, Optional):
                        port. Defaults to None.

                    * protocol (str, Optional):
                        protocol. Defaults to None.

                    * query (str, Optional):
                        query. Defaults to None.

                    * ref (str, Optional):
                        ref. Defaults to None.

                    * serialized_hash_code (int, Optional):
                        serializedHashCode. Defaults to None.

                    * user_info (str, Optional):
                        userInfo. Defaults to None.

            * version (str):
                Version of the release.

        sequence_number(int):
            Manifest sequence number which signifies an update in manifest.

        version(int):
            Manifest version supported by VCF.

        resource_id(str, Optional):
            Manifests unique ID. Defaults to None.

        async_patches(Dict, Optional):
            Async patches used by async patch tool. Defaults to None.

        recalled_bundles(List[dict[str, Any]], Optional):
            Collection of bundles that are recalled and their replacements, if applicable. Defaults to None.

            * recalled_bundle_ids (List[str]):
                List of Bundle IDs that are recalled.

            * replacement_bundle_ids (List[str], Optional):
                List of Bundle IDs that act as replacement. Defaults to None.

            * replacement_bundle_status (str):
                Status of bundle replacement.

        vvs_mappings(Dict, Optional):
            VVS product release Id to VCF version mapping. Defaults to None.

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


          idem_test_vcf.manifests_is_present:
              vcf.vcf.manifests.present:
              - async_patches: Dict
              - creation_time: string
              - published_date: string
              - recalled_bundles:
                - recalled_bundle_ids:
                  - value
                  replacement_bundle_ids:
                  - value
                  replacement_bundle_status: string
              - releases:
                - bom:
                  - additional_metadata: string
                    name: string
                    public_name: string
                    release_url: string
                    version: string
                  description: string
                  eol: string
                  is_applicable: bool
                  min_compatible_vcf_version: string
                  not_applicable_reason: string
                  patch_bundles:
                  - bundle_elements:
                    - value
                    bundle_id: string
                    bundle_type: string
                    cumulative_from_vcf_version: string
                  product: string
                  release_date: string
                  sku:
                  - bom:
                    - additional_metadata: string
                      name: string
                      public_name: string
                      release_url: string
                      version: string
                    description: string
                    name: string
                    sku_specific_patch_bundles:
                    - bundle_type: string
                      bundle_version: string
                      version: string
                  updates:
                  - base_product_version: string
                    description: string
                    id_: string
                    product_name: string
                    release_date: string
                    release_update_url:
                      authority: string
                      content: Dict
                      default_port: int
                      deserialized_fields: '{}'
                      file: string
                      host: string
                      path: string
                      port: int
                      protocol: string
                      query: string
                      ref: string
                      serialized_hash_code: int
                      user_info: string
                  version: string
              - sequence_number: int
              - version: int
              - vvs_mappings: Dict


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "kwargs", "result") and v is not None
    }

    if resource_id:
        # Possible parameters: **{"resource_id": resource_id, "name": name, "async_patches": async_patches, "creation_time": creation_time, "published_date": published_date, "recalled_bundles": recalled_bundles, "releases": releases, "sequence_number": sequence_number, "version": version, "vvs_mappings": vvs_mappings}
        before = await hub.exec.vcf.manifests.get(
            ctx,
            name=name,
            resource_id=resource_id,
        )

        if not before["result"] or not before["ret"]:
            result["result"] = False
            result["comment"] = before["comment"]
            return result

        result["old_state"] = before.ret

        result["comment"].append(f"'vcf.manifests: {name}' already exists")

        # If there are changes in desired state from existing state
        changes = differ.deep_diff(before.ret if before.ret else {}, desired_state)

        if bool(changes.get("new")):
            if ctx.test:
                result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                    enforced_state={}, desired_state=desired_state
                )
                result["comment"].append(f"Would update vcf.manifests: {name}")
                return result
            else:
                # Update the resource
                update_ret = await hub.exec.vcf.manifests.update(
                    ctx,
                    **{
                        "resource_id": resource_id,
                        "name": name,
                        "async_patches": async_patches,
                        "creation_time": creation_time,
                        "published_date": published_date,
                        "recalled_bundles": recalled_bundles,
                        "releases": releases,
                        "sequence_number": sequence_number,
                        "version": version,
                        "vvs_mappings": vvs_mappings,
                    },
                )
                result["result"] = update_ret["result"]

                if result["result"]:
                    result["comment"].append(f"Updated 'vcf.manifests: {name}'")
                else:
                    result["comment"].append(update_ret["comment"])
    else:
        if ctx.test:
            result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                enforced_state={}, desired_state=desired_state
            )
            result["comment"] = (f"Would create vcf.manifests: {name}",)
            return result
        else:
            create_ret = await hub.exec.vcf.manifests.create(
                ctx,
                **{
                    "resource_id": resource_id,
                    "name": name,
                    "async_patches": async_patches,
                    "creation_time": creation_time,
                    "published_date": published_date,
                    "recalled_bundles": recalled_bundles,
                    "releases": releases,
                    "sequence_number": sequence_number,
                    "version": version,
                    "vvs_mappings": vvs_mappings,
                },
            )
            result["result"] = create_ret["result"]

            if result["result"]:
                result["comment"].append(f"Created 'vcf.manifests: {name}'")
                resource_id = create_ret["ret"]["resource_id"]
                # Safeguard for any future errors so that the resource_id is saved in the ESM
                result["new_state"] = dict(name=name, resource_id=resource_id)
            else:
                result["comment"].append(create_ret["comment"])

    if not result["result"]:
        # If there is any failure in create/update, it should reconcile.
        # The type of data is less important here to use default reconciliation
        # If there are no changes for 3 runs with rerun_data, then it will come out of execution
        result["rerun_data"] = dict(name=name, resource_id=resource_id)

    # Possible parameters: **{"resource_id": resource_id, "name": name, "async_patches": async_patches, "creation_time": creation_time, "published_date": published_date, "recalled_bundles": recalled_bundles, "releases": releases, "sequence_number": sequence_number, "version": version, "vvs_mappings": vvs_mappings}
    after = await hub.exec.vcf.manifests.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )
    result["new_state"] = after.ret
    return result


async def absent(
    hub,
    ctx,
) -> Dict[str, Any]:
    """


    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


            idem_test_vcf.manifests_is_absent:
              vcf.vcf.manifests.absent: []


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    if not resource_id:
        resource_id = (ctx.old_state or {}).get("resource_id")

    if not resource_id:
        result["comment"].append(f"'vcf.manifests: {name}' already absent")
        return result

    before = await hub.exec.vcf.manifests.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )

    if before["ret"]:
        if ctx.test:
            result["comment"] = f"Would delete vcf.manifests: {name}"
            return result

        delete_ret = await hub.exec.vcf.manifests.delete(
            ctx,
            name=name,
            resource_id=resource_id,
        )
        result["result"] = delete_ret["result"]

        if result["result"]:
            result["comment"].append(f"Deleted vcf.manifests: {name}")
        else:
            # If there is any failure in delete, it should reconcile.
            # The type of data is less important here to use default reconciliation
            # If there are no changes for 3 runs with rerun_data, then it will come out of execution
            result["rerun_data"] = resource_id
            result["comment"].append(delete_ret["result"])
    else:
        result["comment"].append(f"vcf.manifests: {name} already absent")
        return result

    result["old_state"] = before.ret
    return result


async def describe(hub, ctx) -> Dict[str, Dict[str, Any]]:
    """
    Describe the resource in a way that can be recreated/managed with the corresponding "present" function


    Get manifest
        Get manifest. There should be only one valid manifest in the System.


    Returns:
        Dict[str, Any]

    Example:

        .. code-block:: bash

            $ idem describe vcf.manifests
    """

    result = {}

    # TODO: Add other required parameters from: {}
    ret = await hub.exec.vcf.manifests.list(ctx)

    if not ret or not ret["result"]:
        hub.log.debug(f"Could not describe vcf.manifests {ret['comment']}")
        return result

    for resource in ret["ret"]:
        # TODO: Look for respective identifier in **
        resource_id = resource.get("resource_id")
        result[resource_id] = {
            "vcf.manifests.present": [
                {parameter_key: parameter_value}
                for parameter_key, parameter_value in resource.items()
            ]
        }
    return result

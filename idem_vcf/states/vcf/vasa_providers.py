"""States module for managing Vasa Providerss. """
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List

import dict_tools.differ as differ

__contracts__ = ["resource"]


async def present(
    hub,
    ctx,
    name: str,
    storage_containers: List[
        make_dataclass(
            "storage_containers",
            [
                ("name", str),
                ("protocol_type", str),
                ("cluster_id", str, field(default=None)),
                ("id", str, field(default=None)),
            ],
        )
    ],
    url: str,
    users: List[
        make_dataclass(
            "users",
            [("password", str), ("username", str), ("id", str, field(default=None))],
        )
    ],
    resource_id: str = None,
    id_: str = None,
) -> Dict[str, Any]:
    """
    Add a VASA Provider
        None

    Args:
        name(str):
            Idem name of the resource.

        storage_containers(List[dict[str, Any]]):
            List of storage containers associated with the VASA Provider.

            * cluster_id (str, Optional):
                ID of the cluster which is using the storage container. Defaults to None.

            * id (str, Optional):
                ID of the storage container. Defaults to None.

            * name (str):
                Name of the storage container.

            * protocol_type (str):
                Storage protocol type.

        url(str):
            URL of the VASA Provider.

        users(List[dict[str, Any]]):
            List of users associated with the VASA Provider.

            * id (str, Optional):
                ID of the VASA User. Defaults to None.

            * password (str):
                Password.

            * username (str):
                VASA User name.

        resource_id(str, Optional):
            Vasa_providers unique ID. Defaults to None.

        id_(str, Optional):
            ID of the VASA Provider. Defaults to None.

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


          idem_test_vcf.vasa_providers_is_present:
              vcf.vcf.vasa_providers.present:
              - id_: string
              - storage_containers:
                - cluster_id: string
                  id_: string
                  name: string
                  protocol_type: string
              - url: string
              - users:
                - id_: string
                  password: string
                  username: string


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "kwargs", "result") and v is not None
    }

    if resource_id:
        # Possible parameters: **{"resource_id": resource_id, "name": name, "id": id_, "storage_containers": storage_containers, "url": url, "users": users}
        before = await hub.exec.vcf.vasa_providers.get(
            ctx,
            name=name,
            resource_id=resource_id,
        )

        if not before["result"] or not before["ret"]:
            result["result"] = False
            result["comment"] = before["comment"]
            return result

        result["old_state"] = before.ret

        result["comment"].append(f"'vcf.vasa_providers: {name}' already exists")

        # If there are changes in desired state from existing state
        changes = differ.deep_diff(before.ret if before.ret else {}, desired_state)

        if bool(changes.get("new")):
            if ctx.test:
                result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                    enforced_state={}, desired_state=desired_state
                )
                result["comment"].append(f"Would update vcf.vasa_providers: {name}")
                return result
            else:
                # Update the resource
                update_ret = await hub.exec.vcf.vasa_providers.update(
                    ctx,
                    **{
                        "resource_id": resource_id,
                        "name": name,
                        "id": id_,
                        "storage_containers": storage_containers,
                        "url": url,
                        "users": users,
                    },
                )
                result["result"] = update_ret["result"]

                if result["result"]:
                    result["comment"].append(f"Updated 'vcf.vasa_providers: {name}'")
                else:
                    result["comment"].append(update_ret["comment"])
    else:
        if ctx.test:
            result["new_state"] = hub.tool.vcf.test_state_utils.generate_test_state(
                enforced_state={}, desired_state=desired_state
            )
            result["comment"] = (f"Would create vcf.vasa_providers: {name}",)
            return result
        else:
            create_ret = await hub.exec.vcf.vasa_providers.create(
                ctx,
                **{
                    "resource_id": resource_id,
                    "name": name,
                    "id": id_,
                    "storage_containers": storage_containers,
                    "url": url,
                    "users": users,
                },
            )
            result["result"] = create_ret["result"]

            if result["result"]:
                result["comment"].append(f"Created 'vcf.vasa_providers: {name}'")
                resource_id = create_ret["ret"]["resource_id"]
                # Safeguard for any future errors so that the resource_id is saved in the ESM
                result["new_state"] = dict(name=name, resource_id=resource_id)
            else:
                result["comment"].append(create_ret["comment"])

    if not result["result"]:
        # If there is any failure in create/update, it should reconcile.
        # The type of data is less important here to use default reconciliation
        # If there are no changes for 3 runs with rerun_data, then it will come out of execution
        result["rerun_data"] = dict(name=name, resource_id=resource_id)

    # Possible parameters: **{"resource_id": resource_id, "name": name, "id": id_, "storage_containers": storage_containers, "url": url, "users": users}
    after = await hub.exec.vcf.vasa_providers.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )
    result["new_state"] = after.ret
    return result


async def absent(
    hub,
    ctx,
    name: str,
    resource_id: str = None,
    id_: str = None,
) -> Dict[str, Any]:
    """

    Delete a VASA Provider
        None

    Args:
        name(str):
            Idem name of the resource.

        id_(str):
            VASA Provider ID.

        resource_id(str, Optional):
            Vasa_providers unique ID. Defaults to None.

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


            idem_test_vcf.vasa_providers_is_absent:
              vcf.vcf.vasa_providers.absent:
              - id_: string


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    if not resource_id:
        resource_id = (ctx.old_state or {}).get("resource_id")

    if not resource_id:
        result["comment"].append(f"'vcf.vasa_providers: {name}' already absent")
        return result

    before = await hub.exec.vcf.vasa_providers.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )

    if before["ret"]:
        if ctx.test:
            result["comment"] = f"Would delete vcf.vasa_providers: {name}"
            return result

        delete_ret = await hub.exec.vcf.vasa_providers.delete(
            ctx,
            name=name,
            resource_id=resource_id,
        )
        result["result"] = delete_ret["result"]

        if result["result"]:
            result["comment"].append(f"Deleted vcf.vasa_providers: {name}")
        else:
            # If there is any failure in delete, it should reconcile.
            # The type of data is less important here to use default reconciliation
            # If there are no changes for 3 runs with rerun_data, then it will come out of execution
            result["rerun_data"] = resource_id
            result["comment"].append(delete_ret["result"])
    else:
        result["comment"].append(f"vcf.vasa_providers: {name} already absent")
        return result

    result["old_state"] = before.ret
    return result


async def describe(hub, ctx) -> Dict[str, Dict[str, Any]]:
    """
    Describe the resource in a way that can be recreated/managed with the corresponding "present" function


    Get the VASA Providers
        None


    Returns:
        Dict[str, Any]

    Example:

        .. code-block:: bash

            $ idem describe vcf.vasa_providers
    """

    result = {}

    # TODO: Add other required parameters from: {}
    ret = await hub.exec.vcf.vasa_providers.list(ctx)

    if not ret or not ret["result"]:
        hub.log.debug(f"Could not describe vcf.vasa_providers {ret['comment']}")
        return result

    for resource in ret["ret"]:
        # TODO: Look for respective identifier in **
        resource_id = resource.get("resource_id")
        result[resource_id] = {
            "vcf.vasa_providers.present": [
                {parameter_key: parameter_value}
                for parameter_key, parameter_value in resource.items()
            ]
        }
    return result

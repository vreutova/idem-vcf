from typing import Any
from typing import Dict

from dict_tools.data import NamespaceDict

DEFAULT_ENDPOINT_URL = "https://sfo-vcf01.rainpole.io/"


async def gather(hub, profiles) -> Dict[str, Any]:
    """
    Generate token with basic auth

    Example:
    .. code-block:: yaml

        vcf:
          profile_name:
            username: my_user
            password: my_token
            endpoint_url: https://vcf.com
    """
    sub_profiles = {}
    for (
        profile,
        ctx,
    ) in profiles.get("vcf", {}).items():
        endpoint_url = ctx.get("endpoint_url")

        temp_ctx = NamespaceDict(acct={"endpoint_url": endpoint_url})

        ret = await hub.tool.vcf.session.request(
            temp_ctx,
            method="post",
            path="/v1/tokens",
            data={
                "username": f"{ctx.get('username')}",
                "password": f"{ctx.get('password')}",
            },
        )

        if not ret["result"]:
            error = f"Unable to authenticate: {ret.get('comment', '')}"
            hub.log.error(error)
            raise ConnectionError(error)

        access_token = ret["ret"]["accessToken"]
        sub_profiles[profile] = dict(
            endpoint_url=endpoint_url,
            headers={"Authorization": f"Bearer {access_token}"},
        )
    return sub_profiles

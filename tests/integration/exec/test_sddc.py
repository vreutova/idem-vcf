"""Tests for validating Sddcs. """
import pytest


PARAMETER = {"name": "idem-test-resource- + TODO: Add unique identifier generator"}


@pytest.mark.asyncio
async def test_create(hub, ctx):
    r"""
    **Test function**
    """

    # Test - create new resource
    global PARAMETER
    # TODO: replace call param values as necessary
    ret = await hub.exec.vcf.sddc.create(
        ctx,
        name=PARAMETER["name"],
        ceip_enabled=value,
        cluster_spec=value,
        dns_spec=value,
        dv_switch_version=value,
        dvs_specs=value,
        esx_license=value,
        excluded_components=value,
        fips_enabled=value,
        host_specs=value,
        management_pool_name=value,
        network_specs=value,
        nsxt_spec=value,
        ntp_servers=value,
        proxy_spec=value,
        psc_specs=value,
        sddc_id=value,
        sddc_manager_spec=value,
        security_spec=value,
        skip_esx_thumbprint_validation=value,
        skip_gateway_ping_validation=value,
        task_name=value,
        vcenter_spec=value,
        vsan_spec=value,
        vx_manager_spec=value,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    PARAMETER["resource_id"] = resource.get("resource_id", None)
    assert PARAMETER["resource_id"]
    # TODO: Add manual validations

    # Now get the resource
    # TODO: replace call param values as necessary
    ret = await hub.exec.vcf.sddc.get(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"], id_=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_get(hub, ctx):
    r"""
    **Test function**
    """

    # Test - Invalid/Not-Found resource lookup
    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    # TODO: replace call param values as necessary
    ret = await hub.exec.vcf.sddc.get(
        ctx, name=PARAMETER["name"], resource_id="invalid_resource_id", id_=value
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert "result is empty" in str(ret["comment"])

    # TODO: replace call param values as necessary
    ret = await hub.exec.vcf.sddc.get(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"], id_=value
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_list(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    global PARAMETER
    ret = await hub.exec.vcf.sddc.list(
        ctx,
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    for resource in ret["ret"]:
        assert resource
        # TODO: Add manual validations


@pytest.mark.asyncio
async def test_update(hub, ctx):
    r"""
    **Test function**
    """

    # Test - Update existing resource
    global PARAMETER

    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    # TODO: replace call param values as necessary
    ret = await hub.exec.vcf.sddc.update(
        ctx,
        # TODO: replace call param values as necessary
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        id_=value,
        ceip_enabled=value,
        cluster_spec=value,
        dns_spec=value,
        dv_switch_version=value,
        dvs_specs=value,
        esx_license=value,
        excluded_components=value,
        fips_enabled=value,
        host_specs=value,
        management_pool_name=value,
        network_specs=value,
        nsxt_spec=value,
        ntp_servers=value,
        proxy_spec=value,
        psc_specs=value,
        sddc_id=value,
        sddc_manager_spec=value,
        security_spec=value,
        skip_esx_thumbprint_validation=value,
        skip_gateway_ping_validation=value,
        task_name=value,
        vcenter_spec=value,
        vsan_spec=value,
        vx_manager_spec=value,
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    # TODO: Add manual validations

    # Now get the resource
    # TODO: replace call param values as necessary
    ret = await hub.exec.vcf.sddc.get(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"], id_=value
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_delete(hub, ctx):
    r"""
    **Test function**
    """

    # Test - Delete existing resource
    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    # TODO: replace call param values as necessary
    ret = await hub.exec.vcf.sddc.delete(
        ctx,
        name="",
        resource_id=PARAMETER["resource_id"],
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert not ret["ret"]

    # Now get the resource
    # TODO: replace call param values as necessary
    ret = await hub.exec.vcf.sddc.get(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"], id_=value
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert "result is empty" in str(ret["comment"])

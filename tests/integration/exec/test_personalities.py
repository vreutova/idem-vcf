"""Tests for validating Personalitieses. """
import pytest


PARAMETER = {"name": "idem-test-resource- + TODO: Add unique identifier generator"}


@pytest.mark.asyncio
async def test_create(hub, ctx):
    r"""
    **Test function**
    """

    # Test - create new resource
    global PARAMETER
    # TODO: replace call param values as necessary
    ret = await hub.exec.vcf.personalities.create(
        ctx,
        name=PARAMETER["name"],
        upload_mode=value,
        upload_spec_raw_mode=value,
        upload_spec_raw_with_file_upload_id_mode=value,
        upload_spec_referred_mode=value,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    PARAMETER["resource_id"] = resource.get("resource_id", None)
    assert PARAMETER["resource_id"]
    # TODO: Add manual validations

    # Now get the resource
    # TODO: replace call param values as necessary
    ret = await hub.exec.vcf.personalities.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        personality_id=value,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_get(hub, ctx):
    r"""
    **Test function**
    """

    # Test - Invalid/Not-Found resource lookup
    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    # TODO: replace call param values as necessary
    ret = await hub.exec.vcf.personalities.get(
        ctx,
        name=PARAMETER["name"],
        resource_id="invalid_resource_id",
        personality_id=value,
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert "result is empty" in str(ret["comment"])

    # TODO: replace call param values as necessary
    ret = await hub.exec.vcf.personalities.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        personality_id=value,
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_list(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    global PARAMETER
    ret = await hub.exec.vcf.personalities.list(
        ctx,
        base_os_version=value,
        add_on_name=value,
        add_on_vendor_name=value,
        component_name=value,
        component_vendor_name=value,
        personality_name=value,
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    for resource in ret["ret"]:
        assert resource
        # TODO: Add manual validations


@pytest.mark.asyncio
async def test_update(hub, ctx):
    r"""
    **Test function**
    """

    # Test - Update existing resource
    global PARAMETER

    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    # TODO: replace call param values as necessary
    ret = await hub.exec.vcf.personalities.update(
        ctx,
        # TODO: replace call param values as necessary
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        personality_id=value,
        created_by=value,
        description=value,
        display_name=value,
        image_checksum=value,
        image_size=value,
        kb_articles=value,
        personality_id=value,
        personality_name=value,
        release_date=value,
        software_info=value,
        tags=value,
        version=value,
        vsphere_exported_iso_path=value,
        vsphere_exported_json_path=value,
        vsphere_exported_zip_path=value,
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    # TODO: Add manual validations

    # Now get the resource
    # TODO: replace call param values as necessary
    ret = await hub.exec.vcf.personalities.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        personality_id=value,
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_delete(hub, ctx):
    r"""
    **Test function**
    """

    # Test - Delete existing resource
    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    # TODO: replace call param values as necessary
    ret = await hub.exec.vcf.personalities.delete(
        ctx,
        name="",
        resource_id=PARAMETER["resource_id"],
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert not ret["ret"]

    # Now get the resource
    # TODO: replace call param values as necessary
    ret = await hub.exec.vcf.personalities.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        personality_id=value,
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert "result is empty" in str(ret["comment"])

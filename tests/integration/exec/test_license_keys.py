"""Tests for validating License Keyss. """
import pytest


PARAMETER = {"name": "idem-test-resource- + TODO: Add unique identifier generator"}


@pytest.mark.asyncio
async def test_create(hub, ctx):
    r"""
    **Test function**
    """

    # Test - create new resource
    global PARAMETER
    # TODO: replace call param values as necessary
    ret = await hub.exec.vcf.license_keys.create(
        ctx,
        name=PARAMETER["name"],
        description=value,
        id_=value,
        is_unlimited=value,
        key=value,
        license_key_usage=value,
        license_key_validity=value,
        product_type=value,
        product_version=value,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    PARAMETER["resource_id"] = resource.get("resource_id", None)
    assert PARAMETER["resource_id"]
    # TODO: Add manual validations

    # Now get the resource
    # TODO: replace call param values as necessary
    ret = await hub.exec.vcf.license_keys.get(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"], key=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_get(hub, ctx):
    r"""
    **Test function**
    """

    # Test - Invalid/Not-Found resource lookup
    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    # TODO: replace call param values as necessary
    ret = await hub.exec.vcf.license_keys.get(
        ctx, name=PARAMETER["name"], resource_id="invalid_resource_id", key=value
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert "result is empty" in str(ret["comment"])

    # TODO: replace call param values as necessary
    ret = await hub.exec.vcf.license_keys.get(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"], key=value
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_list(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    global PARAMETER
    ret = await hub.exec.vcf.license_keys.list(
        ctx, product_type=value, license_key_status=value, product_version=value
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    for resource in ret["ret"]:
        assert resource
        # TODO: Add manual validations


@pytest.mark.asyncio
async def test_update(hub, ctx):
    r"""
    **Test function**
    """

    # Test - Update existing resource
    global PARAMETER

    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    # TODO: replace call param values as necessary
    ret = await hub.exec.vcf.license_keys.update(
        ctx,
        # TODO: replace call param values as necessary
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    # TODO: Add manual validations

    # Now get the resource
    # TODO: replace call param values as necessary
    ret = await hub.exec.vcf.license_keys.get(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"], key=value
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_delete(hub, ctx):
    r"""
    **Test function**
    """

    # Test - Delete existing resource
    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    # TODO: replace call param values as necessary
    ret = await hub.exec.vcf.license_keys.delete(
        ctx, name="", resource_id=PARAMETER["resource_id"], key=value
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert not ret["ret"]

    # Now get the resource
    # TODO: replace call param values as necessary
    ret = await hub.exec.vcf.license_keys.get(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"], key=value
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert "result is empty" in str(ret["comment"])

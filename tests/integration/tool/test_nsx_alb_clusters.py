"""Tests for validating Nsx Alb Clusterss. """
import pytest


@pytest.mark.asyncio
async def test_validate_alb_cluster(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.nsx_alb_clusters.validate_alb_cluster(
        ctx,
        domain_id=value,
        cluster_fqdn=value,
        cluster_ip_address=value,
        form_factor=value,
        admin_password=value,
        nodes=value,
        alb_bundle_id=value,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_validate_version(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.nsx_alb_clusters.validate_version(
        ctx, domain_id=value, alb_bundle_id=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations

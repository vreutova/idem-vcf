"""Tests for validating Proxy Configurations. """
import pytest


@pytest.mark.asyncio
async def test_update_proxy_configuration(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.proxy_configuration.update_proxy_configuration(
        ctx, host=value, is_configured=value, is_enabled=value, port=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations

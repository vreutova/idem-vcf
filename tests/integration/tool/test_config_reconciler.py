"""Tests for validating Config Reconcilers. """
import pytest


@pytest.mark.asyncio
async def test_reconcile_configs(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.config_reconciler.reconcile_configs(
        ctx, reconciliation_for_resources=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_get_reconciliation_task(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.config_reconciler.get_reconciliation_task(
        ctx, task_id=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_get_configs(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.config_reconciler.get_configs(
        ctx,
        resource_id=value,
        resource_type=value,
        config_id=value,
        drift_type=value,
        size=value,
        page=value,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations

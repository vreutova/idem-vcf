"""Tests for validating Sddcs. """
import pytest


@pytest.mark.asyncio
async def test_validate_bringup_spec(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.sddc.validate_bringup_spec(
        ctx,
        redo=value,
        ceip_enabled=value,
        cluster_spec=value,
        dns_spec=value,
        dv_switch_version=value,
        dvs_specs=value,
        esx_license=value,
        excluded_components=value,
        fips_enabled=value,
        host_specs=value,
        management_pool_name=value,
        network_specs=value,
        nsxt_spec=value,
        ntp_servers=value,
        proxy_spec=value,
        psc_specs=value,
        sddc_id=value,
        sddc_manager_spec=value,
        security_spec=value,
        skip_esx_thumbprint_validation=value,
        skip_gateway_ping_validation=value,
        task_name=value,
        vcenter_spec=value,
        vsan_spec=value,
        vx_manager_spec=value,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_get_bringup_validation(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.sddc.get_bringup_validation(ctx, id_=value)
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_retry_bringup_validation(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.sddc.retry_bringup_validation(ctx, id_=value)
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_export_bringup_validation_report(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.sddc.export_bringup_validation_report(
        ctx, validation_id=value, start_time=value, cur_client_time=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_export_bringup_detail_report(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.sddc.export_bringup_detail_report(
        ctx, id_=value, format_=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_get_sddc_manager_info(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.sddc.get_sddc_manager_info(ctx, id_=value)
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_start_bringup_spec_conversion(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.sddc.start_bringup_spec_conversion(ctx, design=value)
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations

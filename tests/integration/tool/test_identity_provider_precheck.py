"""Tests for validating Identity Provider Prechecks. """
import pytest


@pytest.mark.asyncio
async def test_get_identity_precheck_result(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.identity_provider_precheck.get_identity_precheck_result(
        ctx,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations

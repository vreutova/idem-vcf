"""Tests for validating Tokenss. """
import pytest


@pytest.mark.asyncio
async def test_refresh_access_token(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.tokens.refresh_access_token(
        ctx,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations

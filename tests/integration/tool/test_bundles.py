"""Tests for validating Bundleses. """
import pytest


@pytest.mark.asyncio
async def test_update_bundle_compatibility_sets(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.bundles.update_bundle_compatibility_sets(
        ctx, bundle_download_spec=value, compatibility_sets_file_path=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_get_bundles_for_skip_upgrade(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.bundles.get_bundles_for_skip_upgrade(
        ctx, id_=value, target_version=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations

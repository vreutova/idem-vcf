"""Tests for validating Network Poolss. """
import pytest


@pytest.mark.asyncio
async def test_get_networks_of_network_pool(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.network_pools.get_networks_of_network_pool(ctx, id_=value)
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_get_network_of_network_pool(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.network_pools.get_network_of_network_pool(
        ctx, id_=value, network_id=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_add_ip_pool_to_network_of_network_pool(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.network_pools.add_ip_pool_to_network_of_network_pool(
        ctx, id_=value, network_id=value, end=value, start=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_delete_ip_pool_from_network_of_network_pool(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.network_pools.delete_ip_pool_from_network_of_network_pool(
        ctx, id_=value, network_id=value, end=value, start=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations

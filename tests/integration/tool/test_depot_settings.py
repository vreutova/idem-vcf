"""Tests for validating Depot Settingss. """
import pytest


@pytest.mark.asyncio
async def test_get_depot_settings(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.depot_settings.get_depot_settings(
        ctx,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_update_depot_settings(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.depot_settings.update_depot_settings(
        ctx,
        dell_emc_support_account=value,
        vmware_account=value,
        offline_account=value,
        depot_configuration=value,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations

"""Tests for validating Upgradableses. """
import pytest


@pytest.mark.asyncio
async def test_get_upgradables_by_domain(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.upgradables.get_upgradables_by_domain(
        ctx, domain_id=value, target_version=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_get_upgradables_clusters(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.upgradables.get_upgradables_clusters(ctx, domain_id=value)
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_get_nsx_upgrade_resources(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.upgradables.get_nsx_upgrade_resources(
        ctx, domain_id=value, bundle_id=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations

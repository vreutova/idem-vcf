"""Tests for validating Fips Mode Detailss. """
import pytest


@pytest.mark.asyncio
async def test_get_fips_configuration(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.fips_mode_details.get_fips_configuration(
        ctx,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations

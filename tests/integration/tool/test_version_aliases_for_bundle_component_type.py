"""Tests for validating Version Aliases For Bundle Component Types. """
import pytest


@pytest.mark.asyncio
async def test_get_version_alias_configuration(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.version_aliases_for_bundle_component_type.get_version_alias_configuration(
        ctx,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_update_version_alias_configurations(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.version_aliases_for_bundle_component_type.update_version_alias_configurations(
        ctx,
        force_update=value,
        target_vcf_version=value,
        version_aliases_for_bundle_component_types=value,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_delete_version_alias_by_software_type(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.version_aliases_for_bundle_component_type.delete_version_alias_by_software_type(
        ctx, bundle_component_type=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_update_version_alias_configuration(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.version_aliases_for_bundle_component_type.update_version_alias_configuration(
        ctx,
        bundle_component_type=value,
        version=value,
        aliases=value,
        force_update=value,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_delete_alias_versions_by_software_type_and_base_version(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.version_aliases_for_bundle_component_type.delete_alias_versions_by_software_type_and_base_version(
        ctx, bundle_component_type=value, version=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations

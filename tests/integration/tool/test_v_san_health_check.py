"""Tests for validating V San Health Checks. """
import pytest


@pytest.mark.asyncio
async def test_get_vsan_health_check_by_domain(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.v_san_health_check.get_vsan_health_check_by_domain(
        ctx, domain_id=value, status=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_update_vsan_health_check_by_domain(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.v_san_health_check.update_vsan_health_check_by_domain(
        ctx, domain_id=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_get_vsan_health_check_by_query_id(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.v_san_health_check.get_vsan_health_check_by_query_id(
        ctx, domain_id=value, query_id=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_get_vsan_health_check_by_task_id(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.v_san_health_check.get_vsan_health_check_by_task_id(
        ctx, domain_id=value, task_id=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations

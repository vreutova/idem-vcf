"""Tests for validating Nsx T Edge Clusterss. """
import pytest


@pytest.mark.asyncio
async def test_get_edge_clusters(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.nsx_t_edge_clusters.get_edge_clusters(
        ctx, cluster_id=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_create_edge_cluster(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.nsx_t_edge_clusters.create_edge_cluster(
        ctx,
        asn=value,
        edge_admin_password=value,
        edge_audit_password=value,
        edge_cluster_name=value,
        edge_cluster_profile_spec=value,
        edge_cluster_profile_type=value,
        edge_cluster_type=value,
        edge_form_factor=value,
        edge_node_specs=value,
        new_ip_address_pool_specs=value,
        edge_root_password=value,
        internal_transit_subnets=value,
        mtu=value,
        skip_tep_routability_check=value,
        tier0_name=value,
        tier0_routing_type=value,
        tier0_services_high_availability=value,
        tier1_name=value,
        tier1_unhosted=value,
        transit_subnets=value,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_validate_edge_cluster_creation_spec(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.nsx_t_edge_clusters.validate_edge_cluster_creation_spec(
        ctx,
        asn=value,
        edge_admin_password=value,
        edge_audit_password=value,
        edge_cluster_name=value,
        edge_cluster_profile_spec=value,
        edge_cluster_profile_type=value,
        edge_cluster_type=value,
        edge_form_factor=value,
        edge_node_specs=value,
        new_ip_address_pool_specs=value,
        edge_root_password=value,
        internal_transit_subnets=value,
        mtu=value,
        skip_tep_routability_check=value,
        tier0_name=value,
        tier0_routing_type=value,
        tier0_services_high_availability=value,
        tier1_name=value,
        tier1_unhosted=value,
        transit_subnets=value,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_get_edge_cluster_validation_by_id(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.nsx_t_edge_clusters.get_edge_cluster_validation_by_id(
        ctx, id_=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_get_edge_cluster(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.nsx_t_edge_clusters.get_edge_cluster(ctx, id_=value)
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_update_edge_cluster(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.nsx_t_edge_clusters.update_edge_cluster(
        ctx,
        id_=value,
        edge_cluster_expansion_spec=value,
        edge_cluster_shrinkage_spec=value,
        operation=value,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_validate_edge_cluster_update_spec(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.nsx_t_edge_clusters.validate_edge_cluster_update_spec(
        ctx,
        id_=value,
        edge_cluster_expansion_spec=value,
        edge_cluster_shrinkage_spec=value,
        operation=value,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations

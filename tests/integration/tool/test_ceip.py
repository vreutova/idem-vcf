"""Tests for validating Ceips. """
import pytest


@pytest.mark.asyncio
async def test_set_ceip_status(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.ceip.set_ceip_status(ctx, status=value)
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations

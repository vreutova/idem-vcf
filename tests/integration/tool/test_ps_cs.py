"""Tests for validating Ps Css. """
import pytest


@pytest.mark.asyncio
async def test_get_pscs(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.ps_cs.get_pscs(
        ctx,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_get_psc(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.ps_cs.get_psc(ctx, id_=value)
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations

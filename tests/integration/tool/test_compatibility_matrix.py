"""Tests for validating Compatibility Matrixes. """
import pytest


@pytest.mark.asyncio
async def test_get_compatibility_matrices(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.compatibility_matrix.get_compatibility_matrices(
        ctx,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_update_compatibility_matrix(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.compatibility_matrix.update_compatibility_matrix(
        ctx, compatibility_matrix_source=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_get_compatibility_matrix(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.compatibility_matrix.get_compatibility_matrix(
        ctx, compatibility_matrix_source=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_get_compatibility_matrix_content(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.compatibility_matrix.get_compatibility_matrix_content(
        ctx, compatibility_matrix_source=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_get_compatibility_matrix_metadata(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.compatibility_matrix.get_compatibility_matrix_metadata(
        ctx, compatibility_matrix_source=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations

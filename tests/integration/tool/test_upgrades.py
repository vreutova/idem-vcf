"""Tests for validating Upgradeses. """
import pytest


@pytest.mark.asyncio
async def test_start_upgrade_precheck(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.upgrades.start_upgrade_precheck(ctx, upgrade_id=value)
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_get_upgrade_precheck_by_id(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.vcf.upgrades.get_upgrade_precheck_by_id(
        ctx, upgrade_id=value, precheck_id=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations
